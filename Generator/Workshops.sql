use ConferenceCompany
exec AddWorkshop "2013-2-1", "13:28:00", "15:28:00", 97, 26, "European Navigation Conference 2014 (ENC-GNSS 2014)", 27
exec AddWorkshop "2013-8-21", "13:32:00", "15:32:00", 88, 22, "Workshop on Logic ", 70
exec AddWorkshop "2013-7-20", "19:17:00", "20:17:00", 136, 30, "International Conference on Artificial Neural Networks", 54
exec AddWorkshop "2011-3-21", "8:1:00", "10:1:00", 73, 38, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 2
exec AddWorkshop "2013-11-20", "10:58:00", "13:58:00", 124, 33, "Conference on Uncertainty in Artificial Intelligence", 28
exec AddWorkshop "2012-11-9", "15:9:00", "18:9:00", 132, 47, "4S Symposium 2014", 67
exec AddWorkshop "2012-11-17", "12:19:00", "14:19:00", 72, 34, "RuleML Symposium		", 39
exec AddWorkshop "2012-12-26", "18:33:00", "19:33:00", 125, 28, "Conference on Learning Theory		", 32
exec AddWorkshop "2013-8-16", "10:15:00", "12:15:00", 100, 33, "ARTES 1 Final Presentation Days", 18
exec AddWorkshop "2011-4-29", "19:36:00", "21:36:00", 52, 40, "International Conference on Computer Vision", 50
exec AddWorkshop "2012-12-16", "14:46:00", "16:46:00", 80, 45, "Microwave Workshop", 74
exec AddWorkshop "2012-9-14", "15:33:00", "16:33:00", 86, 27, "X-Ray Universe", 65
exec AddWorkshop "2013-9-25", "17:8:00", "20:8:00", 110, 28, "Global Conference on Sustainability and Reporting", 63
exec AddWorkshop "2013-7-1", "13:55:00", "15:55:00", 119, 28, "2013 Ethical Leadership Conference: Ethics in Action", 7
exec AddWorkshop "2012-8-3", "13:34:00", "16:34:00", 124, 44, "Asian Conference on Computer Vision", 1
exec AddWorkshop "2013-6-21", "8:31:00", "10:31:00", 114, 36, "Global Conference on Sustainability and Reporting", 59
exec AddWorkshop "2012-1-11", "14:43:00", "15:43:00", 95, 21, "Life in Space for Life on Earth Symposium", 7
exec AddWorkshop "2011-7-29", "15:33:00", "18:33:00", 91, 20, "Science and Challenges of Lunar Sample Return Workshop", 18
exec AddWorkshop "2012-3-11", "13:26:00", "14:26:00", 74, 48, "Conference on Automated Deduction		", 39
exec AddWorkshop "2013-2-2", "11:5:00", "14:5:00", 60, 31, "KU Leuven CSR Symposium", 20
exec AddWorkshop "2013-1-6", "8:17:00", "9:17:00", 90, 22, "European Conference on Artificial Intelligence	", 78
exec AddWorkshop "2011-3-24", "8:27:00", "9:27:00", 134, 34, "4S Symposium 2014", 15
exec AddWorkshop "2013-10-21", "8:48:00", "10:48:00", 50, 37, "Global Conference on Sustainability and Reporting", 22
exec AddWorkshop "2013-7-4", "12:50:00", "13:50:00", 149, 20, "The Corporate Philanthropy Forum", 8
exec AddWorkshop "2011-6-27", "8:27:00", "9:27:00", 125, 25, "International Conference on Computer Vision Theory and Applications", 25
exec AddWorkshop "2012-5-2", "18:25:00", "19:25:00", 98, 48, "International Conference on Artificial Neural Networks", 55
exec AddWorkshop "2011-5-29", "12:9:00", "15:9:00", 74, 49, "Conference on Uncertainty in Artificial Intelligence", 31
exec AddWorkshop "2012-1-16", "14:46:00", "16:46:00", 92, 30, "International Conference on the Principles of", 67
exec AddWorkshop "2012-1-1", "17:46:00", "20:46:00", 84, 27, "International Joint Conference on Automated Reasoning", 57
exec AddWorkshop "2013-10-9", "13:19:00", "16:19:00", 122, 38, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 23
exec AddWorkshop "2011-6-15", "14:30:00", "15:30:00", 79, 49, "Computer Analysis of Images and Patterns", 41
exec AddWorkshop "2011-6-25", "14:14:00", "15:14:00", 87, 35, "Client Summit", 13
exec AddWorkshop "2011-4-15", "19:59:00", "20:59:00", 102, 24, "The BCLC CSR Conference", 8
exec AddWorkshop "2011-7-9", "17:44:00", "19:44:00", 74, 49, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 62
exec AddWorkshop "2012-8-7", "12:20:00", "13:20:00", 146, 31, "International Semantic Web Conference		", 30
exec AddWorkshop "2013-5-16", "8:42:00", "9:42:00", 55, 29, "International Joint Conference on Artificial Intelligence", 29
exec AddWorkshop "2013-11-10", "12:35:00", "14:35:00", 146, 26, "The 12th annual Responsible Business Summit", 58
exec AddWorkshop "2012-8-20", "8:39:00", "10:39:00", 115, 23, "EuroCOW the Calibration and Orientation Workshop", 56
exec AddWorkshop "2011-5-3", "15:45:00", "16:45:00", 112, 27, "International Conference on Automated Reasoning", 52
exec AddWorkshop "2011-2-30", "18:53:00", "20:53:00", 72, 47, "�International� Corporate Citizenship Conference", 19
exec AddWorkshop "2012-5-18", "18:36:00", "19:36:00", 104, 24, "Cause Marketing Forum", 49
exec AddWorkshop "2013-3-17", "10:5:00", "12:5:00", 124, 30, "Science and Challenges of Lunar Sample Return Workshop", 6
exec AddWorkshop "2011-5-8", "18:37:00", "19:37:00", 95, 46, "ICATT", 72
exec AddWorkshop "2013-12-9", "18:36:00", "19:36:00", 91, 20, "International Joint Conference on Automated Reasoning", 26
exec AddWorkshop "2012-3-8", "19:6:00", "22:6:00", 58, 36, "Business4Better: The Community Partnership Movement", 76
exec AddWorkshop "2013-9-14", "19:56:00", "21:56:00", 130, 49, "Sentinel-2 for Science WS", 54
exec AddWorkshop "2011-8-28", "17:10:00", "18:10:00", 91, 20, "Microwave Workshop", 26
exec AddWorkshop "2013-7-4", "12:31:00", "15:31:00", 135, 39, "The 2nd International CSR Communication Conference", 36
exec AddWorkshop "2012-9-29", "11:51:00", "14:51:00", 131, 25, "International Conference on Automated Planning", 4
exec AddWorkshop "2012-6-21", "8:19:00", "9:19:00", 121, 40, "4S Symposium 2014", 42
exec AddWorkshop "2012-4-16", "19:48:00", "21:48:00", 125, 44, "Conference on Computer Vision and Pattern", 76
exec AddWorkshop "2012-2-13", "18:51:00", "21:51:00", 108, 41, "International Conference on Autonomous Agents and", 8
exec AddWorkshop "2013-1-4", "10:44:00", "11:44:00", 91, 30, "The BCLC CSR Conference", 44
exec AddWorkshop "2012-4-26", "14:48:00", "17:48:00", 101, 41, "Workshop on Image Processing", 37
exec AddWorkshop "2013-5-18", "13:22:00", "16:22:00", 115, 23, "Net Impact Conference", 22
exec AddWorkshop "2011-3-1", "18:50:00", "20:50:00", 51, 27, "Mayo Clinic Presents", 47
exec AddWorkshop "2013-9-7", "16:57:00", "17:57:00", 112, 21, "ARTES 1 Final Presentation Days", 48
exec AddWorkshop "2013-11-2", "17:4:00", "19:4:00", 131, 42, "International Conference on Pattern Recognition", 0
exec AddWorkshop "2012-7-13", "13:18:00", "15:18:00", 106, 47, "European Conference on Computer Vision", 10
exec AddWorkshop "2011-6-10", "8:58:00", "11:58:00", 108, 28, "Science and Challenges of Lunar Sample Return Workshop", 42
exec AddWorkshop "2011-9-20", "15:37:00", "17:37:00", 75, 31, "International Conference on Autonomous Agents and", 79
exec AddWorkshop "2012-12-20", "15:11:00", "16:11:00", 69, 38, "European Conference on Artificial Intelligence	", 13
exec AddWorkshop "2012-3-14", "8:9:00", "11:9:00", 87, 37, "ICATT", 62
exec AddWorkshop "2011-1-13", "17:6:00", "19:6:00", 70, 43, "The 12th annual Responsible Business Summit", 44
exec AddWorkshop "2013-7-13", "16:24:00", "19:24:00", 57, 47, "Mayo Clinic Presents", 70
exec AddWorkshop "2013-6-29", "19:29:00", "20:29:00", 77, 41, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 16
exec AddWorkshop "2012-10-24", "16:25:00", "18:25:00", 99, 21, "Cause Marketing Forum", 11
exec AddWorkshop "2012-11-30", "17:59:00", "19:59:00", 78, 39, "Business4Better: The Community Partnership Movement", 12
exec AddWorkshop "2012-2-22", "18:39:00", "19:39:00", 55, 36, "The Sixth International Conferences on Advances in Multimedia", 50
exec AddWorkshop "2011-8-3", "18:58:00", "21:58:00", 114, 20, "International Conference on Signal and Imaging Systems Engineering", 64
exec AddWorkshop "2012-9-17", "18:54:00", "20:54:00", 63, 38, "Foundations of Genetic Algorithms		", 35
exec AddWorkshop "2011-4-14", "14:37:00", "15:37:00", 148, 44, "Microwave Workshop", 22
exec AddWorkshop "2013-3-16", "17:16:00", "18:16:00", 94, 33, "The Corporate Philanthropy Forum", 49
exec AddWorkshop "2012-6-23", "17:51:00", "20:51:00", 137, 48, "Client Summit", 17
exec AddWorkshop "2013-7-24", "9:25:00", "11:25:00", 52, 28, "International Conference on Signal and Imaging Systems Engineering", 82
exec AddWorkshop "2012-6-30", "11:12:00", "13:12:00", 65, 32, "Computer Analysis of Images and Patterns", 37
exec AddWorkshop "2013-7-20", "9:15:00", "11:15:00", 51, 35, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 4
exec AddWorkshop "2013-7-20", "9:2:00", "10:2:00", 86, 26, "KU Leuven CSR Symposium", 36
exec AddWorkshop "2011-6-13", "13:8:00", "14:8:00", 116, 45, "Foundations of Genetic Algorithms		", 82
exec AddWorkshop "2012-12-2", "18:39:00", "19:39:00", 108, 43, "International Conference on Artificial Neural Networks", 63
exec AddWorkshop "2011-7-29", "8:25:00", "11:25:00", 57, 22, "Science and Challenges of Lunar Sample Return Workshop", 7
exec AddWorkshop "2013-8-19", "18:50:00", "21:50:00", 72, 30, "�International� Corporate Citizenship Conference", 54
exec AddWorkshop "2011-3-25", "13:36:00", "16:36:00", 135, 32, "3rd International Conference on Pattern Recognition Applications and Methods", 5
exec AddWorkshop "2011-6-30", "9:60:00", "11:60:00", 93, 29, "X-Ray Universe", 41
exec AddWorkshop "2011-10-14", "8:25:00", "10:25:00", 98, 34, "ICATT", 67
exec AddWorkshop "2011-11-30", "17:21:00", "20:21:00", 97, 42, "Science and Challenges of Lunar Sample Return Workshop", 46
exec AddWorkshop "2013-1-23", "18:45:00", "20:45:00", 139, 30, "ARTES 1 Final Presentation Days", 69
exec AddWorkshop "2012-10-25", "9:46:00", "11:46:00", 133, 34, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 72
exec AddWorkshop "2013-10-19", "10:14:00", "13:14:00", 122, 39, "International Conference on Computer Vision", 6
exec AddWorkshop "2012-5-17", "10:28:00", "13:28:00", 55, 20, "European Conference on Machine Learning", 2
exec AddWorkshop "2013-6-3", "13:10:00", "14:10:00", 60, 35, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 55
exec AddWorkshop "2011-6-23", "15:22:00", "16:22:00", 100, 37, "Conference on Automated Deduction		", 4
exec AddWorkshop "2012-12-22", "16:31:00", "19:31:00", 64, 47, "Microwave Workshop", 64
exec AddWorkshop "2013-6-20", "10:38:00", "13:38:00", 52, 29, "British	Machine	Vision	Conference		", 55
exec AddWorkshop "2012-6-6", "13:30:00", "16:30:00", 69, 28, "The Sixth International Conferences on Advances in Multimedia", 61
exec AddWorkshop "2011-3-6", "13:3:00", "14:3:00", 137, 29, "International Joint Conference on Artificial Intelligence", 27
exec AddWorkshop "2011-8-7", "13:55:00", "16:55:00", 102, 22, "International Joint Conference on Automated Reasoning", 57
exec AddWorkshop "2011-4-16", "9:42:00", "10:42:00", 100, 23, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 63
exec AddWorkshop "2012-9-5", "11:53:00", "13:53:00", 53, 47, "LPVE Land product validation and evolution", 61
exec AddWorkshop "2012-4-24", "11:29:00", "13:29:00", 93, 22, "X-Ray Universe", 82
exec AddWorkshop "2011-5-1", "11:17:00", "12:17:00", 117, 39, "Sustainable Brands Conference", 62
exec AddWorkshop "2011-11-28", "13:9:00", "16:9:00", 82, 42, "International Conference on Automated Planning", 26
exec AddWorkshop "2011-2-1", "12:8:00", "14:8:00", 115, 47, "International Conference on MultiMedia Modeling", 54
exec AddWorkshop "2013-5-13", "11:29:00", "12:29:00", 84, 35, "�International� Corporate Citizenship Conference", 60
exec AddWorkshop "2013-3-8", "8:33:00", "9:33:00", 139, 26, "2nd International Conference on Photonics, Optics and Laser Technology", 70
exec AddWorkshop "2013-7-17", "18:49:00", "21:49:00", 54, 24, "European Space Technology Harmonisation Conference", 80
exec AddWorkshop "2011-11-15", "19:41:00", "22:41:00", 107, 27, "7 th International Conference on Bio-inspired Systems and Signal Processing", 80
exec AddWorkshop "2011-4-10", "16:56:00", "19:56:00", 119, 22, "X-Ray Universe", 9
exec AddWorkshop "2013-12-23", "13:49:00", "14:49:00", 124, 29, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 68
exec AddWorkshop "2011-12-5", "13:16:00", "14:16:00", 97, 41, "International Conference on Automated Reasoning", 11
exec AddWorkshop "2013-1-1", "8:60:00", "10:60:00", 91, 42, "Sustainable Brands Conference", 58
exec AddWorkshop "2013-11-18", "8:4:00", "10:4:00", 125, 30, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 68
exec AddWorkshop "2011-2-17", "13:46:00", "14:46:00", 101, 22, "Conference on Learning Theory		", 63
exec AddWorkshop "2013-1-2", "9:41:00", "10:41:00", 71, 26, "Client Summit", 44
exec AddWorkshop "2011-7-4", "14:35:00", "17:35:00", 55, 37, "Mechanisms Final Presentation Days", 51
exec AddWorkshop "2011-8-1", "19:34:00", "21:34:00", 123, 41, "X-Ray Universe", 67
exec AddWorkshop "2011-8-21", "8:16:00", "10:16:00", 72, 49, "Sustainable Brands Conference", 23
exec AddWorkshop "2011-2-12", "9:34:00", "11:34:00", 128, 38, "The Sixth International Conferences on Advances in Multimedia", 55
exec AddWorkshop "2013-10-15", "17:31:00", "19:31:00", 140, 22, "International Conference on Automated Reasoning", 6
exec AddWorkshop "2013-4-20", "13:3:00", "16:3:00", 87, 33, "International Conference on Autonomous Agents and", 6
exec AddWorkshop "2013-5-17", "12:1:00", "15:1:00", 82, 32, "The Corporate Philanthropy Forum", 68
exec AddWorkshop "2013-7-10", "15:12:00", "16:12:00", 61, 28, "2013 Ethical Leadership Conference: Ethics in Action", 3
exec AddWorkshop "2011-6-27", "13:28:00", "15:28:00", 53, 27, "British	Machine	Vision	Conference		", 48
exec AddWorkshop "2013-1-25", "11:6:00", "14:6:00", 108, 31, "2nd International Conference on Photonics, Optics and Laser Technology", 21
exec AddWorkshop "2013-11-15", "13:38:00", "16:38:00", 107, 45, "Photonics West", 12
exec AddWorkshop "2013-2-9", "10:19:00", "13:19:00", 76, 21, "�International� Corporate Citizenship Conference", 25
exec AddWorkshop "2011-2-9", "10:30:00", "13:30:00", 105, 22, "International Conference on Signal and Imaging Systems Engineering", 45
exec AddWorkshop "2012-8-9", "19:27:00", "21:27:00", 108, 39, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 9
exec AddWorkshop "2011-1-27", "12:17:00", "13:17:00", 58, 42, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 50
exec AddWorkshop "2011-1-12", "9:15:00", "10:15:00", 127, 29, "International Joint Conference on Automated Reasoning", 53
exec AddWorkshop "2011-10-17", "17:43:00", "20:43:00", 122, 31, "Conference on Uncertainty in Artificial Intelligence", 59
exec AddWorkshop "2013-8-1", "11:35:00", "14:35:00", 69, 25, "European Conference on Computer Vision", 46
exec AddWorkshop "2013-10-14", "19:41:00", "20:41:00", 122, 46, "4th DUE Permafrost User Workshop", 0
exec AddWorkshop "2011-1-27", "10:12:00", "12:12:00", 117, 32, "Conference on Uncertainty in Artificial Intelligence", 25
exec AddWorkshop "2011-4-30", "17:26:00", "19:26:00", 82, 49, "Science and Challenges of Lunar Sample Return Workshop", 24
exec AddWorkshop "2013-9-10", "18:54:00", "19:54:00", 65, 40, "Conference on Automated Deduction		", 3
exec AddWorkshop "2011-12-3", "13:45:00", "16:45:00", 122, 43, "Conference on Learning Theory		", 61
exec AddWorkshop "2012-4-3", "9:15:00", "10:15:00", 91, 30, "Computer Analysis of Images and Patterns", 42
exec AddWorkshop "2011-7-17", "16:46:00", "17:46:00", 105, 32, "�International� Corporate Citizenship Conference", 23
exec AddWorkshop "2011-9-20", "19:44:00", "22:44:00", 54, 47, "International Conference on Signal and Imaging Systems Engineering", 84
exec AddWorkshop "2011-10-18", "10:18:00", "13:18:00", 90, 41, "The 12th annual Responsible Business Summit", 84
exec AddWorkshop "2013-4-25", "10:10:00", "12:10:00", 129, 27, "International Semantic Web Conference		", 57
exec AddWorkshop "2011-2-4", "14:23:00", "16:23:00", 120, 48, "Sentinel-2 for Science WS", 78
exec AddWorkshop "2012-10-22", "9:16:00", "10:16:00", 66, 33, "Conference on Uncertainty in Artificial Intelligence", 43
exec AddWorkshop "2011-10-18", "16:24:00", "17:24:00", 144, 24, "Leadership Strategies for Information Technology in Health Care Boston", 28
exec AddWorkshop "2011-2-10", "18:8:00", "21:8:00", 95, 28, "Client Summit", 68
exec AddWorkshop "2012-8-22", "14:24:00", "15:24:00", 57, 21, "Workshop on Logic ", 38
exec AddWorkshop "2013-6-6", "9:20:00", "12:20:00", 124, 20, "European Conference on Machine Learning", 55
exec AddWorkshop "2012-7-26", "8:45:00", "9:45:00", 92, 20, "International Joint Conference on Artificial Intelligence", 21
exec AddWorkshop "2013-8-11", "14:15:00", "16:15:00", 88, 22, "The Corporate Philanthropy Forum", 8
exec AddWorkshop "2013-1-4", "16:39:00", "17:39:00", 120, 36, "Mayo Clinic Presents", 36
exec AddWorkshop "2013-2-4", "15:43:00", "17:43:00", 85, 32, "European Conference on Artificial Intelligence	", 49
exec AddWorkshop "2011-8-20", "14:22:00", "15:22:00", 132, 34, "The BCLC CSR Conference", 43
exec AddWorkshop "2011-11-10", "10:48:00", "12:48:00", 54, 40, "International Conference on Signal and Imaging Systems Engineering", 11
exec AddWorkshop "2011-3-12", "8:1:00", "9:1:00", 51, 29, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 20
exec AddWorkshop "2011-12-9", "17:38:00", "20:38:00", 121, 41, "European Conference on Artificial Intelligence	", 58
exec AddWorkshop "2012-4-2", "16:44:00", "17:44:00", 112, 49, "International Conference on Signal and Imaging Systems Engineering", 25
exec AddWorkshop "2012-2-10", "16:15:00", "17:15:00", 60, 31, "Cause Marketing Forum", 35
exec AddWorkshop "2013-11-27", "17:60:00", "19:60:00", 60, 29, "Sustainable Brands Conference", 0
exec AddWorkshop "2013-1-3", "18:40:00", "21:40:00", 119, 23, "Microwave Workshop", 56
exec AddWorkshop "2011-4-2", "11:32:00", "14:32:00", 96, 39, "7 th International Conference on Bio-inspired Systems and Signal Processing", 64
exec AddWorkshop "2012-9-30", "15:4:00", "16:4:00", 62, 41, "Business4Better: The Community Partnership Movement", 76
exec AddWorkshop "2011-4-30", "11:51:00", "14:51:00", 70, 35, "Annual Interdisciplinary Conference", 28
exec AddWorkshop "2011-7-4", "8:1:00", "11:1:00", 56, 36, "Business4Better: The Community Partnership Movement", 76
exec AddWorkshop "2013-4-13", "15:52:00", "16:52:00", 105, 40, "Business4Better: The Community Partnership Movement", 4
exec AddWorkshop "2012-3-11", "16:11:00", "18:11:00", 137, 37, "International Conference on Computer Vision Theory and Applications", 41
exec AddWorkshop "2012-7-23", "10:17:00", "12:17:00", 55, 45, "The BCLC CSR Conference", 53
exec AddWorkshop "2013-12-1", "18:43:00", "21:43:00", 121, 41, "International Conference on MultiMedia Modeling", 84
exec AddWorkshop "2012-10-11", "14:4:00", "16:4:00", 112, 43, "Photonics West", 73
exec AddWorkshop "2012-12-25", "8:29:00", "11:29:00", 81, 43, "ICATT", 71
exec AddWorkshop "2012-6-4", "8:54:00", "11:54:00", 101, 31, "International Conference on the Principles of", 15
exec AddWorkshop "2012-1-15", "19:12:00", "20:12:00", 80, 35, "Leadership Strategies for Information Technology in Health Care Boston", 4
exec AddWorkshop "2011-1-25", "13:5:00", "15:5:00", 82, 35, "International Conference on Autonomous Agents and", 61
exec AddWorkshop "2012-12-30", "8:57:00", "11:57:00", 93, 43, "Leadership Strategies for Information Technology in Health Care Boston", 63
exec AddWorkshop "2012-12-20", "19:40:00", "21:40:00", 96, 23, "International Conference on Automated Reasoning", 33
exec AddWorkshop "2012-4-27", "18:33:00", "19:33:00", 128, 33, "Microwave Workshop", 46
exec AddWorkshop "2011-3-6", "15:57:00", "17:57:00", 149, 28, "Global Conference on Sustainability and Reporting", 35
exec AddWorkshop "2012-2-23", "8:48:00", "9:48:00", 138, 38, "Euclid Spacecraft Industry Day", 75
exec AddWorkshop "2012-6-22", "13:31:00", "15:31:00", 61, 41, "International Semantic Web Conference		", 24
exec AddWorkshop "2011-11-2", "16:19:00", "17:19:00", 74, 41, "RuleML Symposium		", 37
exec AddWorkshop "2012-12-22", "19:33:00", "21:33:00", 118, 43, "International Joint Conference on Artificial Intelligence", 62
exec AddWorkshop "2013-2-16", "16:44:00", "18:44:00", 52, 32, "Foundations of Genetic Algorithms		", 44
exec AddWorkshop "2012-9-1", "18:34:00", "21:34:00", 98, 35, "International Conference on the Principles of", 42
exec AddWorkshop "2011-3-26", "13:48:00", "16:48:00", 120, 32, "4S Symposium 2014", 39
exec AddWorkshop "2011-9-19", "18:46:00", "19:46:00", 133, 44, "Leadership Strategies for Information Technology in Health Care Boston", 15
exec AddWorkshop "2012-3-9", "18:1:00", "20:1:00", 92, 28, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 54
exec AddWorkshop "2011-2-7", "14:48:00", "16:48:00", 104, 49, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 22
exec AddWorkshop "2013-10-4", "16:14:00", "17:14:00", 77, 25, "Global Conference on Sustainability and Reporting", 57
exec AddWorkshop "2011-5-28", "11:37:00", "13:37:00", 104, 25, "The Corporate Philanthropy Forum", 15
exec AddWorkshop "2012-6-30", "18:4:00", "21:4:00", 144, 24, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 3
exec AddWorkshop "2012-5-19", "8:56:00", "10:56:00", 148, 34, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 59
exec AddWorkshop "2011-1-22", "18:5:00", "21:5:00", 91, 31, "The 12th annual Responsible Business Summit", 44
exec AddWorkshop "2012-12-15", "14:59:00", "16:59:00", 72, 47, "Conference on Automated Deduction		", 8
exec AddWorkshop "2012-1-20", "13:10:00", "16:10:00", 58, 26, "European Space Technology Harmonisation Conference", 69
exec AddWorkshop "2013-11-22", "8:33:00", "9:33:00", 75, 26, "European Conference on Computer Vision", 60
exec AddWorkshop "2011-2-29", "10:4:00", "12:4:00", 124, 21, "Business4Better: The Community Partnership Movement", 65
exec AddWorkshop "2011-11-6", "10:9:00", "13:9:00", 118, 31, "Science and Challenges of Lunar Sample Return Workshop", 25
exec AddWorkshop "2011-4-9", "15:23:00", "18:23:00", 110, 29, "�International� Corporate Citizenship Conference", 72
exec AddWorkshop "2013-4-2", "18:54:00", "21:54:00", 146, 37, "European Space Power Conference", 56
exec AddWorkshop "2011-3-30", "8:28:00", "11:28:00", 70, 30, "International Conference on Automated Planning", 2
exec AddWorkshop "2011-9-28", "10:52:00", "12:52:00", 59, 20, "Business4Better: The Community Partnership Movement", 11
exec AddWorkshop "2013-4-20", "13:32:00", "14:32:00", 103, 36, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 56
exec AddWorkshop "2011-9-5", "16:27:00", "17:27:00", 103, 22, "International Conference on the Principles of", 12
exec AddWorkshop "2013-9-27", "17:1:00", "18:1:00", 63, 22, "ARTES 1 Final Presentation Days", 84
exec AddWorkshop "2011-1-1", "19:47:00", "20:47:00", 149, 27, "Annual Convention of The Society", 70
exec AddWorkshop "2012-11-12", "11:59:00", "14:59:00", 53, 28, "4S Symposium 2014", 65
exec AddWorkshop "2013-11-26", "16:27:00", "17:27:00", 121, 35, "Annual Convention of The Society", 1
exec AddWorkshop "2013-6-10", "17:26:00", "18:26:00", 54, 30, "Cause Marketing Forum", 67
exec AddWorkshop "2013-10-15", "13:56:00", "16:56:00", 64, 20, "Sentinel-2 for Science WS", 68
exec AddWorkshop "2012-9-15", "19:3:00", "21:3:00", 78, 36, "Business4Better: The Community Partnership Movement", 33
exec AddWorkshop "2013-10-12", "11:36:00", "13:36:00", 140, 20, "2013 Ethical Leadership Conference: Ethics in Action", 43
exec AddWorkshop "2011-11-14", "13:41:00", "15:41:00", 64, 46, "LPVE Land product validation and evolution", 24
exec AddWorkshop "2013-6-23", "18:23:00", "21:23:00", 62, 20, "Science and Challenges of Lunar Sample Return Workshop", 1
exec AddWorkshop "2012-6-5", "12:42:00", "14:42:00", 145, 24, "Leadership Strategies for Information Technology in Health Care Boston", 22
exec AddWorkshop "2012-7-22", "12:15:00", "14:15:00", 111, 47, "2013 Ethical Leadership Conference: Ethics in Action", 59
exec AddWorkshop "2011-7-21", "13:29:00", "16:29:00", 67, 32, "The BCLC CSR Conference", 6
exec AddWorkshop "2013-11-16", "18:16:00", "19:16:00", 59, 34, "RuleML Symposium		", 49
exec AddWorkshop "2011-6-7", "8:38:00", "10:38:00", 82, 27, "ICATT", 55
exec AddWorkshop "2013-10-20", "19:37:00", "22:37:00", 148, 29, "Mechanisms Final Presentation Days", 42
exec AddWorkshop "2011-3-10", "8:51:00", "11:51:00", 66, 23, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 66
exec AddWorkshop "2012-5-5", "9:31:00", "10:31:00", 122, 29, "LPVE Land product validation and evolution", 2
exec AddWorkshop "2013-12-9", "10:32:00", "11:32:00", 117, 44, "RuleML Symposium				", 41
exec AddWorkshop "2012-1-17", "15:26:00", "17:26:00", 67, 46, "European Conference on Computer Vision", 37
exec AddWorkshop "2011-4-10", "11:2:00", "14:2:00", 95, 23, "International Conference on Computer Vision", 82
exec AddWorkshop "2013-1-16", "15:31:00", "16:31:00", 58, 23, "�International� Corporate Citizenship Conference", 26
exec AddWorkshop "2012-8-18", "9:28:00", "12:28:00", 50, 47, "4th DUE Permafrost User Workshop", 77
exec AddWorkshop "2013-7-25", "16:53:00", "17:53:00", 99, 29, "The 12th annual Responsible Business Summit", 49
exec AddWorkshop "2011-12-14", "13:11:00", "14:11:00", 56, 38, "European Navigation Conference 2014 (ENC-GNSS 2014)", 58
exec AddWorkshop "2011-11-20", "12:40:00", "13:40:00", 134, 26, "Euclid Spacecraft Industry Day", 75
exec AddWorkshop "2012-12-23", "15:35:00", "16:35:00", 118, 35, "The BCLC CSR Conference", 11
exec AddWorkshop "2012-10-29", "9:18:00", "12:18:00", 116, 27, "The 12th annual Responsible Business Summit", 58
exec AddWorkshop "2013-5-3", "14:45:00", "16:45:00", 79, 20, "X-Ray Universe", 74
exec AddWorkshop "2011-2-27", "18:23:00", "20:23:00", 65, 46, "Ceres Conference", 56
exec AddWorkshop "2011-10-17", "11:35:00", "14:35:00", 57, 38, "Conference on Computer Vision and Pattern", 37
exec AddWorkshop "2012-9-30", "11:48:00", "14:48:00", 139, 24, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 36
exec AddWorkshop "2012-12-6", "8:9:00", "10:9:00", 118, 41, "Annual Convention of The Society", 61
exec AddWorkshop "2011-3-2", "13:30:00", "14:30:00", 58, 37, "European Space Power Conference", 76
exec AddWorkshop "2011-9-13", "16:30:00", "17:30:00", 122, 45, "2nd International Conference on Photonics, Optics and Laser Technology", 35
exec AddWorkshop "2011-11-11", "16:60:00", "18:60:00", 54, 45, "Sustainable Brands Conference", 44
exec AddWorkshop "2011-6-27", "8:56:00", "10:56:00", 140, 30, "Net Impact Conference", 38
exec AddWorkshop "2013-6-5", "10:58:00", "11:58:00", 117, 46, "EuroCOW the Calibration and Orientation Workshop", 6
exec AddWorkshop "2011-12-30", "15:23:00", "16:23:00", 110, 25, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 63
exec AddWorkshop "2012-10-26", "17:16:00", "18:16:00", 145, 44, "International Joint Conference on Artificial Intelligence", 68
exec AddWorkshop "2012-3-15", "15:20:00", "16:20:00", 138, 21, "Life in Space for Life on Earth Symposium", 6
exec AddWorkshop "2011-5-23", "15:47:00", "16:47:00", 85, 40, "Photonics West", 53
exec AddWorkshop "2012-2-14", "17:44:00", "19:44:00", 77, 29, "European Space Technology Harmonisation Conference", 81
exec AddWorkshop "2013-1-19", "9:13:00", "10:13:00", 73, 23, "Global Conference on Sustainability and Reporting", 13
exec AddWorkshop "2012-7-29", "10:3:00", "12:3:00", 82, 32, "Conference on Uncertainty in Artificial Intelligence", 2
exec AddWorkshop "2012-6-25", "9:27:00", "11:27:00", 72, 42, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 59
exec AddWorkshop "2013-4-19", "10:23:00", "11:23:00", 112, 45, "Foundations of Genetic Algorithms		", 41
exec AddWorkshop "2012-3-28", "8:12:00", "10:12:00", 61, 39, "Asian Conference on Computer Vision", 4
exec AddWorkshop "2011-7-20", "9:8:00", "11:8:00", 128, 39, "2013 Ethical Leadership Conference: Ethics in Action", 27
exec AddWorkshop "2011-9-1", "11:60:00", "13:60:00", 58, 28, "KU Leuven CSR Symposium", 25
exec AddWorkshop "2013-2-7", "8:41:00", "10:41:00", 113, 44, "2013 Ethical Leadership Conference: Ethics in Action", 69
exec AddWorkshop "2013-9-20", "16:26:00", "17:26:00", 132, 49, "Mayo Clinic Presents", 27
exec AddWorkshop "2011-2-2", "17:55:00", "19:55:00", 84, 33, "International Conference on Automated Planning", 46
exec AddWorkshop "2011-8-5", "12:1:00", "14:1:00", 74, 44, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 70
exec AddWorkshop "2012-2-5", "14:50:00", "17:50:00", 90, 34, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 79
exec AddWorkshop "2012-12-13", "11:60:00", "13:60:00", 121, 40, "Life in Space for Life on Earth Symposium", 44
exec AddWorkshop "2012-5-15", "18:10:00", "19:10:00", 121, 49, "International Conference on Pattern Recognition", 33
exec AddWorkshop "2012-9-18", "9:37:00", "12:37:00", 135, 34, "Photonics West", 61
exec AddWorkshop "2011-4-16", "9:3:00", "10:3:00", 63, 26, "48th ESLAB Symposium", 28
exec AddWorkshop "2013-5-21", "19:48:00", "22:48:00", 108, 32, "Leadership Strategies for Information Technology in Health Care Boston", 15
exec AddWorkshop "2011-6-3", "18:15:00", "21:15:00", 61, 45, "Conference on Computer Vision and Pattern", 43
exec AddWorkshop "2012-8-11", "12:60:00", "15:60:00", 113, 48, "British	Machine	Vision	Conference		", 68
exec AddWorkshop "2011-12-12", "18:2:00", "21:2:00", 91, 36, "Foundations of Genetic Algorithms		", 32
exec AddWorkshop "2013-3-20", "17:26:00", "20:26:00", 82, 26, "KU Leuven CSR Symposium", 83
exec AddWorkshop "2013-2-21", "16:10:00", "19:10:00", 74, 23, "Annual Interdisciplinary Conference", 39
exec AddWorkshop "2012-10-24", "10:52:00", "12:52:00", 122, 31, "Conference on Learning Theory		", 66
exec AddWorkshop "2011-10-26", "14:2:00", "16:2:00", 95, 23, "Mayo Clinic Presents", 52
exec AddWorkshop "2012-4-3", "19:48:00", "22:48:00", 100, 30, "Mechanisms Final Presentation Days", 72
exec AddWorkshop "2012-6-4", "10:5:00", "13:5:00", 98, 41, "ARTES 1 Final Presentation Days", 77
exec AddWorkshop "2011-3-22", "18:25:00", "21:25:00", 57, 36, "International Conference on Pattern Recognition", 44
exec AddWorkshop "2013-5-14", "13:4:00", "14:4:00", 110, 35, "4S Symposium 2014", 50
exec AddWorkshop "2011-5-27", "10:2:00", "13:2:00", 116, 37, "Conference on Computer Vision and Pattern", 7
exec AddWorkshop "2013-10-7", "14:19:00", "15:19:00", 56, 22, "The 2nd International CSR Communication Conference", 80
exec AddWorkshop "2011-1-1", "8:38:00", "9:38:00", 66, 42, "7 th International Conference on Bio-inspired Systems and Signal Processing", 30
exec AddWorkshop "2011-1-14", "15:23:00", "16:23:00", 106, 40, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 39
exec AddWorkshop "2012-5-1", "17:23:00", "20:23:00", 58, 41, "3rd International Conference on Pattern Recognition Applications and Methods", 23
exec AddWorkshop "2011-5-24", "9:25:00", "12:25:00", 74, 32, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 56
exec AddWorkshop "2011-3-21", "15:10:00", "18:10:00", 147, 34, "Global Conference on Sustainability and Reporting", 25
exec AddWorkshop "2013-2-22", "14:48:00", "17:48:00", 58, 21, "Microwave Workshop", 22
exec AddWorkshop "2012-2-7", "19:37:00", "20:37:00", 81, 22, "Corporate Community Involvement Conference", 67
exec AddWorkshop "2011-3-10", "16:1:00", "19:1:00", 149, 49, "Ceres Conference", 31
exec AddWorkshop "2013-7-27", "11:19:00", "13:19:00", 117, 46, "The 12th annual Responsible Business Summit", 17
exec AddWorkshop "2012-7-11", "18:19:00", "21:19:00", 90, 36, "Client Summit", 70
exec AddWorkshop "2012-7-12", "17:49:00", "20:49:00", 140, 40, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 12
exec AddWorkshop "2012-8-9", "13:5:00", "16:5:00", 60, 30, "Workshop on Logic ", 4
exec AddWorkshop "2013-3-25", "8:50:00", "9:50:00", 61, 28, "Microwave Workshop", 29
exec AddWorkshop "2012-3-24", "8:20:00", "9:20:00", 115, 35, "International Joint Conference on Automated Reasoning", 36
exec AddWorkshop "2012-7-20", "10:49:00", "13:49:00", 137, 39, "The BCLC CSR Conference", 26
exec AddWorkshop "2013-5-1", "12:13:00", "15:13:00", 137, 38, "Workshop on Image Processing", 75
exec AddWorkshop "2012-12-26", "13:1:00", "14:1:00", 127, 47, "International Conference on Computer Vision", 39
exec AddWorkshop "2011-3-12", "12:51:00", "15:51:00", 128, 28, "Client Summit", 58
exec AddWorkshop "2013-1-3", "17:41:00", "18:41:00", 114, 38, "�International� Corporate Citizenship Conference", 36
exec AddWorkshop "2011-10-9", "15:32:00", "16:32:00", 118, 31, "Mechanisms Final Presentation Days", 57
exec AddWorkshop "2011-11-5", "18:48:00", "19:48:00", 71, 23, "2013 Ethical Leadership Conference: Ethics in Action", 60
exec AddWorkshop "2012-9-20", "15:57:00", "16:57:00", 69, 25, "The Sixth International Conferences on Advances in Multimedia", 36
exec AddWorkshop "2012-6-14", "19:26:00", "20:26:00", 122, 33, "The Sixth International Conferences on Advances in Multimedia", 4
exec AddWorkshop "2012-3-20", "17:56:00", "19:56:00", 146, 22, "Sustainable Brands Conference", 55
exec AddWorkshop "2013-6-18", "10:8:00", "11:8:00", 75, 31, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 49
exec AddWorkshop "2013-3-1", "17:37:00", "20:37:00", 52, 25, "Sustainable Brands Conference", 48
exec AddWorkshop "2012-3-22", "9:30:00", "12:30:00", 116, 40, "International Conference on Pattern Recognition", 18
exec AddWorkshop "2011-10-27", "12:31:00", "14:31:00", 57, 46, "4th DUE Permafrost User Workshop", 29
exec AddWorkshop "2012-5-20", "14:55:00", "15:55:00", 147, 38, "Workshop on Image Processing", 18
exec AddWorkshop "2012-6-12", "12:40:00", "15:40:00", 107, 26, "Mechanisms Final Presentation Days", 68
exec AddWorkshop "2011-4-4", "18:30:00", "21:30:00", 73, 34, "Photonics West", 44
exec AddWorkshop "2011-12-8", "15:39:00", "17:39:00", 63, 47, "Sentinel-2 for Science WS", 22
exec AddWorkshop "2011-2-15", "16:26:00", "19:26:00", 133, 46, "European Conference on Machine Learning", 18
exec AddWorkshop "2012-4-24", "19:13:00", "21:13:00", 107, 35, "Life in Space for Life on Earth Symposium", 77
exec AddWorkshop "2012-10-1", "12:14:00", "15:14:00", 81, 37, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 56
exec AddWorkshop "2012-3-17", "13:50:00", "16:50:00", 66, 41, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 49
exec AddWorkshop "2011-12-30", "13:14:00", "14:14:00", 145, 22, "Conference on Uncertainty in Artificial Intelligence", 72
exec AddWorkshop "2011-11-30", "19:38:00", "22:38:00", 141, 30, "International Conference on the Principles of", 66
exec AddWorkshop "2012-5-17", "15:29:00", "17:29:00", 55, 25, "European Navigation Conference 2014 (ENC-GNSS 2014)", 27
exec AddWorkshop "2013-2-12", "15:55:00", "18:55:00", 103, 37, "48th ESLAB Symposium", 21
exec AddWorkshop "2011-1-8", "19:60:00", "21:60:00", 125, 21, "The 2nd International CSR Communication Conference", 18
exec AddWorkshop "2011-7-4", "18:60:00", "21:60:00", 51, 34, "The Sixth International Conferences on Advances in Multimedia", 5
exec AddWorkshop "2013-11-1", "9:38:00", "11:38:00", 74, 46, "48th ESLAB Symposium", 1
exec AddWorkshop "2013-12-28", "14:3:00", "16:3:00", 145, 25, "Workshop on Logic ", 1
exec AddWorkshop "2011-1-15", "8:41:00", "9:41:00", 106, 47, "International Joint Conference on Artificial Intelligence", 67
exec AddWorkshop "2011-4-17", "19:43:00", "20:43:00", 143, 33, "Euclid Spacecraft Industry Day", 56
exec AddWorkshop "2011-3-21", "8:37:00", "10:37:00", 60, 31, "European Conference on Artificial Intelligence	", 1
exec AddWorkshop "2012-5-5", "15:60:00", "18:60:00", 106, 44, "KU Leuven CSR Symposium", 38
exec AddWorkshop "2011-4-23", "17:2:00", "18:2:00", 62, 29, "The BCLC CSR Conference", 1
exec AddWorkshop "2012-9-19", "12:52:00", "13:52:00", 113, 27, "International Conference on Signal and Imaging Systems Engineering", 58
exec AddWorkshop "2012-3-3", "10:7:00", "11:7:00", 146, 26, "International Conference on Automated Reasoning", 81
exec AddWorkshop "2013-10-29", "17:18:00", "20:18:00", 132, 49, "Foundations of Genetic Algorithms		", 27
exec AddWorkshop "2012-11-10", "19:17:00", "22:17:00", 92, 39, "International Joint Conference on Automated Reasoning", 63
exec AddWorkshop "2013-10-13", "16:29:00", "19:29:00", 76, 20, "Conference on Uncertainty in Artificial Intelligence", 26
exec AddWorkshop "2013-7-4", "19:21:00", "22:21:00", 137, 39, "KU Leuven CSR Symposium", 10
exec AddWorkshop "2011-11-8", "11:13:00", "13:13:00", 97, 30, "Microwave Workshop", 83
exec AddWorkshop "2013-4-25", "18:55:00", "19:55:00", 131, 20, "International Joint Conference on Artificial Intelligence", 10
exec AddWorkshop "2012-3-23", "13:41:00", "15:41:00", 89, 39, "Ceres Conference", 14
exec AddWorkshop "2013-11-15", "19:26:00", "22:26:00", 107, 31, "The 12th annual Responsible Business Summit", 46
exec AddWorkshop "2012-12-23", "8:17:00", "11:17:00", 128, 38, "Net Impact Conference", 14
exec AddWorkshop "2012-1-8", "9:59:00", "11:59:00", 99, 35, "The Sixth International Conferences on Advances in Multimedia", 81
exec AddWorkshop "2011-7-7", "8:45:00", "9:45:00", 144, 43, "The BCLC CSR Conference", 47
exec AddWorkshop "2011-1-9", "10:21:00", "13:21:00", 53, 25, "Science and Challenges of Lunar Sample Return Workshop", 4
exec AddWorkshop "2013-1-29", "17:14:00", "20:14:00", 82, 26, "2nd International Conference on Photonics, Optics and Laser Technology", 58
exec AddWorkshop "2011-1-16", "16:49:00", "17:49:00", 62, 36, "ARTES 1 Final Presentation Days", 42
exec AddWorkshop "2013-1-8", "8:48:00", "11:48:00", 73, 30, "3rd International Conference on Pattern Recognition Applications and Methods", 54
exec AddWorkshop "2012-12-13", "18:48:00", "21:48:00", 79, 24, "X-Ray Universe", 63
exec AddWorkshop "2012-9-11", "10:25:00", "12:25:00", 134, 49, "Cause Marketing Forum", 12
exec AddWorkshop "2011-7-13", "14:21:00", "15:21:00", 104, 26, "RuleML Symposium		", 53
exec AddWorkshop "2011-11-5", "14:40:00", "15:40:00", 136, 32, "Global Conference on Sustainability and Reporting", 11
exec AddWorkshop "2011-2-6", "13:43:00", "16:43:00", 51, 42, "Leadership Strategies for Information Technology in Health Care Boston", 42
exec AddWorkshop "2012-4-9", "13:60:00", "15:60:00", 113, 40, "ARTES 1 Final Presentation Days", 44
exec AddWorkshop "2012-10-28", "15:12:00", "17:12:00", 88, 30, "Conference on Computer Vision and Pattern", 4
exec AddWorkshop "2011-10-24", "15:50:00", "18:50:00", 53, 21, "Annual Interdisciplinary Conference", 29
exec AddWorkshop "2012-11-11", "12:45:00", "15:45:00", 130, 41, "4th DUE Permafrost User Workshop", 64
exec AddWorkshop "2013-8-8", "12:1:00", "13:1:00", 110, 30, "Cause Marketing Forum", 62
exec AddWorkshop "2013-3-23", "10:54:00", "13:54:00", 149, 34, "International Conference on Automated Reasoning", 72
exec AddWorkshop "2011-7-7", "12:10:00", "14:10:00", 112, 38, "3rd International Conference on Pattern Recognition Applications and Methods", 56
exec AddWorkshop "2013-4-18", "17:26:00", "18:26:00", 52, 25, "Leadership Strategies for Information Technology in Health Care Boston", 34
exec AddWorkshop "2011-12-25", "8:52:00", "9:52:00", 79, 27, "Client Summit", 5
exec AddWorkshop "2011-10-27", "12:59:00", "15:59:00", 110, 40, "International Conference on Autonomous Agents and", 3
exec AddWorkshop "2012-2-2", "17:17:00", "20:17:00", 70, 35, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 5
exec AddWorkshop "2012-6-17", "14:16:00", "16:16:00", 64, 41, "Conference on Uncertainty in Artificial Intelligence", 42
exec AddWorkshop "2013-12-6", "9:9:00", "11:9:00", 134, 24, "Conference on Learning Theory		", 58
exec AddWorkshop "2012-1-3", "16:33:00", "18:33:00", 107, 34, "International Conference on Pattern Recognition", 1
exec AddWorkshop "2013-6-15", "19:49:00", "21:49:00", 107, 37, "Conference on Uncertainty in Artificial Intelligence", 83
exec AddWorkshop "2012-12-8", "11:30:00", "12:30:00", 119, 36, "LPVE Land product validation and evolution", 36
exec AddWorkshop "2012-7-27", "15:47:00", "17:47:00", 142, 20, "Conference on Learning Theory		", 34
exec AddWorkshop "2013-7-18", "17:19:00", "20:19:00", 57, 47, "Life in Space for Life on Earth Symposium", 42
exec AddWorkshop "2012-2-19", "15:41:00", "17:41:00", 55, 47, "Conference on Learning Theory		", 11
exec AddWorkshop "2011-8-1", "16:8:00", "19:8:00", 64, 46, "British	Machine	Vision	Conference		", 48
exec AddWorkshop "2013-10-17", "12:8:00", "15:8:00", 139, 29, "X-Ray Universe", 1
exec AddWorkshop "2011-10-6", "15:6:00", "18:6:00", 145, 21, "4S Symposium 2014", 27
exec AddWorkshop "2012-6-27", "9:30:00", "12:30:00", 149, 23, "KU Leuven CSR Symposium", 62
exec AddWorkshop "2012-2-25", "9:9:00", "12:9:00", 143, 22, "Annual Interdisciplinary Conference", 70
exec AddWorkshop "2013-6-26", "16:14:00", "18:14:00", 125, 40, "LPVE Land product validation and evolution", 48
exec AddWorkshop "2013-9-17", "14:34:00", "15:34:00", 50, 35, "The 12th annual Responsible Business Summit", 20
exec AddWorkshop "2013-10-8", "17:35:00", "20:35:00", 146, 34, "Mayo Clinic Presents", 67
exec AddWorkshop "2013-12-20", "16:28:00", "17:28:00", 70, 49, "Mechanisms Final Presentation Days", 54
exec AddWorkshop "2012-5-16", "12:7:00", "14:7:00", 102, 37, "48th ESLAB Symposium", 37
exec AddWorkshop "2013-3-11", "18:30:00", "21:30:00", 54, 37, "X-Ray Universe", 10
exec AddWorkshop "2013-6-16", "9:22:00", "11:22:00", 99, 21, "Conference on Computer Vision and Pattern", 57
exec AddWorkshop "2011-8-30", "10:11:00", "12:11:00", 117, 23, "The BCLC CSR Conference", 36
exec AddWorkshop "2012-1-10", "11:34:00", "12:34:00", 83, 40, "Mayo Clinic Presents", 43
exec AddWorkshop "2012-7-13", "13:49:00", "15:49:00", 97, 26, "Client Summit", 58
exec AddWorkshop "2012-9-19", "9:24:00", "10:24:00", 130, 32, "European Space Power Conference", 17
exec AddWorkshop "2013-1-30", "18:23:00", "19:23:00", 88, 45, "�International� Corporate Citizenship Conference", 78
exec AddWorkshop "2012-4-18", "8:24:00", "11:24:00", 139, 36, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 18
exec AddWorkshop "2011-2-28", "8:14:00", "9:14:00", 143, 21, "The Corporate Philanthropy Forum", 81
exec AddWorkshop "2012-3-16", "14:25:00", "17:25:00", 74, 41, "ICATT", 15
exec AddWorkshop "2011-3-18", "17:18:00", "20:18:00", 59, 26, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 4
exec AddWorkshop "2013-6-11", "13:36:00", "16:36:00", 125, 34, "The Corporate Philanthropy Forum", 42
exec AddWorkshop "2012-12-2", "14:1:00", "16:1:00", 125, 45, "European Conference on Machine Learning", 36
exec AddWorkshop "2012-3-28", "16:6:00", "17:6:00", 132, 39, "Conference on Computer Vision and Pattern", 69
exec AddWorkshop "2011-10-13", "9:35:00", "10:35:00", 113, 41, "Ceres Conference", 50
exec AddWorkshop "2012-10-6", "10:43:00", "11:43:00", 77, 42, "LPVE Land product validation and evolution", 20
exec AddWorkshop "2011-8-27", "15:7:00", "17:7:00", 68, 47, "Conference on Uncertainty in Artificial Intelligence", 78
exec AddWorkshop "2013-8-22", "8:13:00", "11:13:00", 78, 41, "Science and Challenges of Lunar Sample Return Workshop", 13
exec AddWorkshop "2013-7-19", "11:10:00", "12:10:00", 106, 26, "Workshop on Image Processing", 38
exec AddWorkshop "2011-2-1", "16:17:00", "17:17:00", 110, 27, "Euclid Spacecraft Industry Day", 50
exec AddWorkshop "2012-7-7", "11:6:00", "14:6:00", 101, 28, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 5
exec AddWorkshop "2012-4-19", "10:45:00", "11:45:00", 108, 30, "International Conference on Logic for Programming", 30
exec AddWorkshop "2012-7-8", "12:5:00", "13:5:00", 60, 32, "4S Symposium 2014", 61
exec AddWorkshop "2012-11-26", "15:58:00", "17:58:00", 71, 36, "4th DUE Permafrost User Workshop", 72
exec AddWorkshop "2012-12-3", "9:3:00", "11:3:00", 130, 41, "Ceres Conference", 9
exec AddWorkshop "2012-1-26", "8:35:00", "11:35:00", 66, 40, "4S Symposium 2014", 41
exec AddWorkshop "2011-6-12", "19:28:00", "20:28:00", 115, 47, "Annual Convention of The Society", 23
exec AddWorkshop "2011-2-12", "12:34:00", "15:34:00", 53, 25, "Conference on Automated Deduction		", 4
exec AddWorkshop "2013-7-20", "8:18:00", "10:18:00", 108, 40, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 16
exec AddWorkshop "2012-2-19", "9:14:00", "11:14:00", 84, 24, "The Sixth International Conferences on Advances in Multimedia", 18
exec AddWorkshop "2012-8-20", "9:14:00", "10:14:00", 124, 27, "Annual Interdisciplinary Conference", 63
exec AddWorkshop "2013-9-20", "15:26:00", "17:26:00", 110, 24, "European Conference on Machine Learning", 69
exec AddWorkshop "2011-1-21", "17:13:00", "18:13:00", 102, 43, "2013 Ethical Leadership Conference: Ethics in Action", 50
exec AddWorkshop "2013-9-16", "14:40:00", "16:40:00", 70, 39, "Science and Challenges of Lunar Sample Return Workshop", 82
exec AddWorkshop "2011-4-9", "11:50:00", "12:50:00", 90, 39, "Sustainable Brands Conference", 39
exec AddWorkshop "2012-10-21", "14:56:00", "16:56:00", 138, 38, "Annual Convention of The Society", 23
exec AddWorkshop "2011-6-18", "15:45:00", "17:45:00", 93, 39, "Photonics West", 70
exec AddWorkshop "2011-3-21", "9:47:00", "10:47:00", 140, 36, "3rd International Conference on Pattern Recognition Applications and Methods", 39
exec AddWorkshop "2013-4-10", "12:15:00", "13:15:00", 116, 32, "British	Machine	Vision	Conference		", 73
exec AddWorkshop "2011-2-5", "17:47:00", "19:47:00", 105, 31, "Cause Marketing Forum", 24
exec AddWorkshop "2012-3-2", "9:52:00", "11:52:00", 94, 48, "RuleML Symposium				", 69
exec AddWorkshop "2011-9-8", "13:40:00", "14:40:00", 63, 29, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 22
exec AddWorkshop "2013-11-22", "17:32:00", "18:32:00", 77, 42, "Leadership Strategies for Information Technology in Health Care Boston", 61
exec AddWorkshop "2012-8-24", "16:33:00", "18:33:00", 94, 32, "International Semantic Web Conference		", 22
exec AddWorkshop "2013-4-27", "8:9:00", "11:9:00", 74, 27, "Mayo Clinic Presents", 29
exec AddWorkshop "2012-2-2", "16:31:00", "19:31:00", 82, 28, "Microwave Workshop", 16
exec AddWorkshop "2012-7-10", "19:4:00", "22:4:00", 84, 34, "2013 Ethical Leadership Conference: Ethics in Action", 84
exec AddWorkshop "2011-8-19", "16:38:00", "19:38:00", 91, 22, "Computer Analysis of Images and Patterns", 18
exec AddWorkshop "2011-8-14", "14:3:00", "15:3:00", 111, 37, "International Conference on Automated Planning", 48
exec AddWorkshop "2013-3-24", "13:46:00", "16:46:00", 122, 24, "Client Summit", 67
exec AddWorkshop "2012-11-24", "9:39:00", "11:39:00", 92, 38, "European Conference on Computer Vision", 78
exec AddWorkshop "2012-12-12", "10:28:00", "13:28:00", 51, 31, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 44
exec AddWorkshop "2011-4-4", "12:23:00", "14:23:00", 64, 21, "Asian Conference on Computer Vision", 34
exec AddWorkshop "2011-2-5", "18:13:00", "19:13:00", 66, 36, "European Navigation Conference 2014 (ENC-GNSS 2014)", 45
exec AddWorkshop "2012-2-4", "15:40:00", "16:40:00", 81, 39, "Business4Better: The Community Partnership Movement", 61
exec AddWorkshop "2013-3-14", "14:49:00", "16:49:00", 50, 48, "RuleML Symposium				", 24
exec AddWorkshop "2012-12-10", "17:1:00", "18:1:00", 53, 35, "The Corporate Philanthropy Forum", 32
exec AddWorkshop "2012-11-19", "19:21:00", "21:21:00", 56, 47, "The 2nd International CSR Communication Conference", 18
exec AddWorkshop "2011-6-22", "8:53:00", "9:53:00", 89, 42, "International Conference on Logic for Programming", 6
exec AddWorkshop "2013-9-24", "18:7:00", "20:7:00", 73, 41, "Workshop on Image Processing", 2
exec AddWorkshop "2012-5-1", "13:39:00", "15:39:00", 64, 43, "International Conference on Autonomous Agents and", 35
exec AddWorkshop "2013-10-13", "17:22:00", "19:22:00", 130, 32, "Asian Conference on Computer Vision", 46
exec AddWorkshop "2011-10-5", "17:46:00", "19:46:00", 125, 20, "International Joint Conference on Artificial Intelligence", 17
exec AddWorkshop "2013-12-28", "16:5:00", "18:5:00", 83, 28, "International Conference on Artificial Neural Networks", 32
exec AddWorkshop "2013-8-20", "11:30:00", "14:30:00", 77, 29, "European Navigation Conference 2014 (ENC-GNSS 2014)", 1
exec AddWorkshop "2013-2-5", "12:11:00", "13:11:00", 106, 29, "Corporate Community Involvement Conference", 31
exec AddWorkshop "2011-8-30", "19:23:00", "22:23:00", 117, 39, "International Conference on Logic for Programming", 62
exec AddWorkshop "2011-2-20", "11:15:00", "13:15:00", 81, 44, "Client Summit", 13
exec AddWorkshop "2012-9-14", "8:48:00", "10:48:00", 67, 32, "The BCLC CSR Conference", 38
exec AddWorkshop "2011-4-26", "18:33:00", "20:33:00", 87, 29, "Mechanisms Final Presentation Days", 14
exec AddWorkshop "2013-5-30", "13:25:00", "15:25:00", 127, 39, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 12
exec AddWorkshop "2011-8-7", "19:18:00", "20:18:00", 127, 29, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 39
exec AddWorkshop "2013-1-29", "9:56:00", "11:56:00", 124, 21, "GNC 2014", 29
exec AddWorkshop "2011-3-22", "14:32:00", "16:32:00", 65, 30, "KU Leuven CSR Symposium", 14
exec AddWorkshop "2012-8-6", "16:52:00", "18:52:00", 116, 41, "Life in Space for Life on Earth Symposium", 84
exec AddWorkshop "2012-4-5", "12:5:00", "13:5:00", 88, 45, "Client Summit", 9
exec AddWorkshop "2011-5-5", "12:36:00", "13:36:00", 113, 34, "Annual Interdisciplinary Conference", 59
exec AddWorkshop "2011-1-28", "17:25:00", "18:25:00", 88, 24, "International Conference on Computer Vision Theory and Applications", 15
exec AddWorkshop "2011-6-13", "13:56:00", "16:56:00", 89, 47, "Photonics West", 18
exec AddWorkshop "2012-12-21", "19:33:00", "20:33:00", 126, 40, "Mechanisms Final Presentation Days", 7
exec AddWorkshop "2011-1-9", "12:46:00", "14:46:00", 52, 28, "LPVE Land product validation and evolution", 28
exec AddWorkshop "2011-4-19", "16:50:00", "19:50:00", 118, 34, "International Joint Conference on Automated Reasoning", 49
exec AddWorkshop "2013-8-15", "15:23:00", "16:23:00", 52, 29, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 28
exec AddWorkshop "2012-10-2", "18:14:00", "19:14:00", 128, 46, "ICATT", 74
exec AddWorkshop "2011-2-11", "13:38:00", "14:38:00", 125, 39, "Client Summit", 31
exec AddWorkshop "2013-6-7", "12:22:00", "13:22:00", 57, 40, "Euclid Spacecraft Industry Day", 50
exec AddWorkshop "2012-11-18", "14:29:00", "17:29:00", 137, 23, "Science and Challenges of Lunar Sample Return Workshop", 84
exec AddWorkshop "2012-6-29", "19:20:00", "21:20:00", 90, 43, "Conference on Uncertainty in Artificial Intelligence", 23
exec AddWorkshop "2013-8-15", "13:43:00", "16:43:00", 126, 25, "International Joint Conference on Artificial Intelligence", 82
exec AddWorkshop "2011-8-10", "16:11:00", "18:11:00", 98, 21, "GNC 2014", 4
exec AddWorkshop "2013-12-24", "13:5:00", "15:5:00", 70, 31, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 11
exec AddWorkshop "2012-3-30", "16:3:00", "17:3:00", 112, 39, "KU Leuven CSR Symposium", 52
exec AddWorkshop "2012-5-9", "13:15:00", "15:15:00", 62, 42, "The Sixth International Conferences on Advances in Multimedia", 15
exec AddWorkshop "2012-9-24", "19:30:00", "21:30:00", 109, 20, "European Conference on Machine Learning", 54
exec AddWorkshop "2011-2-12", "16:41:00", "18:41:00", 75, 48, "The BCLC CSR Conference", 32
exec AddWorkshop "2013-6-19", "10:32:00", "13:32:00", 96, 39, "International Conference on Computer Vision", 40
exec AddWorkshop "2013-12-25", "13:54:00", "15:54:00", 85, 42, "International Conference on Automated Planning", 13
exec AddWorkshop "2012-11-27", "12:33:00", "13:33:00", 114, 24, "International Conference on Automated Planning", 79
exec AddWorkshop "2013-6-19", "19:45:00", "22:45:00", 66, 47, "International Conference on Autonomous Agents and", 49
exec AddWorkshop "2013-5-22", "9:42:00", "12:42:00", 68, 38, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 60
exec AddWorkshop "2011-7-17", "8:4:00", "10:4:00", 91, 41, "International Conference on Computer Vision", 23
exec AddWorkshop "2011-9-8", "16:32:00", "18:32:00", 57, 33, "European Space Power Conference", 11
exec AddWorkshop "2011-6-24", "17:53:00", "18:53:00", 84, 23, "International Conference on Computer Vision Theory and Applications", 8
exec AddWorkshop "2011-5-17", "14:25:00", "17:25:00", 109, 49, "Ceres Conference", 45
exec AddWorkshop "2012-12-22", "17:14:00", "20:14:00", 73, 44, "The 12th annual Responsible Business Summit", 20
exec AddWorkshop "2012-11-27", "11:12:00", "12:12:00", 51, 24, "International Joint Conference on Artificial Intelligence", 79
exec AddWorkshop "2011-10-10", "18:47:00", "19:47:00", 110, 47, "European Conference on Artificial Intelligence	", 11
exec AddWorkshop "2011-12-28", "18:50:00", "21:50:00", 126, 33, "The BCLC CSR Conference", 4
exec AddWorkshop "2013-8-22", "8:37:00", "10:37:00", 111, 35, "British	Machine	Vision	Conference		", 53
exec AddWorkshop "2013-4-4", "11:7:00", "13:7:00", 67, 26, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 2
exec AddWorkshop "2011-6-11", "10:34:00", "12:34:00", 89, 49, "Mechanisms Final Presentation Days", 45
exec AddWorkshop "2013-4-22", "16:56:00", "17:56:00", 81, 46, "LPVE Land product validation and evolution", 6
exec AddWorkshop "2011-4-24", "14:27:00", "16:27:00", 92, 32, "LPVE Land product validation and evolution", 30
exec AddWorkshop "2011-5-28", "12:31:00", "13:31:00", 134, 26, "Leadership Strategies for Information Technology in Health Care Boston", 44
exec AddWorkshop "2013-3-2", "16:49:00", "19:49:00", 110, 35, "Leadership Strategies for Information Technology in Health Care Boston", 33
exec AddWorkshop "2012-11-17", "18:13:00", "21:13:00", 53, 45, "Conference on Uncertainty in Artificial Intelligence", 6
exec AddWorkshop "2012-10-11", "9:32:00", "11:32:00", 84, 26, "Ceres Conference", 3
exec AddWorkshop "2013-11-22", "18:47:00", "19:47:00", 59, 29, "International Conference on Logic for Programming", 18
exec AddWorkshop "2013-9-17", "12:48:00", "15:48:00", 51, 27, "Conference on Uncertainty in Artificial Intelligence", 22
exec AddWorkshop "2013-1-6", "8:40:00", "11:40:00", 86, 49, "Sustainable Brands Conference", 38
exec AddWorkshop "2013-2-30", "8:42:00", "10:42:00", 145, 28, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 77
exec AddWorkshop "2011-1-16", "15:33:00", "16:33:00", 84, 26, "Mechanisms Final Presentation Days", 35
exec AddWorkshop "2013-6-15", "16:38:00", "17:38:00", 145, 23, "International Conference on Signal and Imaging Systems Engineering", 11
exec AddWorkshop "2013-8-22", "12:24:00", "14:24:00", 149, 26, "The 2nd International CSR Communication Conference", 18
exec AddWorkshop "2013-10-2", "17:54:00", "18:54:00", 131, 30, "The 2nd International CSR Communication Conference", 44
exec AddWorkshop "2013-6-26", "10:6:00", "13:6:00", 63, 35, "British	Machine	Vision	Conference		", 7
exec AddWorkshop "2011-2-24", "11:46:00", "14:46:00", 60, 31, "RuleML Symposium		", 81
exec AddWorkshop "2011-11-19", "17:47:00", "18:47:00", 73, 30, "International Conference on Logic for Programming", 61
exec AddWorkshop "2011-1-21", "10:24:00", "13:24:00", 120, 44, "Computer Analysis of Images and Patterns", 43
exec AddWorkshop "2012-4-1", "17:32:00", "18:32:00", 82, 27, "International Conference on Automated Planning", 2
exec AddWorkshop "2011-2-4", "17:43:00", "20:43:00", 85, 29, "2013 Ethical Leadership Conference: Ethics in Action", 53
exec AddWorkshop "2013-3-12", "9:49:00", "12:49:00", 139, 39, "International Conference on Logic for Programming", 63
exec AddWorkshop "2012-1-21", "11:15:00", "12:15:00", 57, 36, "Business4Better: The Community Partnership Movement", 22
exec AddWorkshop "2013-12-23", "19:43:00", "22:43:00", 124, 22, "Leadership Strategies for Information Technology in Health Care Boston", 34
exec AddWorkshop "2011-5-13", "13:36:00", "14:36:00", 71, 20, "European Navigation Conference 2014 (ENC-GNSS 2014)", 67
exec AddWorkshop "2011-5-19", "10:24:00", "13:24:00", 103, 44, "International Conference on Automated Reasoning", 4
exec AddWorkshop "2013-5-12", "12:55:00", "15:55:00", 59, 47, "International Conference on Signal and Imaging Systems Engineering", 37
exec AddWorkshop "2011-7-9", "10:24:00", "11:24:00", 73, 48, "Microwave Workshop", 68
exec AddWorkshop "2012-5-25", "16:8:00", "17:8:00", 66, 21, "The 2nd International CSR Communication Conference", 18
exec AddWorkshop "2011-7-14", "8:27:00", "9:27:00", 108, 29, "Life in Space for Life on Earth Symposium", 70
exec AddWorkshop "2013-12-20", "8:9:00", "9:9:00", 87, 25, "Foundations of Genetic Algorithms		", 5
exec AddWorkshop "2013-1-12", "15:29:00", "16:29:00", 54, 41, "European Conference on Machine Learning", 59
exec AddWorkshop "2011-7-8", "11:2:00", "13:2:00", 127, 28, "European Space Technology Harmonisation Conference", 38
exec AddWorkshop "2012-7-22", "14:44:00", "17:44:00", 141, 44, "Mechanisms Final Presentation Days", 55
exec AddWorkshop "2011-7-15", "14:30:00", "16:30:00", 50, 44, "Science and Challenges of Lunar Sample Return Workshop", 83
exec AddWorkshop "2011-5-17", "9:57:00", "12:57:00", 50, 25, "Asian Conference on Computer Vision", 66
exec AddWorkshop "2012-3-8", "16:6:00", "18:6:00", 75, 37, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 52
exec AddWorkshop "2011-3-16", "8:4:00", "11:4:00", 58, 33, "British	Machine	Vision	Conference		", 29
exec AddWorkshop "2012-7-27", "8:8:00", "11:8:00", 125, 43, "The 12th annual Responsible Business Summit", 23
exec AddWorkshop "2013-10-27", "10:28:00", "13:28:00", 102, 45, "48th ESLAB Symposium", 70
exec AddWorkshop "2013-5-14", "17:22:00", "20:22:00", 60, 36, "International Conference on Computer Vision", 40
exec AddWorkshop "2011-11-13", "16:19:00", "19:19:00", 115, 45, "Workshop on Image Processing", 63
exec AddWorkshop "2012-9-13", "11:24:00", "13:24:00", 127, 35, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 44
exec AddWorkshop "2013-8-2", "18:59:00", "19:59:00", 58, 40, "2013 Ethical Leadership Conference: Ethics in Action", 38
exec AddWorkshop "2011-7-1", "19:6:00", "21:6:00", 50, 40, "ICATT", 83
exec AddWorkshop "2012-9-7", "14:57:00", "16:57:00", 141, 20, "Client Summit", 79
exec AddWorkshop "2012-8-8", "8:2:00", "9:2:00", 114, 42, "Euclid Spacecraft Industry Day", 60
exec AddWorkshop "2013-4-11", "14:58:00", "17:58:00", 118, 40, "Cause Marketing Forum", 68
exec AddWorkshop "2012-8-26", "9:37:00", "10:37:00", 114, 20, "International Conference on Artificial Neural Networks", 24
exec AddWorkshop "2011-12-16", "13:47:00", "15:47:00", 123, 36, "KU Leuven CSR Symposium", 38
exec AddWorkshop "2011-7-24", "14:48:00", "17:48:00", 126, 23, "Cause Marketing Forum", 71
exec AddWorkshop "2011-3-8", "10:48:00", "11:48:00", 86, 26, "International Conference on MultiMedia Modeling", 60
exec AddWorkshop "2013-10-16", "11:10:00", "14:10:00", 143, 32, "The BCLC CSR Conference", 60
exec AddWorkshop "2011-9-4", "17:40:00", "20:40:00", 82, 40, "Ceres Conference", 30
exec AddWorkshop "2011-9-13", "10:28:00", "12:28:00", 137, 48, "Corporate Community Involvement Conference", 11
exec AddWorkshop "2011-11-2", "8:55:00", "9:55:00", 107, 49, "48th ESLAB Symposium", 23
exec AddWorkshop "2013-9-13", "16:32:00", "17:32:00", 117, 27, "ARTES 1 Final Presentation Days", 48
exec AddWorkshop "2013-5-23", "16:52:00", "17:52:00", 82, 24, "Workshop on Logic ", 67
exec AddWorkshop "2012-3-1", "18:9:00", "20:9:00", 124, 45, "GNC 2014", 9
exec AddWorkshop "2012-3-6", "19:20:00", "22:20:00", 99, 35, "International Conference on Automated Planning", 70
exec AddWorkshop "2013-10-29", "16:7:00", "18:7:00", 84, 46, "ARTES 1 Final Presentation Days", 0
exec AddWorkshop "2011-10-13", "16:8:00", "19:8:00", 87, 44, "48th ESLAB Symposium", 81
exec AddWorkshop "2013-9-25", "10:34:00", "11:34:00", 145, 32, "International Conference on MultiMedia Modeling", 54
exec AddWorkshop "2012-10-25", "16:36:00", "18:36:00", 97, 21, "EuroCOW the Calibration and Orientation Workshop", 39
exec AddWorkshop "2012-1-24", "18:36:00", "19:36:00", 63, 30, "Conference on Learning Theory		", 20
exec AddWorkshop "2012-6-2", "9:16:00", "10:16:00", 68, 32, "International Conference on Logic for Programming", 47
exec AddWorkshop "2013-5-25", "10:7:00", "12:7:00", 127, 45, "The Corporate Philanthropy Forum", 30
exec AddWorkshop "2013-4-17", "16:18:00", "17:18:00", 98, 23, "Corporate Community Involvement Conference", 72
exec AddWorkshop "2012-4-20", "19:40:00", "20:40:00", 138, 24, "Leadership Strategies for Information Technology in Health Care Boston", 62
exec AddWorkshop "2011-10-5", "8:47:00", "9:47:00", 92, 36, "International Conference on Signal and Imaging Systems Engineering", 52
exec AddWorkshop "2011-9-27", "18:58:00", "19:58:00", 145, 39, "The 12th annual Responsible Business Summit", 54
exec AddWorkshop "2012-12-19", "19:54:00", "21:54:00", 101, 38, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 8
exec AddWorkshop "2011-6-20", "17:26:00", "20:26:00", 54, 32, "Foundations of Genetic Algorithms		", 71
exec AddWorkshop "2013-4-3", "8:35:00", "10:35:00", 140, 41, "EuroCOW the Calibration and Orientation Workshop", 68
exec AddWorkshop "2011-10-7", "13:32:00", "14:32:00", 131, 33, "2nd International Conference on Photonics, Optics and Laser Technology", 31
exec AddWorkshop "2012-11-22", "11:49:00", "13:49:00", 117, 29, "Net Impact Conference", 45
exec AddWorkshop "2011-12-18", "14:4:00", "16:4:00", 73, 27, "GNC 2014", 72
exec AddWorkshop "2011-1-6", "16:59:00", "18:59:00", 66, 35, "Business4Better: The Community Partnership Movement", 14
exec AddWorkshop "2011-12-25", "14:29:00", "15:29:00", 75, 32, "Corporate Community Involvement Conference", 66
exec AddWorkshop "2011-4-1", "16:26:00", "19:26:00", 149, 35, "The Sixth International Conferences on Advances in Multimedia", 31
exec AddWorkshop "2013-11-22", "14:11:00", "16:11:00", 92, 24, "Conference on Automated Deduction		", 45
exec AddWorkshop "2011-12-29", "11:3:00", "13:3:00", 101, 21, "The Sixth International Conferences on Advances in Multimedia", 65
exec AddWorkshop "2013-3-20", "10:43:00", "11:43:00", 93, 24, "Ceres Conference", 9
exec AddWorkshop "2013-6-10", "14:55:00", "17:55:00", 83, 31, "Life in Space for Life on Earth Symposium", 70
exec AddWorkshop "2012-8-30", "16:60:00", "18:60:00", 55, 21, "Conference on Automated Deduction		", 81
exec AddWorkshop "2013-2-14", "11:23:00", "12:23:00", 138, 30, "The Corporate Philanthropy Forum", 71
exec AddWorkshop "2011-9-2", "12:23:00", "14:23:00", 92, 38, "Cause Marketing Forum", 29
exec AddWorkshop "2012-5-14", "17:42:00", "19:42:00", 78, 49, "International Conference on Automated Reasoning", 26
exec AddWorkshop "2013-2-30", "10:39:00", "13:39:00", 136, 49, "2013 Ethical Leadership Conference: Ethics in Action", 42
exec AddWorkshop "2012-4-28", "15:4:00", "18:4:00", 65, 49, "Foundations of Genetic Algorithms		", 67
exec AddWorkshop "2012-10-13", "19:42:00", "21:42:00", 55, 27, "Conference on Learning Theory		", 60
exec AddWorkshop "2013-9-6", "12:45:00", "14:45:00", 50, 29, "Computer Analysis of Images and Patterns", 65
exec AddWorkshop "2013-12-30", "8:44:00", "11:44:00", 141, 22, "Workshop on Logic ", 49
exec AddWorkshop "2012-4-15", "9:42:00", "11:42:00", 98, 20, "European Space Technology Harmonisation Conference", 59
exec AddWorkshop "2011-11-29", "12:58:00", "14:58:00", 69, 44, "Business4Better: The Community Partnership Movement", 22
exec AddWorkshop "2012-2-20", "14:34:00", "16:34:00", 64, 42, "Conference on Computer Vision and Pattern", 25
exec AddWorkshop "2013-5-12", "9:37:00", "12:37:00", 73, 22, "Workshop on Image Processing", 34
exec AddWorkshop "2013-7-6", "9:20:00", "10:20:00", 116, 38, "4th DUE Permafrost User Workshop", 22
exec AddWorkshop "2013-3-9", "9:35:00", "12:35:00", 137, 21, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 73
exec AddWorkshop "2013-10-14", "12:60:00", "15:60:00", 127, 38, "Sentinel-2 for Science WS", 36
exec AddWorkshop "2011-2-8", "10:57:00", "11:57:00", 75, 22, "Ceres Conference", 59
exec AddWorkshop "2013-8-27", "8:3:00", "10:3:00", 50, 44, "Workshop on Logic ", 49
exec AddWorkshop "2013-5-6", "19:38:00", "21:38:00", 127, 22, "Ceres Conference", 0
exec AddWorkshop "2011-6-4", "10:20:00", "11:20:00", 84, 21, "Computer Analysis of Images and Patterns", 40
exec AddWorkshop "2011-8-30", "9:36:00", "11:36:00", 75, 31, "Global Conference on Sustainability and Reporting", 24
exec AddWorkshop "2012-8-5", "18:7:00", "21:7:00", 73, 41, "International Conference on Computer Vision", 5
exec AddWorkshop "2012-3-21", "18:22:00", "21:22:00", 73, 42, "Computer Analysis of Images and Patterns", 23
exec AddWorkshop "2011-9-20", "9:36:00", "11:36:00", 135, 40, "International Conference on Computer Vision Theory and Applications", 71
exec AddWorkshop "2011-6-13", "15:13:00", "17:13:00", 87, 27, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 58
exec AddWorkshop "2011-9-17", "12:16:00", "15:16:00", 83, 24, "International Conference on Signal and Imaging Systems Engineering", 46
exec AddWorkshop "2012-9-9", "15:50:00", "16:50:00", 90, 28, "ICATT", 43
exec AddWorkshop "2013-4-3", "18:11:00", "20:11:00", 149, 36, "3rd International Conference on Pattern Recognition Applications and Methods", 56
exec AddWorkshop "2013-7-28", "11:10:00", "12:10:00", 127, 25, "4S Symposium 2014", 48
exec AddWorkshop "2013-7-22", "18:15:00", "19:15:00", 51, 21, "3rd International Conference on Pattern Recognition Applications and Methods", 45
exec AddWorkshop "2011-8-18", "13:4:00", "16:4:00", 99, 33, "International Conference on Autonomous Agents and", 51
exec AddWorkshop "2011-12-29", "14:43:00", "16:43:00", 144, 47, "European Conference on Computer Vision", 83
exec AddWorkshop "2011-7-2", "9:52:00", "12:52:00", 89, 42, "Conference on Uncertainty in Artificial Intelligence", 18
exec AddWorkshop "2013-10-19", "18:27:00", "20:27:00", 94, 39, "Mechanisms Final Presentation Days", 23
exec AddWorkshop "2013-4-18", "9:8:00", "10:8:00", 93, 28, "Conference on Automated Deduction		", 10
exec AddWorkshop "2013-11-19", "9:2:00", "10:2:00", 125, 30, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 68
exec AddWorkshop "2011-1-6", "15:5:00", "16:5:00", 62, 44, "The 12th annual Responsible Business Summit", 34
exec AddWorkshop "2013-2-21", "19:20:00", "22:20:00", 88, 32, "X-Ray Universe", 71
exec AddWorkshop "2011-10-29", "16:46:00", "17:46:00", 124, 37, "International Conference on Logic for Programming", 35
exec AddWorkshop "2013-3-5", "11:42:00", "13:42:00", 61, 32, "Sustainable Brands Conference", 13
exec AddWorkshop "2013-11-30", "19:11:00", "20:11:00", 99, 25, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 60
exec AddWorkshop "2011-5-10", "16:19:00", "17:19:00", 125, 45, "EuroCOW the Calibration and Orientation Workshop", 47
exec AddWorkshop "2012-2-8", "9:28:00", "11:28:00", 131, 37, "Computer Analysis of Images and Patterns", 51
exec AddWorkshop "2012-1-21", "9:29:00", "10:29:00", 139, 26, "RuleML Symposium		", 38
exec AddWorkshop "2012-2-26", "15:4:00", "17:4:00", 54, 24, "International Conference on the Principles of", 61
exec AddWorkshop "2011-12-14", "19:24:00", "22:24:00", 148, 39, "International Conference on Artificial Neural Networks", 83
exec AddWorkshop "2013-11-17", "8:13:00", "11:13:00", 86, 30, "Business4Better: The Community Partnership Movement", 50
exec AddWorkshop "2012-12-2", "13:58:00", "16:58:00", 68, 25, "International Conference on Computer Vision Theory and Applications", 82
exec AddWorkshop "2012-2-12", "12:8:00", "13:8:00", 142, 21, "Global Conference on Sustainability and Reporting", 23
exec AddWorkshop "2013-10-12", "18:35:00", "21:35:00", 80, 46, "Photonics West", 59
exec AddWorkshop "2013-10-13", "16:9:00", "17:9:00", 135, 48, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 38
exec AddWorkshop "2013-11-18", "18:6:00", "21:6:00", 64, 41, "Euclid Spacecraft Industry Day", 52
exec AddWorkshop "2011-7-7", "12:17:00", "15:17:00", 142, 46, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 46
exec AddWorkshop "2013-4-1", "12:48:00", "15:48:00", 142, 41, "Conference on Computer Vision and Pattern", 57
exec AddWorkshop "2013-2-1", "10:46:00", "11:46:00", 149, 42, "2nd International Conference on Photonics, Optics and Laser Technology", 65
exec AddWorkshop "2012-3-5", "9:39:00", "10:39:00", 143, 38, "GNC 2014", 63
exec AddWorkshop "2013-6-13", "8:59:00", "9:59:00", 111, 28, "The Sixth International Conferences on Advances in Multimedia", 11
exec AddWorkshop "2013-3-18", "13:58:00", "14:58:00", 136, 30, "3rd International Conference on Pattern Recognition Applications and Methods", 80
exec AddWorkshop "2011-7-12", "12:53:00", "14:53:00", 88, 23, "International Conference on Computer Vision", 36
exec AddWorkshop "2013-6-8", "14:48:00", "15:48:00", 92, 30, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 25
exec AddWorkshop "2011-1-26", "18:8:00", "21:8:00", 105, 42, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 3
exec AddWorkshop "2013-10-24", "10:55:00", "13:55:00", 61, 31, "International Conference on Pattern Recognition", 70
exec AddWorkshop "2013-10-11", "11:35:00", "14:35:00", 103, 44, "International Conference on the Principles of", 25
exec AddWorkshop "2013-1-6", "8:55:00", "9:55:00", 93, 22, "Microwave Workshop", 83
exec AddWorkshop "2011-7-5", "12:54:00", "13:54:00", 94, 31, "European Conference on Computer Vision", 50
exec AddWorkshop "2011-1-4", "19:7:00", "21:7:00", 91, 27, "British	Machine	Vision	Conference		", 83
exec AddWorkshop "2011-3-11", "9:32:00", "11:32:00", 64, 48, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 38
exec AddWorkshop "2012-12-14", "13:54:00", "15:54:00", 121, 40, "LPVE Land product validation and evolution", 70
exec AddWorkshop "2012-1-5", "11:28:00", "12:28:00", 74, 26, "European Conference on Computer Vision", 74
exec AddWorkshop "2013-3-22", "9:5:00", "11:5:00", 139, 32, "GNC 2014", 40
exec AddWorkshop "2011-4-12", "8:27:00", "11:27:00", 120, 21, "KU Leuven CSR Symposium", 57
exec AddWorkshop "2012-4-23", "11:3:00", "14:3:00", 84, 29, "EuroCOW the Calibration and Orientation Workshop", 63
exec AddWorkshop "2011-8-3", "10:29:00", "11:29:00", 72, 24, "European Space Power Conference", 81
exec AddWorkshop "2012-5-8", "12:49:00", "15:49:00", 68, 37, "European Conference on Computer Vision", 67
exec AddWorkshop "2012-3-21", "13:16:00", "16:16:00", 84, 22, "The Corporate Philanthropy Forum", 4
exec AddWorkshop "2012-8-7", "18:20:00", "21:20:00", 123, 42, "Client Summit", 35
exec AddWorkshop "2011-4-7", "12:25:00", "13:25:00", 112, 42, "European Space Power Conference", 16
exec AddWorkshop "2013-12-5", "9:17:00", "12:17:00", 81, 31, "Conference on Computer Vision and Pattern", 16
exec AddWorkshop "2011-11-6", "17:17:00", "20:17:00", 121, 29, "The Corporate Philanthropy Forum", 80
exec AddWorkshop "2011-5-15", "11:28:00", "14:28:00", 70, 39, "Sentinel-2 for Science WS", 58
exec AddWorkshop "2012-2-17", "17:38:00", "20:38:00", 84, 39, "International Conference on Autonomous Agents and", 36
exec AddWorkshop "2013-7-4", "17:3:00", "20:3:00", 114, 47, "Foundations of Genetic Algorithms		", 11
exec AddWorkshop "2013-8-4", "12:22:00", "14:22:00", 53, 20, "International Conference on Automated Planning", 28
exec AddWorkshop "2013-8-30", "10:36:00", "11:36:00", 72, 42, "Global Conference on Sustainability and Reporting", 40
exec AddWorkshop "2011-3-5", "10:3:00", "12:3:00", 131, 32, "X-Ray Universe", 4
exec AddWorkshop "2012-11-21", "13:32:00", "16:32:00", 59, 41, "International Conference on Computer Vision", 3
exec AddWorkshop "2012-10-2", "9:53:00", "12:53:00", 140, 31, "European Conference on Computer Vision", 16
exec AddWorkshop "2013-12-17", "9:30:00", "10:30:00", 82, 31, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 34
exec AddWorkshop "2011-7-3", "16:12:00", "18:12:00", 149, 30, "International Conference on Artificial Neural Networks", 52
exec AddWorkshop "2012-5-30", "15:8:00", "18:8:00", 118, 41, "Workshop on Image Processing", 43
exec AddWorkshop "2013-10-21", "12:6:00", "13:6:00", 80, 30, "European Space Power Conference", 27
exec AddWorkshop "2013-9-8", "12:41:00", "14:41:00", 104, 24, "European Space Technology Harmonisation Conference", 54
exec AddWorkshop "2012-8-25", "13:49:00", "15:49:00", 93, 33, "48th ESLAB Symposium", 28
exec AddWorkshop "2012-11-12", "8:60:00", "11:60:00", 107, 33, "Conference on Uncertainty in Artificial Intelligence", 1
exec AddWorkshop "2013-2-7", "8:4:00", "10:4:00", 113, 37, "Leadership Strategies for Information Technology in Health Care Boston", 82
exec AddWorkshop "2011-6-22", "15:11:00", "17:11:00", 70, 45, "Science and Challenges of Lunar Sample Return Workshop", 35
exec AddWorkshop "2011-8-2", "11:47:00", "14:47:00", 68, 21, "Microwave Workshop", 81
exec AddWorkshop "2011-8-13", "16:27:00", "18:27:00", 115, 24, "The 12th annual Responsible Business Summit", 45
exec AddWorkshop "2013-4-28", "9:26:00", "11:26:00", 146, 40, "GNC 2014", 44
exec AddWorkshop "2013-8-23", "14:50:00", "15:50:00", 85, 20, "RuleML Symposium				", 57
exec AddWorkshop "2011-2-23", "11:58:00", "14:58:00", 137, 36, "X-Ray Universe", 19
exec AddWorkshop "2013-3-11", "8:46:00", "9:46:00", 55, 21, "International Conference on Logic for Programming", 84
exec AddWorkshop "2013-2-16", "19:43:00", "22:43:00", 113, 30, "EuroCOW the Calibration and Orientation Workshop", 13
exec AddWorkshop "2013-9-5", "16:44:00", "18:44:00", 56, 35, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 6
exec AddWorkshop "2011-5-13", "11:49:00", "12:49:00", 56, 42, "Corporate Community Involvement Conference", 8
exec AddWorkshop "2013-4-1", "8:24:00", "11:24:00", 148, 41, "4th DUE Permafrost User Workshop", 76
exec AddWorkshop "2013-1-20", "17:39:00", "19:39:00", 131, 42, "�International� Corporate Citizenship Conference", 66
exec AddWorkshop "2013-5-16", "8:20:00", "9:20:00", 99, 41, "KU Leuven CSR Symposium", 77
exec AddWorkshop "2013-3-16", "17:40:00", "18:40:00", 77, 38, "Conference on Uncertainty in Artificial Intelligence", 50
exec AddWorkshop "2012-12-18", "17:34:00", "19:34:00", 81, 22, "European Conference on Machine Learning", 54
exec AddWorkshop "2012-9-16", "14:7:00", "17:7:00", 146, 21, "Client Summit", 3
exec AddWorkshop "2013-1-5", "18:50:00", "19:50:00", 63, 42, "International Conference on the Principles of", 4
exec AddWorkshop "2011-8-18", "14:45:00", "16:45:00", 55, 23, "Asian Conference on Computer Vision", 76
exec AddWorkshop "2011-11-9", "13:19:00", "14:19:00", 97, 27, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 30
exec AddWorkshop "2013-3-15", "8:25:00", "10:25:00", 114, 22, "Euclid Spacecraft Industry Day", 18
exec AddWorkshop "2012-3-6", "17:2:00", "20:2:00", 52, 42, "X-Ray Universe", 59
exec AddWorkshop "2011-1-26", "19:12:00", "22:12:00", 131, 39, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 0
exec AddWorkshop "2013-1-9", "18:5:00", "21:5:00", 53, 47, "GNC 2014", 28
exec AddWorkshop "2013-8-13", "8:42:00", "10:42:00", 108, 20, "International Conference on Pattern Recognition", 9
exec AddWorkshop "2012-8-6", "19:30:00", "22:30:00", 81, 24, "Leadership Strategies for Information Technology in Health Care Boston", 61
exec AddWorkshop "2011-10-30", "15:12:00", "16:12:00", 121, 22, "The BCLC CSR Conference", 27
exec AddWorkshop "2011-10-21", "15:31:00", "16:31:00", 103, 28, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 9
exec AddWorkshop "2012-7-12", "19:10:00", "22:10:00", 94, 47, "International Conference on Automated Planning", 61
exec AddWorkshop "2011-2-17", "10:35:00", "13:35:00", 128, 26, "48th ESLAB Symposium", 68
exec AddWorkshop "2013-12-14", "19:59:00", "20:59:00", 79, 24, "International Semantic Web Conference		", 8
exec AddWorkshop "2012-8-14", "10:37:00", "13:37:00", 82, 24, "Life in Space for Life on Earth Symposium", 60
exec AddWorkshop "2012-6-30", "8:15:00", "11:15:00", 89, 40, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 6
exec AddWorkshop "2011-6-9", "15:8:00", "18:8:00", 100, 41, "European Conference on Computer Vision", 10
exec AddWorkshop "2011-11-5", "13:21:00", "14:21:00", 65, 43, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 35
exec AddWorkshop "2011-2-10", "15:52:00", "18:52:00", 140, 35, "European Conference on Artificial Intelligence	", 19
exec AddWorkshop "2012-1-9", "8:54:00", "10:54:00", 129, 22, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 70
exec AddWorkshop "2012-3-22", "17:51:00", "18:51:00", 89, 25, "Ceres Conference", 76
exec AddWorkshop "2012-12-19", "14:38:00", "17:38:00", 66, 26, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 45
exec AddWorkshop "2011-3-3", "16:39:00", "19:39:00", 122, 21, "48th ESLAB Symposium", 36
exec AddWorkshop "2012-2-21", "9:25:00", "10:25:00", 61, 47, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 26
exec AddWorkshop "2011-8-21", "18:46:00", "20:46:00", 95, 42, "British	Machine	Vision	Conference		", 77
exec AddWorkshop "2011-12-5", "16:19:00", "19:19:00", 61, 31, "International Joint Conference on Automated Reasoning", 60
exec AddWorkshop "2013-3-14", "15:50:00", "18:50:00", 132, 26, "The Corporate Philanthropy Forum", 13
exec AddWorkshop "2012-12-21", "19:32:00", "22:32:00", 124, 47, "European Conference on Artificial Intelligence	", 14
exec AddWorkshop "2013-6-26", "15:19:00", "17:19:00", 135, 39, "The BCLC CSR Conference", 81
exec AddWorkshop "2011-12-7", "9:18:00", "12:18:00", 93, 22, "4S Symposium 2014", 80
exec AddWorkshop "2013-6-15", "11:47:00", "12:47:00", 98, 31, "Workshop on Logic ", 65
exec AddWorkshop "2012-5-27", "10:52:00", "11:52:00", 110, 46, "International Conference on Pattern Recognition", 20
exec AddWorkshop "2011-8-8", "11:5:00", "13:5:00", 97, 44, "European Navigation Conference 2014 (ENC-GNSS 2014)", 22
exec AddWorkshop "2011-4-7", "9:55:00", "10:55:00", 102, 42, "Sustainable Brands Conference", 14
exec AddWorkshop "2012-11-15", "15:12:00", "18:12:00", 128, 22, "Ceres Conference", 48
exec AddWorkshop "2013-7-2", "14:46:00", "16:46:00", 64, 48, "International Joint Conference on Automated Reasoning", 40
exec AddWorkshop "2012-10-15", "8:49:00", "11:49:00", 68, 24, "Microwave Workshop", 20
exec AddWorkshop "2012-12-5", "12:19:00", "15:19:00", 113, 37, "Mayo Clinic Presents", 74
exec AddWorkshop "2013-2-12", "9:38:00", "11:38:00", 103, 30, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 83
exec AddWorkshop "2012-6-22", "14:21:00", "17:21:00", 136, 28, "International Conference on Signal and Imaging Systems Engineering", 25
exec AddWorkshop "2012-11-14", "10:18:00", "12:18:00", 53, 23, "Life in Space for Life on Earth Symposium", 70
exec AddWorkshop "2012-5-23", "18:45:00", "21:45:00", 100, 24, "Client Summit", 13
exec AddWorkshop "2013-10-22", "16:18:00", "19:18:00", 89, 45, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 75
exec AddWorkshop "2012-9-4", "17:48:00", "19:48:00", 142, 28, "Computer Analysis of Images and Patterns", 76
exec AddWorkshop "2013-11-20", "14:9:00", "16:9:00", 88, 30, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 14
exec AddWorkshop "2013-1-23", "17:32:00", "19:32:00", 63, 28, "International Conference on Automated Planning", 18
exec AddWorkshop "2011-5-16", "14:58:00", "15:58:00", 57, 29, "RuleML Symposium				", 19
exec AddWorkshop "2011-4-29", "16:57:00", "19:57:00", 108, 35, "Business4Better: The Community Partnership Movement", 76
exec AddWorkshop "2011-1-27", "12:25:00", "13:25:00", 122, 22, "International Joint Conference on Artificial Intelligence", 59
exec AddWorkshop "2013-3-16", "10:34:00", "11:34:00", 116, 39, "International Conference on Artificial Neural Networks", 69
exec AddWorkshop "2011-3-16", "15:32:00", "18:32:00", 89, 23, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 56
exec AddWorkshop "2011-9-25", "14:49:00", "16:49:00", 55, 20, "Annual Interdisciplinary Conference", 47
exec AddWorkshop "2013-5-22", "9:36:00", "11:36:00", 121, 25, "GNC 2014", 68
exec AddWorkshop "2011-4-17", "16:20:00", "19:20:00", 125, 24, "European Conference on Artificial Intelligence	", 53
exec AddWorkshop "2013-2-29", "13:12:00", "14:12:00", 110, 29, "Photonics West", 48
exec AddWorkshop "2013-4-22", "11:53:00", "14:53:00", 99, 32, "International Joint Conference on Automated Reasoning", 70
exec AddWorkshop "2011-3-17", "10:56:00", "12:56:00", 71, 22, "Corporate Community Involvement Conference", 65
exec AddWorkshop "2013-5-19", "16:56:00", "17:56:00", 85, 31, "ARTES 1 Final Presentation Days", 47
exec AddWorkshop "2012-9-1", "14:29:00", "17:29:00", 72, 37, "KU Leuven CSR Symposium", 3
exec AddWorkshop "2011-4-21", "17:11:00", "20:11:00", 68, 44, "Corporate Community Involvement Conference", 76
exec AddWorkshop "2011-7-11", "15:39:00", "18:39:00", 80, 41, "European Navigation Conference 2014 (ENC-GNSS 2014)", 67
exec AddWorkshop "2011-6-28", "12:17:00", "15:17:00", 118, 48, "Asian Conference on Computer Vision", 84
exec AddWorkshop "2011-12-15", "17:40:00", "20:40:00", 63, 43, "Foundations of Genetic Algorithms		", 71
exec AddWorkshop "2013-1-7", "14:59:00", "17:59:00", 115, 41, "ARTES 1 Final Presentation Days", 58
exec AddWorkshop "2013-11-15", "13:24:00", "16:24:00", 71, 46, "X-Ray Universe", 23
exec AddWorkshop "2013-4-2", "8:35:00", "10:35:00", 78, 26, "International Conference on Artificial Neural Networks", 40
exec AddWorkshop "2011-6-30", "17:9:00", "19:9:00", 87, 43, "Sustainable Brands Conference", 15
exec AddWorkshop "2011-9-16", "12:42:00", "15:42:00", 105, 34, "European Space Power Conference", 15
exec AddWorkshop "2011-8-21", "17:8:00", "19:8:00", 101, 26, "GNC 2014", 65
exec AddWorkshop "2013-4-25", "16:49:00", "17:49:00", 69, 25, "Cause Marketing Forum", 72
exec AddWorkshop "2011-7-25", "16:47:00", "19:47:00", 69, 24, "International Joint Conference on Artificial Intelligence", 10
exec AddWorkshop "2013-6-27", "11:34:00", "13:34:00", 117, 43, "Computer Analysis of Images and Patterns", 30
exec AddWorkshop "2012-1-9", "18:58:00", "19:58:00", 89, 26, "The BCLC CSR Conference", 54
exec AddWorkshop "2012-2-13", "16:29:00", "18:29:00", 73, 26, "Net Impact Conference", 67
exec AddWorkshop "2012-6-30", "18:28:00", "19:28:00", 115, 26, "Microwave Workshop", 60
exec AddWorkshop "2013-8-16", "11:48:00", "12:48:00", 85, 26, "International Conference on Automated Reasoning", 34
exec AddWorkshop "2012-3-7", "11:43:00", "13:43:00", 61, 45, "International Conference on Computer Vision", 28
exec AddWorkshop "2011-3-4", "10:51:00", "12:51:00", 66, 32, "X-Ray Universe", 70
exec AddWorkshop "2013-12-17", "8:4:00", "11:4:00", 92, 29, "International Conference on Signal and Imaging Systems Engineering", 59
exec AddWorkshop "2012-12-18", "15:1:00", "17:1:00", 129, 43, "ARTES 1 Final Presentation Days", 36
exec AddWorkshop "2013-3-1", "11:54:00", "13:54:00", 92, 20, "ARTES 1 Final Presentation Days", 81
exec AddWorkshop "2011-12-17", "11:10:00", "12:10:00", 138, 35, "International Semantic Web Conference		", 65
exec AddWorkshop "2013-3-10", "13:37:00", "16:37:00", 66, 44, "2013 Ethical Leadership Conference: Ethics in Action", 54
exec AddWorkshop "2012-1-7", "15:58:00", "16:58:00", 147, 31, "Conference on Automated Deduction		", 59
exec AddWorkshop "2012-11-20", "11:5:00", "14:5:00", 86, 47, "European Conference on Computer Vision", 79
exec AddWorkshop "2011-1-24", "18:1:00", "20:1:00", 62, 36, "The BCLC CSR Conference", 7
exec AddWorkshop "2013-8-5", "19:41:00", "20:41:00", 75, 42, "European Conference on Artificial Intelligence	", 80
exec AddWorkshop "2013-6-28", "19:60:00", "20:60:00", 120, 37, "European Space Technology Harmonisation Conference", 53
exec AddWorkshop "2012-9-12", "15:22:00", "17:22:00", 104, 34, "Computer Analysis of Images and Patterns", 37
exec AddWorkshop "2013-6-16", "8:47:00", "11:47:00", 139, 39, "Conference on Uncertainty in Artificial Intelligence", 60
exec AddWorkshop "2011-10-24", "11:60:00", "12:60:00", 68, 28, "International Joint Conference on Automated Reasoning", 28
exec AddWorkshop "2012-6-27", "14:48:00", "17:48:00", 114, 20, "KU Leuven CSR Symposium", 30
exec AddWorkshop "2011-2-5", "13:3:00", "15:3:00", 136, 48, "3rd International Conference on Pattern Recognition Applications and Methods", 75
exec AddWorkshop "2013-9-18", "14:59:00", "17:59:00", 98, 29, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 22
exec AddWorkshop "2011-9-17", "13:53:00", "14:53:00", 129, 24, "4S Symposium 2014", 25
exec AddWorkshop "2012-7-22", "8:44:00", "10:44:00", 112, 44, "Client Summit", 62
exec AddWorkshop "2013-1-22", "12:11:00", "14:11:00", 97, 35, "European Conference on Machine Learning", 27
exec AddWorkshop "2012-10-10", "19:54:00", "20:54:00", 60, 32, "Global Conference on Sustainability and Reporting", 68
exec AddWorkshop "2012-8-13", "13:22:00", "15:22:00", 138, 42, "International Conference on Computer Vision", 35
exec AddWorkshop "2012-8-20", "9:56:00", "12:56:00", 117, 46, "3rd International Conference on Pattern Recognition Applications and Methods", 16
exec AddWorkshop "2013-1-12", "11:23:00", "13:23:00", 77, 40, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 60
exec AddWorkshop "2012-2-20", "11:16:00", "14:16:00", 103, 40, "4th DUE Permafrost User Workshop", 32
exec AddWorkshop "2011-3-26", "18:29:00", "21:29:00", 131, 46, "The Corporate Philanthropy Forum", 15
exec AddWorkshop "2013-4-15", "11:46:00", "13:46:00", 101, 34, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 6
exec AddWorkshop "2013-11-21", "8:18:00", "9:18:00", 134, 49, "Conference on Uncertainty in Artificial Intelligence", 25
exec AddWorkshop "2012-8-24", "17:47:00", "19:47:00", 108, 28, "LPVE Land product validation and evolution", 55
exec AddWorkshop "2011-5-14", "13:60:00", "16:60:00", 78, 48, "Business4Better: The Community Partnership Movement", 61
exec AddWorkshop "2013-4-6", "16:38:00", "17:38:00", 102, 38, "International Semantic Web Conference		", 83
exec AddWorkshop "2011-6-25", "9:49:00", "11:49:00", 138, 26, "Microwave Workshop", 57
exec AddWorkshop "2011-11-12", "9:56:00", "12:56:00", 55, 41, "International Semantic Web Conference		", 27
exec AddWorkshop "2011-3-2", "14:5:00", "17:5:00", 128, 45, "Computer Analysis of Images and Patterns", 39
exec AddWorkshop "2013-4-24", "17:15:00", "19:15:00", 60, 22, "International Conference on MultiMedia Modeling", 78
exec AddWorkshop "2012-5-6", "15:15:00", "17:15:00", 118, 25, "International Joint Conference on Artificial Intelligence", 0
exec AddWorkshop "2013-6-25", "11:55:00", "14:55:00", 130, 48, "International Semantic Web Conference		", 12
exec AddWorkshop "2011-7-12", "11:34:00", "14:34:00", 128, 25, "The 12th annual Responsible Business Summit", 63
exec AddWorkshop "2012-3-19", "8:13:00", "11:13:00", 146, 49, "RuleML Symposium				", 82
exec AddWorkshop "2013-8-10", "17:58:00", "20:58:00", 71, 29, "International Conference on Artificial Neural Networks", 52
exec AddWorkshop "2011-11-2", "19:47:00", "20:47:00", 141, 40, "European Conference on Computer Vision", 63
exec AddWorkshop "2012-10-23", "19:35:00", "22:35:00", 51, 26, "Client Summit", 72
exec AddWorkshop "2012-6-25", "15:48:00", "18:48:00", 91, 23, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 77
exec AddWorkshop "2013-7-3", "9:60:00", "11:60:00", 60, 49, "Global Conference on Sustainability and Reporting", 79
exec AddWorkshop "2012-1-5", "10:4:00", "11:4:00", 119, 42, "Net Impact Conference", 38
exec AddWorkshop "2011-6-24", "17:35:00", "18:35:00", 73, 29, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 45
exec AddWorkshop "2012-5-21", "15:24:00", "17:24:00", 66, 41, "Corporate Community Involvement Conference", 60
exec AddWorkshop "2013-5-6", "8:60:00", "9:60:00", 53, 48, "International Conference on Automated Reasoning", 42
exec AddWorkshop "2012-2-24", "8:32:00", "11:32:00", 92, 48, "International Conference on Signal and Imaging Systems Engineering", 1
exec AddWorkshop "2012-5-9", "14:55:00", "15:55:00", 90, 27, "The Corporate Philanthropy Forum", 75
exec AddWorkshop "2013-10-4", "14:19:00", "15:19:00", 134, 23, "2nd International Conference on Photonics, Optics and Laser Technology", 44
exec AddWorkshop "2013-10-14", "13:22:00", "14:22:00", 144, 25, "International Conference on Artificial Neural Networks", 70
exec AddWorkshop "2013-5-3", "12:10:00", "13:10:00", 141, 26, "Client Summit", 74
exec AddWorkshop "2012-9-16", "18:11:00", "21:11:00", 130, 46, "Leadership Strategies for Information Technology in Health Care Boston", 17
exec AddWorkshop "2011-12-4", "13:44:00", "14:44:00", 66, 36, "European Space Power Conference", 49
exec AddWorkshop "2013-6-6", "10:47:00", "12:47:00", 147, 38, "Client Summit", 22
exec AddWorkshop "2011-8-30", "9:58:00", "12:58:00", 72, 34, "Asian Conference on Computer Vision", 57
exec AddWorkshop "2012-2-28", "12:43:00", "13:43:00", 86, 24, "Ceres Conference", 70
exec AddWorkshop "2013-6-28", "15:40:00", "18:40:00", 50, 31, "International Conference on Signal and Imaging Systems Engineering", 44
exec AddWorkshop "2012-12-11", "15:38:00", "18:38:00", 50, 34, "International Conference on Automated Planning", 16
exec AddWorkshop "2011-8-15", "17:31:00", "19:31:00", 129, 46, "Cause Marketing Forum", 76
exec AddWorkshop "2012-3-5", "10:51:00", "11:51:00", 117, 22, "Corporate Community Involvement Conference", 68
exec AddWorkshop "2011-10-10", "12:59:00", "14:59:00", 85, 48, "Global Conference on Sustainability and Reporting", 16
exec AddWorkshop "2013-4-3", "9:20:00", "12:20:00", 95, 33, "2nd International Conference on Photonics, Optics and Laser Technology", 25
exec AddWorkshop "2011-8-20", "9:19:00", "10:19:00", 114, 26, "Mechanisms Final Presentation Days", 29
exec AddWorkshop "2013-5-21", "18:19:00", "19:19:00", 56, 26, "European Conference on Computer Vision", 52
exec AddWorkshop "2013-2-14", "19:16:00", "22:16:00", 93, 47, "Sentinel-2 for Science WS", 72
exec AddWorkshop "2011-5-10", "12:57:00", "15:57:00", 89, 24, "Client Summit", 57
exec AddWorkshop "2013-6-29", "10:51:00", "12:51:00", 75, 34, "International Semantic Web Conference		", 46
exec AddWorkshop "2012-2-24", "19:40:00", "22:40:00", 113, 29, "International Joint Conference on Automated Reasoning", 65
exec AddWorkshop "2011-2-23", "8:46:00", "9:46:00", 142, 42, "The Sixth International Conferences on Advances in Multimedia", 27
exec AddWorkshop "2013-12-6", "17:8:00", "19:8:00", 148, 29, "Business4Better: The Community Partnership Movement", 33
exec AddWorkshop "2011-6-11", "19:28:00", "21:28:00", 111, 30, "Conference on Learning Theory		", 14
exec AddWorkshop "2011-11-30", "16:17:00", "19:17:00", 60, 33, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 70
exec AddWorkshop "2013-7-23", "15:27:00", "17:27:00", 104, 24, "ICATT", 51
exec AddWorkshop "2012-7-16", "13:35:00", "14:35:00", 129, 29, "The BCLC CSR Conference", 44
exec AddWorkshop "2012-1-22", "12:47:00", "14:47:00", 106, 44, "International Semantic Web Conference		", 27
exec AddWorkshop "2012-2-2", "16:3:00", "17:3:00", 54, 25, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 34
exec AddWorkshop "2012-5-19", "11:25:00", "13:25:00", 76, 27, "International Conference on MultiMedia Modeling", 72
exec AddWorkshop "2011-11-19", "12:4:00", "15:4:00", 131, 43, "European Navigation Conference 2014 (ENC-GNSS 2014)", 9
exec AddWorkshop "2013-11-30", "12:14:00", "15:14:00", 76, 47, "ARTES 1 Final Presentation Days", 65
exec AddWorkshop "2012-9-5", "8:25:00", "10:25:00", 68, 40, "Science and Challenges of Lunar Sample Return Workshop", 48
exec AddWorkshop "2012-7-20", "15:1:00", "16:1:00", 66, 39, "International Conference on Logic for Programming", 35
exec AddWorkshop "2011-3-1", "15:22:00", "16:22:00", 90, 43, "International Conference on Automated Planning", 45
exec AddWorkshop "2011-1-28", "13:57:00", "14:57:00", 128, 20, "The 12th annual Responsible Business Summit", 15
exec AddWorkshop "2011-5-28", "9:41:00", "12:41:00", 90, 22, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 63
exec AddWorkshop "2013-10-1", "17:18:00", "18:18:00", 102, 33, "European Space Technology Harmonisation Conference", 17
exec AddWorkshop "2011-10-9", "18:50:00", "21:50:00", 104, 38, "Sustainable Brands Conference", 28
exec AddWorkshop "2011-4-4", "11:38:00", "13:38:00", 83, 23, "The Corporate Philanthropy Forum", 58
exec AddWorkshop "2012-4-22", "12:9:00", "15:9:00", 109, 36, "The Corporate Philanthropy Forum", 38
exec AddWorkshop "2012-10-24", "18:40:00", "20:40:00", 110, 42, "Net Impact Conference", 43
exec AddWorkshop "2013-4-29", "11:23:00", "12:23:00", 147, 43, "7 th International Conference on Bio-inspired Systems and Signal Processing", 83
exec AddWorkshop "2013-8-8", "12:5:00", "14:5:00", 105, 36, "European Conference on Computer Vision", 82
exec AddWorkshop "2011-8-22", "11:15:00", "13:15:00", 128, 33, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 49
exec AddWorkshop "2012-4-9", "11:26:00", "13:26:00", 82, 40, "X-Ray Universe", 68
exec AddWorkshop "2012-6-9", "17:30:00", "19:30:00", 56, 31, "Leadership Strategies for Information Technology in Health Care Boston", 7
exec AddWorkshop "2013-6-6", "19:53:00", "20:53:00", 115, 40, "The Sixth International Conferences on Advances in Multimedia", 23
exec AddWorkshop "2013-2-16", "15:33:00", "18:33:00", 148, 31, "X-Ray Universe", 44
exec AddWorkshop "2013-9-14", "12:6:00", "14:6:00", 141, 45, "Corporate Community Involvement Conference", 84
exec AddWorkshop "2011-2-29", "10:55:00", "12:55:00", 112, 28, "LPVE Land product validation and evolution", 2
exec AddWorkshop "2012-11-23", "12:45:00", "13:45:00", 76, 28, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 27
exec AddWorkshop "2011-8-7", "12:58:00", "13:58:00", 112, 34, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 68
exec AddWorkshop "2011-2-30", "19:13:00", "20:13:00", 109, 27, "International Conference on Pattern Recognition", 42
exec AddWorkshop "2011-11-3", "14:22:00", "16:22:00", 138, 39, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 4
exec AddWorkshop "2012-3-7", "10:12:00", "13:12:00", 135, 45, "X-Ray Universe", 58
exec AddWorkshop "2011-11-5", "14:7:00", "17:7:00", 105, 44, "International Conference on Autonomous Agents and", 13
exec AddWorkshop "2012-9-25", "13:45:00", "16:45:00", 95, 49, "3rd International Conference on Pattern Recognition Applications and Methods", 33
exec AddWorkshop "2013-11-11", "9:18:00", "10:18:00", 78, 20, "International Joint Conference on Artificial Intelligence", 61
exec AddWorkshop "2012-6-9", "17:9:00", "19:9:00", 94, 48, "7 th International Conference on Bio-inspired Systems and Signal Processing", 10
exec AddWorkshop "2012-1-2", "16:43:00", "19:43:00", 63, 44, "ICATT", 37
exec AddWorkshop "2013-11-10", "14:45:00", "15:45:00", 148, 22, "2013 Ethical Leadership Conference: Ethics in Action", 80
exec AddWorkshop "2011-12-10", "9:45:00", "10:45:00", 88, 37, "X-Ray Universe", 82
exec AddWorkshop "2012-6-16", "13:23:00", "15:23:00", 147, 20, "International Conference on Logic for Programming", 25
exec AddWorkshop "2013-4-1", "8:8:00", "11:8:00", 58, 49, "Life in Space for Life on Earth Symposium", 49
exec AddWorkshop "2011-12-30", "16:16:00", "17:16:00", 109, 23, "3rd International Conference on Pattern Recognition Applications and Methods", 47
exec AddWorkshop "2011-2-21", "18:14:00", "21:14:00", 137, 49, "International Conference on Computer Vision Theory and Applications", 75
exec AddWorkshop "2012-11-10", "9:26:00", "10:26:00", 146, 20, "International Conference on Computer Vision", 81
exec AddWorkshop "2012-1-25", "9:18:00", "11:18:00", 102, 48, "Euclid Spacecraft Industry Day", 41
exec AddWorkshop "2012-1-29", "8:27:00", "9:27:00", 124, 33, "Workshop on Image Processing", 13
exec AddWorkshop "2013-9-19", "8:46:00", "9:46:00", 129, 44, "Mayo Clinic Presents", 51
exec AddWorkshop "2011-5-19", "8:33:00", "10:33:00", 92, 28, "Mechanisms Final Presentation Days", 52
exec AddWorkshop "2013-10-28", "17:55:00", "18:55:00", 64, 34, "Leadership Strategies for Information Technology in Health Care Boston", 63
exec AddWorkshop "2013-7-25", "8:22:00", "10:22:00", 81, 30, "Photonics West", 8
exec AddWorkshop "2011-4-24", "12:60:00", "15:60:00", 52, 49, "Annual Convention of The Society", 4
exec AddWorkshop "2011-6-15", "15:14:00", "18:14:00", 90, 31, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 2
exec AddWorkshop "2012-1-14", "10:54:00", "13:54:00", 119, 34, "International Conference on Computer Vision", 79
exec AddWorkshop "2012-8-7", "18:55:00", "21:55:00", 86, 21, "Sustainable Brands Conference", 73
exec AddWorkshop "2013-11-11", "15:5:00", "16:5:00", 66, 26, "International Conference on Autonomous Agents and", 12
exec AddWorkshop "2012-10-13", "9:17:00", "12:17:00", 75, 22, "EuroCOW the Calibration and Orientation Workshop", 22
exec AddWorkshop "2013-2-22", "9:20:00", "10:20:00", 132, 30, "Workshop on Image Processing", 22
exec AddWorkshop "2013-1-8", "16:15:00", "19:15:00", 127, 38, "Euclid Spacecraft Industry Day", 28
exec AddWorkshop "2013-5-18", "14:45:00", "17:45:00", 111, 40, "Conference on Computer Vision and Pattern", 36
exec AddWorkshop "2012-1-20", "10:29:00", "11:29:00", 114, 29, "The 2nd International CSR Communication Conference", 50
exec AddWorkshop "2012-1-8", "14:43:00", "17:43:00", 62, 22, "The 12th annual Responsible Business Summit", 14
exec AddWorkshop "2011-3-17", "18:39:00", "20:39:00", 139, 47, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 79
exec AddWorkshop "2013-10-6", "12:32:00", "14:32:00", 79, 39, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 14
exec AddWorkshop "2013-5-24", "12:45:00", "15:45:00", 129, 39, "The BCLC CSR Conference", 75
exec AddWorkshop "2011-6-14", "10:30:00", "12:30:00", 74, 34, "Conference on Uncertainty in Artificial Intelligence", 75
exec AddWorkshop "2011-1-24", "11:56:00", "13:56:00", 88, 33, "Mayo Clinic Presents", 61
exec AddWorkshop "2011-10-15", "19:53:00", "20:53:00", 59, 45, "The BCLC CSR Conference", 70
exec AddWorkshop "2011-1-2", "10:8:00", "12:8:00", 61, 34, "Client Summit", 35
exec AddWorkshop "2013-11-5", "14:7:00", "17:7:00", 68, 43, "Photonics West", 33
exec AddWorkshop "2011-1-17", "12:36:00", "15:36:00", 103, 37, "British	Machine	Vision	Conference		", 76
exec AddWorkshop "2013-9-23", "16:55:00", "18:55:00", 90, 47, "2013 Ethical Leadership Conference: Ethics in Action", 48
exec AddWorkshop "2013-2-18", "16:2:00", "18:2:00", 80, 38, "European Space Power Conference", 8
exec AddWorkshop "2013-6-20", "17:34:00", "18:34:00", 145, 28, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 34
exec AddWorkshop "2013-7-20", "15:7:00", "18:7:00", 82, 41, "GNC 2014", 25
exec AddWorkshop "2012-9-26", "10:6:00", "11:6:00", 65, 25, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 51
exec AddWorkshop "2012-6-2", "16:1:00", "19:1:00", 137, 26, "European Conference on Machine Learning", 58
exec AddWorkshop "2013-8-13", "18:9:00", "21:9:00", 63, 24, "International Conference on Automated Reasoning", 10
exec AddWorkshop "2013-5-16", "10:14:00", "12:14:00", 113, 37, "Mechanisms Final Presentation Days", 24
exec AddWorkshop "2011-11-21", "19:59:00", "21:59:00", 51, 23, "International Conference on Signal and Imaging Systems Engineering", 43
exec AddWorkshop "2011-8-23", "12:17:00", "13:17:00", 71, 26, "ICATT", 77
exec AddWorkshop "2012-3-16", "15:42:00", "16:42:00", 64, 40, "2nd International Conference on Photonics, Optics and Laser Technology", 8
exec AddWorkshop "2013-1-7", "10:11:00", "11:11:00", 105, 40, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 8
exec AddWorkshop "2011-11-7", "15:45:00", "18:45:00", 78, 30, "Conference on Uncertainty in Artificial Intelligence", 47
exec AddWorkshop "2013-5-7", "17:29:00", "20:29:00", 91, 23, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 41
exec AddWorkshop "2012-4-29", "15:3:00", "16:3:00", 118, 32, "International Conference on Autonomous Agents and", 43
exec AddWorkshop "2012-10-8", "8:55:00", "9:55:00", 108, 34, "EuroCOW the Calibration and Orientation Workshop", 11
exec AddWorkshop "2012-3-18", "11:37:00", "13:37:00", 148, 28, "International Conference on Logic for Programming", 71
exec AddWorkshop "2013-7-25", "18:15:00", "21:15:00", 61, 40, "�International� Corporate Citizenship Conference", 58
exec AddWorkshop "2013-6-2", "8:26:00", "9:26:00", 112, 31, "Conference on Uncertainty in Artificial Intelligence", 17
exec AddWorkshop "2013-3-15", "19:38:00", "20:38:00", 95, 44, "The Corporate Philanthropy Forum", 11
exec AddWorkshop "2011-7-28", "15:7:00", "16:7:00", 90, 48, "The BCLC CSR Conference", 80
exec AddWorkshop "2011-9-12", "15:51:00", "18:51:00", 55, 35, "Foundations of Genetic Algorithms		", 60
exec AddWorkshop "2011-2-1", "18:26:00", "20:26:00", 52, 31, "Computer Analysis of Images and Patterns", 36
exec AddWorkshop "2012-6-2", "11:55:00", "14:55:00", 102, 20, "Conference on Computer Vision and Pattern", 74
exec AddWorkshop "2012-8-17", "16:26:00", "17:26:00", 93, 37, "Business4Better: The Community Partnership Movement", 3
exec AddWorkshop "2012-4-28", "18:9:00", "21:9:00", 90, 31, "The 12th annual Responsible Business Summit", 6
exec AddWorkshop "2012-9-30", "11:44:00", "13:44:00", 68, 37, "Corporate Community Involvement Conference", 25
exec AddWorkshop "2011-6-22", "8:50:00", "11:50:00", 120, 44, "International Conference on Pattern Recognition", 66
exec AddWorkshop "2013-5-3", "11:24:00", "14:24:00", 76, 29, "The BCLC CSR Conference", 29
exec AddWorkshop "2013-8-4", "14:59:00", "15:59:00", 107, 28, "RuleML Symposium		", 40
exec AddWorkshop "2012-9-1", "17:51:00", "20:51:00", 122, 27, "International Conference on Signal and Imaging Systems Engineering", 68
exec AddWorkshop "2011-5-14", "17:20:00", "18:20:00", 108, 47, "GNC 2014", 72
exec AddWorkshop "2012-12-17", "8:44:00", "9:44:00", 88, 46, "International Conference on Signal and Imaging Systems Engineering", 55
exec AddWorkshop "2011-4-3", "14:28:00", "15:28:00", 135, 33, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 37
exec AddWorkshop "2013-3-19", "11:29:00", "14:29:00", 127, 43, "International Conference on Automated Reasoning", 20
exec AddWorkshop "2013-3-3", "10:7:00", "12:7:00", 117, 33, "Foundations of Genetic Algorithms		", 65
exec AddWorkshop "2011-11-28", "8:27:00", "9:27:00", 104, 30, "Conference on Uncertainty in Artificial Intelligence", 10
exec AddWorkshop "2013-2-15", "10:11:00", "12:11:00", 91, 24, "Photonics West", 50
exec AddWorkshop "2012-6-6", "8:39:00", "10:39:00", 51, 23, "Conference on Uncertainty in Artificial Intelligence", 49
exec AddWorkshop "2011-6-20", "15:11:00", "17:11:00", 67, 40, "International Conference on Automated Reasoning", 13
exec AddWorkshop "2013-5-3", "11:31:00", "13:31:00", 149, 30, "International Conference on Computer Vision", 62
exec AddWorkshop "2013-8-18", "15:16:00", "18:16:00", 109, 37, "Global Conference on Sustainability and Reporting", 65
exec AddWorkshop "2011-11-21", "18:29:00", "21:29:00", 67, 38, "International Conference on Computer Vision", 59
exec AddWorkshop "2011-4-16", "14:50:00", "17:50:00", 116, 43, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 48
exec AddWorkshop "2012-5-8", "11:23:00", "14:23:00", 115, 20, "Workshop on Image Processing", 34
exec AddWorkshop "2012-6-17", "11:18:00", "13:18:00", 57, 23, "Conference on Computer Vision and Pattern", 45
exec AddWorkshop "2011-12-25", "8:31:00", "9:31:00", 134, 38, "International Conference on Artificial Neural Networks", 14
exec AddWorkshop "2013-8-26", "17:4:00", "18:4:00", 144, 25, "Photonics West", 54
exec AddWorkshop "2013-6-27", "11:40:00", "14:40:00", 78, 30, "European Conference on Machine Learning", 24
exec AddWorkshop "2013-1-26", "18:28:00", "21:28:00", 61, 27, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 59
exec AddWorkshop "2011-5-14", "13:9:00", "15:9:00", 117, 49, "Photonics West", 37
exec AddWorkshop "2011-1-10", "18:8:00", "19:8:00", 69, 25, "The BCLC CSR Conference", 31
exec AddWorkshop "2011-1-7", "17:46:00", "20:46:00", 110, 32, "International Conference on Signal and Imaging Systems Engineering", 78
exec AddWorkshop "2011-5-21", "19:44:00", "20:44:00", 50, 30, "X-Ray Universe", 66
exec AddWorkshop "2013-10-4", "15:4:00", "17:4:00", 55, 44, "International Joint Conference on Automated Reasoning", 33
exec AddWorkshop "2011-2-29", "16:57:00", "17:57:00", 60, 42, "EuroCOW the Calibration and Orientation Workshop", 67
exec AddWorkshop "2013-9-4", "9:49:00", "11:49:00", 51, 44, "European Space Power Conference", 60
exec AddWorkshop "2012-8-16", "15:35:00", "18:35:00", 132, 33, "International Conference on Automated Reasoning", 29
exec AddWorkshop "2011-10-27", "9:15:00", "10:15:00", 110, 29, "Conference on Computer Vision and Pattern", 78
exec AddWorkshop "2013-8-21", "9:42:00", "10:42:00", 146, 48, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 42
exec AddWorkshop "2011-6-7", "11:44:00", "12:44:00", 114, 24, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 46
exec AddWorkshop "2011-1-20", "16:39:00", "18:39:00", 106, 35, "The 12th annual Responsible Business Summit", 72
exec AddWorkshop "2013-9-2", "9:19:00", "10:19:00", 123, 20, "4S Symposium 2014", 75
exec AddWorkshop "2011-11-10", "10:55:00", "12:55:00", 51, 40, "Conference on Uncertainty in Artificial Intelligence", 62
exec AddWorkshop "2013-11-5", "17:6:00", "20:6:00", 82, 28, "Foundations of Genetic Algorithms		", 16
exec AddWorkshop "2013-2-20", "16:51:00", "17:51:00", 88, 32, "International Conference on Pattern Recognition", 71
exec AddWorkshop "2013-12-28", "14:24:00", "16:24:00", 54, 40, "Asian Conference on Computer Vision", 7
exec AddWorkshop "2013-1-24", "17:17:00", "19:17:00", 67, 36, "7 th International Conference on Bio-inspired Systems and Signal Processing", 29
exec AddWorkshop "2011-1-25", "18:7:00", "19:7:00", 72, 23, "Conference on Computer Vision and Pattern", 15
exec AddWorkshop "2013-11-18", "12:3:00", "15:3:00", 51, 37, "GNC 2014", 15
exec AddWorkshop "2013-4-6", "15:53:00", "16:53:00", 100, 48, "The 12th annual Responsible Business Summit", 6
exec AddWorkshop "2013-8-3", "11:46:00", "14:46:00", 136, 26, "British	Machine	Vision	Conference		", 12
exec AddWorkshop "2011-2-4", "12:56:00", "14:56:00", 88, 39, "The 12th annual Responsible Business Summit", 16
exec AddWorkshop "2012-5-29", "18:57:00", "19:57:00", 81, 39, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 56
exec AddWorkshop "2011-11-27", "11:12:00", "14:12:00", 55, 35, "Global Conference on Sustainability and Reporting", 36
exec AddWorkshop "2013-5-15", "14:43:00", "17:43:00", 93, 29, "ARTES 1 Final Presentation Days", 70
exec AddWorkshop "2012-6-9", "16:17:00", "19:17:00", 145, 28, "RuleML Symposium				", 60
exec AddWorkshop "2012-11-15", "11:29:00", "14:29:00", 130, 30, "Corporate Community Involvement Conference", 49
exec AddWorkshop "2013-6-5", "11:24:00", "13:24:00", 94, 28, "RuleML Symposium				", 48
exec AddWorkshop "2011-4-9", "13:60:00", "14:60:00", 70, 38, "International Conference on Artificial Neural Networks", 57
exec AddWorkshop "2012-5-8", "17:40:00", "19:40:00", 116, 41, "Conference on Uncertainty in Artificial Intelligence", 73
exec AddWorkshop "2013-8-23", "17:38:00", "20:38:00", 143, 41, "Microwave Workshop", 15
exec AddWorkshop "2011-11-22", "15:41:00", "18:41:00", 50, 39, "EuroCOW the Calibration and Orientation Workshop", 28
exec AddWorkshop "2012-1-19", "11:24:00", "12:24:00", 94, 44, "European Space Power Conference", 26
exec AddWorkshop "2012-2-21", "8:20:00", "9:20:00", 142, 49, "International Conference on Signal and Imaging Systems Engineering", 36
exec AddWorkshop "2013-7-3", "10:55:00", "12:55:00", 99, 35, "Sustainable Brands Conference", 14
exec AddWorkshop "2012-9-8", "17:7:00", "20:7:00", 94, 30, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 12
exec AddWorkshop "2012-11-13", "12:38:00", "15:38:00", 84, 40, "Conference on Automated Deduction		", 31
exec AddWorkshop "2013-10-30", "19:36:00", "21:36:00", 74, 30, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 84
exec AddWorkshop "2013-2-12", "15:39:00", "17:39:00", 121, 27, "International Conference on Autonomous Agents and", 45
exec AddWorkshop "2012-11-17", "16:23:00", "19:23:00", 136, 37, "International Conference on Artificial Neural Networks", 47
exec AddWorkshop "2013-11-16", "12:36:00", "15:36:00", 106, 32, "Conference on Learning Theory		", 47
exec AddWorkshop "2011-3-12", "9:48:00", "10:48:00", 76, 32, "European Conference on Computer Vision", 38
exec AddWorkshop "2012-8-23", "19:13:00", "21:13:00", 124, 26, "Client Summit", 29
exec AddWorkshop "2013-1-29", "18:8:00", "19:8:00", 94, 34, "GNC 2014", 15
exec AddWorkshop "2013-5-7", "10:28:00", "13:28:00", 69, 23, "The 2nd International CSR Communication Conference", 58
exec AddWorkshop "2013-6-13", "19:12:00", "20:12:00", 57, 28, "Mayo Clinic Presents", 37
exec AddWorkshop "2013-2-14", "13:29:00", "14:29:00", 135, 42, "Conference on Uncertainty in Artificial Intelligence", 47
exec AddWorkshop "2011-8-20", "19:55:00", "20:55:00", 93, 32, "RuleML Symposium				", 56
exec AddWorkshop "2013-7-13", "10:6:00", "13:6:00", 99, 41, "�International� Corporate Citizenship Conference", 12
exec AddWorkshop "2012-9-16", "14:28:00", "15:28:00", 132, 46, "Euclid Spacecraft Industry Day", 20
exec AddWorkshop "2012-2-30", "17:5:00", "19:5:00", 109, 35, "International Conference on Automated Planning", 55
exec AddWorkshop "2011-4-15", "16:20:00", "17:20:00", 89, 21, "4th DUE Permafrost User Workshop", 45
exec AddWorkshop "2011-8-9", "19:24:00", "21:24:00", 87, 29, "Annual Interdisciplinary Conference", 27
exec AddWorkshop "2012-6-27", "11:33:00", "13:33:00", 146, 46, "British	Machine	Vision	Conference		", 48
exec AddWorkshop "2011-3-5", "8:53:00", "11:53:00", 75, 23, "3rd International Conference on Pattern Recognition Applications and Methods", 66
exec AddWorkshop "2013-1-3", "13:27:00", "15:27:00", 124, 37, "4th DUE Permafrost User Workshop", 82
exec AddWorkshop "2011-11-10", "10:26:00", "12:26:00", 133, 46, "ICATT", 78
exec AddWorkshop "2013-6-27", "14:39:00", "15:39:00", 127, 49, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 34
exec AddWorkshop "2012-10-13", "15:39:00", "18:39:00", 128, 32, "48th ESLAB Symposium", 67
exec AddWorkshop "2012-11-15", "15:41:00", "18:41:00", 74, 45, "Workshop on Logic ", 48
exec AddWorkshop "2013-1-25", "9:30:00", "12:30:00", 51, 20, "Conference on Computer Vision and Pattern", 64
exec AddWorkshop "2012-11-27", "8:28:00", "9:28:00", 103, 37, "European Conference on Machine Learning", 57
exec AddWorkshop "2012-5-29", "16:35:00", "18:35:00", 92, 32, "The BCLC CSR Conference", 23
exec AddWorkshop "2013-9-9", "18:60:00", "19:60:00", 139, 31, "Conference on Computer Vision and Pattern", 82
exec AddWorkshop "2011-6-18", "12:18:00", "13:18:00", 61, 40, "The 12th annual Responsible Business Summit", 38
exec AddWorkshop "2011-6-27", "9:35:00", "10:35:00", 142, 33, "Microwave Workshop", 26
exec AddWorkshop "2011-10-6", "17:24:00", "20:24:00", 50, 27, "Sentinel-2 for Science WS", 84
exec AddWorkshop "2011-11-6", "8:42:00", "9:42:00", 73, 31, "International Joint Conference on Artificial Intelligence", 42
exec AddWorkshop "2013-9-27", "12:1:00", "13:1:00", 98, 49, "ARTES 1 Final Presentation Days", 9
exec AddWorkshop "2013-1-18", "10:39:00", "13:39:00", 132, 43, "European Navigation Conference 2014 (ENC-GNSS 2014)", 80
exec AddWorkshop "2013-8-15", "19:42:00", "21:42:00", 104, 27, "LPVE Land product validation and evolution", 16
exec AddWorkshop "2012-8-5", "8:38:00", "9:38:00", 98, 28, "Photonics West", 19
exec AddWorkshop "2013-8-16", "15:17:00", "16:17:00", 121, 43, "International Conference on Pattern Recognition", 34
exec AddWorkshop "2012-3-26", "16:44:00", "19:44:00", 114, 44, "International Conference on MultiMedia Modeling", 29
exec AddWorkshop "2012-11-17", "12:58:00", "13:58:00", 104, 35, "Conference on Computer Vision and Pattern", 43
exec AddWorkshop "2011-12-17", "13:41:00", "14:41:00", 132, 42, "GNC 2014", 36
exec AddWorkshop "2011-9-15", "9:19:00", "10:19:00", 81, 27, "International Conference on Autonomous Agents and", 52
exec AddWorkshop "2011-3-18", "11:10:00", "13:10:00", 111, 31, "International Conference on Autonomous Agents and", 34
exec AddWorkshop "2011-3-3", "10:41:00", "11:41:00", 53, 29, "Computer Analysis of Images and Patterns", 10
exec AddWorkshop "2012-3-1", "13:43:00", "15:43:00", 125, 39, "The 2nd International CSR Communication Conference", 38
exec AddWorkshop "2013-11-17", "11:49:00", "13:49:00", 97, 48, "International Joint Conference on Automated Reasoning", 47
exec AddWorkshop "2013-1-19", "16:46:00", "19:46:00", 132, 29, "Conference on Learning Theory		", 40
exec AddWorkshop "2013-1-3", "17:33:00", "18:33:00", 106, 35, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 1
exec AddWorkshop "2013-4-28", "10:47:00", "13:47:00", 115, 28, "Conference on Uncertainty in Artificial Intelligence", 14
exec AddWorkshop "2011-1-17", "14:24:00", "17:24:00", 100, 20, "Business4Better: The Community Partnership Movement", 4
exec AddWorkshop "2013-1-12", "15:38:00", "18:38:00", 53, 40, "Client Summit", 24
exec AddWorkshop "2011-2-14", "13:22:00", "16:22:00", 63, 26, "EuroCOW the Calibration and Orientation Workshop", 48
exec AddWorkshop "2013-3-6", "16:27:00", "18:27:00", 148, 37, "International Conference on Signal and Imaging Systems Engineering", 62
exec AddWorkshop "2011-6-26", "12:32:00", "14:32:00", 82, 32, "3rd International Conference on Pattern Recognition Applications and Methods", 59
exec AddWorkshop "2012-6-4", "10:9:00", "11:9:00", 105, 39, "�International� Corporate Citizenship Conference", 11
exec AddWorkshop "2012-6-12", "10:11:00", "11:11:00", 58, 29, "International Conference on Signal and Imaging Systems Engineering", 50
exec AddWorkshop "2012-12-8", "8:18:00", "10:18:00", 135, 33, "2013 Ethical Leadership Conference: Ethics in Action", 25
exec AddWorkshop "2011-12-15", "8:11:00", "11:11:00", 62, 22, "Workshop on Logic ", 58
exec AddWorkshop "2012-8-3", "12:20:00", "15:20:00", 98, 28, "International Semantic Web Conference		", 49
exec AddWorkshop "2013-6-29", "18:2:00", "19:2:00", 143, 43, "EuroCOW the Calibration and Orientation Workshop", 74
exec AddWorkshop "2011-2-28", "19:12:00", "22:12:00", 57, 47, "Conference on Computer Vision and Pattern", 59
exec AddWorkshop "2013-5-4", "9:38:00", "10:38:00", 114, 39, "International Joint Conference on Automated Reasoning", 46
exec AddWorkshop "2012-6-30", "19:11:00", "20:11:00", 118, 27, "International Conference on Automated Planning", 13
exec AddWorkshop "2012-7-18", "11:52:00", "12:52:00", 140, 32, "The BCLC CSR Conference", 49
exec AddWorkshop "2013-9-4", "17:1:00", "20:1:00", 117, 34, "ARTES 1 Final Presentation Days", 45
exec AddWorkshop "2011-9-15", "14:50:00", "17:50:00", 142, 25, "EuroCOW the Calibration and Orientation Workshop", 60
exec AddWorkshop "2012-4-14", "15:30:00", "18:30:00", 115, 24, "Workshop on Image Processing", 73
exec AddWorkshop "2011-12-25", "18:26:00", "21:26:00", 114, 49, "Mechanisms Final Presentation Days", 14
exec AddWorkshop "2012-11-24", "18:22:00", "19:22:00", 69, 36, "Conference on Learning Theory		", 63
exec AddWorkshop "2011-1-7", "17:4:00", "19:4:00", 129, 37, "3rd International Conference on Pattern Recognition Applications and Methods", 61
exec AddWorkshop "2011-11-5", "12:12:00", "15:12:00", 65, 24, "Workshop on Logic ", 63
exec AddWorkshop "2013-12-6", "13:49:00", "15:49:00", 78, 28, "International Joint Conference on Artificial Intelligence", 53
exec AddWorkshop "2012-7-25", "17:34:00", "18:34:00", 59, 24, "International Conference on the Principles of", 53
exec AddWorkshop "2013-6-15", "8:29:00", "9:29:00", 123, 22, "Annual Convention of The Society", 14
exec AddWorkshop "2013-12-6", "12:56:00", "15:56:00", 98, 41, "Sustainable Brands Conference", 67
exec AddWorkshop "2012-6-23", "9:27:00", "11:27:00", 101, 45, "3rd International Conference on Pattern Recognition Applications and Methods", 66
exec AddWorkshop "2013-1-21", "8:31:00", "11:31:00", 84, 23, "Annual Convention of The Society", 55
exec AddWorkshop "2013-4-8", "10:39:00", "12:39:00", 61, 25, "International Conference on Automated Reasoning", 16
exec AddWorkshop "2011-5-27", "10:3:00", "13:3:00", 73, 39, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 32
exec AddWorkshop "2013-10-17", "17:56:00", "18:56:00", 101, 41, "7 th International Conference on Bio-inspired Systems and Signal Processing", 0
exec AddWorkshop "2011-8-23", "13:7:00", "15:7:00", 92, 38, "International Conference on Automated Reasoning", 35
exec AddWorkshop "2013-3-9", "13:10:00", "14:10:00", 108, 46, "International Conference on Computer Vision", 33
exec AddWorkshop "2011-4-15", "16:23:00", "17:23:00", 88, 23, "2nd International Conference on Photonics, Optics and Laser Technology", 46
exec AddWorkshop "2011-7-6", "17:6:00", "20:6:00", 70, 38, "EuroCOW the Calibration and Orientation Workshop", 62
exec AddWorkshop "2012-12-27", "12:55:00", "13:55:00", 61, 40, "International Conference on Computer Vision", 33
exec AddWorkshop "2011-10-7", "18:28:00", "21:28:00", 138, 20, "International Conference on the Principles of", 57
exec AddWorkshop "2011-5-19", "19:7:00", "22:7:00", 108, 33, "4th DUE Permafrost User Workshop", 65
exec AddWorkshop "2013-12-15", "16:49:00", "18:49:00", 137, 32, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 55
exec AddWorkshop "2013-12-29", "19:23:00", "22:23:00", 51, 28, "International Conference on the Principles of", 56
exec AddWorkshop "2012-11-23", "18:42:00", "21:42:00", 131, 42, "International Conference on Automated Planning", 83
exec AddWorkshop "2012-4-7", "18:53:00", "20:53:00", 109, 37, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 60
exec AddWorkshop "2011-9-19", "11:32:00", "12:32:00", 116, 23, "Life in Space for Life on Earth Symposium", 51
exec AddWorkshop "2012-2-9", "16:40:00", "19:40:00", 63, 44, "International Conference on Automated Planning", 53
exec AddWorkshop "2012-5-11", "8:60:00", "9:60:00", 146, 40, "European Conference on Computer Vision", 48
exec AddWorkshop "2013-1-30", "10:42:00", "13:42:00", 127, 23, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 67
exec AddWorkshop "2011-4-6", "16:18:00", "19:18:00", 116, 28, "7 th International Conference on Bio-inspired Systems and Signal Processing", 48
exec AddWorkshop "2012-6-4", "19:28:00", "22:28:00", 129, 42, "The BCLC CSR Conference", 3
exec AddWorkshop "2011-10-21", "16:1:00", "17:1:00", 77, 35, "4S Symposium 2014", 12
exec AddWorkshop "2013-7-2", "8:18:00", "11:18:00", 95, 28, "X-Ray Universe", 25
exec AddWorkshop "2013-6-26", "8:57:00", "10:57:00", 62, 49, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 44
exec AddWorkshop "2011-10-18", "9:42:00", "11:42:00", 100, 37, "48th ESLAB Symposium", 11
exec AddWorkshop "2011-4-7", "14:49:00", "17:49:00", 52, 40, "Mechanisms Final Presentation Days", 80
exec AddWorkshop "2013-9-10", "17:21:00", "20:21:00", 79, 25, "International Conference on MultiMedia Modeling", 35
exec AddWorkshop "2013-2-25", "17:54:00", "19:54:00", 90, 47, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 63
exec AddWorkshop "2012-5-25", "18:4:00", "21:4:00", 105, 30, "The Corporate Philanthropy Forum", 4
exec AddWorkshop "2011-4-8", "18:17:00", "21:17:00", 115, 32, "European Space Technology Harmonisation Conference", 65
exec AddWorkshop "2012-4-1", "14:31:00", "15:31:00", 50, 30, "Net Impact Conference", 10
exec AddWorkshop "2011-5-3", "8:16:00", "9:16:00", 123, 43, "Conference on Computer Vision and Pattern", 6
exec AddWorkshop "2011-3-26", "8:51:00", "11:51:00", 120, 41, "Workshop on Image Processing", 65
exec AddWorkshop "2011-6-4", "10:50:00", "12:50:00", 144, 33, "Mayo Clinic Presents", 79
exec AddWorkshop "2013-11-12", "14:37:00", "16:37:00", 102, 49, "KU Leuven CSR Symposium", 55
exec AddWorkshop "2011-5-3", "8:29:00", "11:29:00", 134, 21, "Workshop on Logic ", 7
exec AddWorkshop "2011-6-11", "14:58:00", "16:58:00", 118, 38, "European Space Power Conference", 39
exec AddWorkshop "2012-11-10", "18:10:00", "20:10:00", 94, 39, "Leadership Strategies for Information Technology in Health Care Boston", 61
exec AddWorkshop "2013-5-22", "15:21:00", "18:21:00", 115, 24, "International Conference on MultiMedia Modeling", 21
exec AddWorkshop "2012-9-6", "17:24:00", "18:24:00", 134, 38, "Conference on Learning Theory		", 20
exec AddWorkshop "2012-5-7", "10:39:00", "11:39:00", 138, 24, "European Space Technology Harmonisation Conference", 29
exec AddWorkshop "2013-8-9", "15:46:00", "18:46:00", 80, 37, "International Joint Conference on Artificial Intelligence", 15
exec AddWorkshop "2012-3-13", "13:6:00", "16:6:00", 79, 22, "Global Conference on Sustainability and Reporting", 83
exec AddWorkshop "2013-6-30", "18:13:00", "21:13:00", 139, 39, "Microwave Workshop", 76
exec AddWorkshop "2012-8-8", "15:53:00", "17:53:00", 84, 21, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 23
exec AddWorkshop "2012-4-22", "15:47:00", "18:47:00", 138, 34, "The Sixth International Conferences on Advances in Multimedia", 35
exec AddWorkshop "2013-4-11", "12:54:00", "13:54:00", 96, 43, "Business4Better: The Community Partnership Movement", 36
exec AddWorkshop "2012-10-1", "8:4:00", "9:4:00", 143, 30, "Conference on Learning Theory		", 74
exec AddWorkshop "2013-9-6", "19:20:00", "21:20:00", 61, 34, "European Conference on Machine Learning", 8
exec AddWorkshop "2012-2-29", "11:29:00", "14:29:00", 63, 46, "Sentinel-2 for Science WS", 25
exec AddWorkshop "2012-3-27", "14:6:00", "15:6:00", 108, 44, "2nd International Conference on Photonics, Optics and Laser Technology", 69
exec AddWorkshop "2011-11-8", "8:22:00", "11:22:00", 146, 36, "International Conference on Logic for Programming", 68
exec AddWorkshop "2012-6-27", "18:16:00", "21:16:00", 65, 25, "Client Summit", 52
exec AddWorkshop "2013-12-4", "14:26:00", "17:26:00", 106, 41, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 54
exec AddWorkshop "2011-4-7", "15:42:00", "18:42:00", 90, 33, "Euclid Spacecraft Industry Day", 6
exec AddWorkshop "2011-8-28", "17:15:00", "20:15:00", 148, 44, "Foundations of Genetic Algorithms		", 50
exec AddWorkshop "2012-4-21", "9:22:00", "10:22:00", 63, 20, "Photonics West", 16
exec AddWorkshop "2013-1-12", "15:36:00", "16:36:00", 62, 31, "Sustainable Brands Conference", 22
exec AddWorkshop "2013-11-26", "16:58:00", "17:58:00", 62, 24, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 32
exec AddWorkshop "2012-1-16", "8:26:00", "11:26:00", 69, 24, "Conference on Uncertainty in Artificial Intelligence", 13
exec AddWorkshop "2011-9-9", "16:2:00", "19:2:00", 60, 26, "International Conference on Automated Planning", 17
exec AddWorkshop "2013-3-12", "11:43:00", "14:43:00", 93, 41, "International Conference on Pattern Recognition", 47
exec AddWorkshop "2012-3-21", "13:14:00", "16:14:00", 117, 46, "International Conference on Logic for Programming", 11
exec AddWorkshop "2011-2-29", "10:7:00", "13:7:00", 80, 36, "RuleML Symposium				", 19
exec AddWorkshop "2012-12-26", "10:19:00", "13:19:00", 64, 48, "Client Summit", 43
exec AddWorkshop "2012-2-3", "8:52:00", "11:52:00", 144, 33, "International Semantic Web Conference		", 34
exec AddWorkshop "2011-5-28", "15:39:00", "16:39:00", 83, 20, "Global Conference on Sustainability and Reporting", 21
exec AddWorkshop "2013-8-10", "9:40:00", "12:40:00", 97, 24, "Sustainable Brands Conference", 0
exec AddWorkshop "2011-8-7", "19:14:00", "22:14:00", 108, 26, "European Conference on Artificial Intelligence	", 23
exec AddWorkshop "2012-12-8", "19:49:00", "20:49:00", 90, 33, "Conference on Computer Vision and Pattern", 53
exec AddWorkshop "2011-7-22", "10:35:00", "12:35:00", 147, 36, "Euclid Spacecraft Industry Day", 50
exec AddWorkshop "2011-8-29", "12:49:00", "13:49:00", 145, 44, "Conference on Uncertainty in Artificial Intelligence", 66
exec AddWorkshop "2011-4-9", "17:39:00", "20:39:00", 107, 39, "Annual Convention of The Society", 20
exec AddWorkshop "2012-7-8", "15:2:00", "16:2:00", 73, 33, "The 12th annual Responsible Business Summit", 67
exec AddWorkshop "2012-5-3", "8:25:00", "11:25:00", 78, 26, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 13
exec AddWorkshop "2011-10-23", "14:44:00", "16:44:00", 82, 40, "LPVE Land product validation and evolution", 0
exec AddWorkshop "2012-10-13", "8:55:00", "11:55:00", 78, 39, "European Conference on Artificial Intelligence	", 45
exec AddWorkshop "2011-6-25", "12:58:00", "15:58:00", 76, 43, "Conference on Learning Theory		", 3
exec AddWorkshop "2013-7-11", "16:43:00", "19:43:00", 68, 30, "3rd International Conference on Pattern Recognition Applications and Methods", 29
exec AddWorkshop "2013-11-9", "15:9:00", "18:9:00", 81, 29, "Cause Marketing Forum", 26
exec AddWorkshop "2011-11-2", "15:20:00", "16:20:00", 79, 36, "Leadership Strategies for Information Technology in Health Care Boston", 62
exec AddWorkshop "2011-12-12", "9:40:00", "12:40:00", 127, 41, "Microwave Workshop", 61
exec AddWorkshop "2013-4-14", "12:42:00", "14:42:00", 85, 43, "4S Symposium 2014", 46
exec AddWorkshop "2013-4-14", "12:4:00", "14:4:00", 79, 47, "Foundations of Genetic Algorithms		", 77
exec AddWorkshop "2013-1-6", "18:40:00", "19:40:00", 65, 33, "European Conference on Machine Learning", 48
exec AddWorkshop "2011-3-1", "10:10:00", "12:10:00", 121, 27, "ARTES 1 Final Presentation Days", 55
exec AddWorkshop "2011-1-4", "12:26:00", "15:26:00", 74, 28, "European Space Technology Harmonisation Conference", 60
exec AddWorkshop "2013-12-19", "13:14:00", "16:14:00", 52, 40, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 38
exec AddWorkshop "2012-9-25", "11:31:00", "14:31:00", 110, 36, "Mechanisms Final Presentation Days", 79
exec AddWorkshop "2012-10-12", "19:33:00", "21:33:00", 81, 35, "British	Machine	Vision	Conference		", 52
exec AddWorkshop "2013-8-2", "12:3:00", "13:3:00", 97, 28, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 36
exec AddWorkshop "2012-2-16", "10:16:00", "13:16:00", 62, 49, "ARTES 1 Final Presentation Days", 36
exec AddWorkshop "2013-10-17", "12:44:00", "15:44:00", 56, 20, "Client Summit", 71
exec AddWorkshop "2011-4-21", "19:59:00", "22:59:00", 78, 38, "Workshop on Image Processing", 80
exec AddWorkshop "2013-6-26", "10:57:00", "12:57:00", 133, 32, "Mechanisms Final Presentation Days", 68
exec AddWorkshop "2012-12-18", "11:20:00", "14:20:00", 115, 36, "Photonics West", 24
exec AddWorkshop "2012-12-7", "8:35:00", "10:35:00", 133, 32, "International Conference on Automated Planning", 31
exec AddWorkshop "2012-11-11", "14:52:00", "15:52:00", 84, 32, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 82
exec AddWorkshop "2013-10-11", "13:9:00", "14:9:00", 132, 37, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 32
exec AddWorkshop "2012-5-28", "11:37:00", "13:37:00", 99, 31, "Conference on Uncertainty in Artificial Intelligence", 20
exec AddWorkshop "2011-1-29", "14:26:00", "16:26:00", 91, 28, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 2
exec AddWorkshop "2011-10-7", "19:34:00", "21:34:00", 144, 29, "International Conference on Automated Reasoning", 36
exec AddWorkshop "2011-7-22", "15:37:00", "16:37:00", 147, 35, "Sustainable Brands Conference", 41
exec AddWorkshop "2013-5-25", "10:47:00", "13:47:00", 143, 44, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 25
exec AddWorkshop "2012-7-11", "9:52:00", "10:52:00", 116, 33, "International Conference on Autonomous Agents and", 45
exec AddWorkshop "2013-3-26", "14:49:00", "16:49:00", 127, 45, "Annual Interdisciplinary Conference", 25
exec AddWorkshop "2013-9-16", "8:36:00", "11:36:00", 117, 45, "2013 Ethical Leadership Conference: Ethics in Action", 80
exec AddWorkshop "2012-3-3", "15:17:00", "16:17:00", 73, 27, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 74
exec AddWorkshop "2013-8-19", "14:9:00", "15:9:00", 123, 26, "The 2nd International CSR Communication Conference", 46
exec AddWorkshop "2013-9-12", "8:51:00", "9:51:00", 124, 22, "Mechanisms Final Presentation Days", 51
exec AddWorkshop "2011-9-18", "9:8:00", "10:8:00", 116, 30, "ARTES 1 Final Presentation Days", 28
exec AddWorkshop "2012-10-22", "10:34:00", "13:34:00", 129, 32, "Photonics West", 26
exec AddWorkshop "2012-5-28", "13:45:00", "14:45:00", 57, 39, "The 12th annual Responsible Business Summit", 73
exec AddWorkshop "2011-3-6", "14:13:00", "16:13:00", 81, 35, "International Conference on MultiMedia Modeling", 49
exec AddWorkshop "2011-5-12", "15:35:00", "18:35:00", 131, 41, "European Conference on Computer Vision", 49
exec AddWorkshop "2013-5-27", "19:12:00", "22:12:00", 81, 21, "Business4Better: The Community Partnership Movement", 60
exec AddWorkshop "2011-8-7", "12:44:00", "14:44:00", 118, 44, "The BCLC CSR Conference", 83
exec AddWorkshop "2012-8-14", "14:4:00", "15:4:00", 131, 33, "2nd International Conference on Photonics, Optics and Laser Technology", 76
exec AddWorkshop "2013-1-25", "17:34:00", "20:34:00", 134, 33, "Photonics West", 3
exec AddWorkshop "2013-1-3", "13:48:00", "16:48:00", 74, 26, "Global Conference on Sustainability and Reporting", 79
exec AddWorkshop "2011-2-14", "14:35:00", "16:35:00", 114, 35, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 14
exec AddWorkshop "2012-9-13", "10:37:00", "11:37:00", 147, 30, "European Conference on Artificial Intelligence	", 57
exec AddWorkshop "2011-4-3", "13:40:00", "16:40:00", 89, 40, "International Conference on Artificial Neural Networks", 48
exec AddWorkshop "2011-7-25", "16:44:00", "18:44:00", 97, 47, "7 th International Conference on Bio-inspired Systems and Signal Processing", 28
exec AddWorkshop "2011-1-26", "17:46:00", "18:46:00", 111, 34, "Annual Convention of The Society", 81
exec AddWorkshop "2011-10-2", "18:14:00", "19:14:00", 105, 28, "Conference on Uncertainty in Artificial Intelligence", 49
exec AddWorkshop "2013-4-20", "12:44:00", "14:44:00", 81, 21, "The BCLC CSR Conference", 75
exec AddWorkshop "2013-4-9", "12:11:00", "13:11:00", 88, 43, "Life in Space for Life on Earth Symposium", 43
exec AddWorkshop "2013-11-8", "12:16:00", "13:16:00", 93, 32, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 46
exec AddWorkshop "2012-1-20", "14:11:00", "17:11:00", 93, 27, "Conference on Uncertainty in Artificial Intelligence", 22
exec AddWorkshop "2012-10-3", "9:35:00", "12:35:00", 113, 27, "Cause Marketing Forum", 44
exec AddWorkshop "2013-7-17", "19:49:00", "22:49:00", 98, 25, "GNC 2014", 2
exec AddWorkshop "2011-1-25", "15:35:00", "17:35:00", 81, 26, "X-Ray Universe", 14
exec AddWorkshop "2013-5-19", "14:49:00", "16:49:00", 89, 35, "Global Conference on Sustainability and Reporting", 20
exec AddWorkshop "2011-6-27", "13:40:00", "16:40:00", 127, 37, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 11
exec AddWorkshop "2013-6-10", "19:52:00", "21:52:00", 50, 41, "Mechanisms Final Presentation Days", 0
exec AddWorkshop "2011-9-18", "18:1:00", "20:1:00", 94, 27, "Mechanisms Final Presentation Days", 72
exec AddWorkshop "2012-4-11", "9:42:00", "10:42:00", 76, 43, "4th DUE Permafrost User Workshop", 66
exec AddWorkshop "2011-1-15", "10:60:00", "13:60:00", 57, 45, "International Conference on MultiMedia Modeling", 64
exec AddWorkshop "2012-2-3", "10:20:00", "13:20:00", 97, 24, "Euclid Spacecraft Industry Day", 17
exec AddWorkshop "2011-1-19", "17:59:00", "18:59:00", 65, 42, "International Joint Conference on Automated Reasoning", 18
exec AddWorkshop "2013-10-11", "15:15:00", "18:15:00", 110, 29, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 56
exec AddWorkshop "2013-6-4", "12:22:00", "13:22:00", 116, 36, "2013 Ethical Leadership Conference: Ethics in Action", 43
exec AddWorkshop "2011-2-21", "16:4:00", "17:4:00", 139, 49, "International Conference on Signal and Imaging Systems Engineering", 42
exec AddWorkshop "2011-10-6", "15:50:00", "18:50:00", 120, 23, "International Conference on MultiMedia Modeling", 50
exec AddWorkshop "2011-4-19", "19:17:00", "21:17:00", 118, 29, "International Conference on Autonomous Agents and", 33
exec AddWorkshop "2012-4-21", "11:11:00", "14:11:00", 68, 41, "Global Conference on Sustainability and Reporting", 20
exec AddWorkshop "2011-8-20", "17:31:00", "18:31:00", 70, 40, "International Conference on Computer Vision", 72
exec AddWorkshop "2013-6-2", "19:1:00", "20:1:00", 94, 25, "KU Leuven CSR Symposium", 12
exec AddWorkshop "2011-2-29", "9:56:00", "10:56:00", 90, 47, "Euclid Spacecraft Industry Day", 72
exec AddWorkshop "2011-4-17", "12:43:00", "14:43:00", 74, 32, "RuleML Symposium				", 9
exec AddWorkshop "2011-12-29", "12:8:00", "15:8:00", 127, 44, "48th ESLAB Symposium", 9
exec AddWorkshop "2011-2-12", "10:60:00", "13:60:00", 125, 32, "International Conference on Autonomous Agents and", 36
exec AddWorkshop "2012-5-21", "8:47:00", "9:47:00", 83, 42, "Net Impact Conference", 75
exec AddWorkshop "2011-8-20", "16:34:00", "17:34:00", 89, 43, "International Semantic Web Conference		", 48
exec AddWorkshop "2012-2-21", "17:58:00", "20:58:00", 87, 38, "KU Leuven CSR Symposium", 36
exec AddWorkshop "2013-4-9", "15:32:00", "17:32:00", 95, 39, "Annual Convention of The Society", 28
exec AddWorkshop "2012-5-30", "17:47:00", "18:47:00", 94, 43, "International Conference on Computer Vision Theory and Applications", 32
exec AddWorkshop "2013-1-24", "11:44:00", "13:44:00", 97, 29, "International Conference on Autonomous Agents and", 73
exec AddWorkshop "2011-8-16", "12:45:00", "14:45:00", 111, 46, "Conference on Computer Vision and Pattern", 76
exec AddWorkshop "2012-6-29", "14:9:00", "16:9:00", 94, 20, "Euclid Spacecraft Industry Day", 3
exec AddWorkshop "2011-12-7", "19:3:00", "21:3:00", 56, 29, "2nd International Conference on Photonics, Optics and Laser Technology", 12
exec AddWorkshop "2013-1-10", "18:6:00", "21:6:00", 143, 43, "KU Leuven CSR Symposium", 73
exec AddWorkshop "2011-7-20", "11:39:00", "12:39:00", 136, 38, "Workshop on Image Processing", 71
exec AddWorkshop "2012-5-13", "12:19:00", "15:19:00", 76, 34, "Annual Convention of The Society", 75
exec AddWorkshop "2011-9-14", "13:54:00", "16:54:00", 140, 31, "British	Machine	Vision	Conference		", 32
exec AddWorkshop "2011-1-5", "13:6:00", "14:6:00", 83, 27, "2013 Ethical Leadership Conference: Ethics in Action", 37
exec AddWorkshop "2011-9-25", "12:23:00", "15:23:00", 140, 30, "�International� Corporate Citizenship Conference", 30
exec AddWorkshop "2011-3-7", "17:2:00", "19:2:00", 116, 44, "Euclid Spacecraft Industry Day", 10
exec AddWorkshop "2012-3-24", "15:5:00", "17:5:00", 99, 49, "International Joint Conference on Automated Reasoning", 2
exec AddWorkshop "2012-5-5", "9:8:00", "11:8:00", 109, 43, "Conference on Computer Vision and Pattern", 60
exec AddWorkshop "2013-9-1", "16:18:00", "19:18:00", 60, 47, "European Conference on Machine Learning", 76
exec AddWorkshop "2012-9-25", "14:14:00", "16:14:00", 95, 46, "European Space Technology Harmonisation Conference", 56
exec AddWorkshop "2011-8-3", "17:53:00", "20:53:00", 146, 26, "Sustainable Brands Conference", 41
exec AddWorkshop "2013-3-18", "8:19:00", "11:19:00", 60, 35, "Life in Space for Life on Earth Symposium", 61
exec AddWorkshop "2013-7-22", "18:17:00", "19:17:00", 148, 42, "Workshop on Logic ", 24
exec AddWorkshop "2013-6-28", "11:47:00", "12:47:00", 75, 38, "ARTES 1 Final Presentation Days", 66
exec AddWorkshop "2011-2-24", "14:22:00", "17:22:00", 144, 31, "The 2nd International CSR Communication Conference", 33
exec AddWorkshop "2011-8-25", "12:12:00", "14:12:00", 106, 46, "Conference on Automated Deduction		", 11
exec AddWorkshop "2011-4-17", "14:4:00", "15:4:00", 113, 35, "RuleML Symposium		", 56
exec AddWorkshop "2011-11-12", "11:41:00", "12:41:00", 119, 29, "Sentinel-2 for Science WS", 13
exec AddWorkshop "2012-9-27", "15:47:00", "16:47:00", 140, 35, "International Semantic Web Conference		", 61
exec AddWorkshop "2011-10-2", "12:39:00", "14:39:00", 50, 38, "International Conference on the Principles of", 60
exec AddWorkshop "2011-3-1", "10:58:00", "13:58:00", 142, 25, "European Space Technology Harmonisation Conference", 68
exec AddWorkshop "2013-9-12", "11:6:00", "14:6:00", 149, 21, "European Conference on Machine Learning", 42
exec AddWorkshop "2011-2-27", "14:8:00", "17:8:00", 125, 28, "ARTES 1 Final Presentation Days", 80
exec AddWorkshop "2012-11-18", "19:48:00", "21:48:00", 65, 24, "Business4Better: The Community Partnership Movement", 81
exec AddWorkshop "2013-7-28", "9:47:00", "12:47:00", 127, 46, "International Conference on Automated Reasoning", 42
exec AddWorkshop "2011-10-16", "15:39:00", "16:39:00", 91, 39, "3rd International Conference on Pattern Recognition Applications and Methods", 80
exec AddWorkshop "2012-2-12", "19:30:00", "21:30:00", 50, 46, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 41
exec AddWorkshop "2011-5-4", "15:60:00", "17:60:00", 130, 39, "Foundations of Genetic Algorithms		", 40
exec AddWorkshop "2012-2-27", "19:16:00", "20:16:00", 109, 21, "KU Leuven CSR Symposium", 20
exec AddWorkshop "2013-11-6", "16:35:00", "17:35:00", 135, 42, "International Conference on Pattern Recognition", 5
exec AddWorkshop "2012-9-14", "10:42:00", "12:42:00", 110, 33, "International Conference on Pattern Recognition", 63
exec AddWorkshop "2012-12-23", "16:45:00", "19:45:00", 85, 49, "ICATT", 74
exec AddWorkshop "2011-11-3", "10:27:00", "11:27:00", 137, 46, "Corporate Community Involvement Conference", 79
exec AddWorkshop "2011-7-24", "11:24:00", "13:24:00", 124, 22, "RuleML Symposium				", 11
exec AddWorkshop "2013-3-6", "11:14:00", "12:14:00", 129, 43, "International Conference on MultiMedia Modeling", 83
exec AddWorkshop "2013-7-22", "18:9:00", "19:9:00", 95, 41, "Conference on Uncertainty in Artificial Intelligence", 9
exec AddWorkshop "2013-1-17", "16:53:00", "19:53:00", 100, 21, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 3
exec AddWorkshop "2011-11-20", "9:47:00", "10:47:00", 114, 29, "Conference on Computer Vision and Pattern", 43
exec AddWorkshop "2012-3-25", "16:27:00", "19:27:00", 93, 21, "International Conference on MultiMedia Modeling", 10
exec AddWorkshop "2011-11-22", "14:28:00", "17:28:00", 65, 42, "Computer Analysis of Images and Patterns", 19
exec AddWorkshop "2013-6-23", "14:1:00", "15:1:00", 80, 39, "4S Symposium 2014", 10
exec AddWorkshop "2012-9-23", "8:53:00", "11:53:00", 57, 29, "3rd International Conference on Pattern Recognition Applications and Methods", 65
exec AddWorkshop "2013-7-30", "11:17:00", "13:17:00", 125, 20, "International Joint Conference on Artificial Intelligence", 52
exec AddWorkshop "2011-11-17", "13:21:00", "15:21:00", 57, 25, "KU Leuven CSR Symposium", 9
exec AddWorkshop "2012-2-29", "18:60:00", "19:60:00", 138, 47, "European Conference on Machine Learning", 25
exec AddWorkshop "2012-2-17", "13:28:00", "16:28:00", 114, 35, "EuroCOW the Calibration and Orientation Workshop", 44
exec AddWorkshop "2013-9-20", "17:14:00", "20:14:00", 121, 43, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 49
exec AddWorkshop "2011-10-3", "11:29:00", "13:29:00", 132, 20, "Life in Space for Life on Earth Symposium", 5
exec AddWorkshop "2011-1-9", "11:35:00", "14:35:00", 59, 35, "Leadership Strategies for Information Technology in Health Care Boston", 65
exec AddWorkshop "2011-11-16", "8:46:00", "9:46:00", 97, 30, "ARTES 1 Final Presentation Days", 27
exec AddWorkshop "2013-1-21", "12:32:00", "15:32:00", 72, 32, "EuroCOW the Calibration and Orientation Workshop", 84
exec AddWorkshop "2013-12-18", "9:25:00", "12:25:00", 99, 26, "4th DUE Permafrost User Workshop", 35
exec AddWorkshop "2011-4-16", "16:31:00", "17:31:00", 67, 32, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 9
exec AddWorkshop "2011-3-28", "13:15:00", "16:15:00", 106, 40, "Asian Conference on Computer Vision", 78
exec AddWorkshop "2012-5-6", "11:46:00", "14:46:00", 98, 42, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 66
exec AddWorkshop "2013-9-17", "16:14:00", "17:14:00", 105, 33, "International Semantic Web Conference		", 1
exec AddWorkshop "2012-8-6", "12:33:00", "14:33:00", 97, 40, "International Conference on Computer Vision", 9
exec AddWorkshop "2012-11-13", "11:38:00", "14:38:00", 136, 45, "ICATT", 49
exec AddWorkshop "2012-3-7", "12:51:00", "15:51:00", 124, 46, "Annual Interdisciplinary Conference", 70
exec AddWorkshop "2011-4-4", "9:32:00", "11:32:00", 146, 20, "GNC 2014", 62
exec AddWorkshop "2011-5-11", "9:7:00", "11:7:00", 60, 26, "Business4Better: The Community Partnership Movement", 45
exec AddWorkshop "2012-9-14", "16:20:00", "17:20:00", 65, 36, "The BCLC CSR Conference", 51
exec AddWorkshop "2011-11-13", "13:39:00", "16:39:00", 138, 27, "The 2nd International CSR Communication Conference", 81
exec AddWorkshop "2011-10-8", "15:40:00", "16:40:00", 55, 24, "Cause Marketing Forum", 64
exec AddWorkshop "2013-1-11", "12:21:00", "13:21:00", 143, 21, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 66
exec AddWorkshop "2011-5-1", "19:29:00", "20:29:00", 87, 36, "�International� Corporate Citizenship Conference", 9
exec AddWorkshop "2012-6-26", "12:16:00", "13:16:00", 140, 23, "2013 Ethical Leadership Conference: Ethics in Action", 72
exec AddWorkshop "2012-9-17", "12:21:00", "13:21:00", 128, 35, "Conference on Learning Theory		", 46
exec AddWorkshop "2012-11-26", "11:56:00", "13:56:00", 107, 32, "Photonics West", 3
exec AddWorkshop "2011-11-19", "14:13:00", "16:13:00", 64, 34, "International Conference on Autonomous Agents and", 35
exec AddWorkshop "2011-8-25", "9:34:00", "12:34:00", 136, 45, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 73
exec AddWorkshop "2012-9-20", "15:24:00", "18:24:00", 73, 42, "LPVE Land product validation and evolution", 81
exec AddWorkshop "2012-4-18", "15:3:00", "16:3:00", 132, 44, "Business4Better: The Community Partnership Movement", 68
exec AddWorkshop "2011-4-18", "12:60:00", "14:60:00", 111, 47, "Photonics West", 15
exec AddWorkshop "2011-3-7", "8:3:00", "9:3:00", 94, 24, "The BCLC CSR Conference", 27
exec AddWorkshop "2013-10-18", "19:16:00", "20:16:00", 98, 24, "International Conference on Computer Vision", 45
exec AddWorkshop "2011-1-29", "18:5:00", "20:5:00", 147, 20, "Leadership Strategies for Information Technology in Health Care Boston", 37
exec AddWorkshop "2012-4-25", "15:14:00", "16:14:00", 110, 41, "Leadership Strategies for Information Technology in Health Care Boston", 13
exec AddWorkshop "2012-2-1", "16:56:00", "18:56:00", 125, 37, "Corporate Community Involvement Conference", 59
exec AddWorkshop "2011-7-27", "11:34:00", "13:34:00", 55, 22, "European Conference on Artificial Intelligence	", 54
exec AddWorkshop "2013-7-10", "10:1:00", "12:1:00", 74, 44, "The BCLC CSR Conference", 45
exec AddWorkshop "2011-3-26", "11:52:00", "12:52:00", 82, 37, "International Conference on Logic for Programming", 61
exec AddWorkshop "2013-7-23", "9:55:00", "10:55:00", 95, 46, "RuleML Symposium		", 31
exec AddWorkshop "2011-1-6", "8:38:00", "10:38:00", 76, 24, "Conference on Computer Vision and Pattern", 43
exec AddWorkshop "2012-2-6", "9:37:00", "12:37:00", 135, 32, "Workshop on Image Processing", 71
exec AddWorkshop "2012-4-2", "17:57:00", "19:57:00", 66, 46, "Photonics West", 41
exec AddWorkshop "2013-9-16", "19:39:00", "20:39:00", 104, 27, "European Space Technology Harmonisation Conference", 5
exec AddWorkshop "2011-2-14", "19:57:00", "22:57:00", 145, 44, "Workshop on Logic ", 64
exec AddWorkshop "2011-11-11", "15:15:00", "16:15:00", 93, 35, "International Conference on Automated Planning", 46
exec AddWorkshop "2012-11-15", "17:49:00", "18:49:00", 112, 46, "European Conference on Machine Learning", 20
exec AddWorkshop "2011-10-22", "17:50:00", "18:50:00", 52, 49, "Annual Interdisciplinary Conference", 2
exec AddWorkshop "2012-7-5", "12:24:00", "13:24:00", 99, 38, "Sentinel-2 for Science WS", 30
exec AddWorkshop "2013-10-15", "15:31:00", "18:31:00", 90, 31, "Sentinel-2 for Science WS", 46
exec AddWorkshop "2012-5-10", "13:10:00", "14:10:00", 82, 36, "International Joint Conference on Automated Reasoning", 73
exec AddWorkshop "2012-6-1", "19:58:00", "22:58:00", 108, 22, "GNC 2014", 32
exec AddWorkshop "2011-11-26", "19:20:00", "21:20:00", 86, 20, "X-Ray Universe", 41
exec AddWorkshop "2012-4-1", "14:6:00", "15:6:00", 80, 38, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 51
exec AddWorkshop "2011-3-26", "16:26:00", "19:26:00", 147, 25, "European Space Power Conference", 21
exec AddWorkshop "2013-6-4", "12:24:00", "14:24:00", 70, 37, "ICATT", 51
exec AddWorkshop "2013-5-24", "15:44:00", "17:44:00", 125, 47, "2013 Ethical Leadership Conference: Ethics in Action", 79
exec AddWorkshop "2013-10-16", "17:50:00", "20:50:00", 147, 44, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 1
exec AddWorkshop "2011-10-17", "8:3:00", "9:3:00", 63, 43, "International Semantic Web Conference		", 43
exec AddWorkshop "2011-3-21", "16:48:00", "18:48:00", 62, 35, "International Conference on Pattern Recognition", 69
exec AddWorkshop "2012-2-14", "10:16:00", "11:16:00", 58, 26, "X-Ray Universe", 23
exec AddWorkshop "2012-12-19", "15:59:00", "17:59:00", 133, 21, "ARTES 1 Final Presentation Days", 52
exec AddWorkshop "2012-10-2", "16:27:00", "18:27:00", 102, 33, "International Joint Conference on Artificial Intelligence", 22
exec AddWorkshop "2012-1-5", "8:60:00", "9:60:00", 145, 36, "Cause Marketing Forum", 11
exec AddWorkshop "2012-12-12", "18:14:00", "21:14:00", 127, 47, "Photonics West", 9
exec AddWorkshop "2013-10-3", "14:56:00", "16:56:00", 94, 44, "European Space Power Conference", 28
exec AddWorkshop "2012-6-7", "19:12:00", "21:12:00", 122, 28, "International Joint Conference on Automated Reasoning", 25
exec AddWorkshop "2011-7-5", "13:57:00", "15:57:00", 114, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 65
exec AddWorkshop "2013-8-10", "16:20:00", "18:20:00", 97, 37, "Science and Challenges of Lunar Sample Return Workshop", 59
exec AddWorkshop "2012-7-20", "17:56:00", "19:56:00", 76, 48, "Conference on Computer Vision and Pattern", 4
exec AddWorkshop "2013-10-22", "15:57:00", "18:57:00", 116, 30, "ARTES 1 Final Presentation Days", 55
exec AddWorkshop "2011-2-17", "13:8:00", "15:8:00", 96, 31, "RuleML Symposium				", 69
exec AddWorkshop "2012-9-3", "8:19:00", "9:19:00", 85, 22, "International Conference on Signal and Imaging Systems Engineering", 47
exec AddWorkshop "2013-5-1", "13:14:00", "15:14:00", 56, 24, "Life in Space for Life on Earth Symposium", 48
exec AddWorkshop "2012-5-23", "14:19:00", "17:19:00", 115, 32, "Photonics West", 19
exec AddWorkshop "2011-6-26", "14:54:00", "15:54:00", 101, 44, "The Corporate Philanthropy Forum", 64
exec AddWorkshop "2012-3-5", "18:47:00", "19:47:00", 102, 24, "European Conference on Computer Vision", 38
exec AddWorkshop "2012-7-10", "17:58:00", "18:58:00", 59, 21, "Global Conference on Sustainability and Reporting", 29
exec AddWorkshop "2012-7-23", "8:53:00", "11:53:00", 121, 28, "LPVE Land product validation and evolution", 13
exec AddWorkshop "2012-6-2", "16:40:00", "18:40:00", 54, 43, "International Semantic Web Conference		", 43
exec AddWorkshop "2013-5-28", "8:53:00", "10:53:00", 52, 25, "RuleML Symposium		", 71
exec AddWorkshop "2011-11-23", "19:25:00", "21:25:00", 79, 38, "The Sixth International Conferences on Advances in Multimedia", 66
exec AddWorkshop "2011-4-16", "16:28:00", "17:28:00", 72, 26, "Science and Challenges of Lunar Sample Return Workshop", 29
exec AddWorkshop "2011-2-1", "12:56:00", "15:56:00", 137, 34, "The Sixth International Conferences on Advances in Multimedia", 76
exec AddWorkshop "2013-7-25", "9:43:00", "10:43:00", 125, 33, "International Conference on Computer Vision Theory and Applications", 22
exec AddWorkshop "2011-12-25", "10:41:00", "11:41:00", 55, 46, "Business4Better: The Community Partnership Movement", 34
exec AddWorkshop "2011-7-6", "14:4:00", "15:4:00", 134, 48, "EuroCOW the Calibration and Orientation Workshop", 63
exec AddWorkshop "2011-12-24", "19:6:00", "21:6:00", 101, 46, "2nd International Conference on Photonics, Optics and Laser Technology", 48
exec AddWorkshop "2012-9-14", "15:44:00", "17:44:00", 119, 37, "International Conference on the Principles of", 18
exec AddWorkshop "2012-2-20", "19:2:00", "22:2:00", 94, 36, "International Conference on Computer Vision", 37
exec AddWorkshop "2012-1-20", "8:21:00", "9:21:00", 122, 49, "�International� Corporate Citizenship Conference", 60
exec AddWorkshop "2011-2-7", "19:40:00", "21:40:00", 114, 26, "International Conference on Artificial Neural Networks", 25
exec AddWorkshop "2013-2-30", "14:45:00", "17:45:00", 130, 44, "2013 Ethical Leadership Conference: Ethics in Action", 29
exec AddWorkshop "2013-8-4", "19:22:00", "20:22:00", 89, 21, "Life in Space for Life on Earth Symposium", 83
exec AddWorkshop "2013-7-11", "18:27:00", "19:27:00", 53, 41, "Photonics West", 13
exec AddWorkshop "2011-11-27", "11:35:00", "13:35:00", 91, 26, "4S Symposium 2014", 70
exec AddWorkshop "2012-10-22", "19:13:00", "22:13:00", 68, 21, "LPVE Land product validation and evolution", 58
exec AddWorkshop "2013-9-27", "12:34:00", "13:34:00", 148, 27, "2nd International Conference on Photonics, Optics and Laser Technology", 30
exec AddWorkshop "2013-11-22", "12:18:00", "13:18:00", 90, 24, "Annual Interdisciplinary Conference", 24
exec AddWorkshop "2013-10-27", "14:20:00", "16:20:00", 66, 29, "Workshop on Logic ", 44
exec AddWorkshop "2011-12-1", "14:10:00", "17:10:00", 64, 47, "British	Machine	Vision	Conference		", 68
exec AddWorkshop "2011-8-17", "12:34:00", "15:34:00", 97, 22, "The Sixth International Conferences on Advances in Multimedia", 15
exec AddWorkshop "2012-4-25", "8:31:00", "10:31:00", 86, 41, "Conference on Learning Theory		", 17
exec AddWorkshop "2011-6-22", "12:55:00", "14:55:00", 83, 26, "Photonics West", 48
exec AddWorkshop "2013-9-1", "13:1:00", "14:1:00", 114, 28, "Annual Interdisciplinary Conference", 30
exec AddWorkshop "2013-2-17", "8:22:00", "9:22:00", 100, 41, "International Joint Conference on Artificial Intelligence", 53
exec AddWorkshop "2012-9-26", "8:57:00", "11:57:00", 92, 44, "Annual Interdisciplinary Conference", 36
exec AddWorkshop "2013-8-18", "13:27:00", "14:27:00", 93, 42, "RuleML Symposium				", 46
exec AddWorkshop "2013-3-18", "15:42:00", "18:42:00", 115, 22, "4S Symposium 2014", 10
exec AddWorkshop "2013-8-21", "18:47:00", "20:47:00", 92, 30, "European Space Power Conference", 26
exec AddWorkshop "2011-6-7", "9:17:00", "12:17:00", 50, 27, "Conference on Uncertainty in Artificial Intelligence", 73
exec AddWorkshop "2012-12-29", "11:20:00", "14:20:00", 135, 26, "International Conference on the Principles of", 36
exec AddWorkshop "2011-6-1", "9:46:00", "12:46:00", 126, 31, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 36
exec AddWorkshop "2013-2-15", "11:22:00", "14:22:00", 85, 41, "Mayo Clinic Presents", 44
exec AddWorkshop "2011-11-7", "11:58:00", "13:58:00", 66, 43, "ICATT", 64
exec AddWorkshop "2011-8-3", "12:60:00", "13:60:00", 92, 31, "3rd International Conference on Pattern Recognition Applications and Methods", 54
exec AddWorkshop "2011-3-15", "8:3:00", "11:3:00", 136, 26, "KU Leuven CSR Symposium", 46
exec AddWorkshop "2012-2-11", "14:34:00", "17:34:00", 60, 43, "International Conference on Pattern Recognition", 33
exec AddWorkshop "2011-11-16", "10:58:00", "11:58:00", 131, 31, "British	Machine	Vision	Conference		", 5
exec AddWorkshop "2011-7-17", "18:39:00", "21:39:00", 67, 24, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 71
exec AddWorkshop "2011-12-19", "13:40:00", "16:40:00", 120, 45, "Global Conference on Sustainability and Reporting", 47
exec AddWorkshop "2011-11-15", "16:13:00", "18:13:00", 103, 34, "Sustainable Brands Conference", 2
exec AddWorkshop "2012-7-5", "17:59:00", "19:59:00", 137, 26, "International Conference on Automated Planning", 15
exec AddWorkshop "2013-11-27", "14:55:00", "15:55:00", 124, 20, "Business4Better: The Community Partnership Movement", 73
exec AddWorkshop "2011-6-25", "12:2:00", "14:2:00", 119, 28, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 5
exec AddWorkshop "2012-6-30", "10:46:00", "13:46:00", 86, 48, "7 th International Conference on Bio-inspired Systems and Signal Processing", 48
exec AddWorkshop "2011-8-9", "17:51:00", "20:51:00", 76, 35, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 62
exec AddWorkshop "2013-8-20", "14:44:00", "15:44:00", 94, 45, "Client Summit", 37
exec AddWorkshop "2012-3-15", "9:40:00", "12:40:00", 98, 34, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 34
exec AddWorkshop "2011-4-8", "14:38:00", "17:38:00", 91, 31, "Conference on Uncertainty in Artificial Intelligence", 16
exec AddWorkshop "2012-12-1", "18:49:00", "19:49:00", 106, 40, "Workshop on Logic ", 63
exec AddWorkshop "2012-3-13", "18:41:00", "19:41:00", 72, 30, "GNC 2014", 39
exec AddWorkshop "2011-12-2", "10:4:00", "11:4:00", 115, 24, "British	Machine	Vision	Conference		", 69
exec AddWorkshop "2013-6-19", "17:31:00", "19:31:00", 55, 25, "4S Symposium 2014", 45
exec AddWorkshop "2013-1-14", "10:33:00", "12:33:00", 132, 22, "European Space Power Conference", 16
exec AddWorkshop "2011-11-7", "14:5:00", "17:5:00", 148, 36, "Asian Conference on Computer Vision", 7
exec AddWorkshop "2012-3-8", "19:43:00", "22:43:00", 136, 36, "Net Impact Conference", 40
exec AddWorkshop "2012-8-28", "14:55:00", "15:55:00", 79, 21, "Ceres Conference", 7
exec AddWorkshop "2012-5-23", "8:15:00", "9:15:00", 81, 22, "4S Symposium 2014", 54
exec AddWorkshop "2011-4-6", "18:19:00", "20:19:00", 66, 48, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 71
exec AddWorkshop "2013-2-12", "17:33:00", "19:33:00", 100, 48, "EuroCOW the Calibration and Orientation Workshop", 46
exec AddWorkshop "2011-6-13", "18:51:00", "20:51:00", 139, 37, "X-Ray Universe", 42
exec AddWorkshop "2013-4-30", "14:33:00", "15:33:00", 100, 30, "European Navigation Conference 2014 (ENC-GNSS 2014)", 51
exec AddWorkshop "2013-3-4", "18:19:00", "20:19:00", 145, 47, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 58
exec AddWorkshop "2011-11-29", "12:11:00", "13:11:00", 52, 35, "Science and Challenges of Lunar Sample Return Workshop", 51
exec AddWorkshop "2013-11-27", "19:25:00", "20:25:00", 118, 35, "The 12th annual Responsible Business Summit", 66
exec AddWorkshop "2012-9-10", "16:21:00", "17:21:00", 81, 45, "ICATT", 50
exec AddWorkshop "2013-11-16", "8:18:00", "11:18:00", 125, 30, "Net Impact Conference", 82
exec AddWorkshop "2013-6-14", "8:35:00", "11:35:00", 103, 38, "European Space Technology Harmonisation Conference", 4
exec AddWorkshop "2013-1-12", "9:9:00", "10:9:00", 104, 40, "Net Impact Conference", 6
exec AddWorkshop "2011-2-6", "19:4:00", "21:4:00", 131, 42, "Euclid Spacecraft Industry Day", 74
exec AddWorkshop "2011-11-6", "12:38:00", "15:38:00", 116, 46, "X-Ray Universe", 22
exec AddWorkshop "2013-2-11", "8:48:00", "9:48:00", 126, 46, "International Joint Conference on Automated Reasoning", 76
exec AddWorkshop "2011-9-19", "12:49:00", "13:49:00", 82, 40, "European Conference on Artificial Intelligence	", 60
exec AddWorkshop "2013-11-7", "19:38:00", "20:38:00", 53, 29, "Workshop on Image Processing", 27
exec AddWorkshop "2013-9-29", "11:47:00", "12:47:00", 105, 45, "Net Impact Conference", 27
exec AddWorkshop "2012-12-2", "10:47:00", "11:47:00", 69, 30, "European Conference on Artificial Intelligence	", 6
exec AddWorkshop "2013-9-21", "12:45:00", "13:45:00", 130, 48, "Net Impact Conference", 68
exec AddWorkshop "2012-7-20", "13:43:00", "16:43:00", 64, 32, "International Conference on Autonomous Agents and", 54
exec AddWorkshop "2011-1-23", "19:56:00", "21:56:00", 64, 32, "Photonics West", 64
exec AddWorkshop "2012-7-29", "12:59:00", "14:59:00", 108, 22, "KU Leuven CSR Symposium", 52
exec AddWorkshop "2013-5-6", "19:44:00", "22:44:00", 129, 25, "GNC 2014", 16
exec AddWorkshop "2012-12-21", "17:55:00", "19:55:00", 110, 34, "ICATT", 42
exec AddWorkshop "2011-4-12", "17:48:00", "19:48:00", 140, 40, "Ceres Conference", 28
exec AddWorkshop "2012-6-25", "15:34:00", "17:34:00", 123, 41, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 25
exec AddWorkshop "2013-5-30", "18:33:00", "21:33:00", 148, 39, "International Conference on Computer Vision Theory and Applications", 83
exec AddWorkshop "2012-2-9", "18:31:00", "20:31:00", 143, 47, "Sustainable Brands Conference", 58
exec AddWorkshop "2011-2-20", "8:53:00", "10:53:00", 63, 25, "4th DUE Permafrost User Workshop", 6
exec AddWorkshop "2012-5-25", "11:14:00", "13:14:00", 138, 40, "Annual Convention of The Society", 59
exec AddWorkshop "2012-3-30", "12:55:00", "15:55:00", 123, 43, "The 2nd International CSR Communication Conference", 67
exec AddWorkshop "2013-7-10", "9:26:00", "12:26:00", 55, 37, "LPVE Land product validation and evolution", 53
exec AddWorkshop "2013-8-25", "15:40:00", "16:40:00", 130, 47, "Global Conference on Sustainability and Reporting", 72
exec AddWorkshop "2012-4-22", "8:25:00", "11:25:00", 62, 40, "ICATT", 63
exec AddWorkshop "2011-6-9", "10:21:00", "13:21:00", 60, 21, "International Semantic Web Conference		", 58
exec AddWorkshop "2013-2-14", "9:1:00", "10:1:00", 53, 26, "4S Symposium 2014", 75
exec AddWorkshop "2013-10-7", "11:27:00", "14:27:00", 90, 29, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 38
exec AddWorkshop "2013-3-8", "11:20:00", "13:20:00", 114, 26, "International Conference on Logic for Programming", 53
exec AddWorkshop "2011-12-18", "9:51:00", "12:51:00", 73, 37, "The 2nd International CSR Communication Conference", 26
exec AddWorkshop "2011-7-7", "10:12:00", "13:12:00", 115, 48, "International Conference on Logic for Programming", 46
exec AddWorkshop "2012-7-28", "11:10:00", "12:10:00", 136, 42, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 1
exec AddWorkshop "2012-8-20", "18:32:00", "20:32:00", 122, 31, "Sentinel-2 for Science WS", 61
exec AddWorkshop "2013-5-15", "8:47:00", "10:47:00", 110, 42, "International Conference on Signal and Imaging Systems Engineering", 57
exec AddWorkshop "2012-4-6", "9:58:00", "10:58:00", 120, 38, "International Conference on Logic for Programming", 10
exec AddWorkshop "2012-12-11", "14:11:00", "17:11:00", 146, 25, "Euclid Spacecraft Industry Day", 37
exec AddWorkshop "2011-8-24", "12:28:00", "15:28:00", 116, 35, "Photonics West", 76
exec AddWorkshop "2012-1-10", "15:8:00", "18:8:00", 88, 21, "EuroCOW the Calibration and Orientation Workshop", 60
exec AddWorkshop "2013-12-1", "10:25:00", "12:25:00", 132, 38, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 68
exec AddWorkshop "2012-5-6", "8:2:00", "9:2:00", 56, 22, "Ceres Conference", 62
exec AddWorkshop "2012-10-15", "18:21:00", "19:21:00", 138, 38, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 66
exec AddWorkshop "2012-2-27", "13:11:00", "14:11:00", 141, 26, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 71
exec AddWorkshop "2013-7-18", "19:3:00", "22:3:00", 60, 41, "The BCLC CSR Conference", 63
exec AddWorkshop "2011-7-21", "13:57:00", "16:57:00", 96, 45, "The 12th annual Responsible Business Summit", 68
exec AddWorkshop "2012-10-10", "13:55:00", "15:55:00", 106, 22, "4th DUE Permafrost User Workshop", 38
exec AddWorkshop "2013-7-19", "15:41:00", "16:41:00", 145, 40, "International Conference on MultiMedia Modeling", 1
exec AddWorkshop "2012-7-19", "14:38:00", "17:38:00", 127, 49, "The BCLC CSR Conference", 13
exec AddWorkshop "2012-11-7", "8:33:00", "10:33:00", 66, 36, "International Conference on Artificial Neural Networks", 12
exec AddWorkshop "2013-12-21", "9:17:00", "11:17:00", 74, 30, "Foundations of Genetic Algorithms		", 50
exec AddWorkshop "2012-2-23", "18:50:00", "20:50:00", 95, 42, "Leadership Strategies for Information Technology in Health Care Boston", 51
exec AddWorkshop "2012-6-4", "18:33:00", "20:33:00", 90, 24, "Workshop on Logic ", 0
exec AddWorkshop "2012-2-1", "17:51:00", "20:51:00", 121, 21, "International Conference on Computer Vision", 43
exec AddWorkshop "2011-11-29", "19:16:00", "20:16:00", 124, 35, "Global Conference on Sustainability and Reporting", 50
exec AddWorkshop "2013-3-20", "9:25:00", "10:25:00", 82, 38, "48th ESLAB Symposium", 26
exec AddWorkshop "2012-9-16", "15:53:00", "16:53:00", 133, 28, "Workshop on Image Processing", 81
exec AddWorkshop "2013-2-22", "12:32:00", "13:32:00", 136, 28, "7 th International Conference on Bio-inspired Systems and Signal Processing", 0
exec AddWorkshop "2013-6-20", "8:8:00", "10:8:00", 51, 33, "Sentinel-2 for Science WS", 84
exec AddWorkshop "2011-7-1", "9:9:00", "12:9:00", 136, 46, "International Conference on Autonomous Agents and", 79
exec AddWorkshop "2011-2-9", "14:30:00", "15:30:00", 147, 25, "Life in Space for Life on Earth Symposium", 82
exec AddWorkshop "2013-8-30", "12:6:00", "15:6:00", 122, 34, "International Joint Conference on Automated Reasoning", 8
exec AddWorkshop "2012-6-22", "9:6:00", "10:6:00", 80, 43, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 83
exec AddWorkshop "2012-3-1", "15:50:00", "16:50:00", 58, 25, "4th DUE Permafrost User Workshop", 11
exec AddWorkshop "2011-11-4", "17:3:00", "18:3:00", 118, 33, "The Corporate Philanthropy Forum", 10
exec AddWorkshop "2012-4-19", "13:1:00", "15:1:00", 146, 31, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 12
exec AddWorkshop "2012-5-23", "9:52:00", "10:52:00", 110, 31, "Conference on Learning Theory		", 24
exec AddWorkshop "2013-2-4", "14:51:00", "16:51:00", 54, 29, "International Conference on Artificial Neural Networks", 15
exec AddWorkshop "2012-6-28", "13:29:00", "15:29:00", 57, 40, "Microwave Workshop", 43
exec AddWorkshop "2013-7-5", "10:2:00", "13:2:00", 75, 43, "Cause Marketing Forum", 59
exec AddWorkshop "2012-1-26", "17:13:00", "20:13:00", 118, 38, "KU Leuven CSR Symposium", 0
exec AddWorkshop "2013-2-29", "14:45:00", "15:45:00", 124, 46, "Business4Better: The Community Partnership Movement", 38
exec AddWorkshop "2011-7-18", "11:8:00", "14:8:00", 52, 49, "European Conference on Computer Vision", 4
exec AddWorkshop "2013-5-11", "8:11:00", "9:11:00", 53, 38, "European Navigation Conference 2014 (ENC-GNSS 2014)", 36
exec AddWorkshop "2012-7-30", "15:59:00", "16:59:00", 108, 38, "Ceres Conference", 69
exec AddWorkshop "2011-7-3", "10:18:00", "13:18:00", 89, 24, "48th ESLAB Symposium", 52
exec AddWorkshop "2011-5-6", "14:56:00", "17:56:00", 124, 41, "Workshop on Image Processing", 58
exec AddWorkshop "2013-3-14", "13:17:00", "14:17:00", 103, 38, "Conference on Automated Deduction		", 30
exec AddWorkshop "2011-1-3", "19:51:00", "22:51:00", 52, 43, "Photonics West", 21
exec AddWorkshop "2011-2-2", "10:11:00", "12:11:00", 108, 44, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 46
exec AddWorkshop "2012-9-4", "8:9:00", "10:9:00", 132, 20, "European Conference on Computer Vision", 49
exec AddWorkshop "2011-11-16", "10:37:00", "13:37:00", 91, 35, "48th ESLAB Symposium", 54
exec AddWorkshop "2013-5-1", "13:7:00", "15:7:00", 114, 35, "ICATT", 25
exec AddWorkshop "2011-4-22", "14:53:00", "17:53:00", 60, 31, "Photonics West", 67
exec AddWorkshop "2012-5-14", "9:60:00", "12:60:00", 97, 28, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 59
exec AddWorkshop "2012-3-21", "13:60:00", "15:60:00", 61, 41, "�International� Corporate Citizenship Conference", 52
exec AddWorkshop "2012-11-20", "9:47:00", "10:47:00", 115, 29, "European Space Technology Harmonisation Conference", 81
exec AddWorkshop "2011-5-22", "13:17:00", "16:17:00", 119, 46, "RuleML Symposium				", 39
exec AddWorkshop "2012-10-12", "18:12:00", "19:12:00", 119, 41, "Conference on Uncertainty in Artificial Intelligence", 77
exec AddWorkshop "2012-10-23", "15:43:00", "17:43:00", 124, 27, "Workshop on Image Processing", 41
exec AddWorkshop "2011-7-29", "18:48:00", "19:48:00", 73, 44, "The Corporate Philanthropy Forum", 25
exec AddWorkshop "2012-9-5", "11:36:00", "14:36:00", 62, 32, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 44
exec AddWorkshop "2012-2-29", "15:10:00", "18:10:00", 133, 31, "European Conference on Artificial Intelligence	", 73
exec AddWorkshop "2013-2-12", "12:23:00", "15:23:00", 75, 47, "International Conference on Signal and Imaging Systems Engineering", 69
exec AddWorkshop "2011-8-21", "15:48:00", "16:48:00", 66, 44, "Workshop on Logic ", 76
exec AddWorkshop "2013-6-9", "12:40:00", "15:40:00", 70, 47, "X-Ray Universe", 2
exec AddWorkshop "2013-3-5", "14:46:00", "15:46:00", 89, 23, "European Navigation Conference 2014 (ENC-GNSS 2014)", 45
exec AddWorkshop "2012-7-17", "18:54:00", "21:54:00", 117, 47, "Asian Conference on Computer Vision", 41
exec AddWorkshop "2012-2-14", "9:5:00", "10:5:00", 98, 23, "International Joint Conference on Automated Reasoning", 60
exec AddWorkshop "2013-4-5", "15:33:00", "17:33:00", 128, 37, "7 th International Conference on Bio-inspired Systems and Signal Processing", 27
exec AddWorkshop "2013-10-27", "18:28:00", "20:28:00", 58, 34, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 84
exec AddWorkshop "2013-10-29", "14:28:00", "16:28:00", 66, 36, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 11
exec AddWorkshop "2011-5-23", "8:58:00", "9:58:00", 149, 29, "International Semantic Web Conference		", 75
exec AddWorkshop "2012-4-9", "12:5:00", "13:5:00", 82, 27, "International Conference on the Principles of", 13
exec AddWorkshop "2011-10-17", "17:31:00", "19:31:00", 109, 36, "European Navigation Conference 2014 (ENC-GNSS 2014)", 60
exec AddWorkshop "2012-7-12", "15:35:00", "17:35:00", 96, 43, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 79
exec AddWorkshop "2013-12-12", "17:25:00", "20:25:00", 102, 27, "Business4Better: The Community Partnership Movement", 50
exec AddWorkshop "2012-2-6", "13:60:00", "14:60:00", 128, 40, "Asian Conference on Computer Vision", 67
exec AddWorkshop "2012-2-1", "10:13:00", "12:13:00", 64, 31, "3rd International Conference on Pattern Recognition Applications and Methods", 29
exec AddWorkshop "2013-12-10", "8:43:00", "9:43:00", 143, 26, "RuleML Symposium				", 39
exec AddWorkshop "2013-11-7", "14:39:00", "15:39:00", 55, 47, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 13
exec AddWorkshop "2011-11-6", "17:4:00", "20:4:00", 104, 32, "Annual Convention of The Society", 77
exec AddWorkshop "2012-2-10", "15:37:00", "16:37:00", 149, 32, "4th DUE Permafrost User Workshop", 72
exec AddWorkshop "2013-8-23", "15:51:00", "18:51:00", 74, 37, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 26
exec AddWorkshop "2012-5-27", "9:42:00", "12:42:00", 62, 28, "2013 Ethical Leadership Conference: Ethics in Action", 19
exec AddWorkshop "2011-7-18", "16:42:00", "19:42:00", 95, 29, "The Sixth International Conferences on Advances in Multimedia", 53
exec AddWorkshop "2011-3-24", "8:7:00", "10:7:00", 68, 41, "Conference on Automated Deduction		", 53
exec AddWorkshop "2013-1-19", "17:23:00", "19:23:00", 107, 30, "International Conference on Signal and Imaging Systems Engineering", 29
exec AddWorkshop "2011-7-28", "9:38:00", "12:38:00", 149, 32, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 72
exec AddWorkshop "2011-4-5", "9:36:00", "11:36:00", 82, 21, "European Conference on Machine Learning", 71
exec AddWorkshop "2011-6-12", "15:32:00", "17:32:00", 133, 40, "International Conference on Pattern Recognition", 0
exec AddWorkshop "2013-6-28", "14:27:00", "16:27:00", 82, 20, "Net Impact Conference", 62
exec AddWorkshop "2013-7-19", "13:8:00", "15:8:00", 143, 33, "Annual Interdisciplinary Conference", 81
exec AddWorkshop "2011-1-6", "12:41:00", "15:41:00", 50, 22, "RuleML Symposium				", 44
exec AddWorkshop "2011-1-22", "14:43:00", "16:43:00", 143, 46, "The Sixth International Conferences on Advances in Multimedia", 27
exec AddWorkshop "2013-6-4", "15:22:00", "17:22:00", 107, 47, "International Joint Conference on Automated Reasoning", 41
exec AddWorkshop "2011-9-25", "11:10:00", "12:10:00", 141, 31, "European Space Power Conference", 32
exec AddWorkshop "2011-8-2", "9:24:00", "11:24:00", 52, 22, "International Conference on MultiMedia Modeling", 83
exec AddWorkshop "2011-6-17", "16:46:00", "19:46:00", 113, 36, "The 2nd International CSR Communication Conference", 64
exec AddWorkshop "2013-3-20", "19:13:00", "20:13:00", 71, 39, "Global Conference on Sustainability and Reporting", 36
exec AddWorkshop "2013-10-27", "18:3:00", "20:3:00", 67, 32, "Corporate Community Involvement Conference", 15
exec AddWorkshop "2012-8-3", "19:17:00", "21:17:00", 141, 38, "Annual Interdisciplinary Conference", 48
exec AddWorkshop "2012-2-6", "12:27:00", "13:27:00", 55, 36, "International Conference on Artificial Neural Networks", 62
exec AddWorkshop "2013-11-17", "13:48:00", "15:48:00", 136, 27, "LPVE Land product validation and evolution", 50
exec AddWorkshop "2011-12-14", "12:23:00", "13:23:00", 136, 27, "British	Machine	Vision	Conference		", 83
exec AddWorkshop "2013-4-4", "17:42:00", "18:42:00", 134, 28, "International Conference on Artificial Neural Networks", 23
exec AddWorkshop "2011-12-20", "8:39:00", "10:39:00", 113, 29, "4S Symposium 2014", 59
exec AddWorkshop "2012-2-4", "13:52:00", "16:52:00", 130, 28, "ARTES 1 Final Presentation Days", 50
exec AddWorkshop "2011-8-7", "9:3:00", "11:3:00", 101, 27, "RuleML Symposium		", 22
exec AddWorkshop "2013-2-12", "16:40:00", "17:40:00", 97, 36, "Asian Conference on Computer Vision", 67
exec AddWorkshop "2011-7-13", "19:58:00", "22:58:00", 108, 40, "RuleML Symposium		", 30
exec AddWorkshop "2012-7-18", "19:52:00", "20:52:00", 98, 44, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 31
exec AddWorkshop "2012-12-28", "10:35:00", "12:35:00", 106, 36, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 44
exec AddWorkshop "2013-9-22", "15:56:00", "18:56:00", 135, 38, "Annual Convention of The Society", 1
exec AddWorkshop "2011-3-17", "12:4:00", "15:4:00", 87, 40, "Science and Challenges of Lunar Sample Return Workshop", 28
exec AddWorkshop "2012-3-17", "15:3:00", "16:3:00", 91, 44, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 77
exec AddWorkshop "2012-9-29", "15:34:00", "18:34:00", 127, 49, "RuleML Symposium		", 83
exec AddWorkshop "2011-12-25", "14:13:00", "16:13:00", 65, 32, "International Conference on the Principles of", 83
exec AddWorkshop "2011-1-2", "9:6:00", "11:6:00", 59, 42, "International Conference on Automated Reasoning", 69
exec AddWorkshop "2011-1-21", "15:52:00", "18:52:00", 68, 42, "Leadership Strategies for Information Technology in Health Care Boston", 79
exec AddWorkshop "2012-6-16", "9:22:00", "10:22:00", 116, 31, "Annual Convention of The Society", 5
exec AddWorkshop "2012-12-21", "8:13:00", "10:13:00", 103, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 21
exec AddWorkshop "2012-10-10", "12:28:00", "13:28:00", 144, 34, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 4
exec AddWorkshop "2012-8-12", "12:50:00", "13:50:00", 51, 44, "Annual Convention of The Society", 80
exec AddWorkshop "2012-6-28", "9:21:00", "12:21:00", 102, 26, "Mayo Clinic Presents", 40
exec AddWorkshop "2013-2-12", "13:20:00", "15:20:00", 96, 39, "KU Leuven CSR Symposium", 17
exec AddWorkshop "2013-4-26", "13:37:00", "15:37:00", 66, 39, "4S Symposium 2014", 72
exec AddWorkshop "2011-7-18", "9:20:00", "12:20:00", 117, 48, "International Conference on Signal and Imaging Systems Engineering", 24
exec AddWorkshop "2011-2-24", "15:33:00", "16:33:00", 88, 23, "4S Symposium 2014", 14
exec AddWorkshop "2013-6-14", "12:26:00", "13:26:00", 128, 34, "The Sixth International Conferences on Advances in Multimedia", 23
exec AddWorkshop "2013-1-13", "19:45:00", "21:45:00", 52, 43, "Conference on Automated Deduction		", 44
exec AddWorkshop "2013-12-20", "19:18:00", "21:18:00", 108, 34, "Photonics West", 6
exec AddWorkshop "2012-5-24", "13:20:00", "15:20:00", 83, 42, "2013 Ethical Leadership Conference: Ethics in Action", 14
exec AddWorkshop "2013-1-15", "14:24:00", "15:24:00", 116, 47, "Microwave Workshop", 58
exec AddWorkshop "2012-10-25", "15:26:00", "18:26:00", 93, 47, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 17
exec AddWorkshop "2011-10-27", "15:34:00", "16:34:00", 83, 49, "International Conference on Artificial Neural Networks", 57
exec AddWorkshop "2012-3-3", "13:33:00", "15:33:00", 59, 34, "2013 Ethical Leadership Conference: Ethics in Action", 38
exec AddWorkshop "2013-8-8", "13:2:00", "16:2:00", 69, 22, "X-Ray Universe", 1
exec AddWorkshop "2013-9-18", "16:23:00", "18:23:00", 62, 34, "The Corporate Philanthropy Forum", 58
exec AddWorkshop "2011-11-30", "11:40:00", "13:40:00", 54, 39, "LPVE Land product validation and evolution", 39
exec AddWorkshop "2012-7-19", "18:13:00", "20:13:00", 131, 34, "International Joint Conference on Automated Reasoning", 59
exec AddWorkshop "2011-12-5", "19:2:00", "22:2:00", 117, 36, "KU Leuven CSR Symposium", 25
exec AddWorkshop "2012-11-30", "9:11:00", "12:11:00", 101, 49, "Annual Interdisciplinary Conference", 7
exec AddWorkshop "2013-10-14", "13:30:00", "14:30:00", 127, 38, "Cause Marketing Forum", 11
exec AddWorkshop "2013-8-28", "11:23:00", "12:23:00", 81, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 40
exec AddWorkshop "2011-4-26", "16:25:00", "19:25:00", 52, 28, "The Sixth International Conferences on Advances in Multimedia", 42
exec AddWorkshop "2011-3-22", "13:48:00", "14:48:00", 101, 37, "International Conference on Pattern Recognition", 11
exec AddWorkshop "2012-6-3", "14:6:00", "16:6:00", 134, 26, "Global Conference on Sustainability and Reporting", 38
exec AddWorkshop "2011-12-2", "10:33:00", "13:33:00", 131, 34, "7 th International Conference on Bio-inspired Systems and Signal Processing", 32
exec AddWorkshop "2013-5-13", "11:11:00", "14:11:00", 89, 26, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 39
exec AddWorkshop "2013-6-10", "14:6:00", "15:6:00", 143, 33, "Annual Convention of The Society", 10
exec AddWorkshop "2011-10-19", "9:15:00", "10:15:00", 130, 38, "European Conference on Computer Vision", 72
exec AddWorkshop "2011-4-7", "15:5:00", "16:5:00", 119, 43, "The BCLC CSR Conference", 13
exec AddWorkshop "2011-1-10", "14:4:00", "16:4:00", 63, 45, "Sustainable Brands Conference", 24
exec AddWorkshop "2011-8-29", "11:8:00", "14:8:00", 75, 46, "International Conference on Signal and Imaging Systems Engineering", 6
exec AddWorkshop "2012-3-26", "18:43:00", "21:43:00", 82, 36, "RuleML Symposium		", 46
exec AddWorkshop "2011-1-21", "11:1:00", "12:1:00", 65, 28, "Euclid Spacecraft Industry Day", 53
exec AddWorkshop "2013-12-15", "9:57:00", "12:57:00", 80, 35, "Ceres Conference", 53
exec AddWorkshop "2012-1-6", "15:22:00", "16:22:00", 101, 33, "Science and Challenges of Lunar Sample Return Workshop", 18
exec AddWorkshop "2013-9-10", "15:28:00", "16:28:00", 125, 30, "RuleML Symposium		", 50
exec AddWorkshop "2011-10-1", "18:55:00", "20:55:00", 139, 28, "Mayo Clinic Presents", 63
exec AddWorkshop "2013-10-28", "15:59:00", "16:59:00", 78, 34, "Science and Challenges of Lunar Sample Return Workshop", 35
exec AddWorkshop "2012-10-7", "11:17:00", "13:17:00", 60, 24, "X-Ray Universe", 68
exec AddWorkshop "2013-10-10", "18:18:00", "21:18:00", 78, 40, "ARTES 1 Final Presentation Days", 26
exec AddWorkshop "2012-4-2", "16:42:00", "19:42:00", 128, 49, "Conference on Computer Vision and Pattern", 75
exec AddWorkshop "2012-12-3", "9:24:00", "12:24:00", 129, 35, "RuleML Symposium		", 53
exec AddWorkshop "2013-12-21", "19:58:00", "22:58:00", 71, 44, "The Sixth International Conferences on Advances in Multimedia", 51
exec AddWorkshop "2013-1-23", "9:58:00", "11:58:00", 57, 26, "European Space Technology Harmonisation Conference", 18
exec AddWorkshop "2012-1-21", "16:59:00", "19:59:00", 146, 49, "EuroCOW the Calibration and Orientation Workshop", 63
exec AddWorkshop "2011-10-16", "9:32:00", "11:32:00", 62, 26, "Corporate Community Involvement Conference", 26
exec AddWorkshop "2011-1-2", "10:23:00", "12:23:00", 67, 36, "3rd International Conference on Pattern Recognition Applications and Methods", 48
exec AddWorkshop "2012-4-16", "16:30:00", "19:30:00", 112, 38, "Conference on Computer Vision and Pattern", 28
exec AddWorkshop "2013-9-6", "12:20:00", "14:20:00", 102, 43, "European Space Technology Harmonisation Conference", 6
exec AddWorkshop "2012-10-6", "16:30:00", "17:30:00", 122, 48, "International Conference on the Principles of", 60
exec AddWorkshop "2012-11-3", "9:49:00", "11:49:00", 66, 34, "Euclid Spacecraft Industry Day", 17
exec AddWorkshop "2011-3-9", "11:7:00", "14:7:00", 75, 30, "Euclid Spacecraft Industry Day", 2
exec AddWorkshop "2013-4-22", "10:36:00", "12:36:00", 72, 39, "ICATT", 31
exec AddWorkshop "2013-12-10", "9:33:00", "11:33:00", 92, 37, "International Joint Conference on Automated Reasoning", 21
exec AddWorkshop "2012-6-20", "8:45:00", "10:45:00", 67, 30, "Global Conference on Sustainability and Reporting", 6
exec AddWorkshop "2011-7-7", "13:30:00", "16:30:00", 148, 24, "The BCLC CSR Conference", 14
exec AddWorkshop "2012-6-11", "19:21:00", "22:21:00", 116, 24, "Euclid Spacecraft Industry Day", 2
exec AddWorkshop "2012-4-19", "8:23:00", "11:23:00", 131, 38, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 36
exec AddWorkshop "2013-6-17", "9:33:00", "11:33:00", 143, 24, "International Conference on Autonomous Agents and", 6
exec AddWorkshop "2012-12-2", "18:32:00", "19:32:00", 139, 48, "4th DUE Permafrost User Workshop", 24
exec AddWorkshop "2012-9-15", "18:21:00", "21:21:00", 79, 38, "The Corporate Philanthropy Forum", 36
exec AddWorkshop "2011-4-4", "15:17:00", "18:17:00", 93, 28, "Photonics West", 66
exec AddWorkshop "2012-11-24", "16:60:00", "19:60:00", 94, 32, "International Conference on Pattern Recognition", 31
exec AddWorkshop "2013-3-14", "14:18:00", "15:18:00", 83, 24, "Client Summit", 61
exec AddWorkshop "2013-7-19", "9:30:00", "10:30:00", 136, 27, "Net Impact Conference", 79
exec AddWorkshop "2011-7-25", "12:29:00", "15:29:00", 63, 21, "Life in Space for Life on Earth Symposium", 49
exec AddWorkshop "2012-2-28", "9:36:00", "12:36:00", 100, 41, "International Conference on Pattern Recognition", 76
exec AddWorkshop "2011-4-7", "16:46:00", "18:46:00", 104, 37, "Ceres Conference", 49
exec AddWorkshop "2012-6-4", "9:22:00", "11:22:00", 115, 29, "The 12th annual Responsible Business Summit", 72
exec AddWorkshop "2012-7-2", "17:10:00", "19:10:00", 94, 21, "Ceres Conference", 15
exec AddWorkshop "2013-3-18", "17:6:00", "18:6:00", 132, 33, "2nd International Conference on Photonics, Optics and Laser Technology", 70
exec AddWorkshop "2012-2-24", "16:40:00", "19:40:00", 83, 25, "2nd International Conference on Photonics, Optics and Laser Technology", 64
exec AddWorkshop "2013-9-12", "9:44:00", "12:44:00", 65, 38, "International Conference on Signal and Imaging Systems Engineering", 67
exec AddWorkshop "2013-7-23", "15:24:00", "16:24:00", 104, 38, "The Corporate Philanthropy Forum", 54
exec AddWorkshop "2011-2-15", "17:53:00", "20:53:00", 69, 43, "International Conference on the Principles of", 29
exec AddWorkshop "2013-10-18", "14:49:00", "17:49:00", 89, 22, "RuleML Symposium		", 68
exec AddWorkshop "2013-6-11", "11:45:00", "12:45:00", 114, 35, "Conference on Automated Deduction		", 2
exec AddWorkshop "2011-1-7", "8:45:00", "11:45:00", 133, 28, "4th DUE Permafrost User Workshop", 74
exec AddWorkshop "2013-1-14", "17:47:00", "20:47:00", 88, 25, "3rd International Conference on Pattern Recognition Applications and Methods", 45
exec AddWorkshop "2011-7-6", "16:51:00", "18:51:00", 136, 43, "International Conference on Automated Reasoning", 67
exec AddWorkshop "2011-2-28", "11:59:00", "13:59:00", 77, 49, "2013 Ethical Leadership Conference: Ethics in Action", 65
exec AddWorkshop "2013-10-20", "9:57:00", "12:57:00", 70, 45, "The 2nd International CSR Communication Conference", 18
exec AddWorkshop "2012-5-30", "18:59:00", "21:59:00", 115, 46, "International Semantic Web Conference		", 46
exec AddWorkshop "2011-1-8", "15:31:00", "18:31:00", 104, 49, "International Conference on Automated Reasoning", 50
exec AddWorkshop "2012-7-22", "14:41:00", "15:41:00", 56, 46, "Conference on Learning Theory		", 26
exec AddWorkshop "2012-1-24", "12:43:00", "15:43:00", 105, 30, "Annual Interdisciplinary Conference", 76
exec AddWorkshop "2013-1-21", "16:1:00", "18:1:00", 109, 41, "International Joint Conference on Artificial Intelligence", 24
exec AddWorkshop "2012-5-13", "19:17:00", "22:17:00", 124, 29, "International Conference on Logic for Programming", 47
exec AddWorkshop "2013-3-4", "16:33:00", "18:33:00", 69, 21, "Mechanisms Final Presentation Days", 21
exec AddWorkshop "2013-12-12", "19:49:00", "21:49:00", 75, 44, "EuroCOW the Calibration and Orientation Workshop", 84
exec AddWorkshop "2012-9-24", "18:45:00", "20:45:00", 132, 27, "Business4Better: The Community Partnership Movement", 3
exec AddWorkshop "2011-3-21", "19:32:00", "21:32:00", 82, 34, "Asian Conference on Computer Vision", 20
exec AddWorkshop "2013-10-18", "19:17:00", "22:17:00", 134, 43, "Workshop on Logic ", 38
exec AddWorkshop "2011-8-21", "19:48:00", "21:48:00", 73, 42, "International Conference on Automated Planning", 24
exec AddWorkshop "2013-4-17", "17:36:00", "18:36:00", 121, 38, "Cause Marketing Forum", 83
exec AddWorkshop "2011-9-6", "10:28:00", "11:28:00", 143, 30, "Mayo Clinic Presents", 34
exec AddWorkshop "2013-4-16", "13:23:00", "15:23:00", 67, 44, "Conference on Computer Vision and Pattern", 15
exec AddWorkshop "2012-7-16", "11:2:00", "13:2:00", 92, 37, "International Conference on Logic for Programming", 52
exec AddWorkshop "2011-3-20", "13:34:00", "16:34:00", 131, 33, "Life in Space for Life on Earth Symposium", 58
exec AddWorkshop "2011-1-24", "8:34:00", "10:34:00", 136, 42, "International Conference on Automated Reasoning", 55
exec AddWorkshop "2012-8-17", "10:53:00", "11:53:00", 79, 48, "Conference on Uncertainty in Artificial Intelligence", 84
exec AddWorkshop "2011-5-25", "18:19:00", "19:19:00", 89, 37, "International Conference on Computer Vision", 5
exec AddWorkshop "2011-11-14", "11:50:00", "14:50:00", 89, 44, "Photonics West", 47
exec AddWorkshop "2011-3-26", "14:32:00", "15:32:00", 74, 36, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 66
exec AddWorkshop "2012-6-12", "12:26:00", "15:26:00", 78, 26, "ICATT", 7
exec AddWorkshop "2012-4-22", "12:53:00", "13:53:00", 117, 37, "Science and Challenges of Lunar Sample Return Workshop", 60
exec AddWorkshop "2013-11-19", "8:1:00", "10:1:00", 51, 22, "Workshop on Logic ", 55
exec AddWorkshop "2011-8-13", "18:10:00", "21:10:00", 127, 37, "RuleML Symposium		", 24
exec AddWorkshop "2012-10-11", "12:23:00", "14:23:00", 121, 28, "International Conference on Autonomous Agents and", 19
exec AddWorkshop "2012-6-27", "18:59:00", "19:59:00", 122, 38, "Ceres Conference", 56
exec AddWorkshop "2011-8-24", "9:24:00", "10:24:00", 73, 48, "Ceres Conference", 40
exec AddWorkshop "2012-8-16", "12:5:00", "13:5:00", 107, 42, "International Conference on Artificial Neural Networks", 54
exec AddWorkshop "2011-3-23", "8:26:00", "11:26:00", 132, 30, "RuleML Symposium				", 79
exec AddWorkshop "2011-7-24", "13:54:00", "15:54:00", 96, 27, "International Joint Conference on Automated Reasoning", 20
exec AddWorkshop "2012-3-12", "13:37:00", "15:37:00", 109, 43, "Workshop on Image Processing", 22
exec AddWorkshop "2013-12-21", "15:8:00", "16:8:00", 92, 34, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 50
exec AddWorkshop "2013-1-1", "15:18:00", "17:18:00", 56, 26, "4th DUE Permafrost User Workshop", 59
exec AddWorkshop "2012-4-17", "11:47:00", "13:47:00", 61, 34, "Corporate Community Involvement Conference", 71
exec AddWorkshop "2011-10-21", "10:58:00", "13:58:00", 135, 48, "International Conference on Automated Planning", 9
exec AddWorkshop "2013-5-2", "8:6:00", "9:6:00", 138, 30, "3rd International Conference on Pattern Recognition Applications and Methods", 74
exec AddWorkshop "2011-3-17", "11:58:00", "13:58:00", 67, 38, "3rd International Conference on Pattern Recognition Applications and Methods", 10
exec AddWorkshop "2012-6-29", "18:10:00", "19:10:00", 122, 29, "European Conference on Artificial Intelligence	", 84
exec AddWorkshop "2011-4-21", "19:38:00", "20:38:00", 128, 40, "European Navigation Conference 2014 (ENC-GNSS 2014)", 4
exec AddWorkshop "2012-4-12", "14:14:00", "16:14:00", 68, 27, "The BCLC CSR Conference", 34
exec AddWorkshop "2013-10-25", "8:25:00", "11:25:00", 106, 48, "Computer Analysis of Images and Patterns", 1
exec AddWorkshop "2013-9-27", "11:30:00", "12:30:00", 104, 38, "GNC 2014", 13
exec AddWorkshop "2011-4-11", "16:9:00", "17:9:00", 129, 43, "Foundations of Genetic Algorithms		", 15
exec AddWorkshop "2012-4-29", "13:16:00", "14:16:00", 65, 37, "International Conference on the Principles of", 25
exec AddWorkshop "2013-4-12", "19:45:00", "21:45:00", 94, 28, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 32
exec AddWorkshop "2012-1-22", "8:34:00", "11:34:00", 63, 41, "Asian Conference on Computer Vision", 45
exec AddWorkshop "2012-10-27", "10:52:00", "13:52:00", 147, 31, "EuroCOW the Calibration and Orientation Workshop", 48
exec AddWorkshop "2012-11-27", "16:53:00", "17:53:00", 127, 20, "International Conference on the Principles of", 81
exec AddWorkshop "2013-6-29", "15:33:00", "17:33:00", 51, 23, "Microwave Workshop", 75
exec AddWorkshop "2013-4-2", "19:34:00", "21:34:00", 54, 44, "Foundations of Genetic Algorithms		", 72
exec AddWorkshop "2012-2-15", "12:59:00", "15:59:00", 125, 47, "ARTES 1 Final Presentation Days", 20
exec AddWorkshop "2012-5-29", "19:22:00", "20:22:00", 51, 24, "Ceres Conference", 1
exec AddWorkshop "2013-6-11", "19:60:00", "21:60:00", 132, 43, "International Conference on Signal and Imaging Systems Engineering", 21
exec AddWorkshop "2012-10-17", "9:4:00", "12:4:00", 93, 36, "International Joint Conference on Automated Reasoning", 47
exec AddWorkshop "2013-3-25", "16:58:00", "17:58:00", 66, 42, "International Conference on Artificial Neural Networks", 38
exec AddWorkshop "2013-11-6", "17:25:00", "18:25:00", 140, 22, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 22
exec AddWorkshop "2011-10-9", "10:5:00", "13:5:00", 117, 44, "Photonics West", 3
exec AddWorkshop "2011-8-10", "19:56:00", "21:56:00", 54, 37, "International Conference on Automated Reasoning", 32
exec AddWorkshop "2012-10-23", "14:57:00", "16:57:00", 83, 34, "International Semantic Web Conference		", 6
exec AddWorkshop "2013-5-1", "18:6:00", "20:6:00", 120, 39, "X-Ray Universe", 36
exec AddWorkshop "2011-7-28", "18:18:00", "21:18:00", 128, 21, "GNC 2014", 83
exec AddWorkshop "2012-5-29", "16:49:00", "19:49:00", 89, 27, "EuroCOW the Calibration and Orientation Workshop", 36
exec AddWorkshop "2012-12-16", "8:51:00", "10:51:00", 54, 25, "Photonics West", 21
exec AddWorkshop "2012-5-2", "14:56:00", "17:56:00", 66, 35, "7 th International Conference on Bio-inspired Systems and Signal Processing", 5
exec AddWorkshop "2012-10-22", "10:4:00", "11:4:00", 116, 40, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 10
exec AddWorkshop "2012-12-30", "13:33:00", "14:33:00", 80, 21, "Global Conference on Sustainability and Reporting", 61
exec AddWorkshop "2011-3-17", "17:35:00", "18:35:00", 70, 22, "British	Machine	Vision	Conference		", 11
exec AddWorkshop "2012-10-16", "13:50:00", "15:50:00", 82, 39, "Sentinel-2 for Science WS", 53
exec AddWorkshop "2013-3-7", "11:10:00", "14:10:00", 96, 30, "Business4Better: The Community Partnership Movement", 84
exec AddWorkshop "2011-1-8", "8:25:00", "10:25:00", 87, 20, "International Conference on Signal and Imaging Systems Engineering", 5
exec AddWorkshop "2013-6-10", "10:50:00", "13:50:00", 141, 33, "International Conference on Autonomous Agents and", 80
exec AddWorkshop "2011-2-5", "13:34:00", "14:34:00", 119, 40, "Annual Convention of The Society", 22
exec AddWorkshop "2011-5-27", "15:46:00", "17:46:00", 66, 37, "Computer Analysis of Images and Patterns", 14
exec AddWorkshop "2011-11-19", "18:26:00", "19:26:00", 96, 21, "Science and Challenges of Lunar Sample Return Workshop", 80
exec AddWorkshop "2011-12-10", "8:2:00", "11:2:00", 68, 32, "Global Conference on Sustainability and Reporting", 1
exec AddWorkshop "2011-4-8", "15:58:00", "18:58:00", 105, 49, "EuroCOW the Calibration and Orientation Workshop", 70
exec AddWorkshop "2012-4-17", "13:34:00", "16:34:00", 70, 23, "The 12th annual Responsible Business Summit", 68
exec AddWorkshop "2013-8-18", "17:29:00", "20:29:00", 53, 38, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 73
exec AddWorkshop "2013-9-28", "16:3:00", "19:3:00", 147, 36, "Photonics West", 69
exec AddWorkshop "2011-6-11", "19:1:00", "20:1:00", 97, 27, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 77
exec AddWorkshop "2012-3-1", "11:28:00", "12:28:00", 63, 38, "International Conference on Signal and Imaging Systems Engineering", 46
exec AddWorkshop "2013-9-29", "15:8:00", "16:8:00", 133, 39, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 4
exec AddWorkshop "2013-1-5", "13:20:00", "16:20:00", 126, 34, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 28
exec AddWorkshop "2012-12-18", "15:11:00", "18:11:00", 99, 43, "Life in Space for Life on Earth Symposium", 29
exec AddWorkshop "2013-2-26", "16:35:00", "17:35:00", 73, 40, "European Conference on Machine Learning", 40
exec AddWorkshop "2011-5-8", "8:30:00", "10:30:00", 107, 47, "Global Conference on Sustainability and Reporting", 57
exec AddWorkshop "2012-11-29", "10:30:00", "11:30:00", 141, 28, "Conference on Uncertainty in Artificial Intelligence", 80
exec AddWorkshop "2011-4-6", "15:18:00", "16:18:00", 75, 44, "International Conference on the Principles of", 48
exec AddWorkshop "2012-12-21", "10:8:00", "11:8:00", 130, 22, "Workshop on Logic ", 74
exec AddWorkshop "2013-11-27", "16:47:00", "18:47:00", 68, 42, "European Space Technology Harmonisation Conference", 81
exec AddWorkshop "2012-11-1", "9:30:00", "10:30:00", 129, 21, "�International� Corporate Citizenship Conference", 18
exec AddWorkshop "2013-4-14", "8:2:00", "11:2:00", 55, 47, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 84
exec AddWorkshop "2011-2-3", "17:50:00", "20:50:00", 144, 21, "Microwave Workshop", 45
exec AddWorkshop "2011-5-22", "19:22:00", "21:22:00", 51, 44, "Annual Convention of The Society", 19
exec AddWorkshop "2011-8-2", "13:22:00", "15:22:00", 112, 41, "The BCLC CSR Conference", 10
exec AddWorkshop "2013-3-5", "9:22:00", "12:22:00", 139, 36, "Conference on Uncertainty in Artificial Intelligence", 66
exec AddWorkshop "2011-7-11", "18:22:00", "19:22:00", 60, 21, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 81
exec AddWorkshop "2012-2-19", "19:38:00", "21:38:00", 60, 27, "Microwave Workshop", 36
exec AddWorkshop "2011-4-18", "17:4:00", "20:4:00", 121, 44, "EuroCOW the Calibration and Orientation Workshop", 61
exec AddWorkshop "2011-8-24", "13:16:00", "15:16:00", 101, 24, "X-Ray Universe", 4
exec AddWorkshop "2012-2-9", "17:59:00", "18:59:00", 66, 33, "Corporate Community Involvement Conference", 80
exec AddWorkshop "2013-4-25", "17:22:00", "19:22:00", 138, 39, "ICATT", 46
exec AddWorkshop "2013-7-28", "10:19:00", "12:19:00", 126, 27, "X-Ray Universe", 66
exec AddWorkshop "2012-9-3", "15:33:00", "17:33:00", 77, 30, "2nd International Conference on Photonics, Optics and Laser Technology", 3
exec AddWorkshop "2011-8-3", "15:59:00", "17:59:00", 103, 23, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 62
exec AddWorkshop "2012-3-7", "16:28:00", "17:28:00", 51, 39, "Ceres Conference", 42
exec AddWorkshop "2013-3-18", "13:43:00", "15:43:00", 144, 38, "Corporate Community Involvement Conference", 65
exec AddWorkshop "2013-9-13", "17:14:00", "18:14:00", 135, 31, "International Joint Conference on Artificial Intelligence", 57
exec AddWorkshop "2013-11-13", "13:48:00", "16:48:00", 98, 45, "Conference on Uncertainty in Artificial Intelligence", 82
exec AddWorkshop "2011-8-6", "13:41:00", "14:41:00", 61, 37, "International Joint Conference on Automated Reasoning", 77
exec AddWorkshop "2013-3-7", "15:41:00", "17:41:00", 83, 38, "International Conference on Logic for Programming", 78
exec AddWorkshop "2013-11-30", "17:52:00", "18:52:00", 80, 40, "International Conference on Autonomous Agents and", 56
exec AddWorkshop "2013-11-3", "15:44:00", "17:44:00", 99, 43, "�International� Corporate Citizenship Conference", 60
exec AddWorkshop "2011-3-26", "14:24:00", "15:24:00", 87, 21, "X-Ray Universe", 35
exec AddWorkshop "2013-3-8", "15:21:00", "17:21:00", 91, 26, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 48
exec AddWorkshop "2013-1-5", "14:33:00", "16:33:00", 53, 45, "48th ESLAB Symposium", 73
exec AddWorkshop "2011-8-19", "14:30:00", "15:30:00", 135, 45, "European Navigation Conference 2014 (ENC-GNSS 2014)", 19
exec AddWorkshop "2013-6-13", "19:22:00", "22:22:00", 84, 38, "Mayo Clinic Presents", 25
exec AddWorkshop "2011-5-24", "8:31:00", "11:31:00", 60, 48, "European Space Technology Harmonisation Conference", 7
exec AddWorkshop "2011-9-10", "13:58:00", "15:58:00", 64, 40, "X-Ray Universe", 67
exec AddWorkshop "2011-3-14", "18:59:00", "20:59:00", 106, 31, "International Conference on Logic for Programming", 35
exec AddWorkshop "2013-7-3", "16:21:00", "17:21:00", 84, 41, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 52
exec AddWorkshop "2013-8-2", "13:55:00", "16:55:00", 107, 29, "The Sixth International Conferences on Advances in Multimedia", 7
exec AddWorkshop "2011-8-24", "9:43:00", "12:43:00", 76, 38, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 74
exec AddWorkshop "2012-9-9", "13:38:00", "14:38:00", 72, 33, "Conference on Learning Theory		", 43
exec AddWorkshop "2013-2-28", "9:10:00", "10:10:00", 72, 44, "European Conference on Artificial Intelligence	", 25
exec AddWorkshop "2012-12-16", "19:33:00", "21:33:00", 94, 30, "International Conference on Logic for Programming", 23
exec AddWorkshop "2012-6-15", "18:6:00", "21:6:00", 105, 30, "Photonics West", 66
exec AddWorkshop "2012-3-26", "11:4:00", "14:4:00", 65, 43, "International Conference on Autonomous Agents and", 69
exec AddWorkshop "2011-8-18", "15:49:00", "16:49:00", 91, 38, "International Conference on Pattern Recognition", 50
exec AddWorkshop "2011-3-20", "14:25:00", "16:25:00", 112, 40, "Conference on Automated Deduction		", 40
exec AddWorkshop "2012-3-21", "12:8:00", "14:8:00", 118, 28, "Mayo Clinic Presents", 12
exec AddWorkshop "2012-5-9", "15:60:00", "16:60:00", 132, 47, "The 2nd International CSR Communication Conference", 25
exec AddWorkshop "2012-6-10", "10:45:00", "13:45:00", 92, 46, "Photonics West", 4
exec AddWorkshop "2012-3-30", "16:36:00", "18:36:00", 119, 34, "GNC 2014", 75
exec AddWorkshop "2011-1-21", "13:49:00", "15:49:00", 134, 31, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 57
exec AddWorkshop "2013-9-10", "8:56:00", "10:56:00", 120, 33, "KU Leuven CSR Symposium", 62
exec AddWorkshop "2013-3-26", "10:49:00", "13:49:00", 149, 46, "Workshop on Image Processing", 74
exec AddWorkshop "2013-5-18", "16:29:00", "19:29:00", 77, 28, "4th DUE Permafrost User Workshop", 84
exec AddWorkshop "2011-11-30", "14:28:00", "16:28:00", 143, 22, "British	Machine	Vision	Conference		", 67
exec AddWorkshop "2011-1-12", "18:26:00", "20:26:00", 130, 27, "X-Ray Universe", 56
exec AddWorkshop "2013-10-23", "9:12:00", "12:12:00", 63, 35, "Science and Challenges of Lunar Sample Return Workshop", 62
exec AddWorkshop "2012-12-5", "16:4:00", "19:4:00", 91, 24, "Workshop on Image Processing", 12
exec AddWorkshop "2012-8-12", "14:54:00", "17:54:00", 98, 30, "The 2nd International CSR Communication Conference", 52
exec AddWorkshop "2012-3-6", "12:20:00", "14:20:00", 128, 48, "Microwave Workshop", 30
exec AddWorkshop "2013-10-8", "16:26:00", "17:26:00", 127, 24, "Conference on Automated Deduction		", 83
exec AddWorkshop "2012-4-18", "19:51:00", "20:51:00", 137, 49, "The BCLC CSR Conference", 78
exec AddWorkshop "2013-6-7", "9:38:00", "10:38:00", 129, 25, "European Conference on Machine Learning", 40
exec AddWorkshop "2012-2-16", "17:53:00", "19:53:00", 66, 40, "British	Machine	Vision	Conference		", 18
exec AddWorkshop "2013-12-17", "14:13:00", "17:13:00", 130, 47, "Ceres Conference", 77
exec AddWorkshop "2011-3-29", "16:48:00", "18:48:00", 51, 20, "Life in Space for Life on Earth Symposium", 46
exec AddWorkshop "2013-8-20", "14:52:00", "17:52:00", 55, 30, "ARTES 1 Final Presentation Days", 23
exec AddWorkshop "2013-12-17", "16:54:00", "18:54:00", 143, 23, "Mechanisms Final Presentation Days", 50
exec AddWorkshop "2012-8-22", "15:48:00", "16:48:00", 78, 44, "Sustainable Brands Conference", 68
exec AddWorkshop "2012-12-4", "14:1:00", "15:1:00", 61, 44, "RuleML Symposium		", 51
exec AddWorkshop "2011-9-1", "18:43:00", "21:43:00", 64, 33, "Annual Convention of The Society", 75
exec AddWorkshop "2013-11-27", "19:6:00", "21:6:00", 124, 46, "Workshop on Logic ", 47
exec AddWorkshop "2013-8-3", "14:44:00", "17:44:00", 55, 49, "Leadership Strategies for Information Technology in Health Care Boston", 47
exec AddWorkshop "2013-7-25", "18:12:00", "21:12:00", 125, 40, "Conference on Computer Vision and Pattern", 69
exec AddWorkshop "2013-6-3", "15:60:00", "16:60:00", 74, 22, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 72
exec AddWorkshop "2012-6-16", "9:26:00", "11:26:00", 53, 42, "International Conference on Pattern Recognition", 5
exec AddWorkshop "2011-8-14", "19:42:00", "20:42:00", 71, 31, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 51
exec AddWorkshop "2011-10-19", "12:51:00", "14:51:00", 120, 32, "48th ESLAB Symposium", 20
exec AddWorkshop "2013-11-12", "18:14:00", "20:14:00", 145, 39, "European Conference on Computer Vision", 56
exec AddWorkshop "2013-10-5", "15:1:00", "18:1:00", 135, 49, "Photonics West", 13
exec AddWorkshop "2011-12-10", "10:38:00", "12:38:00", 79, 26, "The Corporate Philanthropy Forum", 80
exec AddWorkshop "2013-3-22", "18:37:00", "21:37:00", 113, 41, "Euclid Spacecraft Industry Day", 69
exec AddWorkshop "2013-11-9", "18:24:00", "19:24:00", 128, 45, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 78
exec AddWorkshop "2012-6-23", "11:33:00", "12:33:00", 104, 22, "Life in Space for Life on Earth Symposium", 0
exec AddWorkshop "2011-9-2", "15:12:00", "16:12:00", 120, 38, "Euclid Spacecraft Industry Day", 78
exec AddWorkshop "2012-7-5", "19:47:00", "21:47:00", 137, 31, "European Space Technology Harmonisation Conference", 80
exec AddWorkshop "2011-7-26", "18:21:00", "21:21:00", 134, 30, "�International� Corporate Citizenship Conference", 29
exec AddWorkshop "2013-10-27", "8:9:00", "10:9:00", 135, 22, "2nd International Conference on Photonics, Optics and Laser Technology", 76
exec AddWorkshop "2013-9-10", "19:21:00", "20:21:00", 123, 25, "Mechanisms Final Presentation Days", 9
exec AddWorkshop "2011-4-4", "10:56:00", "12:56:00", 56, 21, "X-Ray Universe", 41
exec AddWorkshop "2012-7-12", "14:13:00", "15:13:00", 86, 49, "European Navigation Conference 2014 (ENC-GNSS 2014)", 78
exec AddWorkshop "2013-9-10", "19:21:00", "20:21:00", 65, 34, "The 2nd International CSR Communication Conference", 20
exec AddWorkshop "2011-2-6", "18:30:00", "20:30:00", 113, 48, "International Conference on Pattern Recognition", 84
exec AddWorkshop "2013-4-22", "16:17:00", "19:17:00", 94, 21, "European Conference on Artificial Intelligence	", 53
exec AddWorkshop "2013-9-4", "19:38:00", "21:38:00", 95, 32, "4S Symposium 2014", 24
exec AddWorkshop "2011-8-9", "18:49:00", "21:49:00", 64, 46, "Conference on Automated Deduction		", 34
exec AddWorkshop "2011-3-6", "19:5:00", "20:5:00", 139, 30, "Life in Space for Life on Earth Symposium", 62
exec AddWorkshop "2012-1-20", "18:50:00", "19:50:00", 131, 43, "European Conference on Artificial Intelligence	", 24
exec AddWorkshop "2012-1-18", "19:24:00", "22:24:00", 149, 34, "The Sixth International Conferences on Advances in Multimedia", 53
exec AddWorkshop "2012-4-5", "16:6:00", "17:6:00", 120, 38, "European Space Power Conference", 50
exec AddWorkshop "2011-7-26", "13:50:00", "16:50:00", 139, 40, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 61
exec AddWorkshop "2011-7-1", "16:38:00", "18:38:00", 56, 27, "Corporate Community Involvement Conference", 77
exec AddWorkshop "2011-3-5", "14:12:00", "15:12:00", 82, 48, "The 2nd International CSR Communication Conference", 8
exec AddWorkshop "2011-3-5", "9:42:00", "12:42:00", 129, 45, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 4
exec AddWorkshop "2012-3-18", "10:48:00", "12:48:00", 121, 48, "International Semantic Web Conference		", 58
exec AddWorkshop "2012-5-30", "15:19:00", "17:19:00", 116, 33, "Corporate Community Involvement Conference", 13
exec AddWorkshop "2013-8-22", "19:21:00", "22:21:00", 77, 40, "2nd International Conference on Photonics, Optics and Laser Technology", 43
exec AddWorkshop "2011-4-8", "12:13:00", "13:13:00", 139, 47, "Mayo Clinic Presents", 27
exec AddWorkshop "2013-5-18", "19:6:00", "20:6:00", 55, 37, "British	Machine	Vision	Conference		", 10
exec AddWorkshop "2011-4-4", "16:29:00", "17:29:00", 100, 29, "Annual Interdisciplinary Conference", 52
exec AddWorkshop "2011-9-4", "19:18:00", "21:18:00", 139, 42, "International Conference on MultiMedia Modeling", 9
exec AddWorkshop "2012-3-22", "12:31:00", "15:31:00", 98, 45, "European Conference on Artificial Intelligence	", 30
exec AddWorkshop "2013-4-14", "17:46:00", "19:46:00", 53, 22, "4th DUE Permafrost User Workshop", 28
exec AddWorkshop "2011-5-6", "10:54:00", "11:54:00", 111, 42, "International Conference on Artificial Neural Networks", 35
exec AddWorkshop "2013-10-19", "9:14:00", "10:14:00", 117, 48, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 18
exec AddWorkshop "2011-6-15", "19:35:00", "22:35:00", 130, 22, "KU Leuven CSR Symposium", 49
exec AddWorkshop "2013-9-9", "19:41:00", "20:41:00", 146, 44, "2nd International Conference on Photonics, Optics and Laser Technology", 17
exec AddWorkshop "2013-12-13", "8:1:00", "9:1:00", 110, 28, "International Conference on Pattern Recognition", 59
exec AddWorkshop "2013-4-22", "17:16:00", "19:16:00", 131, 37, "European Space Technology Harmonisation Conference", 24
exec AddWorkshop "2012-7-22", "11:11:00", "14:11:00", 99, 36, "International Conference on Computer Vision Theory and Applications", 38
exec AddWorkshop "2012-9-25", "13:60:00", "16:60:00", 85, 30, "International Conference on Logic for Programming", 7
exec AddWorkshop "2011-8-24", "9:39:00", "11:39:00", 145, 31, "Client Summit", 18
exec AddWorkshop "2013-11-25", "18:47:00", "20:47:00", 96, 39, "LPVE Land product validation and evolution", 0
exec AddWorkshop "2011-7-9", "11:54:00", "12:54:00", 110, 47, "International Conference on Computer Vision", 29
exec AddWorkshop "2013-4-20", "10:41:00", "11:41:00", 53, 30, "European Space Technology Harmonisation Conference", 82
exec AddWorkshop "2013-8-12", "15:27:00", "17:27:00", 80, 29, "4th DUE Permafrost User Workshop", 12
exec AddWorkshop "2011-2-22", "16:55:00", "19:55:00", 136, 22, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 14
exec AddWorkshop "2013-12-3", "19:40:00", "22:40:00", 143, 22, "The Corporate Philanthropy Forum", 0
exec AddWorkshop "2013-3-5", "16:32:00", "18:32:00", 86, 48, "European Conference on Machine Learning", 23
exec AddWorkshop "2011-5-10", "16:31:00", "17:31:00", 63, 34, "ICATT", 0
exec AddWorkshop "2011-10-28", "14:2:00", "15:2:00", 60, 29, "International Conference on Automated Reasoning", 15
exec AddWorkshop "2012-12-14", "16:5:00", "17:5:00", 65, 36, "Life in Space for Life on Earth Symposium", 28
exec AddWorkshop "2011-7-10", "19:46:00", "22:46:00", 137, 38, "European Space Power Conference", 28
exec AddWorkshop "2013-5-11", "16:45:00", "17:45:00", 67, 23, "LPVE Land product validation and evolution", 79
exec AddWorkshop "2011-11-7", "17:9:00", "20:9:00", 142, 45, "ICATT", 27
exec AddWorkshop "2012-8-23", "10:51:00", "13:51:00", 62, 26, "European Conference on Machine Learning", 13
exec AddWorkshop "2012-4-21", "9:54:00", "12:54:00", 72, 31, "Asian Conference on Computer Vision", 7
exec AddWorkshop "2011-2-5", "16:59:00", "19:59:00", 91, 44, "Euclid Spacecraft Industry Day", 77
exec AddWorkshop "2013-3-4", "19:16:00", "21:16:00", 123, 24, "Foundations of Genetic Algorithms		", 3
exec AddWorkshop "2013-6-2", "13:47:00", "14:47:00", 76, 23, "International Conference on Logic for Programming", 81
exec AddWorkshop "2012-11-16", "9:51:00", "12:51:00", 144, 24, "The Sixth International Conferences on Advances in Multimedia", 1
exec AddWorkshop "2012-1-14", "9:23:00", "12:23:00", 92, 21, "European Conference on Artificial Intelligence	", 39
exec AddWorkshop "2013-8-6", "12:9:00", "13:9:00", 110, 31, "International Conference on Artificial Neural Networks", 19
exec AddWorkshop "2011-7-19", "15:25:00", "18:25:00", 60, 35, "The 12th annual Responsible Business Summit", 39
exec AddWorkshop "2012-1-16", "15:56:00", "17:56:00", 145, 23, "2013 Ethical Leadership Conference: Ethics in Action", 48
exec AddWorkshop "2013-8-25", "11:32:00", "12:32:00", 137, 25, "Asian Conference on Computer Vision", 62
exec AddWorkshop "2012-2-26", "13:17:00", "15:17:00", 108, 49, "Conference on Uncertainty in Artificial Intelligence", 24
exec AddWorkshop "2013-2-20", "16:49:00", "17:49:00", 83, 49, "International Conference on Signal and Imaging Systems Engineering", 23
exec AddWorkshop "2012-1-11", "10:16:00", "11:16:00", 58, 21, "The 12th annual Responsible Business Summit", 4
exec AddWorkshop "2011-12-10", "15:31:00", "17:31:00", 135, 31, "Asian Conference on Computer Vision", 45
exec AddWorkshop "2013-7-22", "12:2:00", "13:2:00", 138, 44, "The Sixth International Conferences on Advances in Multimedia", 67
exec AddWorkshop "2013-7-27", "16:14:00", "19:14:00", 124, 49, "Asian Conference on Computer Vision", 28
exec AddWorkshop "2011-6-26", "10:22:00", "13:22:00", 82, 33, "Workshop on Logic ", 39
exec AddWorkshop "2011-3-18", "17:43:00", "18:43:00", 110, 36, "Ceres Conference", 71
exec AddWorkshop "2011-1-8", "17:59:00", "20:59:00", 101, 44, "Cause Marketing Forum", 42
exec AddWorkshop "2012-1-29", "9:2:00", "12:2:00", 84, 32, "International Conference on Pattern Recognition", 77
exec AddWorkshop "2013-7-14", "10:29:00", "12:29:00", 144, 28, "European Conference on Artificial Intelligence	", 16
exec AddWorkshop "2012-7-15", "18:49:00", "21:49:00", 62, 42, "Cause Marketing Forum", 60
exec AddWorkshop "2013-11-13", "8:51:00", "11:51:00", 86, 38, "Microwave Workshop", 65
exec AddWorkshop "2012-12-19", "14:32:00", "15:32:00", 70, 26, "British	Machine	Vision	Conference		", 39
exec AddWorkshop "2013-5-29", "8:53:00", "9:53:00", 127, 47, "LPVE Land product validation and evolution", 62
exec AddWorkshop "2011-9-20", "16:45:00", "19:45:00", 59, 31, "LPVE Land product validation and evolution", 53
exec AddWorkshop "2012-1-19", "13:11:00", "14:11:00", 55, 41, "The BCLC CSR Conference", 2
exec AddWorkshop "2012-11-4", "14:5:00", "15:5:00", 127, 42, "European Conference on Artificial Intelligence	", 84
exec AddWorkshop "2011-8-5", "8:17:00", "11:17:00", 67, 26, "RuleML Symposium				", 27
exec AddWorkshop "2011-7-7", "17:11:00", "19:11:00", 126, 36, "Corporate Community Involvement Conference", 70
exec AddWorkshop "2013-12-13", "11:43:00", "14:43:00", 76, 41, "48th ESLAB Symposium", 8
exec AddWorkshop "2013-11-14", "11:60:00", "12:60:00", 51, 44, "Sentinel-2 for Science WS", 65
exec AddWorkshop "2013-6-8", "12:39:00", "15:39:00", 98, 48, "Computer Analysis of Images and Patterns", 38
exec AddWorkshop "2013-3-26", "8:11:00", "10:11:00", 130, 26, "KU Leuven CSR Symposium", 81
exec AddWorkshop "2011-3-3", "13:1:00", "14:1:00", 74, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 83
exec AddWorkshop "2011-7-4", "11:54:00", "13:54:00", 142, 28, "RuleML Symposium				", 7
exec AddWorkshop "2013-3-5", "15:23:00", "18:23:00", 56, 47, "Euclid Spacecraft Industry Day", 36
exec AddWorkshop "2011-5-1", "15:49:00", "16:49:00", 124, 49, "2013 Ethical Leadership Conference: Ethics in Action", 74
exec AddWorkshop "2011-8-11", "16:38:00", "17:38:00", 52, 22, "4S Symposium 2014", 43
exec AddWorkshop "2013-2-28", "14:19:00", "17:19:00", 148, 35, "ARTES 1 Final Presentation Days", 28
exec AddWorkshop "2013-7-7", "9:60:00", "10:60:00", 140, 24, "European Conference on Artificial Intelligence	", 22
exec AddWorkshop "2012-9-6", "18:54:00", "19:54:00", 95, 32, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 50
exec AddWorkshop "2013-3-4", "19:23:00", "22:23:00", 125, 23, "Computer Analysis of Images and Patterns", 9
exec AddWorkshop "2012-9-5", "10:7:00", "13:7:00", 101, 39, "Leadership Strategies for Information Technology in Health Care Boston", 54
exec AddWorkshop "2013-11-20", "12:53:00", "13:53:00", 104, 22, "Leadership Strategies for Information Technology in Health Care Boston", 4
exec AddWorkshop "2012-3-8", "10:41:00", "13:41:00", 96, 31, "European Conference on Artificial Intelligence	", 82
exec AddWorkshop "2013-10-4", "10:45:00", "13:45:00", 112, 46, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 27
exec AddWorkshop "2011-2-7", "14:25:00", "16:25:00", 97, 38, "7 th International Conference on Bio-inspired Systems and Signal Processing", 28
exec AddWorkshop "2013-8-11", "15:34:00", "16:34:00", 89, 42, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 25
exec AddWorkshop "2011-9-27", "15:47:00", "18:47:00", 119, 39, "European Navigation Conference 2014 (ENC-GNSS 2014)", 51
exec AddWorkshop "2013-8-14", "15:53:00", "18:53:00", 119, 45, "Ceres Conference", 27
exec AddWorkshop "2011-5-6", "11:48:00", "13:48:00", 149, 41, "Ceres Conference", 37
exec AddWorkshop "2012-7-10", "18:52:00", "20:52:00", 128, 40, "Annual Convention of The Society", 13
exec AddWorkshop "2013-9-13", "11:4:00", "12:4:00", 121, 42, "Conference on Uncertainty in Artificial Intelligence", 67
exec AddWorkshop "2013-2-21", "16:33:00", "18:33:00", 63, 42, "2nd International Conference on Photonics, Optics and Laser Technology", 36
exec AddWorkshop "2013-7-15", "11:32:00", "13:32:00", 125, 40, "Computer Analysis of Images and Patterns", 43
exec AddWorkshop "2013-3-11", "15:33:00", "16:33:00", 107, 29, "International Conference on Logic for Programming", 52
exec AddWorkshop "2013-12-16", "9:3:00", "10:3:00", 52, 20, "Mechanisms Final Presentation Days", 31
exec AddWorkshop "2012-1-6", "19:10:00", "21:10:00", 147, 25, "ICATT", 0
exec AddWorkshop "2011-6-20", "13:14:00", "16:14:00", 133, 39, "Leadership Strategies for Information Technology in Health Care Boston", 42
exec AddWorkshop "2013-3-27", "8:33:00", "10:33:00", 130, 44, "Asian Conference on Computer Vision", 12
exec AddWorkshop "2011-8-19", "15:46:00", "17:46:00", 142, 39, "EuroCOW the Calibration and Orientation Workshop", 0
exec AddWorkshop "2013-6-18", "16:44:00", "17:44:00", 65, 27, "European Space Power Conference", 22
exec AddWorkshop "2013-6-5", "13:33:00", "16:33:00", 108, 39, "International Joint Conference on Artificial Intelligence", 45
exec AddWorkshop "2012-9-26", "18:34:00", "21:34:00", 70, 39, "Computer Analysis of Images and Patterns", 59
exec AddWorkshop "2011-2-15", "8:51:00", "10:51:00", 89, 35, "International Conference on Logic for Programming", 80
exec AddWorkshop "2012-12-4", "11:28:00", "12:28:00", 76, 20, "The Corporate Philanthropy Forum", 50
exec AddWorkshop "2012-11-15", "18:6:00", "20:6:00", 95, 28, "British	Machine	Vision	Conference		", 5
exec AddWorkshop "2012-3-15", "8:49:00", "10:49:00", 94, 42, "International Conference on Automated Reasoning", 22
exec AddWorkshop "2013-1-27", "14:52:00", "16:52:00", 69, 20, "GNC 2014", 16
exec AddWorkshop "2013-2-5", "15:25:00", "18:25:00", 140, 25, "3rd International Conference on Pattern Recognition Applications and Methods", 18
exec AddWorkshop "2013-9-9", "19:53:00", "22:53:00", 63, 33, "Net Impact Conference", 45
exec AddWorkshop "2013-10-23", "10:55:00", "12:55:00", 69, 20, "Conference on Learning Theory		", 23
exec AddWorkshop "2013-2-29", "13:12:00", "16:12:00", 141, 39, "Annual Interdisciplinary Conference", 5
exec AddWorkshop "2013-10-28", "17:23:00", "18:23:00", 76, 32, "International Conference on the Principles of", 76
exec AddWorkshop "2012-3-2", "17:9:00", "18:9:00", 107, 37, "Sustainable Brands Conference", 40
exec AddWorkshop "2012-10-16", "18:25:00", "20:25:00", 148, 47, "International Joint Conference on Artificial Intelligence", 1
exec AddWorkshop "2012-2-23", "11:44:00", "14:44:00", 66, 24, "International Conference on Computer Vision", 26
exec AddWorkshop "2011-7-25", "11:23:00", "13:23:00", 55, 21, "2nd International Conference on Photonics, Optics and Laser Technology", 44
exec AddWorkshop "2012-4-5", "17:41:00", "18:41:00", 87, 34, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 82
exec AddWorkshop "2012-8-5", "18:40:00", "21:40:00", 138, 34, "International Semantic Web Conference		", 31
exec AddWorkshop "2012-11-4", "10:3:00", "12:3:00", 57, 28, "European Conference on Artificial Intelligence	", 2
exec AddWorkshop "2011-8-20", "13:38:00", "14:38:00", 52, 47, "Asian Conference on Computer Vision", 52
exec AddWorkshop "2013-1-14", "15:21:00", "16:21:00", 59, 20, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 56
exec AddWorkshop "2012-4-11", "16:22:00", "17:22:00", 99, 36, "International Conference on the Principles of", 11
exec AddWorkshop "2013-10-26", "17:54:00", "18:54:00", 123, 22, "48th ESLAB Symposium", 53
exec AddWorkshop "2012-6-22", "11:30:00", "14:30:00", 135, 48, "International Conference on Computer Vision", 67
exec AddWorkshop "2011-5-14", "12:9:00", "15:9:00", 109, 33, "British	Machine	Vision	Conference		", 49
exec AddWorkshop "2011-7-22", "13:42:00", "16:42:00", 149, 47, "3rd International Conference on Pattern Recognition Applications and Methods", 13
exec AddWorkshop "2013-2-6", "15:25:00", "18:25:00", 120, 24, "British	Machine	Vision	Conference		", 13
exec AddWorkshop "2012-6-17", "16:31:00", "18:31:00", 137, 44, "EuroCOW the Calibration and Orientation Workshop", 18
exec AddWorkshop "2011-11-28", "13:14:00", "15:14:00", 131, 36, "International Conference on Automated Reasoning", 46
exec AddWorkshop "2012-10-18", "15:16:00", "18:16:00", 83, 22, "ARTES 1 Final Presentation Days", 43
exec AddWorkshop "2011-12-30", "10:36:00", "12:36:00", 145, 35, "The Sixth International Conferences on Advances in Multimedia", 11
exec AddWorkshop "2011-9-18", "9:39:00", "12:39:00", 136, 42, "2nd International Conference on Photonics, Optics and Laser Technology", 75
exec AddWorkshop "2012-4-26", "13:51:00", "15:51:00", 126, 24, "Conference on Learning Theory		", 30
exec AddWorkshop "2012-1-2", "11:15:00", "14:15:00", 141, 20, "Business4Better: The Community Partnership Movement", 83
exec AddWorkshop "2012-1-19", "17:8:00", "18:8:00", 81, 34, "Foundations of Genetic Algorithms		", 63
exec AddWorkshop "2011-6-7", "8:18:00", "11:18:00", 88, 21, "European Conference on Machine Learning", 78
exec AddWorkshop "2011-10-2", "19:57:00", "21:57:00", 146, 22, "LPVE Land product validation and evolution", 1
exec AddWorkshop "2011-5-14", "12:34:00", "14:34:00", 142, 29, "International Conference on Autonomous Agents and", 11
exec AddWorkshop "2012-8-29", "15:39:00", "16:39:00", 149, 48, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 2
exec AddWorkshop "2012-12-4", "14:23:00", "17:23:00", 67, 46, "�International� Corporate Citizenship Conference", 71
exec AddWorkshop "2011-7-17", "17:52:00", "20:52:00", 67, 47, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 77
exec AddWorkshop "2011-7-26", "14:31:00", "16:31:00", 83, 28, "Corporate Community Involvement Conference", 18
exec AddWorkshop "2013-1-28", "10:43:00", "13:43:00", 116, 22, "Ceres Conference", 40
exec AddWorkshop "2013-2-3", "14:21:00", "17:21:00", 118, 28, "Cause Marketing Forum", 52
exec AddWorkshop "2013-8-25", "10:56:00", "12:56:00", 137, 24, "The 12th annual Responsible Business Summit", 28
exec AddWorkshop "2013-11-4", "15:3:00", "18:3:00", 139, 38, "KU Leuven CSR Symposium", 44
exec AddWorkshop "2012-7-25", "17:19:00", "18:19:00", 64, 38, "Leadership Strategies for Information Technology in Health Care Boston", 7
exec AddWorkshop "2013-9-24", "19:3:00", "22:3:00", 57, 41, "European Space Technology Harmonisation Conference", 73
exec AddWorkshop "2011-10-1", "18:52:00", "20:52:00", 145, 43, "Conference on Uncertainty in Artificial Intelligence", 0
exec AddWorkshop "2013-9-29", "13:40:00", "14:40:00", 106, 29, "International Joint Conference on Artificial Intelligence", 19
exec AddWorkshop "2013-3-19", "10:20:00", "11:20:00", 59, 21, "European Conference on Machine Learning", 73
exec AddWorkshop "2013-1-23", "14:17:00", "15:17:00", 88, 38, "Asian Conference on Computer Vision", 68
exec AddWorkshop "2013-4-20", "16:35:00", "18:35:00", 114, 23, "The BCLC CSR Conference", 48
exec AddWorkshop "2011-3-8", "18:53:00", "19:53:00", 101, 48, "International Conference on Automated Planning", 11
exec AddWorkshop "2011-7-23", "16:45:00", "17:45:00", 118, 44, "Conference on Computer Vision and Pattern", 15
exec AddWorkshop "2011-12-17", "13:40:00", "14:40:00", 52, 49, "The Sixth International Conferences on Advances in Multimedia", 63
exec AddWorkshop "2011-2-27", "11:13:00", "14:13:00", 132, 38, "The 2nd International CSR Communication Conference", 34
exec AddWorkshop "2012-9-1", "11:46:00", "14:46:00", 61, 26, "International Conference on Computer Vision", 71
exec AddWorkshop "2011-12-9", "18:16:00", "20:16:00", 97, 44, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 16
exec AddWorkshop "2012-9-17", "13:34:00", "16:34:00", 74, 22, "Workshop on Image Processing", 0
exec AddWorkshop "2011-4-7", "18:59:00", "19:59:00", 149, 37, "The 2nd International CSR Communication Conference", 4
exec AddWorkshop "2012-7-11", "18:46:00", "21:46:00", 135, 34, "Business4Better: The Community Partnership Movement", 31
exec AddWorkshop "2013-3-9", "15:16:00", "17:16:00", 113, 48, "Microwave Workshop", 11
exec AddWorkshop "2012-2-25", "9:52:00", "11:52:00", 105, 24, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 63
exec AddWorkshop "2013-9-29", "10:33:00", "12:33:00", 83, 35, "International Conference on Signal and Imaging Systems Engineering", 73
exec AddWorkshop "2013-7-12", "12:31:00", "13:31:00", 113, 45, "Mayo Clinic Presents", 63
exec AddWorkshop "2012-12-19", "16:48:00", "18:48:00", 97, 25, "Annual Interdisciplinary Conference", 41
exec AddWorkshop "2012-10-7", "13:58:00", "14:58:00", 91, 28, "2013 Ethical Leadership Conference: Ethics in Action", 63
exec AddWorkshop "2013-5-3", "12:53:00", "15:53:00", 116, 35, "Conference on Learning Theory		", 69
exec AddWorkshop "2011-9-17", "11:21:00", "12:21:00", 136, 41, "Microwave Workshop", 38
exec AddWorkshop "2012-2-1", "9:14:00", "10:14:00", 142, 27, "Asian Conference on Computer Vision", 61
exec AddWorkshop "2012-12-9", "8:18:00", "11:18:00", 107, 21, "Conference on Automated Deduction		", 11
exec AddWorkshop "2013-8-19", "9:54:00", "10:54:00", 62, 22, "4th DUE Permafrost User Workshop", 4
exec AddWorkshop "2012-1-18", "9:30:00", "12:30:00", 149, 42, "LPVE Land product validation and evolution", 37
exec AddWorkshop "2013-5-2", "8:29:00", "9:29:00", 107, 48, "International Semantic Web Conference		", 32
exec AddWorkshop "2011-11-11", "14:10:00", "17:10:00", 58, 44, "Ceres Conference", 84
exec AddWorkshop "2013-12-1", "12:7:00", "13:7:00", 147, 22, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 1
exec AddWorkshop "2013-8-16", "15:39:00", "18:39:00", 108, 26, "KU Leuven CSR Symposium", 34
exec AddWorkshop "2013-4-1", "8:38:00", "10:38:00", 126, 32, "Sentinel-2 for Science WS", 7
exec AddWorkshop "2013-4-27", "15:49:00", "17:49:00", 58, 31, "4th DUE Permafrost User Workshop", 2
exec AddWorkshop "2011-10-9", "19:56:00", "21:56:00", 74, 37, "International Joint Conference on Artificial Intelligence", 82
exec AddWorkshop "2011-3-10", "15:55:00", "18:55:00", 124, 29, "Global Conference on Sustainability and Reporting", 14
exec AddWorkshop "2011-7-4", "15:5:00", "17:5:00", 77, 24, "The Corporate Philanthropy Forum", 51
exec AddWorkshop "2012-3-15", "9:14:00", "10:14:00", 129, 25, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 44
exec AddWorkshop "2012-10-12", "19:41:00", "20:41:00", 96, 44, "Mayo Clinic Presents", 76
exec AddWorkshop "2011-7-12", "11:43:00", "12:43:00", 60, 48, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 79
exec AddWorkshop "2013-3-23", "17:37:00", "18:37:00", 73, 20, "International Conference on the Principles of", 57
exec AddWorkshop "2012-2-10", "18:41:00", "19:41:00", 148, 36, "European Navigation Conference 2014 (ENC-GNSS 2014)", 32
exec AddWorkshop "2013-7-30", "11:6:00", "12:6:00", 65, 32, "Computer Analysis of Images and Patterns", 79
exec AddWorkshop "2011-7-19", "12:4:00", "13:4:00", 67, 48, "KU Leuven CSR Symposium", 75
exec AddWorkshop "2013-4-23", "11:43:00", "12:43:00", 62, 34, "2013 Ethical Leadership Conference: Ethics in Action", 36
exec AddWorkshop "2011-12-24", "9:44:00", "12:44:00", 89, 31, "International Conference on Pattern Recognition", 38
exec AddWorkshop "2012-8-3", "9:4:00", "10:4:00", 136, 46, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 16
exec AddWorkshop "2012-1-29", "16:41:00", "17:41:00", 141, 26, "International Conference on Automated Reasoning", 78
exec AddWorkshop "2013-10-28", "17:51:00", "18:51:00", 142, 30, "2013 Ethical Leadership Conference: Ethics in Action", 13
exec AddWorkshop "2011-4-29", "12:7:00", "15:7:00", 100, 43, "ARTES 1 Final Presentation Days", 36
exec AddWorkshop "2013-2-14", "13:7:00", "14:7:00", 142, 40, "Photonics West", 40
exec AddWorkshop "2013-10-22", "8:40:00", "10:40:00", 144, 49, "International Conference on the Principles of", 54
exec AddWorkshop "2012-5-24", "8:1:00", "9:1:00", 149, 46, "Business4Better: The Community Partnership Movement", 4
exec AddWorkshop "2013-11-17", "12:24:00", "14:24:00", 75, 40, "International Conference on Autonomous Agents and", 51
exec AddWorkshop "2011-7-16", "15:37:00", "18:37:00", 119, 43, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 48
exec AddWorkshop "2013-1-7", "18:5:00", "20:5:00", 97, 37, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 72
exec AddWorkshop "2012-12-24", "8:46:00", "11:46:00", 104, 20, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 33
exec AddWorkshop "2013-2-29", "17:56:00", "18:56:00", 122, 47, "RuleML Symposium		", 50
exec AddWorkshop "2013-4-9", "18:45:00", "21:45:00", 143, 48, "International Conference on Logic for Programming", 66
exec AddWorkshop "2011-4-9", "12:44:00", "15:44:00", 116, 20, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 4
exec AddWorkshop "2012-11-20", "11:4:00", "12:4:00", 86, 39, "Conference on Automated Deduction		", 9
exec AddWorkshop "2013-9-26", "17:36:00", "18:36:00", 52, 47, "Cause Marketing Forum", 83
exec AddWorkshop "2012-8-25", "16:42:00", "19:42:00", 142, 27, "GNC 2014", 50
exec AddWorkshop "2011-3-5", "19:10:00", "22:10:00", 135, 33, "Leadership Strategies for Information Technology in Health Care Boston", 19
exec AddWorkshop "2012-1-9", "13:6:00", "15:6:00", 140, 35, "The 12th annual Responsible Business Summit", 73
exec AddWorkshop "2013-9-4", "13:34:00", "15:34:00", 144, 32, "Conference on Computer Vision and Pattern", 36
exec AddWorkshop "2012-3-2", "10:52:00", "12:52:00", 97, 23, "International Conference on the Principles of", 54
exec AddWorkshop "2013-11-20", "9:15:00", "10:15:00", 136, 25, "European Conference on Artificial Intelligence	", 63
exec AddWorkshop "2012-2-21", "18:42:00", "19:42:00", 111, 29, "KU Leuven CSR Symposium", 2
exec AddWorkshop "2012-7-25", "13:33:00", "14:33:00", 97, 24, "Conference on Learning Theory		", 59
exec AddWorkshop "2013-8-20", "14:39:00", "15:39:00", 107, 49, "Workshop on Image Processing", 55
exec AddWorkshop "2013-8-27", "19:11:00", "20:11:00", 54, 31, "Annual Convention of The Society", 44
exec AddWorkshop "2011-6-17", "15:44:00", "17:44:00", 69, 36, "KU Leuven CSR Symposium", 8
exec AddWorkshop "2011-10-25", "11:48:00", "14:48:00", 142, 23, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 50
exec AddWorkshop "2011-3-27", "8:56:00", "9:56:00", 53, 32, "International Conference on Logic for Programming", 75
exec AddWorkshop "2011-7-14", "16:9:00", "18:9:00", 112, 26, "The 2nd International CSR Communication Conference", 24
exec AddWorkshop "2012-8-2", "13:18:00", "14:18:00", 112, 26, "�International� Corporate Citizenship Conference", 4
exec AddWorkshop "2012-9-26", "12:28:00", "13:28:00", 99, 34, "Photonics West", 35
exec AddWorkshop "2011-3-4", "13:45:00", "14:45:00", 118, 22, "Photonics West", 33
exec AddWorkshop "2013-1-28", "9:23:00", "10:23:00", 106, 20, "The Corporate Philanthropy Forum", 52
exec AddWorkshop "2013-4-5", "8:3:00", "10:3:00", 110, 36, "Photonics West", 10
exec AddWorkshop "2011-3-30", "18:41:00", "19:41:00", 70, 37, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 62
exec AddWorkshop "2012-9-10", "16:41:00", "19:41:00", 144, 38, "European Space Technology Harmonisation Conference", 28
exec AddWorkshop "2011-2-11", "15:32:00", "18:32:00", 137, 46, "International Conference on Computer Vision Theory and Applications", 50
exec AddWorkshop "2012-10-22", "9:50:00", "10:50:00", 62, 48, "Business4Better: The Community Partnership Movement", 7
exec AddWorkshop "2012-6-9", "19:5:00", "22:5:00", 90, 48, "Euclid Spacecraft Industry Day", 22
exec AddWorkshop "2011-8-28", "12:41:00", "13:41:00", 109, 34, "European Conference on Artificial Intelligence	", 54
exec AddWorkshop "2013-3-29", "16:17:00", "17:17:00", 124, 36, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 19
exec AddWorkshop "2012-12-23", "9:25:00", "11:25:00", 77, 38, "International Conference on Logic for Programming", 74
exec AddWorkshop "2013-12-26", "9:13:00", "11:13:00", 88, 44, "International Joint Conference on Automated Reasoning", 39
exec AddWorkshop "2011-11-15", "15:57:00", "16:57:00", 79, 42, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 58
exec AddWorkshop "2012-4-17", "10:15:00", "13:15:00", 118, 25, "KU Leuven CSR Symposium", 77
exec AddWorkshop "2012-9-12", "8:40:00", "10:40:00", 110, 33, "Conference on Uncertainty in Artificial Intelligence", 61
exec AddWorkshop "2011-9-16", "8:41:00", "10:41:00", 77, 23, "International Conference on Automated Planning", 68
exec AddWorkshop "2011-3-17", "8:21:00", "10:21:00", 141, 40, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 72
exec AddWorkshop "2011-11-1", "14:35:00", "15:35:00", 85, 32, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 29
exec AddWorkshop "2011-8-29", "14:9:00", "16:9:00", 111, 27, "Asian Conference on Computer Vision", 16
exec AddWorkshop "2013-11-28", "19:28:00", "21:28:00", 89, 22, "Sentinel-2 for Science WS", 66
exec AddWorkshop "2012-2-23", "15:37:00", "16:37:00", 111, 32, "4S Symposium 2014", 2
exec AddWorkshop "2012-12-3", "10:54:00", "12:54:00", 138, 40, "The Sixth International Conferences on Advances in Multimedia", 21
exec AddWorkshop "2013-9-3", "14:39:00", "17:39:00", 114, 20, "The 12th annual Responsible Business Summit", 53
exec AddWorkshop "2011-8-27", "15:15:00", "18:15:00", 91, 35, "Science and Challenges of Lunar Sample Return Workshop", 82
exec AddWorkshop "2011-8-17", "11:41:00", "14:41:00", 130, 41, "ARTES 1 Final Presentation Days", 48
exec AddWorkshop "2013-2-5", "15:26:00", "16:26:00", 110, 20, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 16
exec AddWorkshop "2011-4-15", "10:52:00", "12:52:00", 140, 46, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 14
exec AddWorkshop "2012-11-15", "11:57:00", "14:57:00", 134, 24, "Foundations of Genetic Algorithms		", 0
exec AddWorkshop "2012-2-10", "19:1:00", "21:1:00", 61, 47, "International Conference on Artificial Neural Networks", 18
exec AddWorkshop "2013-12-28", "8:3:00", "10:3:00", 50, 25, "International Conference on the Principles of", 65
exec AddWorkshop "2013-4-12", "16:45:00", "19:45:00", 61, 43, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 42
exec AddWorkshop "2013-8-29", "14:2:00", "17:2:00", 134, 47, "International Conference on Logic for Programming", 39
exec AddWorkshop "2011-2-6", "18:22:00", "20:22:00", 77, 49, "Conference on Computer Vision and Pattern", 29
exec AddWorkshop "2012-2-18", "9:14:00", "12:14:00", 120, 31, "LPVE Land product validation and evolution", 13
exec AddWorkshop "2012-5-27", "16:58:00", "19:58:00", 79, 38, "The BCLC CSR Conference", 15
exec AddWorkshop "2011-3-6", "12:25:00", "14:25:00", 77, 39, "International Conference on Autonomous Agents and", 62
exec AddWorkshop "2012-4-8", "17:59:00", "20:59:00", 50, 28, "RuleML Symposium		", 27
exec AddWorkshop "2011-7-12", "17:34:00", "20:34:00", 145, 33, "Workshop on Image Processing", 66
exec AddWorkshop "2011-10-1", "16:8:00", "18:8:00", 146, 38, "ARTES 1 Final Presentation Days", 81
exec AddWorkshop "2012-9-27", "17:39:00", "20:39:00", 95, 20, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 21
exec AddWorkshop "2012-1-19", "17:42:00", "20:42:00", 64, 37, "Global Conference on Sustainability and Reporting", 12
exec AddWorkshop "2013-2-27", "17:58:00", "18:58:00", 69, 39, "Conference on Computer Vision and Pattern", 59
exec AddWorkshop "2013-12-26", "10:21:00", "13:21:00", 51, 47, "The Corporate Philanthropy Forum", 23
exec AddWorkshop "2011-4-2", "16:35:00", "19:35:00", 95, 27, "Business4Better: The Community Partnership Movement", 21
exec AddWorkshop "2011-6-24", "15:20:00", "18:20:00", 96, 41, "International Conference on Pattern Recognition", 51
exec AddWorkshop "2012-2-1", "19:49:00", "21:49:00", 106, 28, "RuleML Symposium				", 54
exec AddWorkshop "2012-9-3", "10:23:00", "11:23:00", 118, 35, "European Conference on Machine Learning", 47
exec AddWorkshop "2012-6-3", "10:13:00", "13:13:00", 119, 39, "International Joint Conference on Automated Reasoning", 36
exec AddWorkshop "2012-9-27", "10:57:00", "13:57:00", 84, 26, "British	Machine	Vision	Conference		", 72
exec AddWorkshop "2012-12-12", "16:58:00", "17:58:00", 140, 21, "Workshop on Logic ", 26
exec AddWorkshop "2013-3-30", "17:5:00", "18:5:00", 90, 22, "International Conference on Logic for Programming", 51
exec AddWorkshop "2012-6-22", "9:33:00", "10:33:00", 80, 35, "Life in Space for Life on Earth Symposium", 0
exec AddWorkshop "2012-5-22", "16:1:00", "17:1:00", 138, 48, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 77
exec AddWorkshop "2011-6-24", "13:60:00", "15:60:00", 110, 26, "Science and Challenges of Lunar Sample Return Workshop", 19
exec AddWorkshop "2013-6-19", "11:36:00", "13:36:00", 52, 28, "ICATT", 57
exec AddWorkshop "2013-6-7", "17:39:00", "18:39:00", 67, 24, "�International� Corporate Citizenship Conference", 20
exec AddWorkshop "2011-8-2", "17:40:00", "19:40:00", 133, 44, "Foundations of Genetic Algorithms		", 54
exec AddWorkshop "2011-2-27", "14:46:00", "17:46:00", 104, 25, "3rd International Conference on Pattern Recognition Applications and Methods", 81
exec AddWorkshop "2013-2-25", "10:48:00", "13:48:00", 68, 40, "The Sixth International Conferences on Advances in Multimedia", 31
exec AddWorkshop "2011-2-17", "10:49:00", "13:49:00", 109, 32, "7 th International Conference on Bio-inspired Systems and Signal Processing", 83
exec AddWorkshop "2011-4-6", "19:23:00", "22:23:00", 138, 48, "European Navigation Conference 2014 (ENC-GNSS 2014)", 53
exec AddWorkshop "2011-2-14", "18:20:00", "20:20:00", 59, 25, "Global Conference on Sustainability and Reporting", 27
exec AddWorkshop "2012-2-11", "9:1:00", "11:1:00", 107, 40, "Sustainable Brands Conference", 82
exec AddWorkshop "2012-7-17", "15:2:00", "17:2:00", 112, 43, "Conference on Computer Vision and Pattern", 39
exec AddWorkshop "2012-4-15", "16:12:00", "18:12:00", 77, 22, "Workshop on Image Processing", 40
exec AddWorkshop "2011-12-16", "9:53:00", "11:53:00", 89, 42, "International Conference on Logic for Programming", 43
exec AddWorkshop "2013-2-18", "10:18:00", "12:18:00", 128, 32, "4th DUE Permafrost User Workshop", 65
exec AddWorkshop "2012-9-10", "17:35:00", "18:35:00", 104, 24, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 61
exec AddWorkshop "2013-11-29", "19:18:00", "22:18:00", 73, 45, "Net Impact Conference", 51
exec AddWorkshop "2013-6-3", "19:19:00", "21:19:00", 138, 23, "Conference on Automated Deduction		", 9
exec AddWorkshop "2011-10-24", "16:5:00", "18:5:00", 127, 27, "Annual Convention of The Society", 67
exec AddWorkshop "2013-10-22", "11:60:00", "13:60:00", 135, 38, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 72
exec AddWorkshop "2011-4-7", "8:3:00", "10:3:00", 144, 25, "LPVE Land product validation and evolution", 39
exec AddWorkshop "2013-2-9", "13:30:00", "14:30:00", 136, 47, "Leadership Strategies for Information Technology in Health Care Boston", 57
exec AddWorkshop "2012-4-19", "13:29:00", "14:29:00", 133, 29, "Asian Conference on Computer Vision", 38
exec AddWorkshop "2012-1-18", "11:8:00", "14:8:00", 121, 21, "European Navigation Conference 2014 (ENC-GNSS 2014)", 17
exec AddWorkshop "2013-8-3", "11:36:00", "12:36:00", 101, 35, "RuleML Symposium				", 53
exec AddWorkshop "2011-5-7", "15:5:00", "17:5:00", 144, 47, "X-Ray Universe", 49
exec AddWorkshop "2011-10-27", "13:27:00", "16:27:00", 98, 30, "48th ESLAB Symposium", 56
exec AddWorkshop "2011-4-3", "11:18:00", "14:18:00", 56, 27, "International Conference on MultiMedia Modeling", 48
exec AddWorkshop "2011-6-14", "17:17:00", "20:17:00", 59, 45, "Conference on Automated Deduction		", 76
exec AddWorkshop "2012-3-24", "9:3:00", "11:3:00", 130, 34, "The 12th annual Responsible Business Summit", 81
exec AddWorkshop "2011-9-21", "10:22:00", "11:22:00", 149, 31, "European Navigation Conference 2014 (ENC-GNSS 2014)", 33
exec AddWorkshop "2011-10-28", "13:45:00", "16:45:00", 89, 34, "Foundations of Genetic Algorithms		", 24
exec AddWorkshop "2012-6-6", "19:10:00", "20:10:00", 81, 35, "4th DUE Permafrost User Workshop", 26
exec AddWorkshop "2011-5-25", "13:27:00", "14:27:00", 81, 45, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 39
exec AddWorkshop "2011-11-1", "10:54:00", "13:54:00", 111, 21, "4th DUE Permafrost User Workshop", 56
exec AddWorkshop "2012-9-30", "14:53:00", "15:53:00", 62, 47, "Business4Better: The Community Partnership Movement", 15
exec AddWorkshop "2013-8-15", "17:55:00", "19:55:00", 133, 33, "Cause Marketing Forum", 52
exec AddWorkshop "2013-11-27", "12:21:00", "14:21:00", 65, 40, "International Conference on Automated Reasoning", 5
exec AddWorkshop "2011-6-19", "8:52:00", "9:52:00", 120, 47, "International Conference on Logic for Programming", 77
exec AddWorkshop "2013-5-21", "12:35:00", "14:35:00", 134, 38, "Conference on Learning Theory		", 56
exec AddWorkshop "2011-2-6", "8:21:00", "9:21:00", 83, 21, "48th ESLAB Symposium", 7
exec AddWorkshop "2013-2-7", "8:11:00", "10:11:00", 131, 32, "Conference on Uncertainty in Artificial Intelligence", 58
exec AddWorkshop "2012-4-15", "15:41:00", "18:41:00", 97, 47, "The 12th annual Responsible Business Summit", 34
exec AddWorkshop "2013-12-12", "8:53:00", "10:53:00", 64, 37, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 42
exec AddWorkshop "2012-9-29", "12:26:00", "15:26:00", 128, 38, "Ceres Conference", 38
exec AddWorkshop "2013-3-1", "8:49:00", "10:49:00", 104, 29, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 30
exec AddWorkshop "2013-5-6", "13:3:00", "15:3:00", 117, 30, "Conference on Computer Vision and Pattern", 45
exec AddWorkshop "2011-4-18", "13:47:00", "14:47:00", 76, 24, "Annual Interdisciplinary Conference", 66
exec AddWorkshop "2013-8-19", "19:13:00", "20:13:00", 140, 27, "International Conference on Artificial Neural Networks", 6
exec AddWorkshop "2011-10-10", "11:37:00", "12:37:00", 106, 20, "Asian Conference on Computer Vision", 23
exec AddWorkshop "2011-12-25", "9:27:00", "12:27:00", 117, 21, "European Navigation Conference 2014 (ENC-GNSS 2014)", 41
exec AddWorkshop "2011-8-20", "19:31:00", "21:31:00", 104, 41, "Leadership Strategies for Information Technology in Health Care Boston", 8
exec AddWorkshop "2013-3-12", "12:28:00", "15:28:00", 55, 48, "Global Conference on Sustainability and Reporting", 59
exec AddWorkshop "2012-8-3", "17:48:00", "20:48:00", 110, 21, "International Conference on Automated Planning", 24
exec AddWorkshop "2011-2-23", "14:14:00", "15:14:00", 83, 25, "European Navigation Conference 2014 (ENC-GNSS 2014)", 84
exec AddWorkshop "2013-3-14", "13:35:00", "16:35:00", 124, 40, "EuroCOW the Calibration and Orientation Workshop", 27
exec AddWorkshop "2011-10-6", "17:20:00", "18:20:00", 114, 26, "The 12th annual Responsible Business Summit", 49
exec AddWorkshop "2011-9-3", "19:11:00", "20:11:00", 116, 46, "X-Ray Universe", 30
exec AddWorkshop "2013-5-28", "9:23:00", "11:23:00", 90, 22, "�International� Corporate Citizenship Conference", 73
exec AddWorkshop "2013-3-26", "19:38:00", "21:38:00", 111, 39, "Science and Challenges of Lunar Sample Return Workshop", 60
exec AddWorkshop "2011-7-27", "17:5:00", "20:5:00", 103, 26, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 45
exec AddWorkshop "2013-3-3", "9:2:00", "10:2:00", 71, 41, "The 12th annual Responsible Business Summit", 74
exec AddWorkshop "2013-2-16", "19:22:00", "20:22:00", 140, 23, "Corporate Community Involvement Conference", 9
exec AddWorkshop "2012-11-16", "9:10:00", "10:10:00", 104, 21, "European Conference on Computer Vision", 5
exec AddWorkshop "2012-4-21", "15:55:00", "16:55:00", 124, 38, "The 12th annual Responsible Business Summit", 0
exec AddWorkshop "2013-9-13", "11:33:00", "12:33:00", 130, 20, "The Corporate Philanthropy Forum", 2
exec AddWorkshop "2013-4-3", "9:4:00", "12:4:00", 95, 38, "Ceres Conference", 71
exec AddWorkshop "2011-6-13", "14:9:00", "15:9:00", 59, 26, "Science and Challenges of Lunar Sample Return Workshop", 58
exec AddWorkshop "2012-6-11", "13:10:00", "15:10:00", 96, 44, "Annual Interdisciplinary Conference", 81
exec AddWorkshop "2012-5-6", "17:22:00", "18:22:00", 136, 38, "International Conference on Computer Vision", 45
exec AddWorkshop "2013-3-6", "19:1:00", "21:1:00", 145, 49, "Ceres Conference", 66
exec AddWorkshop "2011-6-14", "14:23:00", "17:23:00", 61, 26, "European Conference on Artificial Intelligence	", 76
exec AddWorkshop "2013-11-6", "10:11:00", "12:11:00", 144, 43, "International Semantic Web Conference		", 25
exec AddWorkshop "2011-7-11", "14:9:00", "16:9:00", 72, 48, "The BCLC CSR Conference", 70
exec AddWorkshop "2012-9-19", "14:33:00", "17:33:00", 64, 28, "International Conference on Pattern Recognition", 1
exec AddWorkshop "2011-2-7", "16:46:00", "18:46:00", 138, 38, "Sustainable Brands Conference", 78
exec AddWorkshop "2011-5-17", "13:36:00", "16:36:00", 128, 21, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 17
exec AddWorkshop "2012-7-19", "14:24:00", "17:24:00", 70, 44, "Sentinel-2 for Science WS", 56
exec AddWorkshop "2011-8-7", "13:7:00", "14:7:00", 94, 24, "EuroCOW the Calibration and Orientation Workshop", 75
exec AddWorkshop "2011-8-25", "9:42:00", "10:42:00", 111, 38, "Asian Conference on Computer Vision", 60
exec AddWorkshop "2012-9-22", "15:60:00", "17:60:00", 128, 34, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 31
exec AddWorkshop "2013-3-12", "19:54:00", "20:54:00", 52, 46, "Mechanisms Final Presentation Days", 58
exec AddWorkshop "2013-10-9", "19:25:00", "20:25:00", 106, 32, "International Conference on Artificial Neural Networks", 57
exec AddWorkshop "2013-7-3", "19:57:00", "21:57:00", 148, 28, "Photonics West", 23
exec AddWorkshop "2013-9-18", "9:29:00", "12:29:00", 91, 42, "Microwave Workshop", 61
exec AddWorkshop "2013-7-10", "8:36:00", "11:36:00", 58, 37, "International Joint Conference on Artificial Intelligence", 37
exec AddWorkshop "2012-4-1", "11:38:00", "12:38:00", 74, 31, "International Semantic Web Conference		", 20
exec AddWorkshop "2011-2-1", "11:36:00", "13:36:00", 115, 42, "Client Summit", 29
exec AddWorkshop "2011-6-1", "15:21:00", "17:21:00", 79, 27, "International Conference on Artificial Neural Networks", 4
exec AddWorkshop "2012-11-6", "17:56:00", "19:56:00", 123, 44, "International Conference on Computer Vision Theory and Applications", 68
exec AddWorkshop "2013-12-17", "19:7:00", "21:7:00", 115, 28, "Euclid Spacecraft Industry Day", 62
exec AddWorkshop "2011-4-21", "19:20:00", "21:20:00", 57, 46, "European Space Technology Harmonisation Conference", 4
exec AddWorkshop "2012-7-8", "11:25:00", "14:25:00", 71, 35, "International Conference on MultiMedia Modeling", 49
exec AddWorkshop "2013-12-5", "17:56:00", "20:56:00", 125, 34, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 73
exec AddWorkshop "2012-7-9", "10:19:00", "11:19:00", 68, 38, "European Space Technology Harmonisation Conference", 1
exec AddWorkshop "2013-6-13", "12:54:00", "14:54:00", 98, 45, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 45
exec AddWorkshop "2012-4-7", "16:13:00", "17:13:00", 71, 27, "International Conference on Computer Vision", 1
exec AddWorkshop "2012-11-18", "13:21:00", "16:21:00", 85, 42, "7 th International Conference on Bio-inspired Systems and Signal Processing", 59
exec AddWorkshop "2011-3-26", "18:50:00", "19:50:00", 118, 32, "Mechanisms Final Presentation Days", 63
exec AddWorkshop "2013-4-28", "14:27:00", "16:27:00", 98, 34, "European Space Technology Harmonisation Conference", 78
exec AddWorkshop "2011-4-15", "19:45:00", "22:45:00", 140, 47, "Conference on Learning Theory		", 79
exec AddWorkshop "2012-12-25", "14:5:00", "17:5:00", 109, 49, "International Joint Conference on Artificial Intelligence", 54
exec AddWorkshop "2012-6-25", "19:42:00", "21:42:00", 109, 46, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 17
exec AddWorkshop "2011-12-14", "11:33:00", "13:33:00", 122, 40, "�International� Corporate Citizenship Conference", 28
exec AddWorkshop "2011-11-7", "9:49:00", "11:49:00", 118, 24, "Microwave Workshop", 62
exec AddWorkshop "2013-5-2", "14:32:00", "15:32:00", 108, 28, "The Corporate Philanthropy Forum", 84
exec AddWorkshop "2011-9-9", "14:21:00", "15:21:00", 50, 44, "Microwave Workshop", 62
exec AddWorkshop "2011-5-30", "16:21:00", "17:21:00", 103, 43, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 68
exec AddWorkshop "2011-1-13", "15:55:00", "16:55:00", 56, 47, "Workshop on Logic ", 36
exec AddWorkshop "2011-1-4", "19:8:00", "21:8:00", 109, 20, "Foundations of Genetic Algorithms		", 52
exec AddWorkshop "2011-10-15", "10:8:00", "12:8:00", 120, 20, "Ceres Conference", 84
exec AddWorkshop "2013-2-6", "11:47:00", "13:47:00", 103, 22, "3rd International Conference on Pattern Recognition Applications and Methods", 6
exec AddWorkshop "2013-9-7", "17:16:00", "19:16:00", 131, 32, "LPVE Land product validation and evolution", 34
exec AddWorkshop "2013-9-12", "11:45:00", "13:45:00", 129, 22, "Life in Space for Life on Earth Symposium", 42
exec AddWorkshop "2012-11-21", "15:52:00", "18:52:00", 140, 36, "International Joint Conference on Automated Reasoning", 60
exec AddWorkshop "2013-8-15", "10:57:00", "13:57:00", 94, 49, "Net Impact Conference", 77
exec AddWorkshop "2013-7-9", "8:35:00", "10:35:00", 127, 22, "2013 Ethical Leadership Conference: Ethics in Action", 54
exec AddWorkshop "2011-3-3", "18:34:00", "19:34:00", 100, 20, "Cause Marketing Forum", 64
exec AddWorkshop "2011-5-3", "14:43:00", "16:43:00", 64, 22, "2013 Ethical Leadership Conference: Ethics in Action", 70
exec AddWorkshop "2011-7-14", "11:3:00", "12:3:00", 119, 41, "Conference on Automated Deduction		", 24
exec AddWorkshop "2012-6-13", "16:13:00", "18:13:00", 117, 22, "4S Symposium 2014", 31
exec AddWorkshop "2012-5-5", "8:54:00", "11:54:00", 116, 42, "GNC 2014", 47
exec AddWorkshop "2012-9-13", "18:11:00", "20:11:00", 147, 43, "European Conference on Artificial Intelligence	", 8
exec AddWorkshop "2012-2-19", "10:4:00", "11:4:00", 117, 39, "International Conference on Autonomous Agents and", 49
exec AddWorkshop "2013-10-23", "10:54:00", "12:54:00", 67, 40, "3rd International Conference on Pattern Recognition Applications and Methods", 7
exec AddWorkshop "2011-6-5", "12:24:00", "15:24:00", 110, 37, "International Joint Conference on Artificial Intelligence", 44
exec AddWorkshop "2012-11-7", "14:44:00", "17:44:00", 51, 22, "Euclid Spacecraft Industry Day", 57
exec AddWorkshop "2013-2-10", "11:56:00", "13:56:00", 126, 39, "International Conference on Automated Reasoning", 10
exec AddWorkshop "2012-3-11", "10:54:00", "12:54:00", 75, 33, "GNC 2014", 51
exec AddWorkshop "2012-2-11", "10:59:00", "13:59:00", 95, 33, "Ceres Conference", 44
exec AddWorkshop "2012-6-19", "8:21:00", "10:21:00", 118, 41, "Annual Convention of The Society", 8
exec AddWorkshop "2012-3-15", "12:8:00", "14:8:00", 63, 29, "Annual Convention of The Society", 65
exec AddWorkshop "2011-9-10", "18:33:00", "20:33:00", 65, 42, "International Conference on Autonomous Agents and", 2
exec AddWorkshop "2012-1-8", "18:43:00", "20:43:00", 128, 32, "Cause Marketing Forum", 32
exec AddWorkshop "2011-9-16", "8:27:00", "9:27:00", 94, 27, "European Conference on Computer Vision", 55
exec AddWorkshop "2013-12-1", "15:30:00", "18:30:00", 138, 43, "Workshop on Image Processing", 71
exec AddWorkshop "2011-11-18", "10:40:00", "13:40:00", 56, 26, "Asian Conference on Computer Vision", 50
exec AddWorkshop "2013-12-7", "10:30:00", "11:30:00", 114, 33, "Microwave Workshop", 13
exec AddWorkshop "2013-1-26", "15:54:00", "17:54:00", 57, 42, "Conference on Uncertainty in Artificial Intelligence", 63
exec AddWorkshop "2011-6-8", "18:50:00", "19:50:00", 138, 35, "Leadership Strategies for Information Technology in Health Care Boston", 0
exec AddWorkshop "2011-12-26", "11:45:00", "14:45:00", 76, 25, "3rd International Conference on Pattern Recognition Applications and Methods", 44
exec AddWorkshop "2012-12-19", "16:32:00", "17:32:00", 63, 44, "Business4Better: The Community Partnership Movement", 58
exec AddWorkshop "2011-4-18", "13:17:00", "16:17:00", 106, 49, "The 2nd International CSR Communication Conference", 66
exec AddWorkshop "2013-12-23", "13:26:00", "16:26:00", 134, 20, "RuleML Symposium		", 58
exec AddWorkshop "2013-4-24", "8:16:00", "10:16:00", 127, 30, "Mechanisms Final Presentation Days", 68
exec AddWorkshop "2012-9-18", "9:43:00", "10:43:00", 75, 36, "X-Ray Universe", 55
exec AddWorkshop "2012-7-13", "15:58:00", "18:58:00", 78, 38, "International Conference on Computer Vision", 65
exec AddWorkshop "2011-10-26", "8:15:00", "9:15:00", 55, 36, "International Conference on Autonomous Agents and", 23
exec AddWorkshop "2011-7-16", "19:24:00", "21:24:00", 88, 49, "Client Summit", 71
exec AddWorkshop "2011-4-14", "12:27:00", "14:27:00", 104, 36, "4th DUE Permafrost User Workshop", 52
exec AddWorkshop "2011-5-15", "10:40:00", "13:40:00", 95, 31, "Conference on Automated Deduction		", 48
exec AddWorkshop "2011-1-19", "19:45:00", "22:45:00", 78, 34, "Conference on Automated Deduction		", 75
exec AddWorkshop "2013-1-9", "9:46:00", "10:46:00", 103, 41, "International Joint Conference on Artificial Intelligence", 53
exec AddWorkshop "2013-1-8", "13:11:00", "14:11:00", 91, 20, "Conference on Uncertainty in Artificial Intelligence", 81
exec AddWorkshop "2011-11-26", "10:40:00", "13:40:00", 140, 31, "International Conference on MultiMedia Modeling", 43
exec AddWorkshop "2013-1-28", "16:59:00", "17:59:00", 67, 41, "British	Machine	Vision	Conference		", 83
exec AddWorkshop "2013-5-15", "10:18:00", "12:18:00", 148, 33, "Conference on Learning Theory		", 71
exec AddWorkshop "2011-10-19", "10:42:00", "13:42:00", 121, 45, "X-Ray Universe", 15
exec AddWorkshop "2013-2-19", "19:57:00", "20:57:00", 63, 40, "Workshop on Image Processing", 74
exec AddWorkshop "2012-3-27", "16:12:00", "17:12:00", 60, 34, "Annual Convention of The Society", 1
exec AddWorkshop "2012-6-10", "10:12:00", "11:12:00", 103, 41, "Business4Better: The Community Partnership Movement", 25
exec AddWorkshop "2013-2-7", "10:24:00", "11:24:00", 50, 48, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 67
exec AddWorkshop "2011-12-26", "8:28:00", "11:28:00", 146, 33, "Conference on Learning Theory		", 79
exec AddWorkshop "2013-4-17", "14:19:00", "16:19:00", 58, 40, "Business4Better: The Community Partnership Movement", 34
exec AddWorkshop "2013-9-9", "15:45:00", "18:45:00", 89, 39, "International Joint Conference on Automated Reasoning", 40
exec AddWorkshop "2011-3-25", "9:31:00", "11:31:00", 121, 24, "European Space Technology Harmonisation Conference", 3
exec AddWorkshop "2012-10-20", "15:8:00", "16:8:00", 67, 37, "Sentinel-2 for Science WS", 72
exec AddWorkshop "2013-12-16", "18:25:00", "19:25:00", 139, 48, "European Space Technology Harmonisation Conference", 2
exec AddWorkshop "2012-5-25", "8:47:00", "9:47:00", 54, 28, "The BCLC CSR Conference", 71
exec AddWorkshop "2013-5-1", "17:38:00", "19:38:00", 115, 39, "Ceres Conference", 48
exec AddWorkshop "2012-11-16", "10:54:00", "13:54:00", 97, 30, "European Conference on Computer Vision", 44
exec AddWorkshop "2011-3-27", "16:54:00", "19:54:00", 75, 41, "2013 Ethical Leadership Conference: Ethics in Action", 9
exec AddWorkshop "2011-10-21", "15:20:00", "18:20:00", 101, 46, "RuleML Symposium				", 59
exec AddWorkshop "2013-4-27", "12:5:00", "14:5:00", 119, 48, "The Sixth International Conferences on Advances in Multimedia", 48
exec AddWorkshop "2012-12-11", "13:19:00", "16:19:00", 148, 47, "Leadership Strategies for Information Technology in Health Care Boston", 33
exec AddWorkshop "2011-4-6", "19:23:00", "22:23:00", 68, 27, "Foundations of Genetic Algorithms		", 9
exec AddWorkshop "2011-2-14", "12:36:00", "13:36:00", 143, 31, "International Conference on Automated Reasoning", 41
exec AddWorkshop "2011-6-19", "15:39:00", "17:39:00", 112, 48, "Workshop on Image Processing", 69
exec AddWorkshop "2011-7-9", "17:37:00", "19:37:00", 128, 29, "International Joint Conference on Automated Reasoning", 59
exec AddWorkshop "2011-5-22", "18:31:00", "20:31:00", 57, 26, "International Conference on MultiMedia Modeling", 77
exec AddWorkshop "2012-3-14", "16:34:00", "18:34:00", 116, 25, "The 2nd International CSR Communication Conference", 78
exec AddWorkshop "2013-2-9", "8:2:00", "11:2:00", 123, 27, "Cause Marketing Forum", 59
exec AddWorkshop "2013-4-6", "19:46:00", "21:46:00", 123, 23, "Sentinel-2 for Science WS", 50
exec AddWorkshop "2013-12-30", "14:33:00", "17:33:00", 119, 34, "Annual Interdisciplinary Conference", 54
exec AddWorkshop "2012-3-5", "12:35:00", "14:35:00", 93, 32, "Asian Conference on Computer Vision", 56
exec AddWorkshop "2012-6-14", "13:38:00", "16:38:00", 69, 40, "European Conference on Computer Vision", 9
exec AddWorkshop "2012-7-8", "11:49:00", "12:49:00", 102, 48, "International Conference on Artificial Neural Networks", 25
exec AddWorkshop "2013-9-28", "10:51:00", "12:51:00", 63, 27, "Workshop on Logic ", 64
exec AddWorkshop "2013-2-19", "11:40:00", "13:40:00", 148, 46, "KU Leuven CSR Symposium", 35
exec AddWorkshop "2012-3-24", "12:47:00", "14:47:00", 106, 26, "GNC 2014", 76
exec AddWorkshop "2012-8-29", "14:31:00", "16:31:00", 56, 37, "LPVE Land product validation and evolution", 11
exec AddWorkshop "2012-6-19", "13:51:00", "14:51:00", 121, 35, "Workshop on Logic ", 13
exec AddWorkshop "2011-12-13", "19:42:00", "21:42:00", 143, 21, "International Conference on Computer Vision", 32
exec AddWorkshop "2013-8-5", "17:51:00", "19:51:00", 68, 23, "International Conference on Autonomous Agents and", 48
exec AddWorkshop "2012-9-20", "8:27:00", "10:27:00", 135, 33, "International Joint Conference on Automated Reasoning", 42
exec AddWorkshop "2011-1-11", "8:5:00", "9:5:00", 136, 31, "Cause Marketing Forum", 19
exec AddWorkshop "2011-5-9", "8:29:00", "11:29:00", 146, 26, "International Conference on MultiMedia Modeling", 66
exec AddWorkshop "2013-10-1", "15:41:00", "17:41:00", 133, 29, "RuleML Symposium		", 10
exec AddWorkshop "2012-7-6", "10:18:00", "13:18:00", 145, 34, "2013 Ethical Leadership Conference: Ethics in Action", 23
exec AddWorkshop "2013-2-2", "17:42:00", "20:42:00", 147, 26, "Conference on Computer Vision and Pattern", 24
exec AddWorkshop "2013-7-30", "19:26:00", "21:26:00", 128, 47, "Life in Space for Life on Earth Symposium", 57
exec AddWorkshop "2013-3-26", "19:4:00", "20:4:00", 85, 27, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 16
exec AddWorkshop "2011-5-15", "10:4:00", "13:4:00", 68, 46, "Life in Space for Life on Earth Symposium", 19
exec AddWorkshop "2011-6-15", "9:36:00", "12:36:00", 98, 37, "KU Leuven CSR Symposium", 83
exec AddWorkshop "2013-3-6", "13:4:00", "14:4:00", 126, 48, "Asian Conference on Computer Vision", 74
exec AddWorkshop "2011-10-30", "11:9:00", "13:9:00", 117, 47, "British	Machine	Vision	Conference		", 60
exec AddWorkshop "2011-8-10", "18:60:00", "20:60:00", 90, 47, "Conference on Uncertainty in Artificial Intelligence", 63
exec AddWorkshop "2013-1-17", "15:3:00", "17:3:00", 115, 26, "Net Impact Conference", 12
exec AddWorkshop "2011-4-26", "18:40:00", "19:40:00", 124, 21, "International Conference on MultiMedia Modeling", 8
exec AddWorkshop "2012-4-7", "8:48:00", "11:48:00", 143, 28, "International Semantic Web Conference		", 76
exec AddWorkshop "2012-6-6", "9:44:00", "11:44:00", 118, 27, "7 th International Conference on Bio-inspired Systems and Signal Processing", 11
exec AddWorkshop "2011-5-4", "11:21:00", "13:21:00", 146, 32, "Mechanisms Final Presentation Days", 7
exec AddWorkshop "2011-2-25", "15:29:00", "16:29:00", 105, 42, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 21
exec AddWorkshop "2011-10-27", "8:55:00", "9:55:00", 92, 44, "Client Summit", 29
exec AddWorkshop "2013-11-29", "9:48:00", "11:48:00", 90, 35, "International Conference on the Principles of", 62
exec AddWorkshop "2012-9-28", "16:59:00", "18:59:00", 118, 30, "Science and Challenges of Lunar Sample Return Workshop", 25
exec AddWorkshop "2011-5-29", "16:28:00", "18:28:00", 59, 29, "European Conference on Machine Learning", 4
exec AddWorkshop "2011-9-16", "9:4:00", "11:4:00", 80, 23, "International Conference on Pattern Recognition", 33
exec AddWorkshop "2012-11-16", "14:58:00", "16:58:00", 51, 20, "2nd International Conference on Photonics, Optics and Laser Technology", 45
exec AddWorkshop "2012-11-6", "14:34:00", "17:34:00", 113, 46, "The 12th annual Responsible Business Summit", 51
exec AddWorkshop "2013-3-29", "14:37:00", "15:37:00", 125, 43, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 51
exec AddWorkshop "2013-9-4", "9:30:00", "12:30:00", 65, 29, "2nd International Conference on Photonics, Optics and Laser Technology", 1
exec AddWorkshop "2013-9-1", "17:28:00", "18:28:00", 64, 31, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 21
exec AddWorkshop "2011-2-18", "17:56:00", "18:56:00", 148, 49, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 62
exec AddWorkshop "2011-10-3", "17:37:00", "19:37:00", 64, 33, "International Joint Conference on Artificial Intelligence", 73
exec AddWorkshop "2013-2-30", "12:16:00", "13:16:00", 77, 44, "Foundations of Genetic Algorithms		", 54
exec AddWorkshop "2011-5-29", "18:46:00", "19:46:00", 71, 29, "EuroCOW the Calibration and Orientation Workshop", 67
exec AddWorkshop "2011-4-1", "8:32:00", "10:32:00", 100, 24, "X-Ray Universe", 34
exec AddWorkshop "2012-4-22", "15:41:00", "17:41:00", 51, 22, "Annual Interdisciplinary Conference", 56
exec AddWorkshop "2012-2-5", "9:29:00", "10:29:00", 55, 39, "International Joint Conference on Automated Reasoning", 33
exec AddWorkshop "2013-8-21", "15:24:00", "16:24:00", 58, 35, "International Conference on Computer Vision", 13
exec AddWorkshop "2013-9-14", "15:25:00", "18:25:00", 94, 40, "The 2nd International CSR Communication Conference", 62
exec AddWorkshop "2011-8-19", "8:50:00", "10:50:00", 142, 29, "Computer Analysis of Images and Patterns", 49
exec AddWorkshop "2011-6-20", "8:17:00", "10:17:00", 118, 31, "International Conference on MultiMedia Modeling", 23
exec AddWorkshop "2012-11-7", "11:13:00", "14:13:00", 84, 39, "4S Symposium 2014", 27
exec AddWorkshop "2013-11-8", "9:36:00", "11:36:00", 72, 43, "2nd International Conference on Photonics, Optics and Laser Technology", 57
exec AddWorkshop "2012-4-7", "12:15:00", "13:15:00", 131, 25, "Workshop on Image Processing", 60
exec AddWorkshop "2011-8-14", "9:14:00", "10:14:00", 114, 23, "International Joint Conference on Automated Reasoning", 69
exec AddWorkshop "2012-7-12", "12:22:00", "13:22:00", 97, 29, "Net Impact Conference", 44
exec AddWorkshop "2013-8-3", "15:1:00", "17:1:00", 109, 34, "Sentinel-2 for Science WS", 30
exec AddWorkshop "2012-1-30", "13:56:00", "16:56:00", 64, 27, "GNC 2014", 1
exec AddWorkshop "2013-7-10", "12:5:00", "14:5:00", 57, 44, "Microwave Workshop", 7
exec AddWorkshop "2013-12-5", "17:36:00", "19:36:00", 125, 40, "Conference on Learning Theory		", 40
exec AddWorkshop "2013-11-8", "11:18:00", "12:18:00", 149, 30, "European Conference on Artificial Intelligence	", 71
exec AddWorkshop "2012-9-3", "18:51:00", "19:51:00", 94, 33, "Global Conference on Sustainability and Reporting", 81
exec AddWorkshop "2011-7-5", "12:47:00", "14:47:00", 95, 28, "European Conference on Machine Learning", 62
exec AddWorkshop "2011-5-6", "9:9:00", "12:9:00", 54, 30, "The Sixth International Conferences on Advances in Multimedia", 50
exec AddWorkshop "2012-3-4", "14:4:00", "15:4:00", 67, 42, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 74
exec AddWorkshop "2011-10-16", "9:54:00", "12:54:00", 126, 20, "International Conference on Automated Planning", 12
exec AddWorkshop "2011-9-25", "16:12:00", "17:12:00", 110, 30, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 76
exec AddWorkshop "2013-1-7", "10:24:00", "12:24:00", 65, 42, "International Conference on Computer Vision", 45
exec AddWorkshop "2011-6-7", "14:12:00", "16:12:00", 106, 33, "International Semantic Web Conference		", 84
exec AddWorkshop "2011-8-21", "17:2:00", "18:2:00", 82, 27, "International Conference on Logic for Programming", 81
exec AddWorkshop "2012-12-27", "17:55:00", "20:55:00", 111, 44, "Workshop on Image Processing", 51
exec AddWorkshop "2012-6-2", "18:22:00", "20:22:00", 145, 36, "Mayo Clinic Presents", 65
exec AddWorkshop "2011-7-4", "12:27:00", "15:27:00", 129, 43, "2nd International Conference on Photonics, Optics and Laser Technology", 57
exec AddWorkshop "2011-2-15", "17:47:00", "18:47:00", 116, 25, "Annual Convention of The Society", 1
exec AddWorkshop "2013-6-5", "18:55:00", "19:55:00", 87, 32, "British	Machine	Vision	Conference		", 46
exec AddWorkshop "2012-9-27", "10:14:00", "13:14:00", 56, 34, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 6
exec AddWorkshop "2012-5-4", "8:2:00", "9:2:00", 70, 45, "European Conference on Computer Vision", 69
exec AddWorkshop "2011-6-1", "19:22:00", "21:22:00", 140, 49, "International Conference on Signal and Imaging Systems Engineering", 11
exec AddWorkshop "2011-8-13", "17:60:00", "19:60:00", 99, 21, "Euclid Spacecraft Industry Day", 30
exec AddWorkshop "2011-11-22", "10:22:00", "11:22:00", 124, 26, "International Conference on Automated Planning", 81
exec AddWorkshop "2012-1-16", "8:59:00", "9:59:00", 98, 27, "European Space Technology Harmonisation Conference", 20
exec AddWorkshop "2012-5-25", "14:5:00", "17:5:00", 113, 30, "RuleML Symposium				", 63
exec AddWorkshop "2012-6-18", "17:40:00", "20:40:00", 85, 33, "International Conference on Computer Vision Theory and Applications", 25
exec AddWorkshop "2012-11-24", "10:10:00", "11:10:00", 108, 31, "Microwave Workshop", 84
exec AddWorkshop "2012-12-12", "9:14:00", "12:14:00", 80, 20, "LPVE Land product validation and evolution", 5
exec AddWorkshop "2012-7-2", "17:11:00", "18:11:00", 112, 36, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 56
exec AddWorkshop "2013-4-20", "11:51:00", "14:51:00", 117, 30, "Annual Convention of The Society", 29
exec AddWorkshop "2013-6-24", "8:20:00", "9:20:00", 144, 41, "European Navigation Conference 2014 (ENC-GNSS 2014)", 8
exec AddWorkshop "2011-7-25", "10:49:00", "11:49:00", 61, 49, "LPVE Land product validation and evolution", 5
exec AddWorkshop "2011-5-16", "17:41:00", "20:41:00", 73, 47, "International Conference on Logic for Programming", 35
exec AddWorkshop "2012-4-5", "12:51:00", "13:51:00", 88, 38, "Business4Better: The Community Partnership Movement", 24
exec AddWorkshop "2013-5-2", "15:51:00", "18:51:00", 76, 41, "Annual Interdisciplinary Conference", 71
exec AddWorkshop "2013-10-15", "18:21:00", "20:21:00", 79, 34, "Client Summit", 38
exec AddWorkshop "2011-2-23", "19:14:00", "20:14:00", 112, 45, "RuleML Symposium		", 32
exec AddWorkshop "2012-9-3", "10:36:00", "13:36:00", 136, 45, "The Corporate Philanthropy Forum", 62
exec AddWorkshop "2012-10-21", "18:4:00", "19:4:00", 90, 23, "The Corporate Philanthropy Forum", 45
exec AddWorkshop "2013-6-20", "14:34:00", "15:34:00", 143, 22, "KU Leuven CSR Symposium", 51
exec AddWorkshop "2013-10-23", "11:48:00", "13:48:00", 133, 38, "International Conference on the Principles of", 0
exec AddWorkshop "2013-9-30", "19:58:00", "22:58:00", 122, 41, "48th ESLAB Symposium", 41
exec AddWorkshop "2011-4-23", "11:17:00", "12:17:00", 95, 31, "2013 Ethical Leadership Conference: Ethics in Action", 31
exec AddWorkshop "2013-2-3", "17:27:00", "19:27:00", 85, 44, "The Sixth International Conferences on Advances in Multimedia", 55
exec AddWorkshop "2011-3-28", "14:44:00", "16:44:00", 108, 42, "EuroCOW the Calibration and Orientation Workshop", 24
exec AddWorkshop "2011-5-27", "12:54:00", "13:54:00", 75, 23, "International Conference on MultiMedia Modeling", 84
exec AddWorkshop "2013-6-19", "9:41:00", "11:41:00", 134, 41, "7 th International Conference on Bio-inspired Systems and Signal Processing", 39
exec AddWorkshop "2013-1-24", "11:28:00", "13:28:00", 68, 23, "Annual Convention of The Society", 56
exec AddWorkshop "2013-10-30", "9:10:00", "10:10:00", 105, 37, "The BCLC CSR Conference", 31
exec AddWorkshop "2011-4-22", "11:53:00", "14:53:00", 128, 42, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 7
exec AddWorkshop "2012-12-6", "10:1:00", "11:1:00", 73, 49, "Foundations of Genetic Algorithms		", 31
exec AddWorkshop "2011-6-14", "11:34:00", "13:34:00", 83, 38, "The Corporate Philanthropy Forum", 54
exec AddWorkshop "2011-11-24", "9:43:00", "11:43:00", 62, 24, "Conference on Learning Theory		", 37
exec AddWorkshop "2012-7-13", "13:37:00", "15:37:00", 58, 20, "2nd International Conference on Photonics, Optics and Laser Technology", 25
exec AddWorkshop "2012-6-10", "17:32:00", "19:32:00", 142, 47, "Euclid Spacecraft Industry Day", 79
exec AddWorkshop "2011-11-20", "12:43:00", "14:43:00", 93, 26, "ARTES 1 Final Presentation Days", 36
exec AddWorkshop "2012-12-3", "17:25:00", "20:25:00", 72, 21, "International Conference on Artificial Neural Networks", 54
exec AddWorkshop "2011-7-16", "17:25:00", "18:25:00", 114, 31, "48th ESLAB Symposium", 40
exec AddWorkshop "2011-7-8", "9:32:00", "10:32:00", 88, 39, "International Conference on MultiMedia Modeling", 15
exec AddWorkshop "2013-11-27", "16:55:00", "19:55:00", 118, 24, "Annual Convention of The Society", 19
exec AddWorkshop "2011-11-10", "14:3:00", "15:3:00", 50, 21, "Global Conference on Sustainability and Reporting", 33
exec AddWorkshop "2012-9-3", "12:29:00", "13:29:00", 114, 35, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 83
exec AddWorkshop "2012-7-28", "13:39:00", "15:39:00", 95, 35, "�International� Corporate Citizenship Conference", 14
exec AddWorkshop "2013-12-21", "8:3:00", "11:3:00", 104, 43, "Annual Interdisciplinary Conference", 70
exec AddWorkshop "2013-9-15", "10:38:00", "13:38:00", 146, 35, "Mechanisms Final Presentation Days", 57
exec AddWorkshop "2012-10-11", "15:60:00", "18:60:00", 126, 27, "Sustainable Brands Conference", 15
exec AddWorkshop "2012-10-21", "10:47:00", "13:47:00", 111, 45, "Cause Marketing Forum", 10
exec AddWorkshop "2012-10-8", "13:46:00", "15:46:00", 94, 29, "ARTES 1 Final Presentation Days", 39
exec AddWorkshop "2012-7-15", "13:13:00", "15:13:00", 97, 38, "The BCLC CSR Conference", 14
exec AddWorkshop "2011-5-4", "12:48:00", "15:48:00", 100, 30, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 41
exec AddWorkshop "2012-3-5", "16:22:00", "17:22:00", 86, 28, "Computer Analysis of Images and Patterns", 54
exec AddWorkshop "2013-10-24", "18:7:00", "19:7:00", 105, 29, "7 th International Conference on Bio-inspired Systems and Signal Processing", 41
exec AddWorkshop "2013-1-6", "15:38:00", "18:38:00", 72, 33, "Mayo Clinic Presents", 83
exec AddWorkshop "2011-8-18", "12:22:00", "14:22:00", 137, 36, "The 12th annual Responsible Business Summit", 22
exec AddWorkshop "2012-5-22", "13:55:00", "16:55:00", 149, 29, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 72
exec AddWorkshop "2011-7-18", "16:17:00", "18:17:00", 138, 46, "British	Machine	Vision	Conference		", 2
exec AddWorkshop "2013-6-17", "9:34:00", "12:34:00", 74, 40, "Conference on Learning Theory		", 55
exec AddWorkshop "2013-11-22", "18:32:00", "19:32:00", 105, 40, "Foundations of Genetic Algorithms		", 21
exec AddWorkshop "2013-9-12", "12:47:00", "15:47:00", 83, 29, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 75
exec AddWorkshop "2011-9-27", "13:51:00", "16:51:00", 89, 47, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 13
exec AddWorkshop "2012-9-30", "11:37:00", "12:37:00", 83, 35, "International Conference on Automated Planning", 16
exec AddWorkshop "2013-6-28", "15:8:00", "16:8:00", 147, 32, "The BCLC CSR Conference", 26
exec AddWorkshop "2012-6-26", "16:41:00", "17:41:00", 127, 39, "2nd International Conference on Photonics, Optics and Laser Technology", 77
exec AddWorkshop "2011-1-19", "17:28:00", "18:28:00", 58, 23, "International Conference on MultiMedia Modeling", 65
exec AddWorkshop "2012-7-24", "19:52:00", "22:52:00", 51, 35, "International Joint Conference on Automated Reasoning", 46
exec AddWorkshop "2012-3-20", "9:32:00", "11:32:00", 143, 46, "Global Conference on Sustainability and Reporting", 60
exec AddWorkshop "2013-8-23", "10:42:00", "11:42:00", 129, 42, "48th ESLAB Symposium", 44
exec AddWorkshop "2012-6-20", "12:53:00", "13:53:00", 98, 41, "The Sixth International Conferences on Advances in Multimedia", 58
exec AddWorkshop "2011-11-23", "9:5:00", "10:5:00", 99, 41, "International Conference on Signal and Imaging Systems Engineering", 40
exec AddWorkshop "2013-2-7", "8:56:00", "10:56:00", 114, 33, "Conference on Uncertainty in Artificial Intelligence", 69
exec AddWorkshop "2013-6-14", "10:14:00", "12:14:00", 88, 32, "British	Machine	Vision	Conference		", 51
exec AddWorkshop "2011-11-16", "10:58:00", "13:58:00", 138, 34, "Conference on Automated Deduction		", 44
exec AddWorkshop "2011-1-24", "13:23:00", "16:23:00", 139, 32, "European Conference on Artificial Intelligence	", 71
exec AddWorkshop "2012-4-25", "12:38:00", "14:38:00", 77, 39, "Net Impact Conference", 17
exec AddWorkshop "2013-8-24", "15:5:00", "17:5:00", 63, 33, "Ceres Conference", 11
exec AddWorkshop "2011-2-24", "9:60:00", "12:60:00", 80, 44, "Ceres Conference", 81
exec AddWorkshop "2013-4-7", "13:55:00", "14:55:00", 127, 29, "�International� Corporate Citizenship Conference", 37
exec AddWorkshop "2011-1-20", "9:47:00", "12:47:00", 74, 33, "International Conference on Pattern Recognition", 16
exec AddWorkshop "2011-1-15", "14:19:00", "16:19:00", 127, 32, "Client Summit", 79
exec AddWorkshop "2013-7-19", "11:33:00", "14:33:00", 144, 20, "Foundations of Genetic Algorithms		", 11
exec AddWorkshop "2011-7-4", "12:12:00", "15:12:00", 111, 46, "Conference on Learning Theory		", 56
exec AddWorkshop "2013-2-22", "17:23:00", "20:23:00", 54, 39, "International Joint Conference on Artificial Intelligence", 75
exec AddWorkshop "2013-7-3", "15:17:00", "17:17:00", 87, 31, "Asian Conference on Computer Vision", 65
exec AddWorkshop "2012-3-11", "18:39:00", "20:39:00", 61, 35, "Annual Convention of The Society", 22
exec AddWorkshop "2012-11-1", "12:20:00", "14:20:00", 147, 48, "KU Leuven CSR Symposium", 55
exec AddWorkshop "2011-3-23", "17:56:00", "18:56:00", 85, 48, "GNC 2014", 51
exec AddWorkshop "2011-10-1", "17:29:00", "20:29:00", 116, 45, "Asian Conference on Computer Vision", 47
exec AddWorkshop "2012-2-9", "13:49:00", "15:49:00", 142, 33, "European Navigation Conference 2014 (ENC-GNSS 2014)", 73
exec AddWorkshop "2011-12-28", "16:13:00", "19:13:00", 99, 44, "European Conference on Computer Vision", 71
exec AddWorkshop "2012-2-24", "16:47:00", "17:47:00", 134, 43, "48th ESLAB Symposium", 61
exec AddWorkshop "2011-8-30", "9:46:00", "10:46:00", 133, 26, "Mayo Clinic Presents", 27
exec AddWorkshop "2011-1-15", "18:57:00", "21:57:00", 97, 40, "Life in Space for Life on Earth Symposium", 26
exec AddWorkshop "2011-4-20", "19:13:00", "20:13:00", 116, 31, "International Conference on Computer Vision Theory and Applications", 43
exec AddWorkshop "2011-10-24", "19:46:00", "22:46:00", 140, 36, "LPVE Land product validation and evolution", 1
exec AddWorkshop "2011-7-21", "15:58:00", "17:58:00", 132, 49, "The Corporate Philanthropy Forum", 18
exec AddWorkshop "2011-9-30", "14:37:00", "15:37:00", 63, 49, "X-Ray Universe", 30
exec AddWorkshop "2011-11-17", "19:46:00", "22:46:00", 125, 36, "4S Symposium 2014", 18
exec AddWorkshop "2012-1-4", "11:53:00", "13:53:00", 114, 39, "International Conference on Pattern Recognition", 74
exec AddWorkshop "2013-6-4", "12:29:00", "14:29:00", 83, 31, "Life in Space for Life on Earth Symposium", 40
exec AddWorkshop "2012-2-29", "11:49:00", "12:49:00", 145, 45, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 53
exec AddWorkshop "2012-1-4", "11:10:00", "12:10:00", 95, 22, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 16
exec AddWorkshop "2012-2-8", "11:42:00", "12:42:00", 140, 25, "International Conference on Automated Reasoning", 47
exec AddWorkshop "2011-12-12", "13:45:00", "14:45:00", 89, 34, "International Conference on the Principles of", 15
exec AddWorkshop "2013-12-7", "19:3:00", "22:3:00", 105, 29, "The BCLC CSR Conference", 1
exec AddWorkshop "2012-10-2", "14:24:00", "15:24:00", 62, 41, "LPVE Land product validation and evolution", 56
exec AddWorkshop "2013-4-12", "11:41:00", "12:41:00", 131, 46, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 16
exec AddWorkshop "2011-12-25", "10:48:00", "13:48:00", 106, 49, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 14
exec AddWorkshop "2013-10-23", "14:31:00", "17:31:00", 91, 33, "Business4Better: The Community Partnership Movement", 1
exec AddWorkshop "2013-2-3", "17:17:00", "18:17:00", 77, 23, "3rd International Conference on Pattern Recognition Applications and Methods", 52
exec AddWorkshop "2011-9-30", "10:56:00", "12:56:00", 89, 45, "The 12th annual Responsible Business Summit", 40
exec AddWorkshop "2011-8-20", "8:20:00", "9:20:00", 99, 33, "International Conference on Pattern Recognition", 14
exec AddWorkshop "2011-2-24", "16:7:00", "18:7:00", 120, 43, "European Space Technology Harmonisation Conference", 0
exec AddWorkshop "2013-11-23", "17:56:00", "19:56:00", 129, 46, "Conference on Learning Theory		", 0
exec AddWorkshop "2011-12-3", "13:51:00", "14:51:00", 86, 46, "European Space Technology Harmonisation Conference", 6
exec AddWorkshop "2011-10-21", "12:56:00", "14:56:00", 115, 25, "Business4Better: The Community Partnership Movement", 3
exec AddWorkshop "2011-6-1", "16:60:00", "17:60:00", 71, 41, "International Conference on Computer Vision", 81
exec AddWorkshop "2012-9-3", "14:23:00", "15:23:00", 51, 38, "International Conference on Computer Vision Theory and Applications", 30
exec AddWorkshop "2011-12-18", "17:49:00", "19:49:00", 116, 26, "The 2nd International CSR Communication Conference", 51
exec AddWorkshop "2012-9-1", "18:16:00", "19:16:00", 135, 22, "The Sixth International Conferences on Advances in Multimedia", 26
exec AddWorkshop "2013-2-29", "14:43:00", "15:43:00", 83, 41, "Annual Convention of The Society", 53
exec AddWorkshop "2012-1-29", "13:39:00", "14:39:00", 58, 49, "Foundations of Genetic Algorithms		", 35
exec AddWorkshop "2011-2-14", "17:29:00", "18:29:00", 140, 26, "The 2nd International CSR Communication Conference", 82
exec AddWorkshop "2013-2-9", "15:25:00", "17:25:00", 67, 38, "Conference on Automated Deduction		", 25
exec AddWorkshop "2011-11-4", "18:29:00", "20:29:00", 79, 29, "Client Summit", 7
exec AddWorkshop "2012-9-18", "15:48:00", "17:48:00", 94, 30, "International Conference on MultiMedia Modeling", 50
exec AddWorkshop "2013-7-16", "10:11:00", "12:11:00", 84, 35, "ICATT", 20
exec AddWorkshop "2012-12-30", "19:6:00", "22:6:00", 126, 48, "International Semantic Web Conference		", 4
exec AddWorkshop "2013-2-18", "19:28:00", "20:28:00", 54, 30, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 27
exec AddWorkshop "2013-6-11", "8:23:00", "9:23:00", 51, 43, "International Conference on Autonomous Agents and", 79
exec AddWorkshop "2012-3-19", "17:53:00", "20:53:00", 115, 20, "Client Summit", 69
exec AddWorkshop "2012-10-27", "15:24:00", "17:24:00", 54, 20, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 63
exec AddWorkshop "2012-3-16", "13:40:00", "15:40:00", 55, 32, "International Conference on Pattern Recognition", 53
exec AddWorkshop "2012-7-15", "13:54:00", "16:54:00", 75, 42, "British	Machine	Vision	Conference		", 44
exec AddWorkshop "2012-12-3", "17:50:00", "19:50:00", 136, 38, "International Conference on Automated Reasoning", 55
exec AddWorkshop "2013-6-29", "12:7:00", "14:7:00", 122, 21, "2013 Ethical Leadership Conference: Ethics in Action", 52
exec AddWorkshop "2012-11-8", "16:14:00", "17:14:00", 133, 39, "Annual Convention of The Society", 6
exec AddWorkshop "2013-3-5", "19:28:00", "20:28:00", 112, 36, "Asian Conference on Computer Vision", 69
exec AddWorkshop "2011-5-13", "10:6:00", "11:6:00", 115, 42, "Conference on Automated Deduction		", 35
exec AddWorkshop "2012-4-29", "11:35:00", "12:35:00", 106, 38, "Life in Space for Life on Earth Symposium", 56
exec AddWorkshop "2013-10-28", "14:60:00", "15:60:00", 80, 45, "International Conference on Automated Reasoning", 68
exec AddWorkshop "2011-7-20", "14:59:00", "16:59:00", 107, 35, "Corporate Community Involvement Conference", 72
exec AddWorkshop "2011-11-10", "10:7:00", "11:7:00", 137, 40, "Computer Analysis of Images and Patterns", 39
exec AddWorkshop "2013-11-29", "15:60:00", "17:60:00", 119, 34, "7 th International Conference on Bio-inspired Systems and Signal Processing", 39
exec AddWorkshop "2012-9-4", "16:11:00", "19:11:00", 67, 30, "Workshop on Image Processing", 29
exec AddWorkshop "2012-9-14", "10:48:00", "11:48:00", 76, 47, "Mechanisms Final Presentation Days", 2
exec AddWorkshop "2011-11-1", "12:58:00", "13:58:00", 129, 49, "GNC 2014", 79
exec AddWorkshop "2012-4-7", "18:39:00", "19:39:00", 112, 47, "Corporate Community Involvement Conference", 65
exec AddWorkshop "2011-10-14", "14:44:00", "15:44:00", 112, 33, "4th DUE Permafrost User Workshop", 68
exec AddWorkshop "2012-2-23", "17:41:00", "19:41:00", 87, 23, "Annual Interdisciplinary Conference", 58
exec AddWorkshop "2013-6-19", "17:40:00", "19:40:00", 134, 32, "International Conference on Pattern Recognition", 52
exec AddWorkshop "2012-1-10", "16:35:00", "17:35:00", 126, 44, "International Conference on Logic for Programming", 14
exec AddWorkshop "2012-3-12", "11:60:00", "13:60:00", 139, 49, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 0
exec AddWorkshop "2011-5-1", "15:34:00", "16:34:00", 104, 25, "Science and Challenges of Lunar Sample Return Workshop", 58
exec AddWorkshop "2012-12-24", "17:46:00", "18:46:00", 89, 28, "KU Leuven CSR Symposium", 70
exec AddWorkshop "2011-11-21", "10:48:00", "11:48:00", 57, 24, "ICATT", 52
exec AddWorkshop "2012-7-13", "18:40:00", "19:40:00", 75, 42, "Workshop on Logic ", 56
exec AddWorkshop "2013-2-22", "17:28:00", "19:28:00", 84, 35, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 81
exec AddWorkshop "2012-9-6", "12:1:00", "15:1:00", 109, 32, "Workshop on Logic ", 66
exec AddWorkshop "2013-8-7", "12:43:00", "13:43:00", 92, 21, "European Conference on Computer Vision", 76
exec AddWorkshop "2012-8-15", "15:1:00", "17:1:00", 131, 25, "Leadership Strategies for Information Technology in Health Care Boston", 26
exec AddWorkshop "2013-12-9", "15:57:00", "17:57:00", 146, 28, "Conference on Uncertainty in Artificial Intelligence", 79
exec AddWorkshop "2013-3-15", "10:46:00", "11:46:00", 139, 23, "European Navigation Conference 2014 (ENC-GNSS 2014)", 22
exec AddWorkshop "2011-2-29", "12:20:00", "14:20:00", 141, 46, "Workshop on Image Processing", 50
exec AddWorkshop "2012-2-17", "19:44:00", "21:44:00", 59, 22, "EuroCOW the Calibration and Orientation Workshop", 69
exec AddWorkshop "2012-12-26", "17:39:00", "20:39:00", 67, 34, "International Semantic Web Conference		", 78
exec AddWorkshop "2013-10-10", "9:48:00", "11:48:00", 126, 48, "The 12th annual Responsible Business Summit", 30
exec AddWorkshop "2013-2-11", "9:58:00", "10:58:00", 99, 20, "Corporate Community Involvement Conference", 30
exec AddWorkshop "2012-2-26", "19:12:00", "21:12:00", 88, 38, "Ceres Conference", 66
exec AddWorkshop "2011-7-14", "18:49:00", "21:49:00", 70, 29, "The 12th annual Responsible Business Summit", 81
exec AddWorkshop "2012-3-27", "8:25:00", "9:25:00", 117, 40, "European Conference on Computer Vision", 4
exec AddWorkshop "2013-11-12", "17:19:00", "19:19:00", 91, 20, "International Conference on the Principles of", 68
exec AddWorkshop "2011-12-7", "9:56:00", "11:56:00", 80, 22, "Conference on Computer Vision and Pattern", 76
exec AddWorkshop "2013-11-29", "8:21:00", "9:21:00", 145, 30, "Microwave Workshop", 46
exec AddWorkshop "2013-2-3", "10:49:00", "11:49:00", 76, 20, "KU Leuven CSR Symposium", 70
exec AddWorkshop "2012-12-17", "11:13:00", "12:13:00", 131, 29, "Photonics West", 58
exec AddWorkshop "2011-10-23", "12:33:00", "15:33:00", 147, 40, "European Space Technology Harmonisation Conference", 59
exec AddWorkshop "2011-3-11", "13:40:00", "15:40:00", 126, 33, "International Conference on Artificial Neural Networks", 74
exec AddWorkshop "2012-4-10", "15:31:00", "17:31:00", 69, 40, "EuroCOW the Calibration and Orientation Workshop", 9
exec AddWorkshop "2013-5-12", "9:18:00", "11:18:00", 70, 29, "International Joint Conference on Artificial Intelligence", 49
exec AddWorkshop "2012-3-4", "15:42:00", "18:42:00", 75, 20, "EuroCOW the Calibration and Orientation Workshop", 67
exec AddWorkshop "2011-7-11", "14:44:00", "15:44:00", 115, 35, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 39
exec AddWorkshop "2013-9-3", "15:29:00", "16:29:00", 149, 46, "Client Summit", 21
exec AddWorkshop "2011-3-2", "15:42:00", "18:42:00", 97, 39, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 81
exec AddWorkshop "2013-9-4", "10:26:00", "13:26:00", 133, 32, "European Space Technology Harmonisation Conference", 42
exec AddWorkshop "2013-5-15", "14:57:00", "15:57:00", 107, 28, "The 2nd International CSR Communication Conference", 58
exec AddWorkshop "2012-2-23", "13:32:00", "16:32:00", 97, 33, "Net Impact Conference", 74
exec AddWorkshop "2012-7-3", "19:27:00", "21:27:00", 55, 27, "Sentinel-2 for Science WS", 26
exec AddWorkshop "2012-11-5", "9:28:00", "12:28:00", 106, 25, "Mayo Clinic Presents", 83
exec AddWorkshop "2011-6-21", "8:56:00", "10:56:00", 138, 20, "Workshop on Logic ", 75
exec AddWorkshop "2011-9-23", "9:50:00", "10:50:00", 61, 30, "The BCLC CSR Conference", 16
exec AddWorkshop "2013-7-30", "19:20:00", "20:20:00", 98, 34, "International Conference on Automated Planning", 45
exec AddWorkshop "2012-8-9", "10:36:00", "12:36:00", 51, 42, "7 th International Conference on Bio-inspired Systems and Signal Processing", 19
exec AddWorkshop "2012-2-10", "14:35:00", "16:35:00", 77, 47, "Conference on Automated Deduction		", 18
exec AddWorkshop "2013-12-18", "10:54:00", "12:54:00", 96, 32, "Workshop on Logic ", 64
exec AddWorkshop "2013-5-22", "18:39:00", "21:39:00", 129, 45, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 81
exec AddWorkshop "2013-6-11", "15:20:00", "16:20:00", 109, 32, "International Joint Conference on Artificial Intelligence", 47
exec AddWorkshop "2013-3-3", "19:17:00", "22:17:00", 123, 45, "X-Ray Universe", 51
exec AddWorkshop "2011-2-30", "10:8:00", "13:8:00", 119, 28, "Sentinel-2 for Science WS", 55
exec AddWorkshop "2012-10-8", "11:5:00", "13:5:00", 52, 28, "Asian Conference on Computer Vision", 50
exec AddWorkshop "2012-11-16", "10:16:00", "11:16:00", 122, 41, "British	Machine	Vision	Conference		", 41
exec AddWorkshop "2013-11-21", "18:50:00", "21:50:00", 133, 47, "3rd International Conference on Pattern Recognition Applications and Methods", 3
exec AddWorkshop "2011-2-12", "9:15:00", "12:15:00", 145, 33, "International Conference on Automated Reasoning", 6
exec AddWorkshop "2013-4-12", "8:46:00", "9:46:00", 71, 34, "Annual Convention of The Society", 0
exec AddWorkshop "2013-12-14", "12:49:00", "14:49:00", 101, 45, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 51
exec AddWorkshop "2011-7-5", "12:3:00", "13:3:00", 67, 38, "ICATT", 71
exec AddWorkshop "2013-6-29", "13:49:00", "14:49:00", 141, 45, "European Conference on Machine Learning", 83
exec AddWorkshop "2013-5-5", "12:2:00", "15:2:00", 95, 21, "RuleML Symposium		", 41
exec AddWorkshop "2013-1-3", "11:42:00", "13:42:00", 106, 34, "Computer Analysis of Images and Patterns", 29
exec AddWorkshop "2011-8-9", "18:48:00", "20:48:00", 141, 44, "2013 Ethical Leadership Conference: Ethics in Action", 34
exec AddWorkshop "2011-1-26", "9:14:00", "12:14:00", 76, 35, "Net Impact Conference", 41
exec AddWorkshop "2012-6-7", "18:6:00", "21:6:00", 127, 47, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 69
exec AddWorkshop "2012-10-17", "15:50:00", "17:50:00", 104, 33, "Cause Marketing Forum", 15
exec AddWorkshop "2011-10-9", "16:20:00", "18:20:00", 107, 48, "Cause Marketing Forum", 23
exec AddWorkshop "2013-1-6", "9:57:00", "11:57:00", 67, 49, "Conference on Learning Theory		", 32
exec AddWorkshop "2012-1-4", "8:49:00", "10:49:00", 105, 31, "International Conference on Computer Vision Theory and Applications", 63
exec AddWorkshop "2011-9-16", "9:11:00", "11:11:00", 138, 36, "2nd International Conference on Photonics, Optics and Laser Technology", 68
exec AddWorkshop "2011-8-29", "19:29:00", "20:29:00", 58, 21, "Global Conference on Sustainability and Reporting", 20
exec AddWorkshop "2013-6-3", "8:49:00", "10:49:00", 126, 22, "European Conference on Machine Learning", 37
exec AddWorkshop "2011-3-12", "8:43:00", "9:43:00", 134, 35, "ICATT", 55
exec AddWorkshop "2011-9-4", "15:59:00", "18:59:00", 110, 45, "Life in Space for Life on Earth Symposium", 61
exec AddWorkshop "2012-10-20", "16:17:00", "17:17:00", 92, 49, "International Conference on MultiMedia Modeling", 76
exec AddWorkshop "2011-9-23", "10:17:00", "11:17:00", 104, 38, "International Conference on Logic for Programming", 65
exec AddWorkshop "2013-2-16", "11:25:00", "12:25:00", 97, 27, "RuleML Symposium				", 82
exec AddWorkshop "2013-1-4", "13:6:00", "15:6:00", 113, 37, "International Semantic Web Conference		", 19
exec AddWorkshop "2012-11-17", "19:14:00", "21:14:00", 60, 21, "7 th International Conference on Bio-inspired Systems and Signal Processing", 44
exec AddWorkshop "2011-7-26", "18:25:00", "21:25:00", 80, 47, "Business4Better: The Community Partnership Movement", 47
exec AddWorkshop "2013-8-26", "17:59:00", "20:59:00", 80, 47, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 49
exec AddWorkshop "2013-7-8", "15:53:00", "18:53:00", 73, 43, "Cause Marketing Forum", 40
exec AddWorkshop "2012-11-6", "17:14:00", "18:14:00", 108, 45, "International Conference on the Principles of", 1
exec AddWorkshop "2011-3-12", "17:47:00", "20:47:00", 105, 20, "2013 Ethical Leadership Conference: Ethics in Action", 22
exec AddWorkshop "2012-10-20", "14:13:00", "15:13:00", 130, 47, "Computer Analysis of Images and Patterns", 65
exec AddWorkshop "2012-2-6", "9:58:00", "10:58:00", 129, 20, "The BCLC CSR Conference", 22
exec AddWorkshop "2013-9-8", "11:44:00", "14:44:00", 112, 39, "Leadership Strategies for Information Technology in Health Care Boston", 17
exec AddWorkshop "2011-11-14", "13:43:00", "15:43:00", 88, 25, "International Conference on Computer Vision", 58
exec AddWorkshop "2012-2-9", "16:50:00", "18:50:00", 144, 39, "2013 Ethical Leadership Conference: Ethics in Action", 76
exec AddWorkshop "2012-12-4", "17:25:00", "19:25:00", 77, 40, "RuleML Symposium		", 83
exec AddWorkshop "2011-2-1", "12:33:00", "13:33:00", 102, 20, "Asian Conference on Computer Vision", 54
exec AddWorkshop "2013-3-20", "16:27:00", "19:27:00", 89, 22, "Workshop on Image Processing", 51
exec AddWorkshop "2013-1-2", "15:28:00", "16:28:00", 88, 25, "Sustainable Brands Conference", 20
exec AddWorkshop "2012-8-10", "13:24:00", "16:24:00", 103, 48, "RuleML Symposium				", 26
exec AddWorkshop "2012-1-19", "19:48:00", "21:48:00", 60, 35, "European Space Power Conference", 62
exec AddWorkshop "2012-4-22", "18:55:00", "19:55:00", 133, 49, "GNC 2014", 18
exec AddWorkshop "2012-2-4", "15:18:00", "18:18:00", 77, 26, "ARTES 1 Final Presentation Days", 6
exec AddWorkshop "2013-5-1", "11:44:00", "12:44:00", 92, 27, "Cause Marketing Forum", 40
exec AddWorkshop "2011-1-20", "19:11:00", "21:11:00", 120, 35, "Business4Better: The Community Partnership Movement", 81
exec AddWorkshop "2013-7-20", "18:2:00", "20:2:00", 67, 41, "Business4Better: The Community Partnership Movement", 50
exec AddWorkshop "2012-9-11", "18:4:00", "21:4:00", 84, 25, "Leadership Strategies for Information Technology in Health Care Boston", 30
exec AddWorkshop "2013-10-25", "15:55:00", "18:55:00", 144, 46, "RuleML Symposium				", 76
exec AddWorkshop "2011-10-20", "9:52:00", "11:52:00", 125, 45, "ARTES 1 Final Presentation Days", 34
exec AddWorkshop "2011-4-24", "10:32:00", "11:32:00", 126, 46, "Net Impact Conference", 6
exec AddWorkshop "2011-5-28", "19:51:00", "21:51:00", 145, 40, "48th ESLAB Symposium", 17
exec AddWorkshop "2013-12-12", "8:12:00", "10:12:00", 109, 38, "International Conference on Pattern Recognition", 68
exec AddWorkshop "2011-8-29", "13:21:00", "16:21:00", 135, 37, "International Conference on Autonomous Agents and", 22
exec AddWorkshop "2013-11-28", "18:54:00", "19:54:00", 148, 41, "Conference on Learning Theory		", 46
exec AddWorkshop "2011-11-12", "8:31:00", "9:31:00", 124, 31, "International Conference on Signal and Imaging Systems Engineering", 37
exec AddWorkshop "2013-3-6", "11:49:00", "12:49:00", 73, 20, "Conference on Automated Deduction		", 19
exec AddWorkshop "2013-3-6", "9:52:00", "10:52:00", 63, 48, "Global Conference on Sustainability and Reporting", 71
exec AddWorkshop "2012-6-22", "17:29:00", "19:29:00", 103, 24, "European Navigation Conference 2014 (ENC-GNSS 2014)", 57
exec AddWorkshop "2013-1-19", "13:7:00", "14:7:00", 80, 27, "International Conference on Autonomous Agents and", 37
exec AddWorkshop "2011-9-26", "14:32:00", "17:32:00", 136, 49, "2013 Ethical Leadership Conference: Ethics in Action", 38
exec AddWorkshop "2013-5-16", "17:32:00", "19:32:00", 100, 37, "ARTES 1 Final Presentation Days", 73
exec AddWorkshop "2012-1-8", "8:24:00", "11:24:00", 94, 28, "European Conference on Artificial Intelligence	", 57
exec AddWorkshop "2011-8-19", "14:8:00", "17:8:00", 128, 39, "KU Leuven CSR Symposium", 6
exec AddWorkshop "2012-9-21", "10:22:00", "11:22:00", 95, 22, "2013 Ethical Leadership Conference: Ethics in Action", 40
exec AddWorkshop "2012-7-28", "9:2:00", "12:2:00", 139, 25, "GNC 2014", 35
exec AddWorkshop "2011-1-6", "9:33:00", "10:33:00", 53, 32, "Foundations of Genetic Algorithms		", 27
exec AddWorkshop "2011-2-5", "14:34:00", "15:34:00", 53, 21, "X-Ray Universe", 36
exec AddWorkshop "2012-6-30", "18:59:00", "19:59:00", 119, 37, "British	Machine	Vision	Conference		", 29
exec AddWorkshop "2013-4-22", "15:8:00", "18:8:00", 93, 26, "2nd International Conference on Photonics, Optics and Laser Technology", 7
exec AddWorkshop "2012-10-20", "13:31:00", "14:31:00", 86, 23, "Corporate Community Involvement Conference", 51
exec AddWorkshop "2011-12-19", "18:34:00", "21:34:00", 69, 43, "European Conference on Computer Vision", 5
exec AddWorkshop "2012-9-30", "10:17:00", "13:17:00", 119, 28, "Workshop on Image Processing", 31
exec AddWorkshop "2011-12-27", "10:29:00", "13:29:00", 144, 23, "Life in Space for Life on Earth Symposium", 18
exec AddWorkshop "2011-1-14", "10:3:00", "13:3:00", 145, 28, "European Space Power Conference", 57
exec AddWorkshop "2012-1-19", "18:50:00", "20:50:00", 127, 32, "International Conference on Automated Reasoning", 60
exec AddWorkshop "2012-6-11", "13:21:00", "15:21:00", 113, 43, "International Conference on Signal and Imaging Systems Engineering", 34
exec AddWorkshop "2011-7-11", "11:51:00", "13:51:00", 61, 45, "Sustainable Brands Conference", 9
exec AddWorkshop "2012-10-8", "10:34:00", "12:34:00", 85, 35, "The Corporate Philanthropy Forum", 22
exec AddWorkshop "2012-10-27", "9:56:00", "11:56:00", 128, 20, "Annual Interdisciplinary Conference", 72
exec AddWorkshop "2013-4-25", "12:44:00", "13:44:00", 93, 38, "International Conference on Signal and Imaging Systems Engineering", 0
exec AddWorkshop "2012-4-2", "8:12:00", "10:12:00", 96, 24, "Net Impact Conference", 81
exec AddWorkshop "2012-6-28", "19:41:00", "21:41:00", 60, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 12
exec AddWorkshop "2011-11-11", "16:27:00", "17:27:00", 60, 23, "International Conference on Automated Planning", 21
exec AddWorkshop "2013-4-10", "13:3:00", "14:3:00", 111, 43, "British	Machine	Vision	Conference		", 81
exec AddWorkshop "2013-6-25", "8:14:00", "9:14:00", 98, 25, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 0
exec AddWorkshop "2012-3-18", "17:22:00", "19:22:00", 123, 30, "International Conference on Computer Vision Theory and Applications", 27
exec AddWorkshop "2013-12-24", "14:4:00", "17:4:00", 95, 28, "Conference on Computer Vision and Pattern", 82
exec AddWorkshop "2012-3-21", "16:4:00", "19:4:00", 100, 33, "International Conference on Autonomous Agents and", 31
exec AddWorkshop "2013-10-3", "9:43:00", "11:43:00", 70, 43, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 34
exec AddWorkshop "2012-7-30", "19:24:00", "22:24:00", 91, 33, "7 th International Conference on Bio-inspired Systems and Signal Processing", 75
exec AddWorkshop "2011-2-1", "11:39:00", "13:39:00", 128, 44, "Annual Convention of The Society", 64
exec AddWorkshop "2011-9-16", "13:30:00", "15:30:00", 64, 40, "Mechanisms Final Presentation Days", 8
exec AddWorkshop "2013-3-10", "10:38:00", "12:38:00", 69, 27, "Life in Space for Life on Earth Symposium", 54
exec AddWorkshop "2011-1-28", "18:19:00", "21:19:00", 78, 36, "Conference on Computer Vision and Pattern", 11
exec AddWorkshop "2013-2-29", "8:28:00", "9:28:00", 94, 37, "GNC 2014", 82
exec AddWorkshop "2012-2-12", "11:9:00", "13:9:00", 83, 26, "Conference on Learning Theory		", 18
exec AddWorkshop "2011-4-1", "13:48:00", "15:48:00", 59, 45, "The 12th annual Responsible Business Summit", 71
exec AddWorkshop "2012-8-11", "16:17:00", "18:17:00", 60, 45, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 17
exec AddWorkshop "2013-2-3", "9:35:00", "10:35:00", 62, 30, "The BCLC CSR Conference", 63
exec AddWorkshop "2011-5-22", "19:16:00", "22:16:00", 124, 38, "The 2nd International CSR Communication Conference", 64
exec AddWorkshop "2013-2-14", "18:15:00", "21:15:00", 68, 40, "International Conference on Computer Vision Theory and Applications", 48
exec AddWorkshop "2012-2-25", "14:38:00", "16:38:00", 58, 25, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 14
exec AddWorkshop "2013-7-21", "11:11:00", "14:11:00", 114, 31, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 72
exec AddWorkshop "2011-3-25", "11:60:00", "14:60:00", 144, 49, "RuleML Symposium		", 48
exec AddWorkshop "2013-11-1", "14:10:00", "15:10:00", 67, 36, "RuleML Symposium		", 41
exec AddWorkshop "2012-10-14", "18:25:00", "19:25:00", 94, 26, "RuleML Symposium				", 0
exec AddWorkshop "2011-4-29", "19:58:00", "22:58:00", 101, 38, "International Joint Conference on Automated Reasoning", 28
exec AddWorkshop "2011-5-24", "15:24:00", "16:24:00", 61, 42, "International Conference on Computer Vision Theory and Applications", 0
exec AddWorkshop "2012-12-28", "13:11:00", "14:11:00", 86, 27, "EuroCOW the Calibration and Orientation Workshop", 45
exec AddWorkshop "2011-2-13", "18:17:00", "19:17:00", 69, 38, "Photonics West", 0
exec AddWorkshop "2013-1-18", "13:32:00", "15:32:00", 141, 44, "Workshop on Logic ", 47
exec AddWorkshop "2011-11-10", "12:28:00", "14:28:00", 109, 42, "European Space Power Conference", 7
exec AddWorkshop "2011-8-21", "16:12:00", "19:12:00", 112, 45, "International Conference on Artificial Neural Networks", 40
exec AddWorkshop "2011-2-25", "19:13:00", "22:13:00", 144, 49, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 59
exec AddWorkshop "2011-5-26", "13:48:00", "15:48:00", 131, 27, "Net Impact Conference", 69
exec AddWorkshop "2013-2-13", "8:49:00", "11:49:00", 66, 33, "Corporate Community Involvement Conference", 49
exec AddWorkshop "2012-11-24", "8:39:00", "11:39:00", 137, 39, "Microwave Workshop", 76
exec AddWorkshop "2012-7-5", "11:4:00", "12:4:00", 108, 27, "Annual Convention of The Society", 55
exec AddWorkshop "2012-1-19", "10:30:00", "13:30:00", 146, 43, "Workshop on Logic ", 26
exec AddWorkshop "2011-7-4", "8:44:00", "10:44:00", 113, 28, "48th ESLAB Symposium", 47
exec AddWorkshop "2011-12-14", "9:54:00", "11:54:00", 82, 28, "International Conference on Logic for Programming", 61
exec AddWorkshop "2012-1-21", "12:21:00", "13:21:00", 86, 47, "Annual Interdisciplinary Conference", 16
exec AddWorkshop "2012-2-4", "10:8:00", "13:8:00", 105, 34, "RuleML Symposium				", 44
exec AddWorkshop "2011-10-3", "18:18:00", "20:18:00", 143, 33, "GNC 2014", 68
exec AddWorkshop "2012-8-14", "8:24:00", "10:24:00", 127, 27, "Microwave Workshop", 1
exec AddWorkshop "2011-10-21", "15:40:00", "16:40:00", 63, 44, "Sentinel-2 for Science WS", 9
exec AddWorkshop "2012-11-15", "19:8:00", "21:8:00", 92, 46, "Ceres Conference", 10
exec AddWorkshop "2011-4-11", "15:46:00", "17:46:00", 78, 47, "4th DUE Permafrost User Workshop", 26
exec AddWorkshop "2011-4-11", "10:34:00", "12:34:00", 135, 44, "Sentinel-2 for Science WS", 33
exec AddWorkshop "2011-1-15", "18:8:00", "19:8:00", 85, 49, "Conference on Uncertainty in Artificial Intelligence", 65
exec AddWorkshop "2011-1-11", "9:3:00", "12:3:00", 147, 44, "Workshop on Image Processing", 22
exec AddWorkshop "2012-1-21", "10:38:00", "11:38:00", 106, 47, "International Conference on the Principles of", 1
exec AddWorkshop "2013-10-21", "10:18:00", "12:18:00", 71, 39, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 63
exec AddWorkshop "2013-3-21", "14:11:00", "16:11:00", 72, 31, "4th DUE Permafrost User Workshop", 61
exec AddWorkshop "2012-5-2", "10:26:00", "13:26:00", 52, 37, "Mechanisms Final Presentation Days", 63
exec AddWorkshop "2012-11-12", "15:10:00", "17:10:00", 128, 34, "Annual Convention of The Society", 13
exec AddWorkshop "2013-5-10", "19:24:00", "22:24:00", 55, 40, "Net Impact Conference", 34
exec AddWorkshop "2011-9-1", "13:1:00", "14:1:00", 50, 20, "48th ESLAB Symposium", 50
exec AddWorkshop "2013-9-10", "12:47:00", "14:47:00", 140, 49, "Science and Challenges of Lunar Sample Return Workshop", 66
exec AddWorkshop "2013-12-4", "17:41:00", "18:41:00", 79, 42, "International Conference on the Principles of", 83
exec AddWorkshop "2012-1-9", "16:44:00", "17:44:00", 135, 37, "International Conference on Computer Vision Theory and Applications", 73
exec AddWorkshop "2013-8-28", "9:13:00", "12:13:00", 120, 46, "International Conference on Automated Planning", 55
exec AddWorkshop "2012-4-3", "15:12:00", "17:12:00", 77, 22, "RuleML Symposium				", 45
exec AddWorkshop "2012-8-13", "8:58:00", "9:58:00", 130, 37, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 72
exec AddWorkshop "2013-4-18", "14:33:00", "16:33:00", 60, 24, "Mayo Clinic Presents", 64
exec AddWorkshop "2013-3-27", "19:6:00", "22:6:00", 106, 22, "2013 Ethical Leadership Conference: Ethics in Action", 49
exec AddWorkshop "2013-5-24", "16:13:00", "17:13:00", 63, 48, "International Conference on Signal and Imaging Systems Engineering", 59
exec AddWorkshop "2013-5-1", "11:24:00", "12:24:00", 116, 38, "European Space Power Conference", 74
exec AddWorkshop "2012-4-4", "9:49:00", "12:49:00", 121, 31, "EuroCOW the Calibration and Orientation Workshop", 39
exec AddWorkshop "2011-9-7", "17:4:00", "18:4:00", 99, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 70
exec AddWorkshop "2013-12-1", "19:33:00", "22:33:00", 130, 21, "Conference on Uncertainty in Artificial Intelligence", 39
exec AddWorkshop "2012-9-19", "19:6:00", "20:6:00", 116, 32, "Science and Challenges of Lunar Sample Return Workshop", 38
exec AddWorkshop "2013-2-6", "11:14:00", "12:14:00", 82, 47, "Mayo Clinic Presents", 43
exec AddWorkshop "2013-1-18", "9:37:00", "10:37:00", 51, 21, "European Conference on Artificial Intelligence	", 73
exec AddWorkshop "2011-5-23", "14:17:00", "17:17:00", 145, 44, "International Conference on Computer Vision Theory and Applications", 36
exec AddWorkshop "2012-8-28", "15:1:00", "17:1:00", 73, 47, "Foundations of Genetic Algorithms		", 16
exec AddWorkshop "2012-8-13", "16:10:00", "19:10:00", 140, 20, "Science and Challenges of Lunar Sample Return Workshop", 40
exec AddWorkshop "2012-8-18", "16:4:00", "19:4:00", 143, 30, "48th ESLAB Symposium", 28
exec AddWorkshop "2013-3-15", "11:44:00", "13:44:00", 103, 24, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 0
exec AddWorkshop "2011-11-8", "10:29:00", "12:29:00", 142, 22, "Sentinel-2 for Science WS", 31
exec AddWorkshop "2011-12-10", "8:32:00", "9:32:00", 79, 46, "RuleML Symposium				", 66
exec AddWorkshop "2013-2-6", "15:45:00", "16:45:00", 149, 48, "International Conference on the Principles of", 20
exec AddWorkshop "2013-3-12", "14:36:00", "15:36:00", 105, 32, "Life in Space for Life on Earth Symposium", 54
exec AddWorkshop "2012-6-2", "9:51:00", "10:51:00", 63, 30, "Net Impact Conference", 59
exec AddWorkshop "2012-6-12", "8:2:00", "11:2:00", 131, 20, "RuleML Symposium		", 43
exec AddWorkshop "2013-1-18", "13:2:00", "14:2:00", 85, 46, "Conference on Computer Vision and Pattern", 77
exec AddWorkshop "2012-2-26", "13:24:00", "16:24:00", 89, 45, "3rd International Conference on Pattern Recognition Applications and Methods", 52
exec AddWorkshop "2013-11-27", "14:33:00", "15:33:00", 63, 49, "The 12th annual Responsible Business Summit", 17
exec AddWorkshop "2012-7-13", "9:1:00", "11:1:00", 51, 20, "Life in Space for Life on Earth Symposium", 7
exec AddWorkshop "2011-9-16", "11:8:00", "12:8:00", 100, 25, "Euclid Spacecraft Industry Day", 50
exec AddWorkshop "2012-6-23", "19:24:00", "20:24:00", 102, 37, "Conference on Computer Vision and Pattern", 55
exec AddWorkshop "2012-1-19", "17:57:00", "18:57:00", 92, 36, "International Conference on Computer Vision Theory and Applications", 73
exec AddWorkshop "2011-5-1", "8:20:00", "10:20:00", 65, 33, "Science and Challenges of Lunar Sample Return Workshop", 75
exec AddWorkshop "2011-10-5", "15:8:00", "17:8:00", 121, 45, "KU Leuven CSR Symposium", 28
exec AddWorkshop "2011-9-1", "13:5:00", "14:5:00", 78, 48, "LPVE Land product validation and evolution", 52
exec AddWorkshop "2013-10-11", "15:10:00", "16:10:00", 118, 28, "RuleML Symposium		", 65
exec AddWorkshop "2013-12-14", "13:13:00", "14:13:00", 76, 49, "Conference on Automated Deduction		", 29
exec AddWorkshop "2011-2-1", "10:18:00", "11:18:00", 131, 49, "European Space Power Conference", 30
exec AddWorkshop "2012-5-24", "16:14:00", "17:14:00", 142, 33, "International Conference on Signal and Imaging Systems Engineering", 79
exec AddWorkshop "2012-5-6", "14:42:00", "15:42:00", 116, 32, "4S Symposium 2014", 23
exec AddWorkshop "2013-3-4", "8:34:00", "10:34:00", 68, 28, "International Conference on Artificial Neural Networks", 40
exec AddWorkshop "2013-4-29", "10:22:00", "13:22:00", 109, 22, "4S Symposium 2014", 33
exec AddWorkshop "2013-7-20", "9:35:00", "11:35:00", 146, 38, "Corporate Community Involvement Conference", 16
exec AddWorkshop "2011-12-14", "10:23:00", "13:23:00", 93, 41, "Mayo Clinic Presents", 39
exec AddWorkshop "2013-11-22", "18:5:00", "21:5:00", 138, 26, "Corporate Community Involvement Conference", 24
exec AddWorkshop "2013-1-21", "13:14:00", "15:14:00", 94, 33, "Mayo Clinic Presents", 77
exec AddWorkshop "2012-12-4", "19:38:00", "20:38:00", 98, 22, "2nd International Conference on Photonics, Optics and Laser Technology", 83
exec AddWorkshop "2011-3-17", "19:53:00", "22:53:00", 104, 30, "Computer Analysis of Images and Patterns", 10
exec AddWorkshop "2012-7-1", "17:13:00", "18:13:00", 122, 41, "Workshop on Logic ", 18
exec AddWorkshop "2013-11-11", "15:38:00", "16:38:00", 125, 33, "Net Impact Conference", 26
exec AddWorkshop "2011-7-18", "9:13:00", "12:13:00", 90, 29, "Mechanisms Final Presentation Days", 68
exec AddWorkshop "2011-4-19", "13:46:00", "16:46:00", 93, 48, "British	Machine	Vision	Conference		", 60
exec AddWorkshop "2012-3-26", "11:59:00", "12:59:00", 123, 28, "7 th International Conference on Bio-inspired Systems and Signal Processing", 77
exec AddWorkshop "2012-5-23", "16:36:00", "19:36:00", 58, 28, "European Space Power Conference", 45
exec AddWorkshop "2011-4-5", "9:17:00", "11:17:00", 115, 31, "Life in Space for Life on Earth Symposium", 68
exec AddWorkshop "2011-1-19", "19:18:00", "21:18:00", 106, 38, "Ceres Conference", 52
exec AddWorkshop "2011-10-15", "11:26:00", "13:26:00", 90, 20, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 1
exec AddWorkshop "2011-5-27", "8:36:00", "11:36:00", 99, 25, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 14
exec AddWorkshop "2012-10-28", "18:47:00", "20:47:00", 55, 29, "International Conference on Computer Vision Theory and Applications", 7
exec AddWorkshop "2012-1-21", "12:47:00", "14:47:00", 127, 40, "The BCLC CSR Conference", 39
exec AddWorkshop "2013-7-26", "9:19:00", "10:19:00", 80, 37, "Workshop on Logic ", 76
exec AddWorkshop "2011-6-7", "9:52:00", "11:52:00", 145, 33, "The Corporate Philanthropy Forum", 32
exec AddWorkshop "2013-10-29", "15:53:00", "18:53:00", 129, 22, "International Conference on Artificial Neural Networks", 69
exec AddWorkshop "2011-3-26", "17:58:00", "20:58:00", 142, 41, "International Conference on Automated Reasoning", 74
exec AddWorkshop "2012-4-17", "9:33:00", "11:33:00", 83, 42, "International Conference on Computer Vision Theory and Applications", 17
exec AddWorkshop "2012-2-28", "12:49:00", "13:49:00", 140, 40, "European Conference on Artificial Intelligence	", 76
exec AddWorkshop "2012-7-14", "14:29:00", "16:29:00", 131, 48, "International Conference on Signal and Imaging Systems Engineering", 30
exec AddWorkshop "2013-1-6", "17:5:00", "20:5:00", 108, 31, "Workshop on Logic ", 48
exec AddWorkshop "2013-2-25", "13:54:00", "15:54:00", 116, 22, "Leadership Strategies for Information Technology in Health Care Boston", 46
exec AddWorkshop "2012-1-23", "12:49:00", "15:49:00", 82, 35, "Ceres Conference", 30
exec AddWorkshop "2013-4-21", "9:3:00", "12:3:00", 138, 36, "Cause Marketing Forum", 26
exec AddWorkshop "2011-6-23", "15:3:00", "16:3:00", 102, 46, "Cause Marketing Forum", 82
exec AddWorkshop "2013-6-9", "15:44:00", "16:44:00", 148, 48, "International Joint Conference on Artificial Intelligence", 80
exec AddWorkshop "2012-12-29", "14:3:00", "16:3:00", 132, 26, "Annual Interdisciplinary Conference", 54
exec AddWorkshop "2012-9-13", "16:58:00", "17:58:00", 114, 24, "Conference on Automated Deduction		", 0
exec AddWorkshop "2011-5-2", "8:34:00", "9:34:00", 84, 32, "Annual Convention of The Society", 19
exec AddWorkshop "2013-3-28", "15:17:00", "16:17:00", 99, 49, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 24
exec AddWorkshop "2012-7-24", "16:38:00", "17:38:00", 144, 39, "Sentinel-2 for Science WS", 50
exec AddWorkshop "2012-6-11", "10:55:00", "11:55:00", 63, 24, "GNC 2014", 33
exec AddWorkshop "2013-8-12", "15:24:00", "18:24:00", 106, 48, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 20
exec AddWorkshop "2012-2-10", "15:36:00", "17:36:00", 140, 42, "Leadership Strategies for Information Technology in Health Care Boston", 17
exec AddWorkshop "2011-4-26", "14:58:00", "17:58:00", 133, 29, "Cause Marketing Forum", 20
exec AddWorkshop "2012-9-30", "16:10:00", "19:10:00", 101, 42, "International Conference on the Principles of", 80
exec AddWorkshop "2013-1-21", "15:2:00", "16:2:00", 130, 46, "European Conference on Artificial Intelligence	", 39
exec AddWorkshop "2011-1-27", "12:22:00", "13:22:00", 144, 35, "International Conference on MultiMedia Modeling", 39
exec AddWorkshop "2012-9-7", "12:57:00", "13:57:00", 70, 40, "International Joint Conference on Automated Reasoning", 32
exec AddWorkshop "2012-9-24", "13:51:00", "16:51:00", 137, 31, "2013 Ethical Leadership Conference: Ethics in Action", 34
exec AddWorkshop "2011-5-2", "12:28:00", "15:28:00", 67, 49, "Mayo Clinic Presents", 69
exec AddWorkshop "2013-8-23", "8:22:00", "9:22:00", 78, 43, "British	Machine	Vision	Conference		", 13
exec AddWorkshop "2012-10-11", "9:27:00", "11:27:00", 68, 45, "Business4Better: The Community Partnership Movement", 57
exec AddWorkshop "2011-10-2", "8:57:00", "10:57:00", 58, 26, "�International� Corporate Citizenship Conference", 46
exec AddWorkshop "2012-4-29", "14:24:00", "15:24:00", 82, 30, "Science and Challenges of Lunar Sample Return Workshop", 83
exec AddWorkshop "2012-3-9", "18:53:00", "19:53:00", 148, 45, "British	Machine	Vision	Conference		", 37
exec AddWorkshop "2012-5-24", "9:47:00", "12:47:00", 135, 23, "European Conference on Machine Learning", 51
exec AddWorkshop "2013-6-4", "16:31:00", "19:31:00", 77, 45, "International Conference on Computer Vision Theory and Applications", 72
exec AddWorkshop "2013-10-9", "12:30:00", "14:30:00", 91, 33, "European Space Technology Harmonisation Conference", 4
exec AddWorkshop "2013-10-5", "8:14:00", "9:14:00", 145, 20, "KU Leuven CSR Symposium", 13
exec AddWorkshop "2012-3-5", "13:8:00", "15:8:00", 57, 46, "GNC 2014", 39
exec AddWorkshop "2013-11-2", "11:42:00", "14:42:00", 108, 42, "British	Machine	Vision	Conference		", 49
exec AddWorkshop "2012-5-22", "10:29:00", "11:29:00", 128, 22, "RuleML Symposium				", 18
exec AddWorkshop "2011-4-21", "18:27:00", "21:27:00", 85, 27, "LPVE Land product validation and evolution", 79
exec AddWorkshop "2011-6-2", "18:33:00", "21:33:00", 107, 46, "Ceres Conference", 14
exec AddWorkshop "2011-6-8", "8:4:00", "9:4:00", 107, 35, "Conference on Computer Vision and Pattern", 31
exec AddWorkshop "2013-5-23", "16:17:00", "19:17:00", 99, 31, "International Conference on Computer Vision", 75
exec AddWorkshop "2012-8-11", "12:18:00", "13:18:00", 113, 22, "Corporate Community Involvement Conference", 56
exec AddWorkshop "2012-2-6", "18:13:00", "20:13:00", 82, 44, "ICATT", 53
exec AddWorkshop "2013-2-15", "9:29:00", "10:29:00", 66, 33, "Life in Space for Life on Earth Symposium", 13
exec AddWorkshop "2011-10-12", "17:8:00", "20:8:00", 106, 43, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 58
exec AddWorkshop "2012-11-23", "8:57:00", "11:57:00", 100, 22, "2nd International Conference on Photonics, Optics and Laser Technology", 83
exec AddWorkshop "2011-10-28", "10:60:00", "13:60:00", 144, 34, "Conference on Automated Deduction		", 80
exec AddWorkshop "2012-7-29", "17:40:00", "20:40:00", 89, 30, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 55
exec AddWorkshop "2013-10-29", "17:49:00", "20:49:00", 62, 42, "Foundations of Genetic Algorithms		", 76
exec AddWorkshop "2011-8-6", "10:35:00", "12:35:00", 74, 43, "KU Leuven CSR Symposium", 12
exec AddWorkshop "2012-1-18", "8:4:00", "9:4:00", 144, 20, "International Conference on Computer Vision Theory and Applications", 11
exec AddWorkshop "2011-7-19", "12:23:00", "15:23:00", 100, 24, "Global Conference on Sustainability and Reporting", 12
exec AddWorkshop "2012-12-29", "16:35:00", "19:35:00", 101, 44, "International Conference on Artificial Neural Networks", 79
exec AddWorkshop "2011-2-20", "14:57:00", "17:57:00", 141, 43, "Microwave Workshop", 31
exec AddWorkshop "2011-11-20", "15:41:00", "17:41:00", 57, 39, "Euclid Spacecraft Industry Day", 19
exec AddWorkshop "2013-1-17", "18:12:00", "20:12:00", 52, 41, "Mechanisms Final Presentation Days", 73
exec AddWorkshop "2012-6-30", "16:51:00", "19:51:00", 59, 25, "EuroCOW the Calibration and Orientation Workshop", 10
exec AddWorkshop "2012-2-25", "17:58:00", "18:58:00", 71, 37, "Euclid Spacecraft Industry Day", 6
exec AddWorkshop "2011-8-10", "18:17:00", "20:17:00", 131, 24, "International Conference on Autonomous Agents and", 28
exec AddWorkshop "2013-1-21", "19:45:00", "21:45:00", 92, 46, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 54
exec AddWorkshop "2011-8-23", "19:14:00", "21:14:00", 106, 24, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 45
exec AddWorkshop "2012-6-1", "8:31:00", "11:31:00", 85, 32, "The Sixth International Conferences on Advances in Multimedia", 9
exec AddWorkshop "2012-12-16", "17:36:00", "20:36:00", 108, 29, "X-Ray Universe", 55
exec AddWorkshop "2013-5-16", "10:20:00", "11:20:00", 88, 43, "European Conference on Artificial Intelligence	", 10
exec AddWorkshop "2012-2-17", "14:29:00", "16:29:00", 146, 46, "European Conference on Artificial Intelligence	", 51
exec AddWorkshop "2013-11-1", "12:47:00", "14:47:00", 91, 40, "International Conference on Artificial Neural Networks", 28
exec AddWorkshop "2013-10-27", "13:2:00", "14:2:00", 109, 21, "International Conference on Automated Planning", 31
exec AddWorkshop "2011-7-22", "13:55:00", "14:55:00", 66, 44, "International Conference on Logic for Programming", 30
exec AddWorkshop "2012-11-16", "19:24:00", "20:24:00", 50, 41, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 78
exec AddWorkshop "2013-9-25", "8:48:00", "9:48:00", 70, 44, "Conference on Computer Vision and Pattern", 67
exec AddWorkshop "2013-12-28", "15:40:00", "17:40:00", 103, 41, "Leadership Strategies for Information Technology in Health Care Boston", 8
exec AddWorkshop "2013-4-22", "19:46:00", "20:46:00", 83, 47, "Conference on Automated Deduction		", 33
exec AddWorkshop "2012-4-8", "16:22:00", "19:22:00", 77, 39, "KU Leuven CSR Symposium", 19
exec AddWorkshop "2011-6-4", "11:21:00", "12:21:00", 65, 49, "Leadership Strategies for Information Technology in Health Care Boston", 12
exec AddWorkshop "2011-8-13", "8:6:00", "11:6:00", 55, 23, "International Conference on Automated Planning", 54
exec AddWorkshop "2011-6-23", "11:28:00", "13:28:00", 91, 42, "Annual Convention of The Society", 55
exec AddWorkshop "2013-5-18", "14:55:00", "16:55:00", 130, 22, "Annual Convention of The Society", 81
exec AddWorkshop "2011-6-9", "11:18:00", "13:18:00", 57, 46, "Conference on Uncertainty in Artificial Intelligence", 11
exec AddWorkshop "2013-11-26", "11:28:00", "12:28:00", 54, 34, "International Conference on Automated Planning", 18
exec AddWorkshop "2013-3-23", "10:30:00", "13:30:00", 74, 24, "The Sixth International Conferences on Advances in Multimedia", 38
exec AddWorkshop "2012-5-21", "11:21:00", "12:21:00", 72, 39, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 62
exec AddWorkshop "2012-5-1", "14:15:00", "17:15:00", 89, 35, "The Corporate Philanthropy Forum", 43
exec AddWorkshop "2013-8-3", "10:41:00", "11:41:00", 149, 43, "International Conference on Automated Reasoning", 18
exec AddWorkshop "2011-1-7", "9:44:00", "11:44:00", 68, 29, "KU Leuven CSR Symposium", 71
exec AddWorkshop "2012-9-11", "8:60:00", "11:60:00", 139, 40, "The 12th annual Responsible Business Summit", 20
exec AddWorkshop "2012-11-1", "13:57:00", "15:57:00", 58, 46, "Conference on Automated Deduction		", 27
exec AddWorkshop "2011-6-18", "13:55:00", "16:55:00", 141, 29, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 66
exec AddWorkshop "2013-8-19", "16:22:00", "19:22:00", 86, 28, "The Sixth International Conferences on Advances in Multimedia", 70
exec AddWorkshop "2013-12-1", "16:13:00", "19:13:00", 113, 29, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 51
exec AddWorkshop "2013-7-21", "8:50:00", "10:50:00", 63, 36, "The Corporate Philanthropy Forum", 15
exec AddWorkshop "2011-11-27", "15:49:00", "16:49:00", 141, 33, "International Conference on Computer Vision", 28
exec AddWorkshop "2013-7-2", "12:60:00", "13:60:00", 99, 37, "International Conference on Artificial Neural Networks", 61
exec AddWorkshop "2011-2-17", "11:51:00", "12:51:00", 94, 36, "Ceres Conference", 54
exec AddWorkshop "2011-5-28", "8:34:00", "9:34:00", 130, 43, "Life in Space for Life on Earth Symposium", 84
exec AddWorkshop "2012-1-24", "8:60:00", "11:60:00", 115, 25, "The 12th annual Responsible Business Summit", 12
exec AddWorkshop "2012-3-19", "14:22:00", "16:22:00", 120, 48, "International Conference on the Principles of", 18
exec AddWorkshop "2013-12-3", "8:37:00", "9:37:00", 100, 35, "The BCLC CSR Conference", 47
exec AddWorkshop "2011-12-22", "17:13:00", "18:13:00", 146, 43, "European Conference on Machine Learning", 48
exec AddWorkshop "2011-1-1", "8:13:00", "11:13:00", 143, 49, "British	Machine	Vision	Conference		", 63
exec AddWorkshop "2011-5-23", "9:25:00", "11:25:00", 144, 44, "International Conference on Logic for Programming", 1
exec AddWorkshop "2012-10-11", "19:45:00", "21:45:00", 99, 46, "GNC 2014", 13
exec AddWorkshop "2013-5-22", "8:34:00", "9:34:00", 143, 22, "The Sixth International Conferences on Advances in Multimedia", 15
exec AddWorkshop "2011-11-3", "11:57:00", "14:57:00", 53, 33, "Cause Marketing Forum", 20
exec AddWorkshop "2013-9-11", "12:32:00", "15:32:00", 120, 35, "International Conference on Automated Planning", 78
exec AddWorkshop "2012-1-2", "12:33:00", "13:33:00", 58, 22, "European Space Power Conference", 35
exec AddWorkshop "2013-10-28", "14:43:00", "17:43:00", 58, 23, "ICATT", 55
exec AddWorkshop "2011-4-4", "18:42:00", "19:42:00", 61, 29, "Computer Analysis of Images and Patterns", 61
exec AddWorkshop "2012-6-3", "14:28:00", "16:28:00", 88, 28, "Photonics West", 21
exec AddWorkshop "2013-2-14", "17:9:00", "18:9:00", 108, 39, "Science and Challenges of Lunar Sample Return Workshop", 81
exec AddWorkshop "2012-8-24", "11:11:00", "14:11:00", 138, 47, "International Conference on Computer Vision Theory and Applications", 19
exec AddWorkshop "2012-4-10", "8:23:00", "11:23:00", 50, 28, "Annual Convention of The Society", 36
exec AddWorkshop "2013-11-7", "9:12:00", "11:12:00", 104, 34, "KU Leuven CSR Symposium", 68
exec AddWorkshop "2013-5-11", "12:14:00", "14:14:00", 89, 25, "EuroCOW the Calibration and Orientation Workshop", 51
exec AddWorkshop "2013-5-9", "8:58:00", "9:58:00", 67, 37, "X-Ray Universe", 42
exec AddWorkshop "2013-5-14", "19:48:00", "22:48:00", 88, 25, "European Navigation Conference 2014 (ENC-GNSS 2014)", 40
exec AddWorkshop "2013-1-26", "11:46:00", "13:46:00", 136, 41, "Leadership Strategies for Information Technology in Health Care Boston", 61
exec AddWorkshop "2012-2-6", "16:34:00", "17:34:00", 59, 33, "4th DUE Permafrost User Workshop", 18
exec AddWorkshop "2013-7-9", "14:44:00", "16:44:00", 91, 35, "European Navigation Conference 2014 (ENC-GNSS 2014)", 19
exec AddWorkshop "2012-2-12", "11:7:00", "13:7:00", 96, 34, "International Conference on Pattern Recognition", 82
exec AddWorkshop "2013-10-6", "16:60:00", "19:60:00", 53, 22, "Life in Space for Life on Earth Symposium", 20
exec AddWorkshop "2012-12-9", "11:57:00", "12:57:00", 73, 20, "Conference on Computer Vision and Pattern", 4
exec AddWorkshop "2013-3-1", "12:23:00", "15:23:00", 132, 49, "Annual Convention of The Society", 52
exec AddWorkshop "2013-12-2", "18:20:00", "19:20:00", 136, 33, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 6
exec AddWorkshop "2011-12-3", "14:4:00", "17:4:00", 74, 40, "European Space Technology Harmonisation Conference", 39
exec AddWorkshop "2013-1-22", "18:50:00", "21:50:00", 123, 34, "Sustainable Brands Conference", 49
exec AddWorkshop "2011-6-19", "17:32:00", "19:32:00", 75, 49, "European Space Technology Harmonisation Conference", 83
exec AddWorkshop "2012-12-26", "15:50:00", "18:50:00", 93, 46, "The Corporate Philanthropy Forum", 0
exec AddWorkshop "2013-4-29", "11:4:00", "13:4:00", 80, 28, "Annual Interdisciplinary Conference", 84
exec AddWorkshop "2012-3-15", "13:36:00", "14:36:00", 64, 38, "European Conference on Artificial Intelligence	", 54
exec AddWorkshop "2012-12-5", "15:3:00", "16:3:00", 149, 41, "Global Conference on Sustainability and Reporting", 78
exec AddWorkshop "2011-10-11", "14:47:00", "16:47:00", 127, 37, "International Joint Conference on Automated Reasoning", 79
exec AddWorkshop "2011-4-25", "18:31:00", "19:31:00", 130, 43, "Foundations of Genetic Algorithms		", 52
exec AddWorkshop "2012-6-1", "18:17:00", "20:17:00", 114, 33, "International Joint Conference on Automated Reasoning", 12
exec AddWorkshop "2013-8-26", "17:48:00", "19:48:00", 125, 37, "International Conference on Computer Vision Theory and Applications", 34
exec AddWorkshop "2013-12-19", "18:24:00", "19:24:00", 52, 40, "ARTES 1 Final Presentation Days", 35
exec AddWorkshop "2012-5-22", "15:42:00", "16:42:00", 57, 37, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 20
exec AddWorkshop "2011-8-8", "18:46:00", "21:46:00", 81, 33, "Workshop on Image Processing", 54
exec AddWorkshop "2011-6-5", "11:14:00", "12:14:00", 127, 28, "Cause Marketing Forum", 18
exec AddWorkshop "2013-1-19", "15:11:00", "17:11:00", 66, 40, "�International� Corporate Citizenship Conference", 57
exec AddWorkshop "2013-5-21", "19:4:00", "21:4:00", 109, 20, "Business4Better: The Community Partnership Movement", 38
exec AddWorkshop "2011-3-19", "12:5:00", "13:5:00", 113, 24, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 72
exec AddWorkshop "2011-5-3", "8:47:00", "11:47:00", 127, 30, "Mayo Clinic Presents", 42
exec AddWorkshop "2012-7-20", "12:44:00", "13:44:00", 128, 37, "Business4Better: The Community Partnership Movement", 28
exec AddWorkshop "2011-10-8", "16:15:00", "18:15:00", 142, 29, "Computer Analysis of Images and Patterns", 35
exec AddWorkshop "2011-3-11", "13:50:00", "15:50:00", 80, 42, "Sustainable Brands Conference", 55
exec AddWorkshop "2012-5-5", "11:50:00", "13:50:00", 142, 40, "International Conference on the Principles of", 48
exec AddWorkshop "2012-2-20", "11:1:00", "13:1:00", 121, 24, "International Conference on the Principles of", 53
exec AddWorkshop "2012-9-10", "11:28:00", "13:28:00", 116, 33, "Euclid Spacecraft Industry Day", 63
exec AddWorkshop "2012-5-19", "14:51:00", "16:51:00", 93, 41, "EuroCOW the Calibration and Orientation Workshop", 38
exec AddWorkshop "2012-10-17", "11:48:00", "14:48:00", 88, 48, "Mayo Clinic Presents", 66
exec AddWorkshop "2013-7-17", "12:5:00", "14:5:00", 145, 37, "European Conference on Artificial Intelligence	", 44
exec AddWorkshop "2012-8-22", "9:14:00", "12:14:00", 80, 29, "Corporate Community Involvement Conference", 62
exec AddWorkshop "2011-1-22", "9:32:00", "10:32:00", 71, 46, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 3
exec AddWorkshop "2011-7-4", "18:4:00", "20:4:00", 105, 41, "KU Leuven CSR Symposium", 65
exec AddWorkshop "2013-10-7", "17:40:00", "20:40:00", 102, 20, "International Joint Conference on Automated Reasoning", 21
exec AddWorkshop "2011-5-16", "17:40:00", "18:40:00", 57, 32, "RuleML Symposium		", 17
exec AddWorkshop "2011-5-12", "17:59:00", "20:59:00", 55, 41, "International Semantic Web Conference		", 76
exec AddWorkshop "2011-7-13", "11:20:00", "14:20:00", 65, 42, "The Corporate Philanthropy Forum", 57
exec AddWorkshop "2011-3-5", "17:45:00", "20:45:00", 124, 49, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 29
exec AddWorkshop "2012-8-12", "13:11:00", "15:11:00", 60, 24, "7 th International Conference on Bio-inspired Systems and Signal Processing", 5
exec AddWorkshop "2011-3-29", "17:17:00", "19:17:00", 61, 27, "European Space Technology Harmonisation Conference", 73
exec AddWorkshop "2013-8-4", "15:1:00", "17:1:00", 56, 28, "International Conference on Signal and Imaging Systems Engineering", 22
exec AddWorkshop "2012-4-18", "8:12:00", "10:12:00", 146, 22, "RuleML Symposium				", 4
exec AddWorkshop "2013-9-16", "13:21:00", "16:21:00", 96, 37, "The BCLC CSR Conference", 40
exec AddWorkshop "2012-7-10", "16:3:00", "17:3:00", 87, 37, "Science and Challenges of Lunar Sample Return Workshop", 72
exec AddWorkshop "2011-11-1", "16:6:00", "18:6:00", 86, 47, "Conference on Learning Theory		", 1
exec AddWorkshop "2012-2-12", "8:6:00", "10:6:00", 51, 38, "Sentinel-2 for Science WS", 60
exec AddWorkshop "2013-12-7", "18:33:00", "20:33:00", 79, 47, "European Conference on Machine Learning", 76
exec AddWorkshop "2013-7-5", "12:54:00", "15:54:00", 135, 31, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 71
exec AddWorkshop "2012-9-21", "9:9:00", "12:9:00", 124, 41, "International Conference on Automated Planning", 10
exec AddWorkshop "2012-4-18", "12:35:00", "14:35:00", 113, 25, "Global Conference on Sustainability and Reporting", 46
exec AddWorkshop "2012-3-27", "16:32:00", "19:32:00", 122, 48, "Net Impact Conference", 44
exec AddWorkshop "2011-2-26", "19:55:00", "21:55:00", 60, 41, "International Conference on Automated Planning", 3
exec AddWorkshop "2011-2-13", "19:17:00", "21:17:00", 57, 39, "Conference on Uncertainty in Artificial Intelligence", 81
exec AddWorkshop "2011-4-3", "8:59:00", "10:59:00", 97, 21, "European Navigation Conference 2014 (ENC-GNSS 2014)", 39
exec AddWorkshop "2012-4-17", "13:17:00", "16:17:00", 141, 43, "European Conference on Artificial Intelligence	", 23
exec AddWorkshop "2012-9-29", "12:24:00", "14:24:00", 129, 33, "Annual Interdisciplinary Conference", 21
exec AddWorkshop "2013-8-23", "15:6:00", "18:6:00", 126, 31, "International Conference on Pattern Recognition", 80
exec AddWorkshop "2012-5-15", "14:36:00", "17:36:00", 57, 21, "Business4Better: The Community Partnership Movement", 57
exec AddWorkshop "2012-8-30", "8:22:00", "11:22:00", 109, 26, "Asian Conference on Computer Vision", 49
exec AddWorkshop "2013-1-13", "8:1:00", "9:1:00", 89, 35, "GNC 2014", 67
exec AddWorkshop "2013-4-17", "14:9:00", "17:9:00", 146, 25, "International Conference on Automated Planning", 33
exec AddWorkshop "2012-5-5", "16:54:00", "18:54:00", 93, 45, "RuleML Symposium		", 32
exec AddWorkshop "2011-5-29", "19:37:00", "21:37:00", 148, 28, "International Conference on Autonomous Agents and", 20
exec AddWorkshop "2011-3-10", "15:44:00", "16:44:00", 56, 38, "Annual Interdisciplinary Conference", 32
exec AddWorkshop "2013-5-2", "12:9:00", "15:9:00", 95, 28, "Photonics West", 24
exec AddWorkshop "2011-9-13", "11:23:00", "14:23:00", 78, 46, "International Conference on Signal and Imaging Systems Engineering", 23
exec AddWorkshop "2012-8-10", "17:60:00", "19:60:00", 112, 23, "Annual Convention of The Society", 50
exec AddWorkshop "2013-6-22", "15:2:00", "18:2:00", 68, 31, "RuleML Symposium				", 31
exec AddWorkshop "2012-8-5", "10:11:00", "11:11:00", 123, 24, "7 th International Conference on Bio-inspired Systems and Signal Processing", 37
exec AddWorkshop "2013-8-27", "14:24:00", "16:24:00", 146, 31, "4th DUE Permafrost User Workshop", 6
exec AddWorkshop "2013-4-24", "9:2:00", "12:2:00", 101, 24, "Microwave Workshop", 64
exec AddWorkshop "2011-1-12", "16:36:00", "17:36:00", 102, 34, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 41
exec AddWorkshop "2013-6-9", "8:47:00", "10:47:00", 107, 24, "British	Machine	Vision	Conference		", 55
exec AddWorkshop "2012-4-28", "17:38:00", "19:38:00", 62, 24, "Microwave Workshop", 39
exec AddWorkshop "2011-10-1", "16:15:00", "18:15:00", 66, 35, "Business4Better: The Community Partnership Movement", 56
exec AddWorkshop "2011-6-3", "10:55:00", "12:55:00", 59, 44, "International Conference on Automated Reasoning", 83
exec AddWorkshop "2012-9-22", "15:24:00", "18:24:00", 84, 45, "Corporate Community Involvement Conference", 69
exec AddWorkshop "2011-1-8", "18:53:00", "20:53:00", 111, 23, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 37
exec AddWorkshop "2012-6-22", "11:31:00", "12:31:00", 56, 22, "Mechanisms Final Presentation Days", 56
exec AddWorkshop "2011-4-17", "9:16:00", "12:16:00", 123, 30, "Net Impact Conference", 60
exec AddWorkshop "2013-3-7", "16:1:00", "19:1:00", 85, 41, "International Semantic Web Conference		", 74
exec AddWorkshop "2012-4-12", "19:57:00", "22:57:00", 98, 37, "The 2nd International CSR Communication Conference", 11
exec AddWorkshop "2011-12-6", "15:36:00", "18:36:00", 112, 36, "Asian Conference on Computer Vision", 39
exec AddWorkshop "2013-10-9", "12:40:00", "13:40:00", 121, 43, "International Conference on Autonomous Agents and", 58
exec AddWorkshop "2012-1-27", "11:26:00", "14:26:00", 79, 36, "48th ESLAB Symposium", 27
exec AddWorkshop "2013-9-27", "18:24:00", "19:24:00", 84, 24, "International Conference on Autonomous Agents and", 48
exec AddWorkshop "2011-1-14", "19:4:00", "22:4:00", 63, 46, "Client Summit", 7
exec AddWorkshop "2011-6-26", "15:8:00", "17:8:00", 136, 39, "International Conference on Computer Vision", 43
exec AddWorkshop "2013-6-26", "13:51:00", "16:51:00", 117, 35, "International Conference on Autonomous Agents and", 5
exec AddWorkshop "2012-5-1", "8:46:00", "10:46:00", 118, 21, "X-Ray Universe", 72
exec AddWorkshop "2013-2-10", "14:20:00", "16:20:00", 57, 44, "Business4Better: The Community Partnership Movement", 59
exec AddWorkshop "2013-6-3", "18:47:00", "20:47:00", 116, 40, "International Conference on Autonomous Agents and", 71
exec AddWorkshop "2013-10-21", "10:21:00", "13:21:00", 76, 42, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 65
exec AddWorkshop "2011-1-22", "19:21:00", "21:21:00", 62, 48, "Conference on Learning Theory		", 21
exec AddWorkshop "2013-5-15", "18:24:00", "21:24:00", 115, 29, "Cause Marketing Forum", 78
exec AddWorkshop "2013-2-11", "19:47:00", "22:47:00", 87, 42, "European Space Technology Harmonisation Conference", 10
exec AddWorkshop "2012-11-13", "8:23:00", "10:23:00", 92, 37, "Conference on Learning Theory		", 6
exec AddWorkshop "2011-9-23", "8:56:00", "11:56:00", 53, 40, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 68
exec AddWorkshop "2013-3-22", "15:56:00", "17:56:00", 112, 21, "International Conference on Automated Reasoning", 2
exec AddWorkshop "2013-1-3", "12:24:00", "15:24:00", 125, 21, "Conference on Uncertainty in Artificial Intelligence", 2
exec AddWorkshop "2011-7-30", "18:42:00", "19:42:00", 125, 26, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 76
exec AddWorkshop "2013-10-2", "18:40:00", "21:40:00", 142, 32, "Cause Marketing Forum", 69
exec AddWorkshop "2012-3-16", "17:44:00", "18:44:00", 125, 40, "Sustainable Brands Conference", 58
exec AddWorkshop "2011-4-7", "10:1:00", "12:1:00", 80, 27, "Conference on Uncertainty in Artificial Intelligence", 41
exec AddWorkshop "2013-8-23", "14:58:00", "15:58:00", 127, 46, "LPVE Land product validation and evolution", 79
exec AddWorkshop "2013-1-3", "17:51:00", "19:51:00", 129, 27, "2013 Ethical Leadership Conference: Ethics in Action", 65
exec AddWorkshop "2012-1-27", "13:29:00", "15:29:00", 145, 47, "X-Ray Universe", 43
exec AddWorkshop "2011-6-2", "11:41:00", "13:41:00", 84, 44, "Business4Better: The Community Partnership Movement", 1
exec AddWorkshop "2011-10-6", "8:27:00", "11:27:00", 54, 42, "Conference on Computer Vision and Pattern", 30
exec AddWorkshop "2012-5-26", "11:43:00", "12:43:00", 50, 24, "Mayo Clinic Presents", 15
exec AddWorkshop "2013-8-6", "9:10:00", "11:10:00", 110, 29, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 50
exec AddWorkshop "2013-2-8", "13:44:00", "16:44:00", 69, 20, "European Conference on Machine Learning", 14
exec AddWorkshop "2013-5-25", "12:4:00", "13:4:00", 131, 34, "Science and Challenges of Lunar Sample Return Workshop", 63
exec AddWorkshop "2011-1-28", "14:25:00", "15:25:00", 102, 44, "2nd International Conference on Photonics, Optics and Laser Technology", 82
exec AddWorkshop "2013-9-13", "15:40:00", "18:40:00", 114, 45, "International Conference on Automated Reasoning", 3
exec AddWorkshop "2013-3-29", "9:39:00", "10:39:00", 116, 32, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 14
exec AddWorkshop "2012-4-21", "8:32:00", "10:32:00", 125, 43, "3rd International Conference on Pattern Recognition Applications and Methods", 50
exec AddWorkshop "2011-8-4", "8:56:00", "10:56:00", 133, 21, "2013 Ethical Leadership Conference: Ethics in Action", 57
exec AddWorkshop "2012-10-7", "11:59:00", "13:59:00", 126, 35, "Ceres Conference", 29
exec AddWorkshop "2011-3-25", "17:35:00", "20:35:00", 54, 38, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 69
exec AddWorkshop "2012-12-16", "12:19:00", "14:19:00", 133, 37, "Client Summit", 55
exec AddWorkshop "2013-10-8", "14:1:00", "17:1:00", 115, 26, "Sustainable Brands Conference", 10
exec AddWorkshop "2013-8-15", "18:3:00", "19:3:00", 62, 49, "Asian Conference on Computer Vision", 24
exec AddWorkshop "2013-5-18", "10:18:00", "11:18:00", 53, 26, "International Conference on Automated Planning", 78
exec AddWorkshop "2011-2-29", "16:34:00", "17:34:00", 75, 43, "International Semantic Web Conference		", 69
exec AddWorkshop "2012-2-27", "10:46:00", "12:46:00", 94, 40, "European Space Technology Harmonisation Conference", 48
exec AddWorkshop "2012-12-4", "11:7:00", "13:7:00", 131, 20, "International Conference on Computer Vision", 14
exec AddWorkshop "2013-11-18", "11:21:00", "12:21:00", 148, 20, "The 12th annual Responsible Business Summit", 51
exec AddWorkshop "2011-9-11", "17:1:00", "20:1:00", 78, 44, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 67
exec AddWorkshop "2011-11-7", "9:8:00", "11:8:00", 93, 22, "Mayo Clinic Presents", 66
exec AddWorkshop "2011-2-12", "9:33:00", "11:33:00", 52, 37, "International Joint Conference on Automated Reasoning", 52
exec AddWorkshop "2013-4-18", "8:48:00", "10:48:00", 67, 44, "The Sixth International Conferences on Advances in Multimedia", 54
exec AddWorkshop "2013-6-29", "8:42:00", "11:42:00", 131, 41, "The Sixth International Conferences on Advances in Multimedia", 78
exec AddWorkshop "2012-1-12", "13:11:00", "16:11:00", 85, 20, "Annual Interdisciplinary Conference", 82
exec AddWorkshop "2011-9-21", "11:32:00", "14:32:00", 110, 40, "EuroCOW the Calibration and Orientation Workshop", 60
exec AddWorkshop "2012-8-24", "12:37:00", "15:37:00", 137, 26, "European Space Technology Harmonisation Conference", 53
exec AddWorkshop "2012-9-13", "10:28:00", "11:28:00", 55, 23, "International Conference on Automated Planning", 59
exec AddWorkshop "2013-3-5", "8:38:00", "9:38:00", 78, 30, "4S Symposium 2014", 27
exec AddWorkshop "2013-9-8", "15:16:00", "16:16:00", 88, 23, "4th DUE Permafrost User Workshop", 6
exec AddWorkshop "2011-7-18", "11:29:00", "14:29:00", 140, 48, "KU Leuven CSR Symposium", 57
exec AddWorkshop "2012-11-22", "10:10:00", "13:10:00", 85, 22, "GNC 2014", 80
exec AddWorkshop "2011-7-16", "11:44:00", "12:44:00", 131, 36, "International Semantic Web Conference		", 22
exec AddWorkshop "2012-9-28", "16:42:00", "18:42:00", 144, 24, "International Conference on Signal and Imaging Systems Engineering", 40
exec AddWorkshop "2013-9-20", "17:22:00", "18:22:00", 109, 24, "The BCLC CSR Conference", 35
exec AddWorkshop "2013-10-29", "18:8:00", "19:8:00", 97, 44, "European Navigation Conference 2014 (ENC-GNSS 2014)", 22
exec AddWorkshop "2011-11-25", "8:12:00", "11:12:00", 119, 47, "2013 Ethical Leadership Conference: Ethics in Action", 73
exec AddWorkshop "2013-9-16", "13:13:00", "14:13:00", 112, 35, "X-Ray Universe", 21
exec AddWorkshop "2012-1-6", "9:52:00", "12:52:00", 81, 31, "Foundations of Genetic Algorithms		", 14
exec AddWorkshop "2011-3-5", "18:38:00", "19:38:00", 116, 34, "Conference on Computer Vision and Pattern", 43
exec AddWorkshop "2011-7-10", "13:57:00", "14:57:00", 105, 33, "Mechanisms Final Presentation Days", 3
exec AddWorkshop "2011-6-6", "15:54:00", "16:54:00", 129, 47, "Computer Analysis of Images and Patterns", 65
exec AddWorkshop "2012-1-30", "19:48:00", "21:48:00", 81, 29, "Cause Marketing Forum", 25
exec AddWorkshop "2013-12-9", "16:36:00", "17:36:00", 137, 31, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 14
exec AddWorkshop "2012-5-10", "19:44:00", "22:44:00", 105, 20, "Conference on Learning Theory		", 62
exec AddWorkshop "2011-12-14", "10:21:00", "12:21:00", 103, 44, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 66
exec AddWorkshop "2011-5-12", "14:60:00", "15:60:00", 100, 33, "International Conference on Pattern Recognition", 11
exec AddWorkshop "2013-3-8", "10:7:00", "12:7:00", 89, 24, "The Sixth International Conferences on Advances in Multimedia", 18
exec AddWorkshop "2013-12-19", "8:33:00", "10:33:00", 112, 48, "�International� Corporate Citizenship Conference", 5
exec AddWorkshop "2013-3-15", "17:54:00", "19:54:00", 88, 21, "Net Impact Conference", 57
exec AddWorkshop "2011-9-29", "14:60:00", "15:60:00", 99, 34, "Computer Analysis of Images and Patterns", 30
exec AddWorkshop "2011-9-2", "11:31:00", "12:31:00", 112, 25, "Asian Conference on Computer Vision", 69
exec AddWorkshop "2011-6-29", "17:26:00", "19:26:00", 56, 43, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 30
exec AddWorkshop "2012-3-18", "13:42:00", "15:42:00", 76, 40, "Business4Better: The Community Partnership Movement", 22
exec AddWorkshop "2011-10-30", "16:10:00", "18:10:00", 60, 26, "Global Conference on Sustainability and Reporting", 63
exec AddWorkshop "2013-7-7", "19:22:00", "22:22:00", 114, 31, "GNC 2014", 52
exec AddWorkshop "2012-2-15", "8:36:00", "10:36:00", 122, 49, "Conference on Automated Deduction		", 14
exec AddWorkshop "2011-3-11", "18:15:00", "20:15:00", 112, 24, "International Conference on Pattern Recognition", 34
exec AddWorkshop "2013-5-17", "9:59:00", "10:59:00", 100, 22, "The Sixth International Conferences on Advances in Multimedia", 34
exec AddWorkshop "2011-5-25", "10:47:00", "12:47:00", 115, 39, "International Semantic Web Conference		", 23
exec AddWorkshop "2011-2-6", "15:43:00", "17:43:00", 94, 24, "The Corporate Philanthropy Forum", 62
exec AddWorkshop "2013-3-8", "19:51:00", "21:51:00", 67, 27, "4S Symposium 2014", 57
exec AddWorkshop "2013-9-6", "19:8:00", "21:8:00", 82, 37, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 34
exec AddWorkshop "2012-6-29", "9:17:00", "11:17:00", 147, 20, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 12
exec AddWorkshop "2012-10-21", "11:50:00", "14:50:00", 102, 43, "Photonics West", 22
exec AddWorkshop "2013-2-26", "9:21:00", "12:21:00", 110, 31, "Global Conference on Sustainability and Reporting", 70
exec AddWorkshop "2011-6-13", "8:10:00", "11:10:00", 67, 22, "Asian Conference on Computer Vision", 33
exec AddWorkshop "2011-6-30", "17:12:00", "18:12:00", 135, 26, "International Conference on Pattern Recognition", 60
exec AddWorkshop "2012-10-21", "10:17:00", "11:17:00", 95, 28, "RuleML Symposium		", 30
exec AddWorkshop "2012-4-11", "11:54:00", "13:54:00", 74, 39, "The 12th annual Responsible Business Summit", 30
exec AddWorkshop "2011-5-18", "16:31:00", "19:31:00", 69, 48, "International Conference on Computer Vision Theory and Applications", 7
exec AddWorkshop "2011-12-9", "18:44:00", "20:44:00", 82, 41, "Corporate Community Involvement Conference", 5
exec AddWorkshop "2012-9-13", "17:50:00", "20:50:00", 143, 42, "Workshop on Image Processing", 58
exec AddWorkshop "2012-9-4", "13:22:00", "14:22:00", 57, 26, "European Conference on Computer Vision", 29
exec AddWorkshop "2013-1-28", "11:3:00", "12:3:00", 71, 26, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 68
exec AddWorkshop "2013-4-1", "9:6:00", "11:6:00", 145, 34, "Business4Better: The Community Partnership Movement", 70
exec AddWorkshop "2013-9-27", "19:16:00", "21:16:00", 129, 35, "Corporate Community Involvement Conference", 14
exec AddWorkshop "2013-8-3", "8:4:00", "10:4:00", 145, 34, "European Navigation Conference 2014 (ENC-GNSS 2014)", 32
exec AddWorkshop "2012-6-24", "13:27:00", "14:27:00", 105, 48, "Microwave Workshop", 27
exec AddWorkshop "2013-10-25", "14:32:00", "17:32:00", 148, 42, "International Conference on Artificial Neural Networks", 76
exec AddWorkshop "2013-12-16", "13:19:00", "14:19:00", 53, 27, "Photonics West", 56
exec AddWorkshop "2011-1-10", "12:34:00", "14:34:00", 64, 46, "The Sixth International Conferences on Advances in Multimedia", 28
exec AddWorkshop "2013-8-25", "17:18:00", "19:18:00", 57, 36, "International Conference on Autonomous Agents and", 60
exec AddWorkshop "2011-5-8", "15:17:00", "16:17:00", 70, 48, "Asian Conference on Computer Vision", 25
exec AddWorkshop "2013-6-8", "8:29:00", "10:29:00", 145, 48, "Sustainable Brands Conference", 39
exec AddWorkshop "2011-8-23", "11:35:00", "13:35:00", 141, 48, "International Conference on Signal and Imaging Systems Engineering", 6
exec AddWorkshop "2012-12-24", "10:50:00", "12:50:00", 78, 39, "RuleML Symposium				", 53
exec AddWorkshop "2011-1-16", "15:38:00", "17:38:00", 69, 44, "�International� Corporate Citizenship Conference", 29
exec AddWorkshop "2012-8-29", "18:47:00", "19:47:00", 124, 36, "International Conference on Logic for Programming", 27
exec AddWorkshop "2011-3-28", "19:23:00", "21:23:00", 105, 22, "European Space Technology Harmonisation Conference", 54
exec AddWorkshop "2012-3-5", "9:3:00", "10:3:00", 77, 47, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 48
exec AddWorkshop "2011-5-19", "12:43:00", "13:43:00", 112, 25, "Workshop on Logic ", 63
exec AddWorkshop "2013-6-10", "8:18:00", "11:18:00", 76, 29, "Science and Challenges of Lunar Sample Return Workshop", 74
exec AddWorkshop "2012-9-16", "11:26:00", "13:26:00", 139, 43, "Business4Better: The Community Partnership Movement", 33
exec AddWorkshop "2013-1-28", "9:21:00", "10:21:00", 51, 21, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 80
exec AddWorkshop "2012-5-10", "18:36:00", "20:36:00", 130, 34, "Conference on Computer Vision and Pattern", 63
exec AddWorkshop "2013-10-19", "17:55:00", "20:55:00", 128, 26, "GNC 2014", 1
exec AddWorkshop "2011-1-10", "19:53:00", "21:53:00", 113, 37, "4th DUE Permafrost User Workshop", 81
exec AddWorkshop "2013-4-1", "18:21:00", "20:21:00", 148, 39, "International Conference on Artificial Neural Networks", 64
exec AddWorkshop "2013-5-3", "13:5:00", "16:5:00", 114, 41, "Net Impact Conference", 50
exec AddWorkshop "2013-11-6", "10:44:00", "13:44:00", 94, 35, "KU Leuven CSR Symposium", 21
exec AddWorkshop "2011-11-27", "11:3:00", "13:3:00", 53, 30, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 72
exec AddWorkshop "2013-10-30", "11:53:00", "14:53:00", 51, 23, "ICATT", 6
exec AddWorkshop "2011-1-22", "11:2:00", "13:2:00", 109, 45, "4th DUE Permafrost User Workshop", 0
exec AddWorkshop "2013-7-25", "13:32:00", "15:32:00", 107, 40, "European Space Power Conference", 73
exec AddWorkshop "2011-3-26", "16:6:00", "19:6:00", 140, 25, "Workshop on Image Processing", 65
exec AddWorkshop "2011-2-8", "11:48:00", "14:48:00", 92, 21, "48th ESLAB Symposium", 41
exec AddWorkshop "2013-11-12", "11:59:00", "14:59:00", 122, 30, "International Conference on Autonomous Agents and", 6
exec AddWorkshop "2012-6-3", "15:42:00", "16:42:00", 80, 26, "Mayo Clinic Presents", 13
exec AddWorkshop "2013-4-12", "16:48:00", "18:48:00", 98, 37, "International Conference on Signal and Imaging Systems Engineering", 45
exec AddWorkshop "2013-4-6", "11:58:00", "13:58:00", 68, 49, "Mechanisms Final Presentation Days", 7
exec AddWorkshop "2013-4-24", "16:45:00", "19:45:00", 131, 32, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 4
exec AddWorkshop "2012-4-4", "9:35:00", "11:35:00", 107, 35, "Conference on Computer Vision and Pattern", 49
exec AddWorkshop "2012-2-10", "12:50:00", "14:50:00", 57, 27, "EuroCOW the Calibration and Orientation Workshop", 27
exec AddWorkshop "2013-5-24", "19:41:00", "20:41:00", 117, 22, "International Joint Conference on Automated Reasoning", 18
exec AddWorkshop "2012-8-28", "8:47:00", "10:47:00", 149, 25, "Mayo Clinic Presents", 55
exec AddWorkshop "2012-3-15", "14:56:00", "16:56:00", 81, 40, "Conference on Computer Vision and Pattern", 23
exec AddWorkshop "2012-12-11", "13:12:00", "16:12:00", 125, 40, "Mayo Clinic Presents", 38
exec AddWorkshop "2013-7-23", "9:29:00", "10:29:00", 52, 24, "GNC 2014", 69
exec AddWorkshop "2011-1-18", "9:51:00", "11:51:00", 68, 49, "Sentinel-2 for Science WS", 19
exec AddWorkshop "2013-3-27", "15:24:00", "17:24:00", 78, 34, "Corporate Community Involvement Conference", 46
exec AddWorkshop "2012-11-15", "17:39:00", "19:39:00", 115, 35, "7 th International Conference on Bio-inspired Systems and Signal Processing", 44
exec AddWorkshop "2012-11-8", "9:34:00", "12:34:00", 60, 42, "Corporate Community Involvement Conference", 9
exec AddWorkshop "2011-12-6", "11:58:00", "12:58:00", 104, 26, "Conference on Learning Theory		", 80
exec AddWorkshop "2012-12-28", "18:60:00", "21:60:00", 139, 37, "International Joint Conference on Artificial Intelligence", 70
exec AddWorkshop "2013-9-13", "9:26:00", "12:26:00", 143, 25, "The BCLC CSR Conference", 44
exec AddWorkshop "2012-2-24", "15:28:00", "17:28:00", 138, 43, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 65
exec AddWorkshop "2013-3-28", "16:60:00", "17:60:00", 106, 24, "3rd International Conference on Pattern Recognition Applications and Methods", 25
exec AddWorkshop "2011-7-24", "15:2:00", "16:2:00", 55, 36, "European Conference on Machine Learning", 50
exec AddWorkshop "2013-8-25", "18:17:00", "19:17:00", 140, 49, "Photonics West", 76
exec AddWorkshop "2013-7-26", "11:38:00", "14:38:00", 107, 46, "International Conference on MultiMedia Modeling", 83
exec AddWorkshop "2012-12-26", "19:53:00", "22:53:00", 52, 43, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 58
exec AddWorkshop "2013-9-29", "10:41:00", "11:41:00", 126, 49, "Conference on Computer Vision and Pattern", 49
exec AddWorkshop "2011-12-29", "16:13:00", "17:13:00", 83, 26, "Photonics West", 0
exec AddWorkshop "2013-8-29", "8:60:00", "9:60:00", 144, 42, "Annual Interdisciplinary Conference", 2
exec AddWorkshop "2011-1-19", "9:25:00", "11:25:00", 97, 32, "7 th International Conference on Bio-inspired Systems and Signal Processing", 26
exec AddWorkshop "2013-3-13", "8:24:00", "9:24:00", 73, 24, "Mechanisms Final Presentation Days", 50
exec AddWorkshop "2011-9-18", "8:30:00", "10:30:00", 128, 46, "RuleML Symposium		", 4
exec AddWorkshop "2013-5-7", "10:12:00", "12:12:00", 58, 29, "International Conference on Logic for Programming", 18
exec AddWorkshop "2013-6-18", "15:17:00", "17:17:00", 115, 23, "Conference on Automated Deduction		", 10
exec AddWorkshop "2011-8-27", "13:43:00", "16:43:00", 133, 46, "International Joint Conference on Artificial Intelligence", 15
exec AddWorkshop "2011-2-15", "16:19:00", "17:19:00", 132, 21, "Conference on Automated Deduction		", 78
exec AddWorkshop "2011-3-5", "9:50:00", "12:50:00", 138, 38, "Conference on Learning Theory		", 22
exec AddWorkshop "2011-2-6", "10:27:00", "11:27:00", 134, 20, "Euclid Spacecraft Industry Day", 7
exec AddWorkshop "2013-2-8", "17:33:00", "20:33:00", 116, 20, "48th ESLAB Symposium", 23
exec AddWorkshop "2012-6-14", "10:54:00", "12:54:00", 78, 31, "European Space Technology Harmonisation Conference", 45
exec AddWorkshop "2012-2-30", "12:44:00", "13:44:00", 59, 35, "Conference on Learning Theory		", 59
exec AddWorkshop "2013-10-14", "18:34:00", "19:34:00", 92, 48, "Life in Space for Life on Earth Symposium", 0
exec AddWorkshop "2011-10-4", "12:8:00", "15:8:00", 134, 28, "KU Leuven CSR Symposium", 17
exec AddWorkshop "2012-11-18", "9:22:00", "10:22:00", 122, 24, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 1
exec AddWorkshop "2012-12-5", "10:6:00", "13:6:00", 91, 30, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 7
exec AddWorkshop "2012-9-28", "16:40:00", "17:40:00", 139, 33, "�International� Corporate Citizenship Conference", 55
exec AddWorkshop "2013-3-25", "19:4:00", "20:4:00", 149, 41, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 61
exec AddWorkshop "2011-11-13", "15:31:00", "16:31:00", 149, 40, "3rd International Conference on Pattern Recognition Applications and Methods", 32
exec AddWorkshop "2011-8-1", "16:10:00", "18:10:00", 94, 24, "International Conference on Automated Planning", 24
exec AddWorkshop "2013-2-27", "8:14:00", "10:14:00", 65, 40, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 82
exec AddWorkshop "2012-7-16", "18:36:00", "21:36:00", 54, 39, "Sentinel-2 for Science WS", 41
exec AddWorkshop "2012-10-17", "12:1:00", "13:1:00", 67, 24, "European Navigation Conference 2014 (ENC-GNSS 2014)", 56
exec AddWorkshop "2011-4-21", "12:58:00", "15:58:00", 115, 43, "Mechanisms Final Presentation Days", 48
exec AddWorkshop "2013-5-11", "11:24:00", "13:24:00", 125, 27, "European Conference on Computer Vision", 53
exec AddWorkshop "2012-2-16", "17:3:00", "18:3:00", 117, 45, "GNC 2014", 27
exec AddWorkshop "2011-2-26", "14:17:00", "16:17:00", 127, 31, "International Conference on Autonomous Agents and", 79
exec AddWorkshop "2011-7-5", "8:14:00", "10:14:00", 148, 31, "Business4Better: The Community Partnership Movement", 19
exec AddWorkshop "2012-6-15", "9:26:00", "12:26:00", 55, 48, "International Conference on Autonomous Agents and", 1
exec AddWorkshop "2011-4-10", "9:13:00", "10:13:00", 89, 43, "International Conference on Logic for Programming", 54
exec AddWorkshop "2013-5-1", "15:59:00", "17:59:00", 83, 31, "International Conference on Automated Reasoning", 70
exec AddWorkshop "2013-12-9", "17:2:00", "19:2:00", 54, 41, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 10
exec AddWorkshop "2011-12-26", "16:4:00", "18:4:00", 96, 45, "The 12th annual Responsible Business Summit", 62
exec AddWorkshop "2011-4-3", "10:35:00", "11:35:00", 138, 30, "International Joint Conference on Artificial Intelligence", 54
exec AddWorkshop "2012-9-23", "18:18:00", "20:18:00", 120, 39, "International Joint Conference on Automated Reasoning", 33
exec AddWorkshop "2011-4-26", "13:34:00", "15:34:00", 107, 41, "48th ESLAB Symposium", 50
exec AddWorkshop "2012-7-2", "8:11:00", "9:11:00", 124, 28, "�International� Corporate Citizenship Conference", 38
exec AddWorkshop "2012-4-6", "11:30:00", "13:30:00", 145, 27, "Business4Better: The Community Partnership Movement", 41
exec AddWorkshop "2012-8-27", "18:23:00", "21:23:00", 90, 35, "Annual Interdisciplinary Conference", 1
exec AddWorkshop "2012-8-25", "10:45:00", "13:45:00", 146, 41, "International Conference on MultiMedia Modeling", 37
exec AddWorkshop "2011-7-26", "10:49:00", "12:49:00", 106, 48, "The 12th annual Responsible Business Summit", 60
exec AddWorkshop "2013-9-29", "12:53:00", "15:53:00", 67, 25, "The 12th annual Responsible Business Summit", 79
exec AddWorkshop "2012-10-18", "17:14:00", "18:14:00", 87, 48, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 13
exec AddWorkshop "2013-1-19", "16:60:00", "18:60:00", 147, 31, "International Conference on Computer Vision Theory and Applications", 60
exec AddWorkshop "2011-3-5", "17:57:00", "20:57:00", 59, 38, "X-Ray Universe", 22
exec AddWorkshop "2011-1-3", "12:7:00", "15:7:00", 126, 21, "Conference on Automated Deduction		", 78
exec AddWorkshop "2012-2-16", "10:20:00", "12:20:00", 108, 32, "International Conference on Pattern Recognition", 55
exec AddWorkshop "2012-10-30", "15:57:00", "16:57:00", 125, 35, "Net Impact Conference", 40
exec AddWorkshop "2013-11-29", "16:41:00", "17:41:00", 91, 48, "RuleML Symposium		", 17
exec AddWorkshop "2011-12-27", "19:42:00", "20:42:00", 59, 30, "International Conference on Automated Planning", 2
exec AddWorkshop "2011-5-16", "12:59:00", "15:59:00", 63, 29, "International Conference on Signal and Imaging Systems Engineering", 70
exec AddWorkshop "2012-2-11", "18:53:00", "20:53:00", 70, 37, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 50
exec AddWorkshop "2011-3-24", "13:58:00", "16:58:00", 50, 45, "Conference on Computer Vision and Pattern", 11
exec AddWorkshop "2012-7-24", "17:6:00", "19:6:00", 66, 34, "Euclid Spacecraft Industry Day", 36
exec AddWorkshop "2013-5-24", "9:21:00", "11:21:00", 83, 38, "Conference on Automated Deduction		", 9
exec AddWorkshop "2011-3-10", "9:2:00", "11:2:00", 142, 21, "Leadership Strategies for Information Technology in Health Care Boston", 28
exec AddWorkshop "2011-6-10", "19:16:00", "21:16:00", 97, 40, "International Joint Conference on Automated Reasoning", 64
exec AddWorkshop "2013-6-13", "13:3:00", "14:3:00", 125, 45, "2013 Ethical Leadership Conference: Ethics in Action", 66
exec AddWorkshop "2011-3-13", "19:54:00", "22:54:00", 62, 29, "Mayo Clinic Presents", 52
exec AddWorkshop "2013-9-3", "16:15:00", "18:15:00", 128, 40, "European Conference on Artificial Intelligence	", 38
exec AddWorkshop "2012-3-3", "9:43:00", "10:43:00", 107, 49, "International Conference on Autonomous Agents and", 73
exec AddWorkshop "2011-1-16", "16:34:00", "19:34:00", 94, 35, "Conference on Automated Deduction		", 42
exec AddWorkshop "2013-9-6", "10:45:00", "13:45:00", 86, 48, "European Navigation Conference 2014 (ENC-GNSS 2014)", 48
exec AddWorkshop "2012-4-26", "10:11:00", "11:11:00", 131, 40, "X-Ray Universe", 63
exec AddWorkshop "2011-11-28", "13:59:00", "14:59:00", 140, 31, "Ceres Conference", 22
exec AddWorkshop "2013-6-24", "15:24:00", "16:24:00", 76, 26, "Workshop on Logic ", 2
exec AddWorkshop "2013-1-15", "17:11:00", "20:11:00", 101, 30, "4S Symposium 2014", 23
exec AddWorkshop "2013-5-2", "14:18:00", "17:18:00", 54, 41, "2nd International Conference on Photonics, Optics and Laser Technology", 1
exec AddWorkshop "2011-8-28", "19:5:00", "20:5:00", 129, 24, "The Sixth International Conferences on Advances in Multimedia", 20
exec AddWorkshop "2013-4-4", "17:28:00", "19:28:00", 58, 24, "KU Leuven CSR Symposium", 3
exec AddWorkshop "2012-1-21", "9:47:00", "11:47:00", 137, 46, "European Conference on Artificial Intelligence	", 82
exec AddWorkshop "2012-10-24", "10:53:00", "11:53:00", 109, 26, "International Conference on Computer Vision Theory and Applications", 39
exec AddWorkshop "2011-8-2", "19:7:00", "21:7:00", 74, 37, "4th DUE Permafrost User Workshop", 27
exec AddWorkshop "2013-2-9", "14:4:00", "16:4:00", 100, 35, "European Conference on Machine Learning", 1
exec AddWorkshop "2013-1-16", "9:11:00", "10:11:00", 64, 35, "2nd International Conference on Photonics, Optics and Laser Technology", 49
exec AddWorkshop "2011-5-13", "11:3:00", "14:3:00", 94, 32, "The BCLC CSR Conference", 47
exec AddWorkshop "2012-12-20", "14:28:00", "15:28:00", 62, 25, "International Joint Conference on Automated Reasoning", 48
exec AddWorkshop "2013-11-27", "16:60:00", "18:60:00", 92, 42, "Leadership Strategies for Information Technology in Health Care Boston", 72
exec AddWorkshop "2012-12-29", "8:54:00", "10:54:00", 109, 21, "International Joint Conference on Automated Reasoning", 42
exec AddWorkshop "2013-6-17", "14:55:00", "15:55:00", 65, 21, "International Conference on Computer Vision", 64
exec AddWorkshop "2012-6-13", "17:18:00", "18:18:00", 115, 28, "Asian Conference on Computer Vision", 64
exec AddWorkshop "2011-11-28", "14:46:00", "17:46:00", 107, 27, "Net Impact Conference", 69
exec AddWorkshop "2012-11-6", "17:16:00", "18:16:00", 134, 48, "�International� Corporate Citizenship Conference", 18
exec AddWorkshop "2012-10-11", "11:23:00", "13:23:00", 64, 39, "Workshop on Image Processing", 24
exec AddWorkshop "2011-8-17", "12:15:00", "13:15:00", 100, 40, "Conference on Uncertainty in Artificial Intelligence", 3
exec AddWorkshop "2012-8-25", "16:11:00", "17:11:00", 145, 26, "KU Leuven CSR Symposium", 44
exec AddWorkshop "2013-11-9", "14:45:00", "17:45:00", 116, 38, "European Conference on Machine Learning", 81
exec AddWorkshop "2011-4-9", "8:24:00", "10:24:00", 123, 22, "Annual Convention of The Society", 23
exec AddWorkshop "2012-6-11", "9:48:00", "10:48:00", 73, 46, "Annual Convention of The Society", 44
exec AddWorkshop "2013-7-11", "17:1:00", "19:1:00", 148, 25, "International Conference on Computer Vision", 24
exec AddWorkshop "2011-8-3", "14:50:00", "17:50:00", 82, 22, "International Conference on Computer Vision", 18
exec AddWorkshop "2011-11-18", "19:39:00", "21:39:00", 149, 31, "The Sixth International Conferences on Advances in Multimedia", 70
exec AddWorkshop "2011-3-3", "11:49:00", "12:49:00", 110, 39, "International Conference on Artificial Neural Networks", 62
exec AddWorkshop "2012-11-10", "14:34:00", "17:34:00", 137, 43, "International Conference on Artificial Neural Networks", 36
exec AddWorkshop "2011-9-4", "15:7:00", "18:7:00", 77, 43, "Mechanisms Final Presentation Days", 62
exec AddWorkshop "2013-6-16", "9:33:00", "12:33:00", 114, 44, "Corporate Community Involvement Conference", 47
exec AddWorkshop "2011-4-3", "15:14:00", "17:14:00", 80, 40, "International Conference on Logic for Programming", 11
exec AddWorkshop "2011-1-11", "16:8:00", "18:8:00", 81, 34, "Asian Conference on Computer Vision", 1
exec AddWorkshop "2013-1-22", "9:51:00", "11:51:00", 101, 39, "International Conference on MultiMedia Modeling", 37
exec AddWorkshop "2012-7-25", "11:28:00", "13:28:00", 135, 43, "3rd International Conference on Pattern Recognition Applications and Methods", 8
exec AddWorkshop "2011-1-29", "14:3:00", "17:3:00", 62, 49, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 40
exec AddWorkshop "2011-3-11", "17:47:00", "18:47:00", 98, 49, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 49
exec AddWorkshop "2012-9-5", "17:16:00", "18:16:00", 88, 42, "The 2nd International CSR Communication Conference", 22
exec AddWorkshop "2012-5-22", "13:49:00", "14:49:00", 125, 43, "European Space Technology Harmonisation Conference", 79
exec AddWorkshop "2013-11-28", "9:42:00", "11:42:00", 118, 49, "Conference on Learning Theory		", 9
exec AddWorkshop "2012-8-24", "11:37:00", "13:37:00", 72, 32, "International Joint Conference on Artificial Intelligence", 61
exec AddWorkshop "2012-3-27", "14:12:00", "15:12:00", 106, 33, "European Navigation Conference 2014 (ENC-GNSS 2014)", 84
exec AddWorkshop "2011-3-20", "18:20:00", "20:20:00", 131, 25, "International Conference on MultiMedia Modeling", 26
exec AddWorkshop "2013-4-21", "16:28:00", "18:28:00", 62, 44, "Computer Analysis of Images and Patterns", 69
exec AddWorkshop "2012-5-19", "16:47:00", "17:47:00", 56, 33, "The BCLC CSR Conference", 56
exec AddWorkshop "2013-1-1", "16:4:00", "19:4:00", 135, 22, "4S Symposium 2014", 79
exec AddWorkshop "2013-2-10", "9:30:00", "12:30:00", 71, 27, "International Semantic Web Conference		", 38
exec AddWorkshop "2012-11-16", "11:16:00", "13:16:00", 87, 34, "ARTES 1 Final Presentation Days", 79
exec AddWorkshop "2011-11-23", "8:21:00", "9:21:00", 81, 36, "Annual Convention of The Society", 21
exec AddWorkshop "2013-8-2", "12:21:00", "15:21:00", 84, 22, "International Conference on Autonomous Agents and", 54
exec AddWorkshop "2012-6-24", "15:22:00", "17:22:00", 136, 22, "The Corporate Philanthropy Forum", 72
exec AddWorkshop "2013-3-9", "16:23:00", "19:23:00", 105, 39, "Microwave Workshop", 2
exec AddWorkshop "2012-11-14", "18:58:00", "19:58:00", 106, 31, "International Joint Conference on Automated Reasoning", 75
exec AddWorkshop "2012-5-27", "9:30:00", "11:30:00", 100, 26, "Science and Challenges of Lunar Sample Return Workshop", 42
exec AddWorkshop "2013-6-18", "11:8:00", "12:8:00", 119, 23, "International Joint Conference on Automated Reasoning", 72
exec AddWorkshop "2011-9-28", "15:37:00", "17:37:00", 93, 26, "International Conference on Signal and Imaging Systems Engineering", 38
exec AddWorkshop "2013-1-7", "16:53:00", "17:53:00", 97, 49, "�International� Corporate Citizenship Conference", 36
exec AddWorkshop "2011-7-8", "10:15:00", "11:15:00", 105, 33, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 23
exec AddWorkshop "2011-11-19", "9:25:00", "12:25:00", 137, 30, "The Sixth International Conferences on Advances in Multimedia", 66
exec AddWorkshop "2011-7-3", "17:26:00", "20:26:00", 118, 32, "The BCLC CSR Conference", 77
exec AddWorkshop "2012-6-14", "18:37:00", "20:37:00", 131, 35, "International Conference on Pattern Recognition", 83
exec AddWorkshop "2011-8-15", "14:29:00", "17:29:00", 61, 26, "Business4Better: The Community Partnership Movement", 50
exec AddWorkshop "2013-1-15", "8:30:00", "9:30:00", 55, 41, "Foundations of Genetic Algorithms		", 82
exec AddWorkshop "2011-5-13", "10:58:00", "13:58:00", 148, 20, "Business4Better: The Community Partnership Movement", 71
exec AddWorkshop "2012-10-30", "15:49:00", "17:49:00", 133, 34, "RuleML Symposium				", 10
exec AddWorkshop "2013-7-6", "10:44:00", "11:44:00", 138, 28, "ICATT", 29
exec AddWorkshop "2011-7-12", "15:29:00", "17:29:00", 67, 48, "4th DUE Permafrost User Workshop", 60
exec AddWorkshop "2011-12-21", "11:14:00", "14:14:00", 108, 45, "International Conference on MultiMedia Modeling", 72
exec AddWorkshop "2013-10-26", "13:1:00", "14:1:00", 72, 46, "Mechanisms Final Presentation Days", 51
exec AddWorkshop "2012-1-28", "14:36:00", "17:36:00", 106, 29, "RuleML Symposium		", 7
exec AddWorkshop "2012-10-19", "8:50:00", "9:50:00", 76, 31, "Life in Space for Life on Earth Symposium", 58
exec AddWorkshop "2012-2-15", "8:57:00", "10:57:00", 102, 31, "RuleML Symposium				", 43
exec AddWorkshop "2013-7-17", "12:52:00", "15:52:00", 90, 32, "Annual Interdisciplinary Conference", 1
exec AddWorkshop "2011-9-5", "9:21:00", "11:21:00", 88, 36, "RuleML Symposium		", 24
exec AddWorkshop "2012-9-3", "16:23:00", "19:23:00", 70, 23, "International Conference on Logic for Programming", 65
exec AddWorkshop "2013-7-6", "11:28:00", "12:28:00", 102, 49, "Corporate Community Involvement Conference", 57
exec AddWorkshop "2011-5-3", "10:44:00", "13:44:00", 87, 32, "KU Leuven CSR Symposium", 66
exec AddWorkshop "2013-5-9", "9:3:00", "11:3:00", 145, 21, "Mayo Clinic Presents", 49
exec AddWorkshop "2012-8-4", "16:36:00", "19:36:00", 55, 34, "International Joint Conference on Automated Reasoning", 31
exec AddWorkshop "2011-12-9", "15:57:00", "18:57:00", 131, 37, "�International� Corporate Citizenship Conference", 51
exec AddWorkshop "2011-8-6", "18:53:00", "20:53:00", 81, 24, "ARTES 1 Final Presentation Days", 60
exec AddWorkshop "2011-12-13", "16:26:00", "18:26:00", 123, 47, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 81
exec AddWorkshop "2011-4-7", "15:40:00", "17:40:00", 95, 35, "International Conference on the Principles of", 4
exec AddWorkshop "2013-10-11", "18:20:00", "19:20:00", 139, 34, "4th DUE Permafrost User Workshop", 13
exec AddWorkshop "2013-7-1", "12:59:00", "15:59:00", 149, 46, "Cause Marketing Forum", 31
exec AddWorkshop "2013-6-22", "13:55:00", "16:55:00", 129, 20, "Workshop on Logic ", 69
exec AddWorkshop "2012-1-26", "14:58:00", "15:58:00", 101, 46, "Sustainable Brands Conference", 68
exec AddWorkshop "2013-3-26", "19:29:00", "21:29:00", 109, 35, "European Conference on Artificial Intelligence	", 31
exec AddWorkshop "2012-5-3", "10:39:00", "12:39:00", 104, 47, "Workshop on Image Processing", 46
exec AddWorkshop "2013-10-10", "15:45:00", "18:45:00", 137, 49, "7 th International Conference on Bio-inspired Systems and Signal Processing", 7
exec AddWorkshop "2013-5-10", "15:45:00", "16:45:00", 128, 35, "3rd International Conference on Pattern Recognition Applications and Methods", 2
exec AddWorkshop "2013-6-2", "17:32:00", "18:32:00", 98, 30, "48th ESLAB Symposium", 77
exec AddWorkshop "2012-2-14", "11:37:00", "14:37:00", 70, 33, "Client Summit", 38
exec AddWorkshop "2013-2-7", "9:28:00", "11:28:00", 85, 41, "EuroCOW the Calibration and Orientation Workshop", 53
exec AddWorkshop "2011-2-7", "10:53:00", "11:53:00", 91, 30, "2013 Ethical Leadership Conference: Ethics in Action", 11
exec AddWorkshop "2012-2-15", "14:6:00", "17:6:00", 97, 43, "KU Leuven CSR Symposium", 10
exec AddWorkshop "2013-12-1", "11:53:00", "13:53:00", 122, 20, "Global Conference on Sustainability and Reporting", 7
exec AddWorkshop "2013-1-13", "9:34:00", "11:34:00", 143, 40, "The Sixth International Conferences on Advances in Multimedia", 18
exec AddWorkshop "2012-11-5", "11:32:00", "14:32:00", 85, 31, "International Conference on Logic for Programming", 84
exec AddWorkshop "2011-11-14", "17:49:00", "18:49:00", 94, 21, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 70
exec AddWorkshop "2012-2-30", "12:21:00", "15:21:00", 72, 21, "Annual Interdisciplinary Conference", 27
exec AddWorkshop "2011-11-29", "13:33:00", "15:33:00", 106, 48, "Client Summit", 73
exec AddWorkshop "2013-12-29", "9:21:00", "12:21:00", 129, 39, "The BCLC CSR Conference", 26
exec AddWorkshop "2012-4-17", "17:3:00", "20:3:00", 71, 28, "International Conference on Logic for Programming", 59
exec AddWorkshop "2011-11-3", "16:13:00", "17:13:00", 74, 26, "4S Symposium 2014", 49
exec AddWorkshop "2013-5-16", "9:30:00", "10:30:00", 142, 26, "Conference on Learning Theory		", 52
exec AddWorkshop "2011-1-20", "13:48:00", "14:48:00", 143, 28, "Asian Conference on Computer Vision", 1
exec AddWorkshop "2012-10-28", "15:48:00", "18:48:00", 78, 30, "The BCLC CSR Conference", 44
exec AddWorkshop "2012-1-28", "8:44:00", "10:44:00", 140, 42, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 74
exec AddWorkshop "2011-1-4", "15:34:00", "18:34:00", 51, 42, "Annual Interdisciplinary Conference", 70
exec AddWorkshop "2013-3-28", "17:45:00", "18:45:00", 114, 28, "Corporate Community Involvement Conference", 0
exec AddWorkshop "2012-12-4", "8:7:00", "11:7:00", 92, 29, "European Navigation Conference 2014 (ENC-GNSS 2014)", 32
exec AddWorkshop "2013-11-19", "18:4:00", "21:4:00", 128, 46, "International Conference on Logic for Programming", 70
exec AddWorkshop "2011-7-13", "17:39:00", "18:39:00", 75, 36, "International Conference on Pattern Recognition", 21
exec AddWorkshop "2012-4-27", "10:8:00", "11:8:00", 100, 31, "EuroCOW the Calibration and Orientation Workshop", 74
exec AddWorkshop "2012-5-25", "16:41:00", "19:41:00", 57, 49, "Euclid Spacecraft Industry Day", 67
exec AddWorkshop "2011-12-29", "12:22:00", "15:22:00", 58, 26, "Ceres Conference", 7
exec AddWorkshop "2012-3-20", "10:8:00", "12:8:00", 75, 24, "Conference on Uncertainty in Artificial Intelligence", 62
exec AddWorkshop "2013-1-24", "15:59:00", "17:59:00", 61, 23, "RuleML Symposium		", 5
exec AddWorkshop "2011-9-10", "13:27:00", "14:27:00", 62, 44, "ICATT", 69
exec AddWorkshop "2012-1-28", "12:15:00", "14:15:00", 117, 25, "Mayo Clinic Presents", 15
exec AddWorkshop "2013-5-6", "18:26:00", "19:26:00", 98, 23, "British	Machine	Vision	Conference		", 16
exec AddWorkshop "2013-7-13", "19:24:00", "20:24:00", 144, 30, "ARTES 1 Final Presentation Days", 40
exec AddWorkshop "2013-12-8", "18:14:00", "21:14:00", 138, 32, "Science and Challenges of Lunar Sample Return Workshop", 1
exec AddWorkshop "2013-3-24", "14:36:00", "17:36:00", 136, 21, "The 2nd International CSR Communication Conference", 76
exec AddWorkshop "2011-7-12", "14:27:00", "17:27:00", 54, 34, "European Space Power Conference", 52
exec AddWorkshop "2013-4-7", "15:2:00", "16:2:00", 140, 35, "Microwave Workshop", 38
exec AddWorkshop "2013-1-8", "16:54:00", "19:54:00", 57, 36, "Conference on Learning Theory		", 83
exec AddWorkshop "2012-11-21", "9:23:00", "12:23:00", 126, 47, "�International� Corporate Citizenship Conference", 81
exec AddWorkshop "2012-6-7", "16:59:00", "18:59:00", 101, 27, "7 th International Conference on Bio-inspired Systems and Signal Processing", 45
exec AddWorkshop "2012-9-28", "17:9:00", "20:9:00", 105, 22, "Conference on Automated Deduction		", 67
exec AddWorkshop "2011-1-18", "8:12:00", "11:12:00", 104, 38, "7 th International Conference on Bio-inspired Systems and Signal Processing", 10
exec AddWorkshop "2013-8-21", "14:20:00", "16:20:00", 107, 20, "International Conference on the Principles of", 62
exec AddWorkshop "2012-1-4", "17:54:00", "20:54:00", 117, 44, "Mayo Clinic Presents", 68
exec AddWorkshop "2011-3-23", "14:51:00", "15:51:00", 64, 21, "Conference on Computer Vision and Pattern", 55
exec AddWorkshop "2013-5-14", "19:27:00", "21:27:00", 87, 38, "3rd International Conference on Pattern Recognition Applications and Methods", 51
exec AddWorkshop "2012-8-30", "19:25:00", "20:25:00", 132, 43, "International Conference on MultiMedia Modeling", 32
exec AddWorkshop "2011-9-8", "18:36:00", "20:36:00", 77, 49, "Ceres Conference", 79
exec AddWorkshop "2012-10-30", "17:60:00", "20:60:00", 95, 39, "International Conference on Artificial Neural Networks", 29
exec AddWorkshop "2013-4-16", "13:60:00", "16:60:00", 98, 32, "ARTES 1 Final Presentation Days", 43
exec AddWorkshop "2013-4-2", "9:18:00", "12:18:00", 51, 47, "4S Symposium 2014", 73
exec AddWorkshop "2013-12-2", "18:57:00", "21:57:00", 77, 37, "48th ESLAB Symposium", 26
exec AddWorkshop "2011-3-8", "14:31:00", "15:31:00", 123, 35, "RuleML Symposium		", 14
exec AddWorkshop "2012-11-18", "18:18:00", "20:18:00", 93, 42, "International Conference on Pattern Recognition", 10
exec AddWorkshop "2012-3-3", "14:54:00", "15:54:00", 89, 47, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 0
exec AddWorkshop "2013-6-12", "11:13:00", "12:13:00", 114, 24, "International Conference on MultiMedia Modeling", 76
exec AddWorkshop "2012-8-10", "16:34:00", "19:34:00", 95, 28, "Sentinel-2 for Science WS", 49
exec AddWorkshop "2012-10-19", "16:14:00", "17:14:00", 68, 42, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 84
exec AddWorkshop "2013-3-15", "8:39:00", "10:39:00", 66, 35, "International Joint Conference on Automated Reasoning", 74
exec AddWorkshop "2012-10-10", "9:29:00", "10:29:00", 56, 44, "Microwave Workshop", 69
exec AddWorkshop "2011-7-30", "16:2:00", "18:2:00", 139, 38, "EuroCOW the Calibration and Orientation Workshop", 32
exec AddWorkshop "2013-6-12", "11:60:00", "13:60:00", 139, 39, "Mayo Clinic Presents", 50
exec AddWorkshop "2012-3-6", "12:34:00", "13:34:00", 100, 45, "KU Leuven CSR Symposium", 58
exec AddWorkshop "2011-2-7", "10:48:00", "13:48:00", 90, 28, "International Conference on Computer Vision", 30
exec AddWorkshop "2013-7-11", "15:40:00", "18:40:00", 123, 36, "Ceres Conference", 70
exec AddWorkshop "2013-1-15", "8:21:00", "10:21:00", 138, 25, "Cause Marketing Forum", 63
exec AddWorkshop "2012-2-17", "13:53:00", "15:53:00", 56, 30, "LPVE Land product validation and evolution", 51
exec AddWorkshop "2013-11-22", "12:3:00", "13:3:00", 82, 43, "ARTES 1 Final Presentation Days", 47
exec AddWorkshop "2013-6-3", "19:6:00", "21:6:00", 108, 26, "Conference on Uncertainty in Artificial Intelligence", 49
exec AddWorkshop "2012-8-3", "15:18:00", "16:18:00", 50, 40, "7 th International Conference on Bio-inspired Systems and Signal Processing", 11
exec AddWorkshop "2013-3-17", "18:35:00", "20:35:00", 140, 26, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 41
exec AddWorkshop "2013-11-5", "10:29:00", "13:29:00", 110, 49, "International Joint Conference on Artificial Intelligence", 45
exec AddWorkshop "2011-8-12", "15:16:00", "18:16:00", 63, 27, "The Corporate Philanthropy Forum", 22
exec AddWorkshop "2013-2-1", "17:20:00", "19:20:00", 71, 36, "X-Ray Universe", 59
exec AddWorkshop "2011-11-3", "15:8:00", "17:8:00", 100, 43, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 5
exec AddWorkshop "2011-3-21", "14:15:00", "15:15:00", 121, 49, "The BCLC CSR Conference", 72
exec AddWorkshop "2013-7-21", "19:35:00", "22:35:00", 50, 41, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 69
exec AddWorkshop "2011-6-18", "18:18:00", "20:18:00", 100, 33, "International Conference on Computer Vision", 33
exec AddWorkshop "2013-11-16", "9:51:00", "12:51:00", 53, 21, "International Joint Conference on Automated Reasoning", 9
exec AddWorkshop "2013-8-13", "16:30:00", "17:30:00", 123, 36, "European Conference on Artificial Intelligence	", 73
exec AddWorkshop "2013-5-14", "9:42:00", "11:42:00", 57, 27, "Workshop on Image Processing", 28
exec AddWorkshop "2012-1-16", "19:32:00", "21:32:00", 127, 30, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 3
exec AddWorkshop "2011-3-7", "17:54:00", "19:54:00", 108, 22, "The 2nd International CSR Communication Conference", 78
exec AddWorkshop "2013-2-7", "18:56:00", "20:56:00", 68, 32, "Photonics West", 77
exec AddWorkshop "2012-5-26", "18:44:00", "20:44:00", 78, 20, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 74
exec AddWorkshop "2012-1-10", "14:56:00", "15:56:00", 110, 31, "RuleML Symposium				", 80
exec AddWorkshop "2011-12-7", "17:45:00", "20:45:00", 84, 28, "LPVE Land product validation and evolution", 32
exec AddWorkshop "2013-12-7", "11:7:00", "13:7:00", 130, 27, "Science and Challenges of Lunar Sample Return Workshop", 84
exec AddWorkshop "2011-2-10", "14:42:00", "15:42:00", 105, 49, "The Sixth International Conferences on Advances in Multimedia", 55
exec AddWorkshop "2013-11-10", "9:28:00", "11:28:00", 94, 31, "International Conference on Automated Planning", 10
exec AddWorkshop "2012-6-25", "11:59:00", "13:59:00", 56, 21, "Conference on Learning Theory		", 31
exec AddWorkshop "2012-9-3", "9:6:00", "10:6:00", 82, 42, "GNC 2014", 31
exec AddWorkshop "2011-2-22", "12:3:00", "15:3:00", 92, 35, "International Conference on Logic for Programming", 61
exec AddWorkshop "2011-10-8", "15:18:00", "17:18:00", 144, 23, "International Conference on Computer Vision", 22
exec AddWorkshop "2011-3-27", "12:47:00", "15:47:00", 102, 47, "European Navigation Conference 2014 (ENC-GNSS 2014)", 11
exec AddWorkshop "2012-11-17", "11:22:00", "13:22:00", 135, 23, "International Conference on Computer Vision", 55
exec AddWorkshop "2012-12-17", "19:27:00", "21:27:00", 56, 36, "International Conference on Autonomous Agents and", 1
exec AddWorkshop "2013-10-16", "19:51:00", "21:51:00", 87, 29, "International Conference on MultiMedia Modeling", 73
exec AddWorkshop "2011-6-28", "11:28:00", "12:28:00", 143, 20, "2nd International Conference on Photonics, Optics and Laser Technology", 52
exec AddWorkshop "2012-11-10", "15:38:00", "17:38:00", 75, 23, "LPVE Land product validation and evolution", 61
exec AddWorkshop "2013-11-19", "15:16:00", "17:16:00", 79, 21, "4S Symposium 2014", 79
exec AddWorkshop "2011-6-28", "15:27:00", "16:27:00", 108, 25, "International Joint Conference on Automated Reasoning", 8
exec AddWorkshop "2012-1-3", "19:42:00", "21:42:00", 140, 27, "Mechanisms Final Presentation Days", 15
exec AddWorkshop "2013-2-29", "16:59:00", "18:59:00", 52, 43, "European Conference on Computer Vision", 57
exec AddWorkshop "2012-9-16", "12:26:00", "15:26:00", 76, 39, "4th DUE Permafrost User Workshop", 65
exec AddWorkshop "2013-10-21", "10:27:00", "12:27:00", 74, 33, "International Conference on MultiMedia Modeling", 64
exec AddWorkshop "2013-8-13", "16:11:00", "18:11:00", 95, 31, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 26
exec AddWorkshop "2011-11-8", "13:33:00", "15:33:00", 149, 25, "Conference on Computer Vision and Pattern", 58
exec AddWorkshop "2013-8-18", "14:18:00", "16:18:00", 98, 46, "Workshop on Logic ", 53
exec AddWorkshop "2011-2-11", "14:10:00", "16:10:00", 70, 42, "Net Impact Conference", 43
exec AddWorkshop "2011-7-24", "15:43:00", "17:43:00", 144, 28, "International Joint Conference on Artificial Intelligence", 62
exec AddWorkshop "2011-3-11", "16:60:00", "17:60:00", 126, 24, "International Conference on Pattern Recognition", 28
exec AddWorkshop "2013-8-4", "16:26:00", "18:26:00", 77, 36, "Annual Interdisciplinary Conference", 65
exec AddWorkshop "2013-9-24", "16:49:00", "17:49:00", 71, 23, "The 2nd International CSR Communication Conference", 64
exec AddWorkshop "2011-12-4", "9:12:00", "11:12:00", 116, 32, "Sustainable Brands Conference", 82
exec AddWorkshop "2013-11-30", "16:36:00", "17:36:00", 83, 41, "Conference on Computer Vision and Pattern", 21
exec AddWorkshop "2011-8-24", "18:51:00", "21:51:00", 123, 46, "European Navigation Conference 2014 (ENC-GNSS 2014)", 76
exec AddWorkshop "2011-11-18", "14:51:00", "16:51:00", 135, 43, "Mechanisms Final Presentation Days", 62
exec AddWorkshop "2012-10-9", "8:39:00", "9:39:00", 145, 42, "Mechanisms Final Presentation Days", 29
exec AddWorkshop "2012-1-22", "16:25:00", "17:25:00", 66, 37, "EuroCOW the Calibration and Orientation Workshop", 73
exec AddWorkshop "2012-1-6", "15:13:00", "17:13:00", 104, 21, "Conference on Learning Theory		", 72
exec AddWorkshop "2011-3-3", "16:18:00", "17:18:00", 141, 35, "International Conference on Automated Planning", 19
exec AddWorkshop "2013-3-7", "14:9:00", "16:9:00", 128, 41, "International Semantic Web Conference		", 35
exec AddWorkshop "2011-7-17", "18:20:00", "21:20:00", 140, 39, "The Corporate Philanthropy Forum", 83
exec AddWorkshop "2012-2-3", "19:47:00", "21:47:00", 95, 32, "Foundations of Genetic Algorithms		", 66
exec AddWorkshop "2012-1-15", "12:59:00", "13:59:00", 65, 34, "International Conference on Signal and Imaging Systems Engineering", 52
exec AddWorkshop "2013-11-3", "12:38:00", "14:38:00", 54, 48, "International Joint Conference on Artificial Intelligence", 72
exec AddWorkshop "2011-7-18", "13:12:00", "14:12:00", 136, 28, "International Conference on Artificial Neural Networks", 72
exec AddWorkshop "2011-10-21", "13:49:00", "14:49:00", 111, 28, "Euclid Spacecraft Industry Day", 19
exec AddWorkshop "2013-10-4", "8:42:00", "11:42:00", 120, 32, "EuroCOW the Calibration and Orientation Workshop", 33
exec AddWorkshop "2011-7-29", "8:26:00", "11:26:00", 60, 37, "British	Machine	Vision	Conference		", 19
exec AddWorkshop "2012-10-4", "19:50:00", "21:50:00", 148, 24, "Conference on Uncertainty in Artificial Intelligence", 8
exec AddWorkshop "2013-12-22", "14:40:00", "17:40:00", 123, 24, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 13
exec AddWorkshop "2012-3-11", "8:16:00", "9:16:00", 145, 28, "International Joint Conference on Artificial Intelligence", 9
exec AddWorkshop "2012-12-19", "15:59:00", "16:59:00", 87, 36, "Net Impact Conference", 69
exec AddWorkshop "2013-1-15", "11:49:00", "12:49:00", 62, 46, "Business4Better: The Community Partnership Movement", 58
exec AddWorkshop "2013-6-30", "13:57:00", "16:57:00", 78, 27, "European Space Technology Harmonisation Conference", 35
exec AddWorkshop "2013-2-17", "9:47:00", "10:47:00", 113, 22, "Sentinel-2 for Science WS", 64
exec AddWorkshop "2012-5-23", "16:48:00", "17:48:00", 148, 28, "European Space Power Conference", 16
exec AddWorkshop "2012-3-4", "9:23:00", "10:23:00", 113, 35, "�International� Corporate Citizenship Conference", 66
exec AddWorkshop "2012-3-24", "13:58:00", "16:58:00", 144, 24, "3rd International Conference on Pattern Recognition Applications and Methods", 15
exec AddWorkshop "2013-5-7", "8:6:00", "11:6:00", 111, 28, "International Conference on Computer Vision Theory and Applications", 53
exec AddWorkshop "2011-12-17", "12:49:00", "14:49:00", 61, 25, "GNC 2014", 37
exec AddWorkshop "2012-12-5", "13:58:00", "15:58:00", 113, 41, "International Conference on Artificial Neural Networks", 43
exec AddWorkshop "2012-5-6", "18:32:00", "20:32:00", 115, 30, "International Conference on Computer Vision", 20
exec AddWorkshop "2012-4-26", "17:10:00", "19:10:00", 129, 23, "RuleML Symposium				", 47
exec AddWorkshop "2011-9-28", "16:49:00", "19:49:00", 94, 43, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 57
exec AddWorkshop "2013-11-11", "15:56:00", "16:56:00", 66, 47, "Computer Analysis of Images and Patterns", 18
exec AddWorkshop "2011-12-9", "16:40:00", "18:40:00", 55, 39, "Corporate Community Involvement Conference", 66
exec AddWorkshop "2011-12-18", "16:14:00", "17:14:00", 145, 24, "Net Impact Conference", 49
exec AddWorkshop "2013-6-30", "10:15:00", "12:15:00", 129, 36, "ARTES 1 Final Presentation Days", 2
exec AddWorkshop "2011-6-10", "10:32:00", "13:32:00", 78, 49, "International Conference on MultiMedia Modeling", 11
exec AddWorkshop "2011-1-20", "19:6:00", "22:6:00", 54, 48, "International Conference on Automated Planning", 57
exec AddWorkshop "2012-2-6", "15:20:00", "16:20:00", 100, 48, "Asian Conference on Computer Vision", 57
exec AddWorkshop "2013-1-19", "15:45:00", "18:45:00", 99, 27, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 52
exec AddWorkshop "2012-5-4", "12:26:00", "15:26:00", 102, 25, "Microwave Workshop", 39
exec AddWorkshop "2011-3-18", "13:25:00", "16:25:00", 104, 26, "�International� Corporate Citizenship Conference", 46
exec AddWorkshop "2012-12-24", "14:49:00", "17:49:00", 75, 22, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 19
exec AddWorkshop "2013-5-20", "18:20:00", "20:20:00", 104, 32, "2nd International Conference on Photonics, Optics and Laser Technology", 35
exec AddWorkshop "2011-6-29", "16:59:00", "18:59:00", 114, 26, "Mayo Clinic Presents", 83
exec AddWorkshop "2012-10-7", "11:52:00", "14:52:00", 113, 48, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 33
exec AddWorkshop "2013-5-10", "18:45:00", "21:45:00", 121, 25, "Conference on Automated Deduction		", 52
exec AddWorkshop "2011-10-19", "16:54:00", "18:54:00", 119, 20, "International Conference on MultiMedia Modeling", 42
exec AddWorkshop "2011-12-30", "17:10:00", "19:10:00", 106, 49, "European Conference on Machine Learning", 79
exec AddWorkshop "2012-11-27", "16:30:00", "18:30:00", 94, 47, "Asian Conference on Computer Vision", 55
exec AddWorkshop "2013-4-12", "15:1:00", "16:1:00", 99, 40, "Euclid Spacecraft Industry Day", 54
exec AddWorkshop "2013-8-12", "11:38:00", "13:38:00", 56, 26, "Client Summit", 33
exec AddWorkshop "2011-3-16", "12:48:00", "14:48:00", 121, 46, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 46
exec AddWorkshop "2012-5-11", "17:51:00", "20:51:00", 87, 20, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 51
exec AddWorkshop "2012-1-7", "14:35:00", "16:35:00", 136, 42, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 78
exec AddWorkshop "2013-4-4", "15:16:00", "17:16:00", 136, 45, "International Conference on MultiMedia Modeling", 59
exec AddWorkshop "2012-2-26", "13:33:00", "16:33:00", 113, 32, "Science and Challenges of Lunar Sample Return Workshop", 26
exec AddWorkshop "2012-3-13", "19:21:00", "20:21:00", 68, 44, "International Conference on Signal and Imaging Systems Engineering", 70
exec AddWorkshop "2011-11-27", "19:30:00", "20:30:00", 85, 49, "Computer Analysis of Images and Patterns", 58
exec AddWorkshop "2013-11-27", "16:38:00", "18:38:00", 76, 41, "The 12th annual Responsible Business Summit", 58
exec AddWorkshop "2012-9-25", "15:21:00", "16:21:00", 65, 36, "International Conference on Computer Vision", 68
exec AddWorkshop "2013-12-22", "8:41:00", "9:41:00", 56, 45, "RuleML Symposium		", 45
exec AddWorkshop "2013-7-27", "19:7:00", "21:7:00", 142, 30, "Annual Interdisciplinary Conference", 49
exec AddWorkshop "2012-1-25", "10:24:00", "13:24:00", 82, 25, "The 2nd International CSR Communication Conference", 38
exec AddWorkshop "2012-11-9", "16:60:00", "19:60:00", 110, 46, "Photonics West", 38
exec AddWorkshop "2013-9-20", "14:55:00", "17:55:00", 149, 33, "3rd International Conference on Pattern Recognition Applications and Methods", 26
exec AddWorkshop "2013-12-3", "12:53:00", "13:53:00", 131, 22, "7 th International Conference on Bio-inspired Systems and Signal Processing", 31
exec AddWorkshop "2013-2-8", "14:9:00", "17:9:00", 67, 32, "International Conference on Pattern Recognition", 27
exec AddWorkshop "2011-9-24", "14:22:00", "15:22:00", 75, 22, "Science and Challenges of Lunar Sample Return Workshop", 12
exec AddWorkshop "2011-6-25", "14:1:00", "15:1:00", 98, 28, "International Conference on the Principles of", 32
exec AddWorkshop "2011-5-3", "16:6:00", "17:6:00", 112, 23, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 45
exec AddWorkshop "2013-5-10", "8:60:00", "9:60:00", 67, 38, "The BCLC CSR Conference", 72
exec AddWorkshop "2013-1-14", "12:36:00", "15:36:00", 125, 30, "International Conference on Computer Vision", 76
exec AddWorkshop "2012-2-9", "15:41:00", "18:41:00", 128, 41, "Client Summit", 19
exec AddWorkshop "2011-10-29", "18:44:00", "20:44:00", 93, 26, "European Space Power Conference", 32
exec AddWorkshop "2011-12-14", "8:44:00", "10:44:00", 149, 48, "The 12th annual Responsible Business Summit", 12
exec AddWorkshop "2012-9-17", "13:1:00", "16:1:00", 103, 35, "International Joint Conference on Artificial Intelligence", 29
exec AddWorkshop "2013-3-10", "12:34:00", "13:34:00", 64, 35, "48th ESLAB Symposium", 18
exec AddWorkshop "2013-3-17", "17:5:00", "20:5:00", 95, 32, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 4
exec AddWorkshop "2013-2-15", "12:56:00", "15:56:00", 148, 36, "Leadership Strategies for Information Technology in Health Care Boston", 18
exec AddWorkshop "2011-5-27", "12:6:00", "15:6:00", 95, 27, "Leadership Strategies for Information Technology in Health Care Boston", 23
exec AddWorkshop "2013-3-8", "10:47:00", "13:47:00", 127, 43, "European Conference on Artificial Intelligence	", 49
exec AddWorkshop "2011-11-12", "15:39:00", "18:39:00", 85, 21, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 81
exec AddWorkshop "2012-1-28", "13:55:00", "14:55:00", 143, 35, "Global Conference on Sustainability and Reporting", 17
exec AddWorkshop "2011-1-11", "14:31:00", "16:31:00", 95, 30, "International Conference on Automated Reasoning", 41
exec AddWorkshop "2013-1-7", "19:44:00", "21:44:00", 71, 45, "Euclid Spacecraft Industry Day", 37
exec AddWorkshop "2012-11-14", "11:56:00", "13:56:00", 76, 41, "The 12th annual Responsible Business Summit", 58
exec AddWorkshop "2012-4-25", "18:9:00", "20:9:00", 149, 26, "7 th International Conference on Bio-inspired Systems and Signal Processing", 4
exec AddWorkshop "2011-4-22", "12:55:00", "15:55:00", 94, 48, "Workshop on Logic ", 18
exec AddWorkshop "2012-12-12", "12:42:00", "14:42:00", 71, 25, "X-Ray Universe", 56
exec AddWorkshop "2011-9-1", "19:51:00", "21:51:00", 139, 47, "Conference on Uncertainty in Artificial Intelligence", 28
exec AddWorkshop "2012-6-18", "15:25:00", "16:25:00", 146, 44, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 57
exec AddWorkshop "2013-5-24", "14:60:00", "16:60:00", 140, 20, "International Semantic Web Conference		", 31
exec AddWorkshop "2013-3-21", "16:20:00", "17:20:00", 126, 46, "Net Impact Conference", 19
exec AddWorkshop "2012-9-12", "15:17:00", "18:17:00", 71, 26, "Euclid Spacecraft Industry Day", 62
exec AddWorkshop "2012-5-1", "15:25:00", "18:25:00", 64, 48, "International Conference on Autonomous Agents and", 83
exec AddWorkshop "2012-10-1", "13:48:00", "14:48:00", 90, 38, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 26
exec AddWorkshop "2013-8-12", "11:9:00", "13:9:00", 69, 45, "The BCLC CSR Conference", 42
exec AddWorkshop "2011-11-8", "16:25:00", "19:25:00", 101, 44, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 60
exec AddWorkshop "2012-12-25", "15:1:00", "17:1:00", 147, 31, "The BCLC CSR Conference", 32
exec AddWorkshop "2012-4-2", "18:5:00", "19:5:00", 79, 26, "Conference on Uncertainty in Artificial Intelligence", 42
exec AddWorkshop "2013-5-12", "13:43:00", "15:43:00", 93, 44, "European Conference on Machine Learning", 80
exec AddWorkshop "2012-3-3", "14:37:00", "15:37:00", 131, 21, "2nd International Conference on Photonics, Optics and Laser Technology", 12
exec AddWorkshop "2013-1-7", "12:28:00", "13:28:00", 118, 49, "Computer Analysis of Images and Patterns", 60
exec AddWorkshop "2013-9-9", "13:14:00", "16:14:00", 148, 45, "The 2nd International CSR Communication Conference", 6
exec AddWorkshop "2012-2-14", "8:23:00", "10:23:00", 53, 23, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 67
exec AddWorkshop "2011-7-28", "11:26:00", "14:26:00", 91, 33, "Client Summit", 54
exec AddWorkshop "2011-1-4", "17:46:00", "18:46:00", 103, 48, "British	Machine	Vision	Conference		", 53
exec AddWorkshop "2012-11-9", "19:36:00", "20:36:00", 93, 39, "International Conference on Computer Vision", 40
exec AddWorkshop "2011-9-7", "9:54:00", "11:54:00", 90, 24, "International Conference on Pattern Recognition", 9
exec AddWorkshop "2011-5-30", "14:32:00", "16:32:00", 92, 38, "Workshop on Image Processing", 82
exec AddWorkshop "2012-7-7", "15:40:00", "18:40:00", 127, 39, "International Conference on the Principles of", 78
exec AddWorkshop "2013-2-28", "12:3:00", "14:3:00", 89, 42, "International Semantic Web Conference		", 28
exec AddWorkshop "2011-8-11", "11:50:00", "13:50:00", 110, 22, "Business4Better: The Community Partnership Movement", 30
exec AddWorkshop "2013-11-24", "8:8:00", "11:8:00", 71, 39, "International Conference on Automated Planning", 45
exec AddWorkshop "2013-4-15", "19:3:00", "20:3:00", 124, 22, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 19
exec AddWorkshop "2012-5-30", "14:38:00", "16:38:00", 134, 22, "Mayo Clinic Presents", 40
exec AddWorkshop "2011-10-6", "14:34:00", "15:34:00", 80, 31, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 68
exec AddWorkshop "2011-12-2", "10:5:00", "11:5:00", 111, 22, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 24
exec AddWorkshop "2013-5-26", "15:29:00", "18:29:00", 129, 27, "The 12th annual Responsible Business Summit", 63
exec AddWorkshop "2013-1-19", "19:2:00", "22:2:00", 55, 29, "European Conference on Artificial Intelligence	", 16
exec AddWorkshop "2011-7-30", "9:22:00", "12:22:00", 100, 35, "Client Summit", 44
exec AddWorkshop "2012-10-12", "13:15:00", "14:15:00", 69, 42, "The Corporate Philanthropy Forum", 2
exec AddWorkshop "2013-11-23", "14:38:00", "16:38:00", 109, 21, "The Sixth International Conferences on Advances in Multimedia", 73
exec AddWorkshop "2012-5-9", "11:51:00", "12:51:00", 120, 47, "Sustainable Brands Conference", 11
exec AddWorkshop "2012-8-26", "16:9:00", "18:9:00", 108, 24, "International Conference on the Principles of", 11
exec AddWorkshop "2012-2-24", "12:47:00", "14:47:00", 129, 34, "Mechanisms Final Presentation Days", 14
exec AddWorkshop "2012-2-23", "11:33:00", "13:33:00", 81, 31, "Conference on Automated Deduction		", 58
exec AddWorkshop "2012-5-29", "12:17:00", "15:17:00", 134, 42, "Sentinel-2 for Science WS", 39
exec AddWorkshop "2012-4-7", "12:57:00", "15:57:00", 91, 48, "KU Leuven CSR Symposium", 11
exec AddWorkshop "2013-6-11", "18:21:00", "19:21:00", 147, 25, "European Space Technology Harmonisation Conference", 81
exec AddWorkshop "2013-4-29", "10:52:00", "11:52:00", 137, 31, "International Conference on Computer Vision Theory and Applications", 6
exec AddWorkshop "2013-7-12", "16:52:00", "18:52:00", 68, 37, "Net Impact Conference", 3
exec AddWorkshop "2011-1-29", "10:7:00", "12:7:00", 149, 22, "International Conference on Autonomous Agents and", 54
exec AddWorkshop "2013-12-11", "18:15:00", "19:15:00", 67, 46, "Photonics West", 8
exec AddWorkshop "2013-11-26", "19:7:00", "20:7:00", 51, 36, "International Conference on Computer Vision", 47
exec AddWorkshop "2013-12-27", "15:25:00", "16:25:00", 91, 39, "2013 Ethical Leadership Conference: Ethics in Action", 50
exec AddWorkshop "2012-7-14", "8:47:00", "10:47:00", 125, 49, "ICATT", 82
exec AddWorkshop "2011-4-2", "17:53:00", "20:53:00", 90, 29, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 69
exec AddWorkshop "2011-2-22", "17:26:00", "20:26:00", 132, 44, "Mayo Clinic Presents", 58
exec AddWorkshop "2013-12-28", "10:11:00", "11:11:00", 87, 49, "Net Impact Conference", 13
exec AddWorkshop "2012-8-16", "12:56:00", "14:56:00", 126, 21, "Corporate Community Involvement Conference", 66
exec AddWorkshop "2013-2-8", "15:4:00", "17:4:00", 50, 36, "Business4Better: The Community Partnership Movement", 33
exec AddWorkshop "2013-9-5", "17:11:00", "19:11:00", 53, 37, "KU Leuven CSR Symposium", 77
exec AddWorkshop "2013-10-23", "9:35:00", "10:35:00", 135, 24, "7 th International Conference on Bio-inspired Systems and Signal Processing", 55
exec AddWorkshop "2013-6-3", "15:39:00", "16:39:00", 80, 29, "European Conference on Machine Learning", 9
exec AddWorkshop "2013-5-14", "14:42:00", "16:42:00", 73, 24, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 52
exec AddWorkshop "2013-8-13", "12:9:00", "14:9:00", 127, 46, "International Conference on Signal and Imaging Systems Engineering", 63
exec AddWorkshop "2012-9-24", "18:37:00", "19:37:00", 137, 32, "KU Leuven CSR Symposium", 62
exec AddWorkshop "2013-9-12", "11:51:00", "14:51:00", 116, 25, "Conference on Uncertainty in Artificial Intelligence", 70
exec AddWorkshop "2011-1-3", "18:13:00", "20:13:00", 143, 43, "7 th International Conference on Bio-inspired Systems and Signal Processing", 14
exec AddWorkshop "2012-12-25", "12:51:00", "15:51:00", 136, 40, "European Conference on Computer Vision", 1
exec AddWorkshop "2011-2-8", "16:53:00", "17:53:00", 108, 24, "2nd International Conference on Photonics, Optics and Laser Technology", 16
exec AddWorkshop "2011-11-12", "9:59:00", "10:59:00", 87, 32, "International Joint Conference on Automated Reasoning", 84
exec AddWorkshop "2011-2-25", "15:4:00", "18:4:00", 56, 45, "Photonics West", 79
exec AddWorkshop "2011-5-27", "12:57:00", "15:57:00", 124, 34, "International Conference on Signal and Imaging Systems Engineering", 80
exec AddWorkshop "2012-3-14", "13:46:00", "16:46:00", 133, 47, "Ceres Conference", 21
exec AddWorkshop "2011-6-13", "12:36:00", "14:36:00", 95, 25, "Mechanisms Final Presentation Days", 68
exec AddWorkshop "2013-7-9", "15:21:00", "18:21:00", 75, 35, "Conference on Computer Vision and Pattern", 66
exec AddWorkshop "2012-9-20", "15:22:00", "18:22:00", 52, 35, "Workshop on Logic ", 19
exec AddWorkshop "2011-6-11", "8:34:00", "10:34:00", 50, 49, "Science and Challenges of Lunar Sample Return Workshop", 64
exec AddWorkshop "2011-9-29", "15:15:00", "16:15:00", 144, 28, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 64
exec AddWorkshop "2012-8-10", "13:9:00", "15:9:00", 114, 23, "ICATT", 21
exec AddWorkshop "2013-4-22", "13:13:00", "16:13:00", 122, 34, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 47
exec AddWorkshop "2013-8-23", "11:44:00", "12:44:00", 66, 20, "International Conference on MultiMedia Modeling", 75
exec AddWorkshop "2013-10-30", "19:57:00", "22:57:00", 60, 38, "International Joint Conference on Automated Reasoning", 80
exec AddWorkshop "2012-4-21", "11:55:00", "14:55:00", 107, 48, "Corporate Community Involvement Conference", 61
exec AddWorkshop "2011-3-20", "10:49:00", "12:49:00", 83, 43, "Corporate Community Involvement Conference", 84
exec AddWorkshop "2012-7-30", "14:32:00", "15:32:00", 62, 24, "International Conference on Computer Vision Theory and Applications", 64
exec AddWorkshop "2011-9-8", "19:26:00", "22:26:00", 100, 48, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 60
exec AddWorkshop "2011-7-30", "12:48:00", "15:48:00", 96, 25, "International Conference on the Principles of", 37
exec AddWorkshop "2013-5-7", "9:48:00", "11:48:00", 83, 20, "2nd International Conference on Photonics, Optics and Laser Technology", 77
exec AddWorkshop "2011-4-9", "19:3:00", "20:3:00", 137, 34, "Euclid Spacecraft Industry Day", 37
exec AddWorkshop "2011-4-23", "11:49:00", "14:49:00", 129, 24, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 13
exec AddWorkshop "2011-2-15", "14:40:00", "16:40:00", 120, 39, "Conference on Computer Vision and Pattern", 8
exec AddWorkshop "2011-12-16", "14:35:00", "17:35:00", 137, 21, "Conference on Computer Vision and Pattern", 20
exec AddWorkshop "2012-5-17", "16:16:00", "18:16:00", 137, 36, "KU Leuven CSR Symposium", 82
exec AddWorkshop "2013-10-21", "14:34:00", "15:34:00", 119, 34, "The BCLC CSR Conference", 66
exec AddWorkshop "2012-6-19", "18:20:00", "21:20:00", 92, 45, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 60
exec AddWorkshop "2011-5-12", "8:41:00", "11:41:00", 61, 21, "International Conference on Automated Reasoning", 66
exec AddWorkshop "2013-12-30", "16:9:00", "17:9:00", 121, 40, "Net Impact Conference", 20
exec AddWorkshop "2012-1-17", "8:50:00", "9:50:00", 130, 45, "Global Conference on Sustainability and Reporting", 30
exec AddWorkshop "2013-1-5", "19:31:00", "21:31:00", 52, 34, "Client Summit", 47
exec AddWorkshop "2013-8-29", "9:32:00", "10:32:00", 76, 27, "Mayo Clinic Presents", 29
exec AddWorkshop "2011-2-23", "18:60:00", "20:60:00", 53, 30, "�International� Corporate Citizenship Conference", 77
exec AddWorkshop "2012-8-12", "8:47:00", "9:47:00", 148, 47, "X-Ray Universe", 78
exec AddWorkshop "2011-5-26", "19:49:00", "21:49:00", 95, 37, "Client Summit", 41
exec AddWorkshop "2013-1-2", "9:51:00", "11:51:00", 128, 49, "Computer Analysis of Images and Patterns", 74
exec AddWorkshop "2012-1-9", "8:34:00", "11:34:00", 89, 20, "X-Ray Universe", 11
exec AddWorkshop "2013-9-15", "14:6:00", "17:6:00", 105, 34, "Microwave Workshop", 4
exec AddWorkshop "2011-6-3", "14:15:00", "16:15:00", 122, 29, "Client Summit", 17
exec AddWorkshop "2013-9-14", "18:34:00", "21:34:00", 81, 39, "International Joint Conference on Artificial Intelligence", 45
exec AddWorkshop "2012-7-6", "17:27:00", "20:27:00", 66, 47, "Photonics West", 28
exec AddWorkshop "2011-3-11", "14:23:00", "17:23:00", 107, 27, "48th ESLAB Symposium", 53
exec AddWorkshop "2012-9-6", "13:29:00", "15:29:00", 125, 26, "Business4Better: The Community Partnership Movement", 37
exec AddWorkshop "2012-6-30", "19:48:00", "21:48:00", 137, 39, "International Conference on the Principles of", 17
exec AddWorkshop "2013-5-28", "8:59:00", "9:59:00", 111, 32, "International Semantic Web Conference		", 74
exec AddWorkshop "2012-9-4", "12:53:00", "14:53:00", 101, 20, "European Space Power Conference", 11
exec AddWorkshop "2012-8-15", "12:14:00", "13:14:00", 60, 24, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 8
exec AddWorkshop "2013-3-19", "8:20:00", "9:20:00", 112, 46, "European Space Power Conference", 33
exec AddWorkshop "2011-8-19", "11:47:00", "12:47:00", 118, 39, "Client Summit", 12
exec AddWorkshop "2012-9-22", "10:19:00", "13:19:00", 116, 30, "Microwave Workshop", 20
exec AddWorkshop "2013-2-10", "17:60:00", "20:60:00", 95, 49, "48th ESLAB Symposium", 28
exec AddWorkshop "2012-9-20", "15:45:00", "18:45:00", 121, 22, "International Conference on Automated Reasoning", 9
exec AddWorkshop "2011-4-11", "13:39:00", "15:39:00", 116, 26, "X-Ray Universe", 51
exec AddWorkshop "2012-10-9", "9:28:00", "12:28:00", 70, 40, "Euclid Spacecraft Industry Day", 35
exec AddWorkshop "2012-5-26", "15:9:00", "18:9:00", 107, 44, "European Conference on Machine Learning", 51
exec AddWorkshop "2011-1-10", "15:58:00", "18:58:00", 107, 22, "Net Impact Conference", 59
exec AddWorkshop "2012-12-14", "8:26:00", "11:26:00", 80, 37, "ICATT", 67
exec AddWorkshop "2013-7-25", "8:43:00", "9:43:00", 69, 23, "European Conference on Machine Learning", 84
exec AddWorkshop "2013-3-3", "19:15:00", "21:15:00", 79, 23, "X-Ray Universe", 45
exec AddWorkshop "2013-3-16", "16:31:00", "19:31:00", 121, 37, "International Conference on Computer Vision", 50
exec AddWorkshop "2013-2-28", "15:4:00", "18:4:00", 84, 38, "2013 Ethical Leadership Conference: Ethics in Action", 62
exec AddWorkshop "2011-2-5", "19:52:00", "20:52:00", 122, 28, "International Semantic Web Conference		", 4
exec AddWorkshop "2011-4-6", "18:8:00", "21:8:00", 104, 46, "European Space Technology Harmonisation Conference", 32
exec AddWorkshop "2013-1-3", "12:50:00", "15:50:00", 75, 34, "Cause Marketing Forum", 81
exec AddWorkshop "2013-1-26", "9:23:00", "11:23:00", 135, 43, "Conference on Computer Vision and Pattern", 24
exec AddWorkshop "2011-2-4", "14:9:00", "15:9:00", 84, 44, "48th ESLAB Symposium", 35
exec AddWorkshop "2011-12-3", "16:27:00", "17:27:00", 98, 21, "GNC 2014", 73
exec AddWorkshop "2012-6-2", "13:35:00", "15:35:00", 78, 39, "Foundations of Genetic Algorithms		", 30
exec AddWorkshop "2011-3-7", "16:6:00", "19:6:00", 86, 33, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 38
exec AddWorkshop "2013-1-29", "8:22:00", "10:22:00", 132, 43, "KU Leuven CSR Symposium", 37
exec AddWorkshop "2011-1-13", "19:57:00", "20:57:00", 128, 37, "RuleML Symposium		", 30
exec AddWorkshop "2011-11-3", "8:36:00", "10:36:00", 88, 21, "ARTES 1 Final Presentation Days", 24
exec AddWorkshop "2013-3-13", "8:12:00", "9:12:00", 127, 22, "International Conference on Pattern Recognition", 50
exec AddWorkshop "2013-2-27", "16:10:00", "19:10:00", 72, 46, "Sustainable Brands Conference", 66
exec AddWorkshop "2013-5-4", "15:49:00", "16:49:00", 88, 41, "Sentinel-2 for Science WS", 55
exec AddWorkshop "2013-1-26", "12:26:00", "15:26:00", 134, 47, "Corporate Community Involvement Conference", 57
exec AddWorkshop "2012-9-17", "14:31:00", "17:31:00", 97, 23, "International Conference on Computer Vision Theory and Applications", 9
exec AddWorkshop "2012-9-16", "15:55:00", "17:55:00", 133, 48, "International Conference on Autonomous Agents and", 57
exec AddWorkshop "2013-10-4", "8:49:00", "10:49:00", 148, 42, "Cause Marketing Forum", 5
exec AddWorkshop "2011-1-7", "13:34:00", "15:34:00", 124, 45, "Workshop on Logic ", 72
exec AddWorkshop "2012-5-1", "9:19:00", "12:19:00", 93, 35, "Asian Conference on Computer Vision", 47
exec AddWorkshop "2013-2-10", "8:38:00", "10:38:00", 99, 45, "Mechanisms Final Presentation Days", 44
exec AddWorkshop "2012-9-22", "14:19:00", "17:19:00", 94, 47, "International Conference on Signal and Imaging Systems Engineering", 47
exec AddWorkshop "2011-2-13", "13:12:00", "16:12:00", 61, 36, "RuleML Symposium				", 73
exec AddWorkshop "2011-6-12", "8:31:00", "10:31:00", 75, 25, "Annual Convention of The Society", 15
exec AddWorkshop "2011-5-19", "18:1:00", "19:1:00", 137, 35, "ARTES 1 Final Presentation Days", 43
exec AddWorkshop "2012-8-29", "16:37:00", "17:37:00", 94, 48, "Client Summit", 57
exec AddWorkshop "2013-7-17", "19:20:00", "22:20:00", 112, 22, "Science and Challenges of Lunar Sample Return Workshop", 28
exec AddWorkshop "2013-9-26", "13:51:00", "16:51:00", 131, 43, "British	Machine	Vision	Conference		", 74
exec AddWorkshop "2011-5-16", "18:3:00", "21:3:00", 146, 44, "European Space Power Conference", 13
exec AddWorkshop "2012-7-4", "13:17:00", "16:17:00", 134, 34, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 7
exec AddWorkshop "2013-11-8", "11:16:00", "14:16:00", 115, 31, "Euclid Spacecraft Industry Day", 58
exec AddWorkshop "2011-4-10", "11:40:00", "14:40:00", 144, 46, "International Conference on Logic for Programming", 15
exec AddWorkshop "2012-4-29", "11:3:00", "12:3:00", 77, 34, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 23
exec AddWorkshop "2012-6-26", "14:30:00", "15:30:00", 113, 30, "2013 Ethical Leadership Conference: Ethics in Action", 32
exec AddWorkshop "2012-11-7", "15:11:00", "16:11:00", 140, 40, "Business4Better: The Community Partnership Movement", 29
exec AddWorkshop "2011-4-21", "14:27:00", "17:27:00", 53, 23, "The 12th annual Responsible Business Summit", 1
exec AddWorkshop "2011-9-6", "12:34:00", "15:34:00", 141, 47, "ICATT", 68
exec AddWorkshop "2012-5-24", "11:37:00", "14:37:00", 97, 31, "Foundations of Genetic Algorithms		", 84
exec AddWorkshop "2013-8-22", "17:45:00", "19:45:00", 147, 29, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 29
exec AddWorkshop "2012-3-14", "14:26:00", "17:26:00", 67, 25, "International Conference on Autonomous Agents and", 52
exec AddWorkshop "2013-11-19", "17:56:00", "19:56:00", 58, 30, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 24
exec AddWorkshop "2013-12-29", "9:52:00", "12:52:00", 71, 41, "Conference on Learning Theory		", 74
exec AddWorkshop "2011-1-17", "14:17:00", "17:17:00", 114, 21, "Conference on Computer Vision and Pattern", 75
exec AddWorkshop "2012-2-22", "16:38:00", "19:38:00", 56, 29, "3rd International Conference on Pattern Recognition Applications and Methods", 12
exec AddWorkshop "2013-11-6", "10:19:00", "12:19:00", 87, 22, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 1
exec AddWorkshop "2013-9-4", "9:50:00", "10:50:00", 63, 33, "European Conference on Machine Learning", 19
exec AddWorkshop "2013-1-26", "12:41:00", "14:41:00", 117, 26, "Mechanisms Final Presentation Days", 29
exec AddWorkshop "2012-4-6", "17:48:00", "20:48:00", 131, 21, "International Joint Conference on Artificial Intelligence", 80
exec AddWorkshop "2013-6-2", "14:36:00", "17:36:00", 95, 22, "Global Conference on Sustainability and Reporting", 71
exec AddWorkshop "2013-12-6", "12:29:00", "15:29:00", 145, 34, "International Conference on Logic for Programming", 18
exec AddWorkshop "2013-8-29", "17:18:00", "19:18:00", 51, 20, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 62
exec AddWorkshop "2012-5-20", "12:34:00", "15:34:00", 81, 42, "International Conference on Signal and Imaging Systems Engineering", 7
exec AddWorkshop "2011-8-7", "13:52:00", "16:52:00", 56, 20, "EuroCOW the Calibration and Orientation Workshop", 23
exec AddWorkshop "2011-2-17", "18:35:00", "19:35:00", 146, 40, "Sustainable Brands Conference", 75
exec AddWorkshop "2013-9-18", "19:17:00", "20:17:00", 99, 49, "4S Symposium 2014", 0
exec AddWorkshop "2011-12-13", "8:43:00", "10:43:00", 118, 34, "GNC 2014", 39
exec AddWorkshop "2013-10-19", "11:16:00", "13:16:00", 70, 26, "International Conference on Signal and Imaging Systems Engineering", 52
exec AddWorkshop "2011-6-30", "11:54:00", "13:54:00", 85, 24, "Annual Convention of The Society", 29
exec AddWorkshop "2013-10-17", "14:59:00", "16:59:00", 79, 23, "International Conference on MultiMedia Modeling", 79
exec AddWorkshop "2011-2-6", "19:45:00", "21:45:00", 74, 30, "3rd International Conference on Pattern Recognition Applications and Methods", 63
exec AddWorkshop "2013-1-26", "15:5:00", "17:5:00", 88, 33, "International Conference on Logic for Programming", 38
exec AddWorkshop "2012-2-26", "8:40:00", "9:40:00", 142, 31, "International Conference on Artificial Neural Networks", 9
exec AddWorkshop "2013-4-6", "9:46:00", "10:46:00", 140, 32, "International Conference on Artificial Neural Networks", 71
exec AddWorkshop "2012-9-24", "17:57:00", "19:57:00", 132, 41, "48th ESLAB Symposium", 25
exec AddWorkshop "2013-12-25", "9:45:00", "10:45:00", 62, 31, "The Sixth International Conferences on Advances in Multimedia", 11
exec AddWorkshop "2012-8-12", "11:24:00", "12:24:00", 146, 22, "Asian Conference on Computer Vision", 1
exec AddWorkshop "2013-2-13", "17:54:00", "19:54:00", 131, 21, "British	Machine	Vision	Conference		", 20
exec AddWorkshop "2013-10-9", "11:18:00", "12:18:00", 87, 49, "Workshop on Image Processing", 46
exec AddWorkshop "2012-10-23", "16:32:00", "19:32:00", 86, 22, "International Joint Conference on Automated Reasoning", 15
exec AddWorkshop "2012-11-12", "19:31:00", "20:31:00", 137, 25, "Cause Marketing Forum", 4
exec AddWorkshop "2011-7-11", "8:53:00", "9:53:00", 58, 21, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 2
exec AddWorkshop "2011-7-28", "9:10:00", "11:10:00", 128, 25, "Mayo Clinic Presents", 24
exec AddWorkshop "2012-2-29", "13:8:00", "15:8:00", 111, 34, "International Conference on Pattern Recognition", 11
exec AddWorkshop "2011-5-22", "13:27:00", "15:27:00", 114, 41, "Cause Marketing Forum", 55
exec AddWorkshop "2012-1-28", "11:55:00", "12:55:00", 142, 40, "Life in Space for Life on Earth Symposium", 43
exec AddWorkshop "2013-9-19", "9:44:00", "10:44:00", 65, 31, "The BCLC CSR Conference", 75
exec AddWorkshop "2012-5-27", "10:22:00", "12:22:00", 77, 45, "2nd International Conference on Photonics, Optics and Laser Technology", 71
exec AddWorkshop "2011-12-4", "13:7:00", "15:7:00", 127, 38, "Leadership Strategies for Information Technology in Health Care Boston", 69
exec AddWorkshop "2012-11-12", "16:23:00", "18:23:00", 103, 39, "RuleML Symposium		", 72
exec AddWorkshop "2012-6-16", "9:32:00", "12:32:00", 69, 29, "KU Leuven CSR Symposium", 40
exec AddWorkshop "2013-12-28", "8:55:00", "11:55:00", 82, 23, "RuleML Symposium				", 52
exec AddWorkshop "2012-10-10", "18:49:00", "19:49:00", 74, 30, "Leadership Strategies for Information Technology in Health Care Boston", 73
exec AddWorkshop "2013-8-16", "9:32:00", "12:32:00", 75, 41, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 54
exec AddWorkshop "2011-1-16", "16:42:00", "18:42:00", 86, 20, "International Conference on Signal and Imaging Systems Engineering", 45
exec AddWorkshop "2011-7-18", "14:12:00", "15:12:00", 143, 20, "7 th International Conference on Bio-inspired Systems and Signal Processing", 83
exec AddWorkshop "2013-7-14", "18:31:00", "20:31:00", 121, 25, "Workshop on Image Processing", 34
exec AddWorkshop "2011-1-11", "15:49:00", "17:49:00", 51, 37, "Photonics West", 51
exec AddWorkshop "2012-10-20", "17:47:00", "18:47:00", 133, 44, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 42
exec AddWorkshop "2013-7-9", "15:37:00", "18:37:00", 89, 30, "The Corporate Philanthropy Forum", 58
exec AddWorkshop "2012-3-2", "15:7:00", "16:7:00", 132, 36, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 33
exec AddWorkshop "2012-3-14", "8:49:00", "11:49:00", 120, 38, "3rd International Conference on Pattern Recognition Applications and Methods", 47
exec AddWorkshop "2011-2-25", "11:47:00", "13:47:00", 113, 44, "2nd International Conference on Photonics, Optics and Laser Technology", 11
exec AddWorkshop "2013-1-5", "11:38:00", "12:38:00", 81, 40, "The Corporate Philanthropy Forum", 64
exec AddWorkshop "2012-1-13", "9:23:00", "11:23:00", 134, 21, "Corporate Community Involvement Conference", 50
exec AddWorkshop "2013-6-9", "10:31:00", "11:31:00", 71, 36, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 5
exec AddWorkshop "2011-9-3", "16:9:00", "17:9:00", 133, 49, "KU Leuven CSR Symposium", 5
exec AddWorkshop "2013-1-3", "15:21:00", "18:21:00", 129, 36, "ARTES 1 Final Presentation Days", 1
exec AddWorkshop "2011-1-4", "13:22:00", "15:22:00", 115, 26, "International Conference on Automated Planning", 82
exec AddWorkshop "2013-7-22", "14:15:00", "17:15:00", 89, 39, "Annual Interdisciplinary Conference", 53
exec AddWorkshop "2012-7-15", "10:31:00", "12:31:00", 133, 36, "European Conference on Machine Learning", 48
exec AddWorkshop "2011-12-18", "12:17:00", "15:17:00", 68, 27, "Workshop on Image Processing", 33
exec AddWorkshop "2013-3-10", "19:17:00", "21:17:00", 112, 30, "Sustainable Brands Conference", 39
exec AddWorkshop "2013-7-15", "14:20:00", "17:20:00", 84, 41, "Leadership Strategies for Information Technology in Health Care Boston", 75
exec AddWorkshop "2012-5-5", "13:23:00", "15:23:00", 81, 31, "4th DUE Permafrost User Workshop", 74
exec AddWorkshop "2011-10-16", "12:39:00", "13:39:00", 73, 46, "Conference on Uncertainty in Artificial Intelligence", 27
exec AddWorkshop "2012-6-11", "13:22:00", "14:22:00", 133, 21, "LPVE Land product validation and evolution", 28
exec AddWorkshop "2011-10-16", "19:12:00", "22:12:00", 112, 48, "4S Symposium 2014", 6
exec AddWorkshop "2012-11-8", "19:34:00", "20:34:00", 147, 22, "Photonics West", 79
exec AddWorkshop "2012-5-18", "10:4:00", "13:4:00", 57, 29, "Global Conference on Sustainability and Reporting", 46
exec AddWorkshop "2012-10-13", "19:5:00", "20:5:00", 103, 28, "2013 Ethical Leadership Conference: Ethics in Action", 65
exec AddWorkshop "2012-2-15", "19:15:00", "22:15:00", 127, 44, "Conference on Computer Vision and Pattern", 51
exec AddWorkshop "2011-8-10", "10:13:00", "12:13:00", 88, 43, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 4
exec AddWorkshop "2012-7-23", "17:32:00", "19:32:00", 137, 23, "Corporate Community Involvement Conference", 65
exec AddWorkshop "2011-6-19", "16:38:00", "17:38:00", 76, 34, "Global Conference on Sustainability and Reporting", 52
exec AddWorkshop "2012-1-29", "12:56:00", "13:56:00", 98, 21, "KU Leuven CSR Symposium", 36
exec AddWorkshop "2011-3-28", "14:55:00", "17:55:00", 62, 29, "The Sixth International Conferences on Advances in Multimedia", 84
exec AddWorkshop "2012-4-25", "9:30:00", "11:30:00", 68, 39, "Science and Challenges of Lunar Sample Return Workshop", 71
exec AddWorkshop "2012-7-1", "15:12:00", "17:12:00", 77, 39, "Net Impact Conference", 76
exec AddWorkshop "2013-6-9", "19:47:00", "22:47:00", 81, 41, "International Joint Conference on Automated Reasoning", 42
exec AddWorkshop "2012-2-5", "8:4:00", "11:4:00", 57, 21, "The 12th annual Responsible Business Summit", 47
exec AddWorkshop "2013-6-24", "14:45:00", "15:45:00", 67, 33, "EuroCOW the Calibration and Orientation Workshop", 74
exec AddWorkshop "2012-7-4", "16:57:00", "19:57:00", 71, 36, "Net Impact Conference", 70
exec AddWorkshop "2013-12-12", "10:1:00", "13:1:00", 99, 30, "X-Ray Universe", 42
exec AddWorkshop "2012-10-14", "8:25:00", "9:25:00", 65, 24, "Annual Interdisciplinary Conference", 5
exec AddWorkshop "2013-3-2", "11:12:00", "14:12:00", 128, 40, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 21
exec AddWorkshop "2013-3-15", "19:31:00", "20:31:00", 130, 46, "4th DUE Permafrost User Workshop", 24
exec AddWorkshop "2011-9-4", "15:47:00", "16:47:00", 68, 21, "Sustainable Brands Conference", 9
exec AddWorkshop "2013-4-2", "16:18:00", "18:18:00", 109, 48, "International Conference on Autonomous Agents and", 61
exec AddWorkshop "2012-3-20", "19:56:00", "20:56:00", 110, 38, "International Conference on Artificial Neural Networks", 46
exec AddWorkshop "2013-5-3", "8:20:00", "9:20:00", 121, 33, "Foundations of Genetic Algorithms		", 19
exec AddWorkshop "2013-1-13", "18:43:00", "21:43:00", 105, 41, "ARTES 1 Final Presentation Days", 62
exec AddWorkshop "2012-7-9", "14:55:00", "15:55:00", 108, 46, "Client Summit", 2
exec AddWorkshop "2012-3-27", "12:20:00", "15:20:00", 52, 22, "Mechanisms Final Presentation Days", 1
exec AddWorkshop "2012-6-30", "10:31:00", "13:31:00", 72, 26, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 36
exec AddWorkshop "2011-1-28", "19:45:00", "20:45:00", 147, 40, "2nd International Conference on Photonics, Optics and Laser Technology", 50
exec AddWorkshop "2013-8-7", "14:56:00", "15:56:00", 55, 21, "The BCLC CSR Conference", 47
exec AddWorkshop "2013-3-22", "19:4:00", "22:4:00", 106, 23, "International Conference on Logic for Programming", 76
exec AddWorkshop "2012-10-26", "15:42:00", "17:42:00", 89, 21, "Business4Better: The Community Partnership Movement", 27
exec AddWorkshop "2013-9-14", "14:51:00", "17:51:00", 76, 42, "Microwave Workshop", 64
exec AddWorkshop "2013-4-6", "10:8:00", "11:8:00", 76, 49, "The 2nd International CSR Communication Conference", 15
exec AddWorkshop "2011-5-15", "13:3:00", "16:3:00", 51, 29, "Sentinel-2 for Science WS", 56
exec AddWorkshop "2013-3-27", "9:26:00", "10:26:00", 67, 34, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 17
exec AddWorkshop "2011-6-2", "17:58:00", "19:58:00", 141, 45, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 60
exec AddWorkshop "2012-9-16", "9:50:00", "12:50:00", 78, 37, "Client Summit", 65
exec AddWorkshop "2013-4-8", "10:20:00", "12:20:00", 85, 31, "EuroCOW the Calibration and Orientation Workshop", 65
exec AddWorkshop "2011-10-21", "13:30:00", "14:30:00", 106, 42, "European Space Power Conference", 68
exec AddWorkshop "2011-8-4", "18:23:00", "19:23:00", 135, 21, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 61
exec AddWorkshop "2013-5-17", "9:35:00", "10:35:00", 83, 44, "Conference on Uncertainty in Artificial Intelligence", 63
exec AddWorkshop "2012-10-4", "12:42:00", "14:42:00", 56, 46, "ARTES 1 Final Presentation Days", 71
exec AddWorkshop "2013-2-6", "18:4:00", "21:4:00", 50, 40, "British	Machine	Vision	Conference		", 78
exec AddWorkshop "2011-4-9", "18:23:00", "20:23:00", 70, 30, "Client Summit", 22
exec AddWorkshop "2011-10-21", "12:5:00", "13:5:00", 108, 23, "EuroCOW the Calibration and Orientation Workshop", 17
exec AddWorkshop "2012-6-25", "8:3:00", "10:3:00", 59, 31, "International Conference on Logic for Programming", 55
exec AddWorkshop "2013-3-22", "18:30:00", "20:30:00", 55, 43, "Asian Conference on Computer Vision", 78
exec AddWorkshop "2012-11-16", "11:43:00", "14:43:00", 92, 49, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 24
exec AddWorkshop "2011-5-13", "14:20:00", "15:20:00", 97, 23, "RuleML Symposium				", 67
exec AddWorkshop "2012-2-10", "13:37:00", "14:37:00", 81, 21, "Business4Better: The Community Partnership Movement", 26
exec AddWorkshop "2012-8-15", "10:10:00", "13:10:00", 94, 40, "Workshop on Image Processing", 57
exec AddWorkshop "2013-2-13", "10:28:00", "12:28:00", 143, 36, "International Conference on Computer Vision", 43
exec AddWorkshop "2013-3-13", "17:39:00", "18:39:00", 114, 35, "International Semantic Web Conference		", 19
exec AddWorkshop "2011-10-1", "12:60:00", "13:60:00", 126, 23, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 50
exec AddWorkshop "2012-9-3", "13:43:00", "14:43:00", 110, 48, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 41
exec AddWorkshop "2011-1-16", "18:33:00", "21:33:00", 55, 36, "7 th International Conference on Bio-inspired Systems and Signal Processing", 47
exec AddWorkshop "2011-2-18", "16:11:00", "17:11:00", 107, 49, "International Conference on Computer Vision", 9
exec AddWorkshop "2013-8-29", "18:2:00", "19:2:00", 87, 44, "Cause Marketing Forum", 35
exec AddWorkshop "2013-5-21", "9:30:00", "12:30:00", 113, 34, "Sustainable Brands Conference", 72
exec AddWorkshop "2012-8-15", "16:20:00", "19:20:00", 121, 27, "Photonics West", 9
exec AddWorkshop "2012-9-6", "13:45:00", "15:45:00", 61, 32, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 61
exec AddWorkshop "2012-10-5", "13:47:00", "16:47:00", 125, 45, "Asian Conference on Computer Vision", 56
exec AddWorkshop "2011-5-19", "10:6:00", "12:6:00", 61, 21, "2013 Ethical Leadership Conference: Ethics in Action", 23
exec AddWorkshop "2013-12-30", "17:25:00", "18:25:00", 75, 43, "Corporate Community Involvement Conference", 41
exec AddWorkshop "2012-8-2", "15:31:00", "18:31:00", 82, 38, "GNC 2014", 21
exec AddWorkshop "2011-11-21", "8:57:00", "11:57:00", 107, 48, "European Conference on Computer Vision", 39
exec AddWorkshop "2013-4-4", "9:36:00", "11:36:00", 79, 27, "European Space Technology Harmonisation Conference", 83
exec AddWorkshop "2012-11-3", "11:23:00", "12:23:00", 149, 48, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 7
exec AddWorkshop "2012-9-12", "13:27:00", "16:27:00", 113, 47, "2013 Ethical Leadership Conference: Ethics in Action", 41
exec AddWorkshop "2012-6-15", "13:57:00", "15:57:00", 68, 48, "Sentinel-2 for Science WS", 33
exec AddWorkshop "2011-1-20", "18:7:00", "19:7:00", 71, 27, "Sentinel-2 for Science WS", 42
exec AddWorkshop "2012-12-11", "19:56:00", "21:56:00", 86, 29, "LPVE Land product validation and evolution", 31
exec AddWorkshop "2013-2-13", "9:56:00", "12:56:00", 72, 28, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 76
exec AddWorkshop "2012-9-20", "10:40:00", "11:40:00", 114, 25, "3rd International Conference on Pattern Recognition Applications and Methods", 56
exec AddWorkshop "2012-4-16", "18:13:00", "20:13:00", 94, 28, "Life in Space for Life on Earth Symposium", 66
exec AddWorkshop "2013-4-5", "12:13:00", "15:13:00", 117, 44, "Sustainable Brands Conference", 18
exec AddWorkshop "2013-1-1", "9:2:00", "11:2:00", 92, 25, "4S Symposium 2014", 59
exec AddWorkshop "2013-3-16", "10:12:00", "11:12:00", 137, 33, "Sustainable Brands Conference", 15
exec AddWorkshop "2012-9-19", "10:36:00", "12:36:00", 87, 24, "Business4Better: The Community Partnership Movement", 59
exec AddWorkshop "2011-4-20", "19:60:00", "22:60:00", 100, 30, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 62
exec AddWorkshop "2013-6-6", "10:43:00", "12:43:00", 129, 24, "Cause Marketing Forum", 68
exec AddWorkshop "2012-11-17", "11:8:00", "13:8:00", 113, 20, "7 th International Conference on Bio-inspired Systems and Signal Processing", 9
exec AddWorkshop "2011-8-9", "14:26:00", "16:26:00", 146, 29, "EuroCOW the Calibration and Orientation Workshop", 42
exec AddWorkshop "2011-12-30", "17:8:00", "19:8:00", 121, 27, "European Conference on Computer Vision", 44
exec AddWorkshop "2012-5-25", "16:4:00", "17:4:00", 111, 39, "EuroCOW the Calibration and Orientation Workshop", 69
exec AddWorkshop "2013-11-4", "19:47:00", "20:47:00", 143, 41, "International Conference on Automated Reasoning", 66
exec AddWorkshop "2013-7-28", "11:42:00", "14:42:00", 116, 46, "Annual Interdisciplinary Conference", 60
exec AddWorkshop "2011-5-11", "17:5:00", "20:5:00", 63, 30, "Computer Analysis of Images and Patterns", 68
exec AddWorkshop "2012-1-11", "19:4:00", "21:4:00", 110, 32, "Conference on Uncertainty in Artificial Intelligence", 65
exec AddWorkshop "2013-10-21", "12:53:00", "15:53:00", 72, 43, "Conference on Learning Theory		", 57
exec AddWorkshop "2013-7-30", "10:17:00", "11:17:00", 113, 47, "X-Ray Universe", 35
exec AddWorkshop "2012-4-15", "12:53:00", "13:53:00", 61, 20, "The BCLC CSR Conference", 12
exec AddWorkshop "2012-2-13", "13:26:00", "14:26:00", 95, 46, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 4
exec AddWorkshop "2012-6-1", "10:24:00", "13:24:00", 59, 32, "Computer Analysis of Images and Patterns", 59
exec AddWorkshop "2011-8-29", "18:2:00", "21:2:00", 141, 32, "International Conference on Logic for Programming", 49
exec AddWorkshop "2011-5-30", "10:28:00", "13:28:00", 60, 31, "EuroCOW the Calibration and Orientation Workshop", 78
exec AddWorkshop "2012-8-27", "13:34:00", "14:34:00", 141, 35, "Photonics West", 45
exec AddWorkshop "2012-10-26", "17:33:00", "18:33:00", 118, 34, "Business4Better: The Community Partnership Movement", 83
exec AddWorkshop "2011-4-8", "18:18:00", "19:18:00", 80, 21, "Euclid Spacecraft Industry Day", 61
exec AddWorkshop "2011-9-8", "15:1:00", "18:1:00", 68, 45, "Sentinel-2 for Science WS", 30
exec AddWorkshop "2012-9-27", "14:20:00", "17:20:00", 85, 35, "International Conference on the Principles of", 26
exec AddWorkshop "2013-1-14", "8:25:00", "11:25:00", 81, 44, "�International� Corporate Citizenship Conference", 53
exec AddWorkshop "2013-3-27", "17:20:00", "20:20:00", 98, 21, "International Conference on Artificial Neural Networks", 4
exec AddWorkshop "2013-9-24", "8:11:00", "10:11:00", 103, 25, "Microwave Workshop", 41
exec AddWorkshop "2012-3-16", "10:5:00", "11:5:00", 65, 45, "International Conference on Automated Reasoning", 63
exec AddWorkshop "2012-1-4", "16:13:00", "19:13:00", 130, 44, "RuleML Symposium				", 69
exec AddWorkshop "2011-2-30", "12:30:00", "13:30:00", 115, 21, "European Conference on Artificial Intelligence	", 45
exec AddWorkshop "2011-1-4", "14:37:00", "17:37:00", 102, 29, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 38
exec AddWorkshop "2013-11-3", "12:28:00", "13:28:00", 129, 32, "RuleML Symposium				", 7
exec AddWorkshop "2013-2-27", "19:26:00", "22:26:00", 122, 34, "Microwave Workshop", 78
exec AddWorkshop "2012-8-3", "19:22:00", "22:22:00", 51, 27, "International Conference on MultiMedia Modeling", 37
exec AddWorkshop "2011-1-17", "14:59:00", "17:59:00", 130, 29, "Photonics West", 33
exec AddWorkshop "2012-8-3", "18:14:00", "20:14:00", 61, 35, "The Sixth International Conferences on Advances in Multimedia", 75
exec AddWorkshop "2012-9-25", "9:5:00", "10:5:00", 106, 45, "European Space Technology Harmonisation Conference", 4
exec AddWorkshop "2012-11-3", "8:11:00", "9:11:00", 119, 38, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 64
exec AddWorkshop "2012-3-18", "16:42:00", "19:42:00", 80, 30, "KU Leuven CSR Symposium", 55
exec AddWorkshop "2011-10-20", "8:14:00", "9:14:00", 117, 34, "European Space Power Conference", 38
exec AddWorkshop "2013-9-8", "14:24:00", "17:24:00", 111, 49, "EuroCOW the Calibration and Orientation Workshop", 42
exec AddWorkshop "2012-1-13", "18:17:00", "21:17:00", 55, 22, "Global Conference on Sustainability and Reporting", 51
exec AddWorkshop "2012-11-29", "11:43:00", "14:43:00", 138, 34, "The Corporate Philanthropy Forum", 41
exec AddWorkshop "2013-7-14", "19:21:00", "21:21:00", 117, 45, "Microwave Workshop", 17
exec AddWorkshop "2011-9-5", "13:15:00", "16:15:00", 56, 20, "Conference on Computer Vision and Pattern", 12
exec AddWorkshop "2013-9-13", "8:32:00", "9:32:00", 134, 33, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 47
exec AddWorkshop "2011-10-22", "19:29:00", "22:29:00", 108, 37, "Conference on Automated Deduction		", 12
exec AddWorkshop "2012-6-6", "10:12:00", "11:12:00", 102, 20, "Mayo Clinic Presents", 38
exec AddWorkshop "2011-3-18", "19:36:00", "21:36:00", 54, 20, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 68
exec AddWorkshop "2012-5-14", "8:46:00", "11:46:00", 73, 38, "RuleML Symposium		", 75
exec AddWorkshop "2011-3-15", "15:34:00", "16:34:00", 103, 44, "European Space Technology Harmonisation Conference", 83
exec AddWorkshop "2011-6-25", "8:29:00", "9:29:00", 143, 43, "4S Symposium 2014", 56
exec AddWorkshop "2013-6-26", "10:20:00", "13:20:00", 95, 29, "X-Ray Universe", 0
exec AddWorkshop "2012-7-9", "16:4:00", "18:4:00", 97, 26, "Workshop on Image Processing", 68
exec AddWorkshop "2012-1-18", "15:39:00", "18:39:00", 133, 44, "European Space Power Conference", 36
exec AddWorkshop "2011-12-16", "16:34:00", "17:34:00", 100, 31, "Euclid Spacecraft Industry Day", 49
exec AddWorkshop "2012-2-8", "15:15:00", "18:15:00", 64, 29, "Ceres Conference", 19
exec AddWorkshop "2012-1-23", "17:41:00", "18:41:00", 88, 37, "European Space Technology Harmonisation Conference", 3
exec AddWorkshop "2013-11-16", "8:5:00", "9:5:00", 51, 26, "2013 Ethical Leadership Conference: Ethics in Action", 77
exec AddWorkshop "2013-6-19", "13:30:00", "14:30:00", 55, 36, "International Semantic Web Conference		", 74
exec AddWorkshop "2012-3-4", "8:20:00", "9:20:00", 81, 37, "The Corporate Philanthropy Forum", 84
exec AddWorkshop "2011-7-26", "10:27:00", "11:27:00", 100, 36, "International Conference on Signal and Imaging Systems Engineering", 41
exec AddWorkshop "2011-3-29", "14:20:00", "15:20:00", 90, 33, "International Joint Conference on Automated Reasoning", 65
exec AddWorkshop "2011-9-23", "12:34:00", "13:34:00", 121, 32, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 14
exec AddWorkshop "2011-6-3", "11:43:00", "12:43:00", 53, 42, "International Conference on the Principles of", 3
exec AddWorkshop "2011-1-12", "13:37:00", "14:37:00", 52, 26, "Conference on Learning Theory		", 56
exec AddWorkshop "2011-4-22", "9:36:00", "10:36:00", 82, 34, "3rd International Conference on Pattern Recognition Applications and Methods", 3
exec AddWorkshop "2011-8-27", "16:21:00", "18:21:00", 146, 40, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 45
exec AddWorkshop "2012-12-15", "17:29:00", "19:29:00", 66, 22, "LPVE Land product validation and evolution", 25
exec AddWorkshop "2011-10-24", "11:43:00", "14:43:00", 64, 40, "Business4Better: The Community Partnership Movement", 15
exec AddWorkshop "2011-3-16", "9:35:00", "11:35:00", 82, 29, "EuroCOW the Calibration and Orientation Workshop", 15
exec AddWorkshop "2013-10-26", "9:28:00", "10:28:00", 98, 21, "RuleML Symposium				", 32
exec AddWorkshop "2012-8-1", "16:27:00", "19:27:00", 134, 44, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 72
exec AddWorkshop "2011-10-19", "11:49:00", "12:49:00", 121, 31, "Cause Marketing Forum", 54
exec AddWorkshop "2011-6-11", "17:48:00", "18:48:00", 71, 45, "Foundations of Genetic Algorithms		", 70
exec AddWorkshop "2013-8-27", "8:55:00", "10:55:00", 141, 33, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 72
exec AddWorkshop "2011-9-15", "9:31:00", "12:31:00", 137, 38, "ARTES 1 Final Presentation Days", 76
exec AddWorkshop "2012-1-18", "15:20:00", "16:20:00", 59, 24, "International Conference on Signal and Imaging Systems Engineering", 38
exec AddWorkshop "2013-8-22", "13:3:00", "14:3:00", 148, 29, "Leadership Strategies for Information Technology in Health Care Boston", 27
exec AddWorkshop "2013-9-12", "16:46:00", "18:46:00", 76, 40, "7 th International Conference on Bio-inspired Systems and Signal Processing", 51
exec AddWorkshop "2013-9-10", "10:34:00", "11:34:00", 76, 34, "X-Ray Universe", 3
exec AddWorkshop "2013-6-28", "19:54:00", "21:54:00", 54, 29, "International Semantic Web Conference		", 50
exec AddWorkshop "2012-7-4", "16:19:00", "17:19:00", 124, 32, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 30
exec AddWorkshop "2011-9-20", "9:1:00", "11:1:00", 134, 25, "European Space Power Conference", 1
exec AddWorkshop "2012-8-5", "10:5:00", "13:5:00", 102, 20, "Sentinel-2 for Science WS", 5
exec AddWorkshop "2011-9-21", "12:23:00", "15:23:00", 102, 35, "ARTES 1 Final Presentation Days", 27
exec AddWorkshop "2013-2-22", "10:10:00", "13:10:00", 64, 30, "Conference on Uncertainty in Artificial Intelligence", 35
exec AddWorkshop "2011-2-7", "8:8:00", "11:8:00", 84, 22, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 76
exec AddWorkshop "2011-7-25", "8:20:00", "9:20:00", 145, 25, "Euclid Spacecraft Industry Day", 57
exec AddWorkshop "2012-6-18", "10:55:00", "11:55:00", 135, 46, "European Conference on Artificial Intelligence	", 67
exec AddWorkshop "2013-7-28", "11:29:00", "14:29:00", 113, 27, "Sustainable Brands Conference", 23
exec AddWorkshop "2012-1-29", "12:11:00", "14:11:00", 145, 42, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 7
exec AddWorkshop "2013-2-12", "16:5:00", "19:5:00", 89, 44, "International Conference on the Principles of", 14
exec AddWorkshop "2012-11-11", "10:5:00", "13:5:00", 121, 30, "International Conference on Artificial Neural Networks", 15
exec AddWorkshop "2013-6-10", "10:25:00", "12:25:00", 95, 41, "Workshop on Image Processing", 65
exec AddWorkshop "2013-10-22", "9:44:00", "12:44:00", 90, 39, "4S Symposium 2014", 40
exec AddWorkshop "2011-11-25", "14:10:00", "17:10:00", 85, 23, "The BCLC CSR Conference", 83
exec AddWorkshop "2013-2-3", "17:19:00", "18:19:00", 72, 42, "International Conference on Automated Reasoning", 11
exec AddWorkshop "2013-8-19", "15:15:00", "18:15:00", 133, 34, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 19
exec AddWorkshop "2011-12-19", "19:36:00", "20:36:00", 83, 40, "ICATT", 84
exec AddWorkshop "2012-4-18", "17:23:00", "18:23:00", 113, 43, "European Space Power Conference", 19
exec AddWorkshop "2013-8-3", "19:4:00", "22:4:00", 113, 29, "International Conference on MultiMedia Modeling", 22
exec AddWorkshop "2013-1-30", "10:5:00", "12:5:00", 135, 34, "�International� Corporate Citizenship Conference", 10
exec AddWorkshop "2011-6-5", "15:42:00", "18:42:00", 104, 32, "Cause Marketing Forum", 5
exec AddWorkshop "2011-12-15", "13:49:00", "16:49:00", 126, 26, "Sustainable Brands Conference", 18
exec AddWorkshop "2011-11-25", "17:16:00", "19:16:00", 132, 20, "EuroCOW the Calibration and Orientation Workshop", 44
exec AddWorkshop "2013-6-17", "10:43:00", "13:43:00", 70, 21, "Mechanisms Final Presentation Days", 25
exec AddWorkshop "2013-1-17", "10:16:00", "13:16:00", 145, 29, "International Conference on Signal and Imaging Systems Engineering", 77
exec AddWorkshop "2012-6-1", "17:2:00", "20:2:00", 65, 29, "Sustainable Brands Conference", 25
exec AddWorkshop "2013-9-2", "19:3:00", "20:3:00", 148, 41, "Computer Analysis of Images and Patterns", 45
exec AddWorkshop "2011-4-18", "11:39:00", "13:39:00", 59, 43, "European Space Power Conference", 51
exec AddWorkshop "2013-11-8", "16:4:00", "18:4:00", 131, 48, "International Conference on Computer Vision Theory and Applications", 32
exec AddWorkshop "2013-5-24", "13:14:00", "16:14:00", 104, 27, "LPVE Land product validation and evolution", 19
exec AddWorkshop "2011-3-9", "15:51:00", "16:51:00", 59, 44, "International Conference on Automated Planning", 29
exec AddWorkshop "2012-7-19", "11:21:00", "12:21:00", 128, 48, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 80
exec AddWorkshop "2013-10-1", "19:20:00", "22:20:00", 141, 35, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 18
exec AddWorkshop "2012-7-23", "9:23:00", "11:23:00", 147, 40, "European Conference on Machine Learning", 78
exec AddWorkshop "2011-7-29", "8:54:00", "10:54:00", 123, 49, "International Semantic Web Conference		", 13
exec AddWorkshop "2013-8-6", "17:4:00", "18:4:00", 130, 47, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 62
exec AddWorkshop "2013-3-24", "9:22:00", "11:22:00", 112, 29, "Science and Challenges of Lunar Sample Return Workshop", 53
exec AddWorkshop "2012-6-9", "13:27:00", "14:27:00", 133, 43, "Mayo Clinic Presents", 20
exec AddWorkshop "2011-10-8", "12:41:00", "14:41:00", 109, 22, "3rd International Conference on Pattern Recognition Applications and Methods", 77
exec AddWorkshop "2013-12-16", "17:11:00", "20:11:00", 89, 30, "European Conference on Artificial Intelligence	", 78
exec AddWorkshop "2011-7-30", "18:45:00", "19:45:00", 59, 49, "Conference on Automated Deduction		", 39
exec AddWorkshop "2011-2-25", "19:5:00", "22:5:00", 128, 25, "Ceres Conference", 5
exec AddWorkshop "2011-11-1", "15:11:00", "17:11:00", 98, 25, "International Joint Conference on Artificial Intelligence", 37
exec AddWorkshop "2012-11-1", "18:44:00", "19:44:00", 83, 40, "European Conference on Machine Learning", 24
exec AddWorkshop "2013-2-9", "19:34:00", "22:34:00", 139, 42, "ARTES 1 Final Presentation Days", 24
exec AddWorkshop "2012-9-15", "17:51:00", "20:51:00", 57, 24, "ARTES 1 Final Presentation Days", 78
exec AddWorkshop "2011-5-27", "9:18:00", "12:18:00", 85, 40, "Leadership Strategies for Information Technology in Health Care Boston", 32
exec AddWorkshop "2012-1-10", "16:45:00", "18:45:00", 78, 24, "International Conference on the Principles of", 21
exec AddWorkshop "2011-7-23", "8:36:00", "11:36:00", 60, 25, "Workshop on Logic ", 24
exec AddWorkshop "2012-1-23", "12:11:00", "15:11:00", 81, 38, "International Semantic Web Conference		", 2
exec AddWorkshop "2011-9-2", "16:8:00", "18:8:00", 51, 32, "4th DUE Permafrost User Workshop", 63
exec AddWorkshop "2011-12-18", "14:14:00", "15:14:00", 133, 39, "European Conference on Machine Learning", 61
exec AddWorkshop "2011-9-11", "11:24:00", "14:24:00", 66, 31, "Conference on Automated Deduction		", 19
exec AddWorkshop "2012-2-26", "9:13:00", "11:13:00", 89, 23, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 19
exec AddWorkshop "2011-10-17", "16:8:00", "17:8:00", 87, 29, "European Conference on Machine Learning", 59
exec AddWorkshop "2013-4-5", "9:48:00", "12:48:00", 127, 20, "International Semantic Web Conference		", 13
exec AddWorkshop "2012-9-12", "18:18:00", "20:18:00", 77, 23, "Business4Better: The Community Partnership Movement", 26
exec AddWorkshop "2011-5-14", "16:26:00", "17:26:00", 139, 41, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 36
exec AddWorkshop "2012-12-11", "13:3:00", "15:3:00", 82, 33, "International Joint Conference on Automated Reasoning", 82
exec AddWorkshop "2011-1-3", "12:57:00", "15:57:00", 60, 29, "International Conference on Autonomous Agents and", 61
exec AddWorkshop "2011-5-14", "17:58:00", "19:58:00", 62, 42, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 41
exec AddWorkshop "2013-2-12", "12:49:00", "13:49:00", 135, 45, "Conference on Computer Vision and Pattern", 35
exec AddWorkshop "2013-6-15", "16:4:00", "17:4:00", 116, 48, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 3
exec AddWorkshop "2013-1-4", "10:18:00", "11:18:00", 102, 35, "International Joint Conference on Automated Reasoning", 36
exec AddWorkshop "2012-10-14", "14:28:00", "17:28:00", 104, 29, "Conference on Uncertainty in Artificial Intelligence", 13
exec AddWorkshop "2013-7-6", "8:57:00", "9:57:00", 127, 45, "The 12th annual Responsible Business Summit", 11
exec AddWorkshop "2013-4-24", "18:5:00", "19:5:00", 137, 22, "International Conference on Logic for Programming", 34
exec AddWorkshop "2012-12-27", "10:8:00", "13:8:00", 113, 29, "British	Machine	Vision	Conference		", 44
exec AddWorkshop "2012-11-12", "11:16:00", "13:16:00", 72, 44, "�International� Corporate Citizenship Conference", 71
exec AddWorkshop "2012-3-4", "17:28:00", "19:28:00", 119, 30, "Workshop on Image Processing", 77
exec AddWorkshop "2013-1-28", "8:35:00", "10:35:00", 105, 27, "�International� Corporate Citizenship Conference", 61
exec AddWorkshop "2013-10-1", "19:3:00", "21:3:00", 95, 37, "Leadership Strategies for Information Technology in Health Care Boston", 39
exec AddWorkshop "2013-9-9", "12:3:00", "15:3:00", 133, 23, "Conference on Learning Theory		", 28
exec AddWorkshop "2011-3-4", "14:31:00", "17:31:00", 57, 43, "European Conference on Machine Learning", 11
exec AddWorkshop "2012-9-10", "13:37:00", "14:37:00", 104, 25, "Microwave Workshop", 24
exec AddWorkshop "2012-5-8", "12:22:00", "13:22:00", 133, 44, "The 12th annual Responsible Business Summit", 73
exec AddWorkshop "2011-5-20", "14:12:00", "15:12:00", 89, 24, "International Conference on Artificial Neural Networks", 13
exec AddWorkshop "2011-4-25", "18:31:00", "21:31:00", 90, 40, "Mayo Clinic Presents", 68
exec AddWorkshop "2012-4-26", "15:41:00", "18:41:00", 106, 46, "RuleML Symposium		", 58
exec AddWorkshop "2011-12-23", "17:12:00", "19:12:00", 65, 31, "�International� Corporate Citizenship Conference", 14
exec AddWorkshop "2013-1-5", "17:36:00", "19:36:00", 109, 20, "The 12th annual Responsible Business Summit", 56
exec AddWorkshop "2012-4-22", "14:57:00", "16:57:00", 92, 41, "British	Machine	Vision	Conference		", 49
exec AddWorkshop "2011-10-14", "15:6:00", "16:6:00", 145, 20, "International Conference on the Principles of", 53
exec AddWorkshop "2012-12-25", "10:13:00", "13:13:00", 54, 39, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 1
exec AddWorkshop "2013-3-22", "15:23:00", "16:23:00", 137, 29, "Ceres Conference", 29
exec AddWorkshop "2011-5-2", "13:31:00", "16:31:00", 142, 22, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 64
exec AddWorkshop "2012-9-27", "8:32:00", "10:32:00", 98, 21, "Leadership Strategies for Information Technology in Health Care Boston", 33
exec AddWorkshop "2013-11-28", "9:11:00", "12:11:00", 118, 22, "International Conference on MultiMedia Modeling", 73
exec AddWorkshop "2013-8-18", "11:48:00", "12:48:00", 134, 49, "Workshop on Image Processing", 65
exec AddWorkshop "2011-5-1", "18:49:00", "21:49:00", 107, 45, "RuleML Symposium				", 54
exec AddWorkshop "2012-10-3", "8:36:00", "11:36:00", 124, 20, "KU Leuven CSR Symposium", 58
exec AddWorkshop "2012-4-9", "16:34:00", "19:34:00", 59, 27, "International Conference on Computer Vision", 9
exec AddWorkshop "2011-6-14", "15:17:00", "18:17:00", 114, 36, "Conference on Computer Vision and Pattern", 27
exec AddWorkshop "2011-8-23", "17:57:00", "18:57:00", 147, 21, "Annual Interdisciplinary Conference", 78
exec AddWorkshop "2013-5-22", "15:57:00", "16:57:00", 137, 49, "Sentinel-2 for Science WS", 27
exec AddWorkshop "2013-8-28", "12:49:00", "14:49:00", 50, 30, "GNC 2014", 61
exec AddWorkshop "2013-6-12", "10:28:00", "12:28:00", 119, 41, "48th ESLAB Symposium", 82
exec AddWorkshop "2012-4-23", "18:20:00", "21:20:00", 53, 47, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 39
exec AddWorkshop "2011-4-12", "18:36:00", "21:36:00", 66, 32, "International Conference on Autonomous Agents and", 82
exec AddWorkshop "2013-9-17", "11:14:00", "14:14:00", 119, 35, "48th ESLAB Symposium", 54
exec AddWorkshop "2011-7-2", "19:3:00", "22:3:00", 109, 23, "International Conference on Signal and Imaging Systems Engineering", 35
exec AddWorkshop "2013-4-4", "16:3:00", "19:3:00", 147, 41, "International Semantic Web Conference		", 80
exec AddWorkshop "2013-4-5", "17:33:00", "19:33:00", 92, 22, "Photonics West", 25
exec AddWorkshop "2011-2-5", "12:1:00", "13:1:00", 68, 29, "Microwave Workshop", 65
exec AddWorkshop "2012-1-8", "16:33:00", "19:33:00", 59, 48, "X-Ray Universe", 20
exec AddWorkshop "2013-6-27", "12:7:00", "14:7:00", 81, 48, "Net Impact Conference", 9
exec AddWorkshop "2011-3-25", "17:11:00", "18:11:00", 126, 31, "International Conference on Autonomous Agents and", 4
exec AddWorkshop "2012-12-26", "10:26:00", "11:26:00", 78, 34, "Conference on Computer Vision and Pattern", 30
exec AddWorkshop "2013-7-5", "15:7:00", "18:7:00", 74, 41, "7 th International Conference on Bio-inspired Systems and Signal Processing", 73
exec AddWorkshop "2012-1-22", "11:36:00", "12:36:00", 90, 41, "Cause Marketing Forum", 70
exec AddWorkshop "2012-7-22", "19:25:00", "20:25:00", 108, 28, "International Conference on Pattern Recognition", 4
exec AddWorkshop "2012-11-9", "12:58:00", "14:58:00", 122, 32, "Corporate Community Involvement Conference", 65
exec AddWorkshop "2013-2-24", "12:20:00", "13:20:00", 51, 44, "International Semantic Web Conference		", 53
exec AddWorkshop "2013-8-13", "16:50:00", "19:50:00", 81, 35, "The 2nd International CSR Communication Conference", 4
exec AddWorkshop "2012-10-3", "15:7:00", "18:7:00", 79, 49, "European Conference on Artificial Intelligence	", 53
exec AddWorkshop "2013-2-21", "12:34:00", "14:34:00", 57, 21, "Client Summit", 64
exec AddWorkshop "2012-4-11", "9:57:00", "12:57:00", 79, 27, "International Conference on Autonomous Agents and", 55
exec AddWorkshop "2013-9-25", "8:19:00", "11:19:00", 85, 20, "Sustainable Brands Conference", 35
exec AddWorkshop "2011-4-3", "10:33:00", "12:33:00", 134, 42, "Foundations of Genetic Algorithms		", 48
exec AddWorkshop "2013-11-4", "11:37:00", "12:37:00", 100, 34, "GNC 2014", 13
exec AddWorkshop "2011-10-27", "9:30:00", "11:30:00", 130, 30, "ICATT", 60
exec AddWorkshop "2011-10-6", "15:50:00", "18:50:00", 102, 36, "Life in Space for Life on Earth Symposium", 0
exec AddWorkshop "2013-4-4", "12:60:00", "14:60:00", 86, 26, "Life in Space for Life on Earth Symposium", 75
exec AddWorkshop "2012-4-30", "11:37:00", "12:37:00", 145, 34, "Conference on Uncertainty in Artificial Intelligence", 83
exec AddWorkshop "2011-10-7", "18:59:00", "21:59:00", 113, 36, "Euclid Spacecraft Industry Day", 45
exec AddWorkshop "2011-8-15", "14:16:00", "16:16:00", 106, 37, "International Conference on the Principles of", 26
exec AddWorkshop "2012-3-23", "15:51:00", "16:51:00", 115, 22, "Corporate Community Involvement Conference", 7
exec AddWorkshop "2012-1-4", "9:14:00", "11:14:00", 92, 35, "Annual Convention of The Society", 42
exec AddWorkshop "2013-5-10", "16:5:00", "19:5:00", 63, 23, "Life in Space for Life on Earth Symposium", 45
exec AddWorkshop "2013-7-12", "19:22:00", "20:22:00", 85, 47, "The Corporate Philanthropy Forum", 36
exec AddWorkshop "2011-11-23", "14:3:00", "15:3:00", 121, 26, "The Corporate Philanthropy Forum", 36
exec AddWorkshop "2011-11-20", "12:2:00", "14:2:00", 131, 28, "Corporate Community Involvement Conference", 21
exec AddWorkshop "2011-2-22", "15:42:00", "18:42:00", 145, 49, "RuleML Symposium				", 81
exec AddWorkshop "2013-3-25", "11:4:00", "12:4:00", 77, 44, "Annual Convention of The Society", 70
exec AddWorkshop "2011-5-25", "19:12:00", "20:12:00", 96, 43, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 41
exec AddWorkshop "2013-10-12", "16:50:00", "19:50:00", 87, 45, "International Semantic Web Conference		", 37
exec AddWorkshop "2012-9-7", "12:32:00", "14:32:00", 142, 31, "Mayo Clinic Presents", 44
exec AddWorkshop "2013-2-4", "12:57:00", "15:57:00", 59, 30, "Conference on Computer Vision and Pattern", 82
exec AddWorkshop "2013-9-15", "10:38:00", "13:38:00", 68, 30, "Ceres Conference", 24
exec AddWorkshop "2011-12-22", "19:36:00", "21:36:00", 65, 31, "Sustainable Brands Conference", 28
exec AddWorkshop "2012-1-23", "13:42:00", "15:42:00", 84, 39, "Net Impact Conference", 80
exec AddWorkshop "2012-7-21", "12:52:00", "13:52:00", 112, 31, "The 12th annual Responsible Business Summit", 35
exec AddWorkshop "2013-12-3", "11:12:00", "14:12:00", 142, 27, "ICATT", 22
exec AddWorkshop "2012-4-3", "19:47:00", "21:47:00", 93, 36, "Annual Interdisciplinary Conference", 7
exec AddWorkshop "2012-7-15", "8:13:00", "10:13:00", 53, 31, "British	Machine	Vision	Conference		", 79
exec AddWorkshop "2011-1-18", "16:52:00", "19:52:00", 136, 30, "X-Ray Universe", 30
exec AddWorkshop "2011-9-15", "8:5:00", "10:5:00", 53, 45, "48th ESLAB Symposium", 22
exec AddWorkshop "2011-12-22", "10:14:00", "11:14:00", 54, 23, "RuleML Symposium				", 81
exec AddWorkshop "2011-1-30", "17:5:00", "19:5:00", 110, 20, "LPVE Land product validation and evolution", 52
exec AddWorkshop "2012-10-20", "14:46:00", "15:46:00", 71, 47, "3rd International Conference on Pattern Recognition Applications and Methods", 7
exec AddWorkshop "2013-9-2", "8:26:00", "11:26:00", 74, 34, "International Conference on MultiMedia Modeling", 36
exec AddWorkshop "2011-4-25", "13:5:00", "15:5:00", 148, 35, "GNC 2014", 44
exec AddWorkshop "2013-7-3", "9:5:00", "10:5:00", 63, 21, "Conference on Computer Vision and Pattern", 71
exec AddWorkshop "2012-5-30", "8:50:00", "9:50:00", 107, 44, "4th DUE Permafrost User Workshop", 75
exec AddWorkshop "2013-9-12", "17:33:00", "20:33:00", 64, 20, "International Semantic Web Conference		", 82
exec AddWorkshop "2012-9-9", "10:50:00", "12:50:00", 125, 21, "Conference on Automated Deduction		", 65
exec AddWorkshop "2012-1-5", "19:47:00", "20:47:00", 141, 43, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 47
exec AddWorkshop "2013-8-22", "18:17:00", "20:17:00", 91, 33, "RuleML Symposium				", 33
exec AddWorkshop "2012-8-14", "12:25:00", "14:25:00", 129, 26, "4S Symposium 2014", 84
exec AddWorkshop "2011-2-30", "10:21:00", "13:21:00", 101, 36, "4th DUE Permafrost User Workshop", 58
exec AddWorkshop "2013-12-12", "14:29:00", "15:29:00", 59, 41, "7 th International Conference on Bio-inspired Systems and Signal Processing", 40
exec AddWorkshop "2012-7-22", "15:38:00", "18:38:00", 103, 34, "LPVE Land product validation and evolution", 71
exec AddWorkshop "2012-10-4", "12:51:00", "15:51:00", 59, 21, "International Conference on Logic for Programming", 57
exec AddWorkshop "2012-6-6", "19:9:00", "20:9:00", 86, 44, "Conference on Automated Deduction		", 62
exec AddWorkshop "2013-8-12", "9:33:00", "11:33:00", 122, 38, "International Conference on Computer Vision", 66
exec AddWorkshop "2013-7-16", "9:20:00", "12:20:00", 50, 29, "Euclid Spacecraft Industry Day", 69
exec AddWorkshop "2011-10-24", "14:2:00", "15:2:00", 88, 38, "European Conference on Artificial Intelligence	", 32
exec AddWorkshop "2013-4-24", "15:17:00", "18:17:00", 120, 27, "International Conference on Computer Vision", 27
exec AddWorkshop "2013-8-27", "10:10:00", "11:10:00", 137, 47, "International Conference on MultiMedia Modeling", 19
exec AddWorkshop "2012-7-22", "9:45:00", "12:45:00", 131, 44, "International Conference on Automated Planning", 76
exec AddWorkshop "2011-1-2", "16:45:00", "19:45:00", 86, 30, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 23
exec AddWorkshop "2013-10-20", "15:60:00", "17:60:00", 126, 37, "ICATT", 10
exec AddWorkshop "2011-6-1", "16:25:00", "18:25:00", 54, 37, "Conference on Learning Theory		", 58
exec AddWorkshop "2011-3-2", "19:23:00", "20:23:00", 60, 49, "International Conference on Computer Vision", 21
exec AddWorkshop "2011-9-11", "17:16:00", "20:16:00", 76, 31, "48th ESLAB Symposium", 28
exec AddWorkshop "2013-3-8", "13:5:00", "15:5:00", 111, 48, "The BCLC CSR Conference", 37
exec AddWorkshop "2011-7-6", "10:42:00", "11:42:00", 131, 39, "International Joint Conference on Automated Reasoning", 51
exec AddWorkshop "2012-7-20", "12:21:00", "13:21:00", 58, 41, "Science and Challenges of Lunar Sample Return Workshop", 11
exec AddWorkshop "2012-7-22", "18:11:00", "20:11:00", 83, 28, "Sentinel-2 for Science WS", 23
exec AddWorkshop "2011-4-16", "15:28:00", "18:28:00", 138, 33, "48th ESLAB Symposium", 76
exec AddWorkshop "2012-4-30", "9:12:00", "11:12:00", 97, 46, "International Conference on Computer Vision Theory and Applications", 82
exec AddWorkshop "2011-1-29", "15:11:00", "16:11:00", 131, 49, "Business4Better: The Community Partnership Movement", 75
exec AddWorkshop "2012-2-7", "16:14:00", "17:14:00", 50, 28, "European Space Power Conference", 36
exec AddWorkshop "2011-7-11", "10:3:00", "12:3:00", 87, 31, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 17
exec AddWorkshop "2012-4-16", "17:24:00", "19:24:00", 80, 42, "Conference on Automated Deduction		", 51
exec AddWorkshop "2013-1-25", "19:11:00", "20:11:00", 118, 35, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 73
exec AddWorkshop "2011-9-3", "8:42:00", "10:42:00", 107, 32, "Sentinel-2 for Science WS", 54
exec AddWorkshop "2012-3-13", "19:45:00", "20:45:00", 119, 44, "RuleML Symposium		", 59
exec AddWorkshop "2011-8-30", "17:46:00", "18:46:00", 94, 31, "The 12th annual Responsible Business Summit", 3
exec AddWorkshop "2013-6-20", "9:29:00", "12:29:00", 67, 27, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 18
exec AddWorkshop "2012-8-12", "10:51:00", "13:51:00", 71, 21, "Science and Challenges of Lunar Sample Return Workshop", 79
exec AddWorkshop "2012-1-12", "14:47:00", "16:47:00", 64, 28, "Photonics West", 80
exec AddWorkshop "2011-1-12", "18:42:00", "20:42:00", 87, 48, "European Space Power Conference", 15
exec AddWorkshop "2013-4-20", "11:48:00", "12:48:00", 68, 38, "European Space Power Conference", 17
exec AddWorkshop "2011-4-14", "15:43:00", "18:43:00", 58, 23, "European Conference on Machine Learning", 26
exec AddWorkshop "2011-3-1", "14:22:00", "15:22:00", 134, 26, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 21
exec AddWorkshop "2012-2-17", "13:37:00", "15:37:00", 64, 45, "GNC 2014", 1
exec AddWorkshop "2011-12-22", "14:26:00", "16:26:00", 93, 47, "Mayo Clinic Presents", 77
exec AddWorkshop "2013-7-29", "11:58:00", "12:58:00", 60, 37, "ARTES 1 Final Presentation Days", 25
exec AddWorkshop "2011-3-13", "18:22:00", "19:22:00", 84, 45, "International Conference on Computer Vision Theory and Applications", 45
exec AddWorkshop "2011-7-9", "10:45:00", "11:45:00", 117, 40, "International Semantic Web Conference		", 18
exec AddWorkshop "2011-6-25", "16:28:00", "18:28:00", 53, 35, "European Space Technology Harmonisation Conference", 33
exec AddWorkshop "2013-1-24", "11:24:00", "12:24:00", 85, 20, "3rd International Conference on Pattern Recognition Applications and Methods", 8
exec AddWorkshop "2011-8-11", "15:34:00", "18:34:00", 73, 44, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 68
exec AddWorkshop "2012-2-19", "9:25:00", "12:25:00", 72, 48, "The Sixth International Conferences on Advances in Multimedia", 54
exec AddWorkshop "2013-3-11", "18:15:00", "21:15:00", 91, 29, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 23
exec AddWorkshop "2011-1-20", "12:20:00", "15:20:00", 62, 39, "European Conference on Machine Learning", 47
exec AddWorkshop "2013-10-28", "15:54:00", "16:54:00", 51, 46, "Computer Analysis of Images and Patterns", 71
exec AddWorkshop "2011-6-2", "8:27:00", "11:27:00", 89, 33, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 75
exec AddWorkshop "2011-11-26", "16:29:00", "17:29:00", 102, 38, "European Navigation Conference 2014 (ENC-GNSS 2014)", 67
exec AddWorkshop "2012-4-30", "10:25:00", "12:25:00", 106, 37, "International Conference on Signal and Imaging Systems Engineering", 53
exec AddWorkshop "2013-5-7", "19:36:00", "22:36:00", 51, 38, "European Conference on Computer Vision", 49
exec AddWorkshop "2012-8-1", "11:51:00", "12:51:00", 121, 33, "International Conference on Logic for Programming", 57
exec AddWorkshop "2013-1-8", "8:50:00", "11:50:00", 117, 27, "International Joint Conference on Automated Reasoning", 60
exec AddWorkshop "2011-2-22", "18:24:00", "20:24:00", 78, 33, "European Navigation Conference 2014 (ENC-GNSS 2014)", 67
exec AddWorkshop "2011-10-12", "14:3:00", "16:3:00", 59, 25, "European Conference on Machine Learning", 57
exec AddWorkshop "2012-4-23", "11:7:00", "14:7:00", 78, 37, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 15
exec AddWorkshop "2011-10-9", "14:15:00", "17:15:00", 119, 23, "Microwave Workshop", 68
exec AddWorkshop "2013-12-14", "12:43:00", "15:43:00", 141, 32, "GNC 2014", 68
exec AddWorkshop "2013-2-30", "9:35:00", "10:35:00", 103, 23, "ICATT", 17
exec AddWorkshop "2012-2-30", "8:46:00", "11:46:00", 146, 47, "The 12th annual Responsible Business Summit", 76
exec AddWorkshop "2012-5-23", "11:14:00", "14:14:00", 144, 37, "Sustainable Brands Conference", 10
exec AddWorkshop "2012-12-28", "14:49:00", "16:49:00", 53, 29, "Business4Better: The Community Partnership Movement", 4
exec AddWorkshop "2013-9-11", "9:3:00", "12:3:00", 149, 25, "Sustainable Brands Conference", 17
exec AddWorkshop "2012-10-23", "8:39:00", "9:39:00", 72, 48, "RuleML Symposium				", 24
exec AddWorkshop "2011-2-24", "18:20:00", "21:20:00", 91, 27, "International Conference on MultiMedia Modeling", 27
exec AddWorkshop "2012-5-29", "15:3:00", "16:3:00", 108, 21, "International Semantic Web Conference		", 17
exec AddWorkshop "2011-8-29", "8:25:00", "9:25:00", 145, 22, "European Space Technology Harmonisation Conference", 56
exec AddWorkshop "2013-2-13", "13:56:00", "14:56:00", 145, 23, "European Conference on Machine Learning", 43
exec AddWorkshop "2012-10-29", "18:26:00", "20:26:00", 82, 48, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 72
exec AddWorkshop "2011-10-27", "13:37:00", "16:37:00", 84, 22, "Science and Challenges of Lunar Sample Return Workshop", 28
exec AddWorkshop "2011-12-5", "15:54:00", "18:54:00", 145, 42, "4S Symposium 2014", 80
exec AddWorkshop "2011-12-11", "18:37:00", "21:37:00", 104, 49, "Client Summit", 43
exec AddWorkshop "2011-5-28", "11:5:00", "14:5:00", 133, 41, "LPVE Land product validation and evolution", 13
exec AddWorkshop "2013-4-30", "19:31:00", "20:31:00", 52, 36, "Photonics West", 3
exec AddWorkshop "2012-8-4", "17:19:00", "18:19:00", 74, 40, "British	Machine	Vision	Conference		", 58
exec AddWorkshop "2011-6-6", "11:16:00", "14:16:00", 75, 38, "Computer Analysis of Images and Patterns", 49
exec AddWorkshop "2013-5-15", "8:34:00", "10:34:00", 107, 49, "International Semantic Web Conference		", 15
exec AddWorkshop "2011-5-13", "12:37:00", "13:37:00", 72, 30, "International Conference on MultiMedia Modeling", 79
exec AddWorkshop "2011-7-29", "15:9:00", "16:9:00", 145, 37, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 10
exec AddWorkshop "2012-5-29", "8:40:00", "11:40:00", 112, 39, "Mechanisms Final Presentation Days", 66
exec AddWorkshop "2012-8-18", "14:22:00", "16:22:00", 68, 37, "International Conference on Pattern Recognition", 83
exec AddWorkshop "2013-5-21", "15:51:00", "18:51:00", 137, 23, "KU Leuven CSR Symposium", 7
exec AddWorkshop "2012-8-7", "11:38:00", "12:38:00", 124, 38, "Computer Analysis of Images and Patterns", 65
exec AddWorkshop "2013-2-9", "10:55:00", "12:55:00", 116, 27, "Mayo Clinic Presents", 40
exec AddWorkshop "2013-10-16", "12:55:00", "13:55:00", 65, 46, "International Conference on Signal and Imaging Systems Engineering", 56
exec AddWorkshop "2012-1-10", "8:57:00", "9:57:00", 54, 49, "International Conference on Pattern Recognition", 38
exec AddWorkshop "2011-3-18", "18:10:00", "20:10:00", 122, 29, "Corporate Community Involvement Conference", 56
exec AddWorkshop "2012-3-24", "19:58:00", "22:58:00", 71, 26, "ICATT", 45
exec AddWorkshop "2013-12-24", "9:41:00", "10:41:00", 143, 43, "International Conference on Automated Planning", 22
exec AddWorkshop "2013-3-3", "13:48:00", "15:48:00", 148, 44, "European Conference on Computer Vision", 55
exec AddWorkshop "2012-3-16", "17:59:00", "20:59:00", 66, 29, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 0
exec AddWorkshop "2013-7-29", "9:29:00", "11:29:00", 63, 28, "International Joint Conference on Automated Reasoning", 67
exec AddWorkshop "2012-6-25", "13:26:00", "15:26:00", 56, 37, "X-Ray Universe", 12
exec AddWorkshop "2013-3-2", "10:39:00", "13:39:00", 104, 24, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 64
exec AddWorkshop "2013-7-16", "11:53:00", "13:53:00", 112, 32, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 54
exec AddWorkshop "2012-6-22", "14:29:00", "16:29:00", 124, 25, "Sentinel-2 for Science WS", 13
exec AddWorkshop "2011-10-2", "12:19:00", "14:19:00", 132, 24, "European Space Power Conference", 17
exec AddWorkshop "2012-9-19", "8:36:00", "10:36:00", 120, 27, "Workshop on Image Processing", 82
exec AddWorkshop "2012-7-27", "17:59:00", "20:59:00", 138, 44, "The Sixth International Conferences on Advances in Multimedia", 53
exec AddWorkshop "2011-6-23", "19:56:00", "21:56:00", 91, 48, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 72
exec AddWorkshop "2012-10-1", "17:57:00", "18:57:00", 109, 25, "Life in Space for Life on Earth Symposium", 13
exec AddWorkshop "2013-5-14", "8:36:00", "11:36:00", 85, 25, "4th DUE Permafrost User Workshop", 69
exec AddWorkshop "2013-1-19", "16:43:00", "19:43:00", 53, 35, "ARTES 1 Final Presentation Days", 55
exec AddWorkshop "2012-1-27", "12:52:00", "15:52:00", 136, 33, "Leadership Strategies for Information Technology in Health Care Boston", 82
exec AddWorkshop "2013-2-12", "17:32:00", "18:32:00", 96, 38, "Global Conference on Sustainability and Reporting", 13
exec AddWorkshop "2013-6-11", "13:43:00", "15:43:00", 123, 30, "Ceres Conference", 55
exec AddWorkshop "2011-6-11", "11:10:00", "14:10:00", 142, 32, "7 th International Conference on Bio-inspired Systems and Signal Processing", 74
exec AddWorkshop "2013-5-18", "10:16:00", "13:16:00", 50, 43, "ICATT", 46
exec AddWorkshop "2011-8-13", "18:11:00", "20:11:00", 134, 37, "European Conference on Machine Learning", 76
exec AddWorkshop "2012-11-22", "13:41:00", "14:41:00", 91, 40, "International Conference on Signal and Imaging Systems Engineering", 20
exec AddWorkshop "2012-1-2", "15:58:00", "18:58:00", 138, 27, "International Conference on Autonomous Agents and", 68
exec AddWorkshop "2011-6-17", "10:9:00", "13:9:00", 66, 37, "International Conference on Artificial Neural Networks", 5
exec AddWorkshop "2012-2-26", "12:14:00", "15:14:00", 80, 26, "International Conference on Signal and Imaging Systems Engineering", 53
exec AddWorkshop "2013-5-30", "9:38:00", "10:38:00", 76, 24, "Science and Challenges of Lunar Sample Return Workshop", 54
exec AddWorkshop "2011-11-7", "14:11:00", "15:11:00", 130, 38, "Corporate Community Involvement Conference", 40
exec AddWorkshop "2011-3-29", "18:55:00", "21:55:00", 64, 25, "International Conference on the Principles of", 27
exec AddWorkshop "2012-4-14", "15:59:00", "16:59:00", 124, 24, "EuroCOW the Calibration and Orientation Workshop", 52
exec AddWorkshop "2012-11-26", "12:3:00", "15:3:00", 63, 34, "Workshop on Logic ", 55
exec AddWorkshop "2011-6-20", "12:47:00", "14:47:00", 51, 47, "Cause Marketing Forum", 56
exec AddWorkshop "2011-2-24", "11:46:00", "14:46:00", 57, 29, "KU Leuven CSR Symposium", 30
exec AddWorkshop "2013-7-10", "10:34:00", "13:34:00", 105, 34, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 9
exec AddWorkshop "2011-9-17", "8:22:00", "11:22:00", 124, 34, "ICATT", 52
exec AddWorkshop "2013-9-26", "9:17:00", "11:17:00", 79, 49, "RuleML Symposium				", 45
exec AddWorkshop "2012-11-7", "18:10:00", "19:10:00", 88, 42, "4S Symposium 2014", 55
exec AddWorkshop "2011-7-3", "14:20:00", "16:20:00", 82, 40, "The Sixth International Conferences on Advances in Multimedia", 8
exec AddWorkshop "2011-1-14", "9:54:00", "10:54:00", 98, 23, "Annual Convention of The Society", 15
exec AddWorkshop "2011-9-11", "16:41:00", "17:41:00", 58, 40, "ARTES 1 Final Presentation Days", 13
exec AddWorkshop "2013-1-29", "10:6:00", "12:6:00", 69, 44, "GNC 2014", 39
exec AddWorkshop "2013-10-3", "13:4:00", "14:4:00", 71, 26, "4S Symposium 2014", 54
exec AddWorkshop "2012-1-13", "19:53:00", "22:53:00", 83, 23, "48th ESLAB Symposium", 41
exec AddWorkshop "2012-12-3", "17:46:00", "20:46:00", 82, 21, "International Conference on Computer Vision Theory and Applications", 2
exec AddWorkshop "2011-5-8", "18:24:00", "21:24:00", 141, 44, "Annual Interdisciplinary Conference", 77
exec AddWorkshop "2012-7-29", "16:45:00", "17:45:00", 83, 24, "RuleML Symposium				", 5
exec AddWorkshop "2011-3-24", "12:44:00", "14:44:00", 102, 43, "Science and Challenges of Lunar Sample Return Workshop", 71
exec AddWorkshop "2011-12-19", "9:39:00", "11:39:00", 75, 40, "Net Impact Conference", 0
exec AddWorkshop "2011-5-4", "19:44:00", "21:44:00", 104, 44, "ICATT", 10
exec AddWorkshop "2012-4-27", "14:34:00", "15:34:00", 81, 29, "Leadership Strategies for Information Technology in Health Care Boston", 53
exec AddWorkshop "2012-7-5", "12:55:00", "15:55:00", 123, 48, "LPVE Land product validation and evolution", 21
exec AddWorkshop "2011-12-12", "13:8:00", "16:8:00", 140, 37, "Client Summit", 57
exec AddWorkshop "2011-3-15", "18:53:00", "21:53:00", 133, 22, "GNC 2014", 70
exec AddWorkshop "2013-8-1", "14:10:00", "15:10:00", 108, 29, "Sentinel-2 for Science WS", 52
exec AddWorkshop "2012-9-9", "12:46:00", "15:46:00", 86, 41, "�International� Corporate Citizenship Conference", 42
exec AddWorkshop "2013-4-23", "10:34:00", "12:34:00", 93, 24, "Photonics West", 17
exec AddWorkshop "2012-3-10", "8:23:00", "9:23:00", 138, 33, "KU Leuven CSR Symposium", 60
exec AddWorkshop "2013-2-3", "13:4:00", "14:4:00", 70, 39, "3rd International Conference on Pattern Recognition Applications and Methods", 31
exec AddWorkshop "2011-5-1", "12:41:00", "15:41:00", 108, 31, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 1
exec AddWorkshop "2011-4-1", "16:6:00", "18:6:00", 130, 29, "ICATT", 40
exec AddWorkshop "2013-12-6", "8:28:00", "9:28:00", 64, 48, "Science and Challenges of Lunar Sample Return Workshop", 40
exec AddWorkshop "2011-12-8", "17:4:00", "18:4:00", 128, 42, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 18
exec AddWorkshop "2011-9-24", "17:9:00", "18:9:00", 136, 33, "International Conference on Autonomous Agents and", 71
exec AddWorkshop "2011-5-29", "10:22:00", "13:22:00", 70, 25, "International Conference on Computer Vision Theory and Applications", 34
exec AddWorkshop "2013-4-13", "17:22:00", "18:22:00", 83, 40, "International Conference on Signal and Imaging Systems Engineering", 6
exec AddWorkshop "2011-4-27", "19:32:00", "20:32:00", 137, 37, "Client Summit", 37
exec AddWorkshop "2011-10-13", "11:44:00", "12:44:00", 115, 28, "Workshop on Logic ", 31
exec AddWorkshop "2011-8-1", "11:9:00", "12:9:00", 134, 33, "International Conference on MultiMedia Modeling", 43
exec AddWorkshop "2012-3-5", "9:14:00", "12:14:00", 117, 49, "The Corporate Philanthropy Forum", 53
exec AddWorkshop "2013-10-1", "14:33:00", "17:33:00", 50, 40, "Annual Convention of The Society", 47
exec AddWorkshop "2013-3-13", "8:45:00", "9:45:00", 133, 21, "International Joint Conference on Artificial Intelligence", 19
exec AddWorkshop "2011-1-16", "17:12:00", "20:12:00", 74, 29, "Workshop on Image Processing", 79
exec AddWorkshop "2012-8-14", "11:24:00", "13:24:00", 51, 20, "International Conference on Signal and Imaging Systems Engineering", 3
exec AddWorkshop "2011-12-23", "14:15:00", "17:15:00", 69, 33, "2nd International Conference on Photonics, Optics and Laser Technology", 55
exec AddWorkshop "2013-8-9", "11:22:00", "14:22:00", 75, 44, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 30
exec AddWorkshop "2012-5-21", "9:36:00", "12:36:00", 127, 46, "International Conference on Signal and Imaging Systems Engineering", 42
exec AddWorkshop "2013-8-28", "18:37:00", "19:37:00", 87, 28, "Client Summit", 64
exec AddWorkshop "2012-5-15", "14:36:00", "16:36:00", 99, 31, "Mayo Clinic Presents", 5
exec AddWorkshop "2011-12-8", "10:36:00", "12:36:00", 59, 40, "RuleML Symposium				", 61
exec AddWorkshop "2013-9-17", "8:34:00", "10:34:00", 128, 31, "Mayo Clinic Presents", 10
exec AddWorkshop "2011-1-3", "12:36:00", "15:36:00", 112, 42, "International Conference on Automated Planning", 28
exec AddWorkshop "2012-12-11", "10:13:00", "13:13:00", 80, 25, "International Conference on Computer Vision", 44
exec AddWorkshop "2013-8-12", "13:59:00", "16:59:00", 54, 46, "International Joint Conference on Automated Reasoning", 75
exec AddWorkshop "2011-12-27", "13:15:00", "14:15:00", 52, 39, "Business4Better: The Community Partnership Movement", 58
exec AddWorkshop "2013-4-16", "19:21:00", "22:21:00", 144, 26, "Mechanisms Final Presentation Days", 81
exec AddWorkshop "2011-10-10", "14:48:00", "15:48:00", 83, 31, "International Conference on Pattern Recognition", 23
exec AddWorkshop "2012-6-7", "8:35:00", "9:35:00", 113, 32, "Workshop on Image Processing", 84
exec AddWorkshop "2012-5-17", "17:4:00", "18:4:00", 54, 27, "Microwave Workshop", 71
exec AddWorkshop "2013-3-16", "12:37:00", "14:37:00", 129, 49, "Net Impact Conference", 0
exec AddWorkshop "2011-2-16", "9:12:00", "11:12:00", 95, 20, "Corporate Community Involvement Conference", 9
exec AddWorkshop "2013-11-30", "13:16:00", "15:16:00", 131, 21, "Conference on Learning Theory		", 61
exec AddWorkshop "2011-6-20", "13:43:00", "16:43:00", 137, 23, "Conference on Learning Theory		", 65
exec AddWorkshop "2012-12-5", "11:12:00", "12:12:00", 133, 26, "The 12th annual Responsible Business Summit", 74
exec AddWorkshop "2013-12-7", "19:29:00", "22:29:00", 131, 24, "GNC 2014", 69
exec AddWorkshop "2011-2-16", "19:7:00", "21:7:00", 53, 41, "7 th International Conference on Bio-inspired Systems and Signal Processing", 17
exec AddWorkshop "2011-6-21", "19:19:00", "21:19:00", 56, 31, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 74
exec AddWorkshop "2013-9-5", "12:26:00", "14:26:00", 114, 29, "Euclid Spacecraft Industry Day", 76
exec AddWorkshop "2012-10-30", "11:57:00", "12:57:00", 87, 36, "Conference on Automated Deduction		", 6
exec AddWorkshop "2011-6-15", "16:10:00", "17:10:00", 51, 31, "Euclid Spacecraft Industry Day", 29
exec AddWorkshop "2012-1-3", "10:56:00", "12:56:00", 135, 37, "Business4Better: The Community Partnership Movement", 31
exec AddWorkshop "2012-10-23", "17:55:00", "19:55:00", 62, 27, "Ceres Conference", 53
exec AddWorkshop "2011-2-14", "14:43:00", "15:43:00", 77, 43, "Computer Analysis of Images and Patterns", 70
exec AddWorkshop "2011-4-2", "12:52:00", "15:52:00", 85, 26, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 46
exec AddWorkshop "2011-2-2", "17:49:00", "19:49:00", 96, 21, "Global Conference on Sustainability and Reporting", 33
exec AddWorkshop "2012-2-6", "13:34:00", "14:34:00", 96, 25, "Workshop on Image Processing", 3
exec AddWorkshop "2013-12-14", "18:44:00", "20:44:00", 105, 49, "4th DUE Permafrost User Workshop", 3
exec AddWorkshop "2013-6-24", "12:43:00", "13:43:00", 86, 49, "Sentinel-2 for Science WS", 38
exec AddWorkshop "2013-12-16", "12:13:00", "15:13:00", 143, 26, "Annual Convention of The Society", 37
exec AddWorkshop "2011-6-29", "19:37:00", "22:37:00", 70, 21, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 65
exec AddWorkshop "2012-1-8", "11:25:00", "14:25:00", 97, 34, "3rd International Conference on Pattern Recognition Applications and Methods", 77
exec AddWorkshop "2012-5-29", "17:1:00", "20:1:00", 128, 29, "Asian Conference on Computer Vision", 48
exec AddWorkshop "2012-10-8", "11:14:00", "13:14:00", 79, 47, "European Conference on Machine Learning", 74
exec AddWorkshop "2012-7-6", "9:40:00", "11:40:00", 117, 27, "Workshop on Image Processing", 54
exec AddWorkshop "2012-8-22", "18:23:00", "21:23:00", 65, 28, "RuleML Symposium				", 47
exec AddWorkshop "2011-8-11", "14:43:00", "17:43:00", 106, 34, "ARTES 1 Final Presentation Days", 73
exec AddWorkshop "2011-1-21", "13:25:00", "15:25:00", 107, 27, "Asian Conference on Computer Vision", 18
exec AddWorkshop "2011-1-10", "18:10:00", "19:10:00", 56, 35, "International Conference on Signal and Imaging Systems Engineering", 63
exec AddWorkshop "2011-8-12", "9:52:00", "11:52:00", 101, 42, "4th DUE Permafrost User Workshop", 46
exec AddWorkshop "2013-7-21", "17:21:00", "19:21:00", 105, 30, "2nd International Conference on Photonics, Optics and Laser Technology", 62
exec AddWorkshop "2011-10-7", "18:32:00", "21:32:00", 117, 37, "International Conference on Automated Reasoning", 45
exec AddWorkshop "2013-3-21", "17:44:00", "19:44:00", 88, 39, "Annual Convention of The Society", 37
exec AddWorkshop "2012-4-17", "12:39:00", "14:39:00", 122, 20, "EuroCOW the Calibration and Orientation Workshop", 43
exec AddWorkshop "2012-1-24", "11:11:00", "12:11:00", 99, 44, "The BCLC CSR Conference", 84
exec AddWorkshop "2013-2-27", "10:18:00", "12:18:00", 110, 34, "International Conference on Logic for Programming", 11
exec AddWorkshop "2011-8-30", "11:37:00", "14:37:00", 137, 34, "48th ESLAB Symposium", 23
exec AddWorkshop "2011-6-21", "12:39:00", "13:39:00", 65, 22, "Corporate Community Involvement Conference", 46
exec AddWorkshop "2011-10-16", "19:17:00", "21:17:00", 147, 20, "International Conference on the Principles of", 20
exec AddWorkshop "2012-8-30", "8:43:00", "11:43:00", 100, 43, "Microwave Workshop", 67
exec AddWorkshop "2012-9-6", "8:34:00", "10:34:00", 61, 35, "British	Machine	Vision	Conference		", 65
exec AddWorkshop "2011-1-9", "18:12:00", "19:12:00", 56, 35, "International Joint Conference on Automated Reasoning", 53
exec AddWorkshop "2012-2-3", "10:1:00", "11:1:00", 143, 38, "Photonics West", 17
exec AddWorkshop "2012-6-10", "15:9:00", "17:9:00", 107, 34, "European Conference on Machine Learning", 70
exec AddWorkshop "2013-4-14", "15:55:00", "17:55:00", 52, 35, "Microwave Workshop", 69
exec AddWorkshop "2012-4-9", "8:12:00", "11:12:00", 90, 30, "Business4Better: The Community Partnership Movement", 40
exec AddWorkshop "2012-4-27", "14:44:00", "15:44:00", 148, 43, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 44
exec AddWorkshop "2013-9-2", "13:24:00", "15:24:00", 112, 20, "International Conference on Automated Planning", 16
exec AddWorkshop "2013-3-11", "17:37:00", "19:37:00", 114, 21, "International Conference on Computer Vision Theory and Applications", 30
exec AddWorkshop "2011-8-12", "13:4:00", "16:4:00", 71, 39, "Conference on Learning Theory		", 69
exec AddWorkshop "2011-11-17", "19:25:00", "20:25:00", 126, 20, "LPVE Land product validation and evolution", 32
exec AddWorkshop "2012-4-13", "14:7:00", "15:7:00", 149, 31, "RuleML Symposium		", 41
exec AddWorkshop "2011-11-11", "18:42:00", "21:42:00", 148, 31, "Leadership Strategies for Information Technology in Health Care Boston", 4
exec AddWorkshop "2013-10-21", "8:57:00", "11:57:00", 70, 31, "International Conference on Automated Planning", 9
exec AddWorkshop "2013-9-22", "9:33:00", "11:33:00", 66, 33, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 18
exec AddWorkshop "2013-9-3", "17:33:00", "20:33:00", 107, 30, "ARTES 1 Final Presentation Days", 28
exec AddWorkshop "2013-1-23", "8:12:00", "10:12:00", 57, 20, "International Conference on Autonomous Agents and", 74
exec AddWorkshop "2013-10-6", "9:60:00", "10:60:00", 129, 37, "Business4Better: The Community Partnership Movement", 59
exec AddWorkshop "2012-3-1", "9:44:00", "10:44:00", 114, 21, "The Sixth International Conferences on Advances in Multimedia", 19
exec AddWorkshop "2011-12-21", "16:33:00", "18:33:00", 87, 36, "Mechanisms Final Presentation Days", 29
exec AddWorkshop "2013-12-23", "10:35:00", "12:35:00", 51, 34, "Sentinel-2 for Science WS", 17
exec AddWorkshop "2013-5-17", "11:22:00", "14:22:00", 81, 45, "Corporate Community Involvement Conference", 32
exec AddWorkshop "2013-10-9", "8:46:00", "9:46:00", 116, 33, "Cause Marketing Forum", 42
exec AddWorkshop "2012-5-15", "19:50:00", "22:50:00", 62, 28, "Conference on Learning Theory		", 69
exec AddWorkshop "2012-2-20", "19:50:00", "21:50:00", 88, 42, "Workshop on Image Processing", 67
exec AddWorkshop "2011-12-28", "11:44:00", "12:44:00", 139, 49, "International Conference on Automated Reasoning", 41
exec AddWorkshop "2012-2-18", "18:51:00", "19:51:00", 112, 40, "Cause Marketing Forum", 26
exec AddWorkshop "2011-2-22", "13:12:00", "15:12:00", 57, 45, "Foundations of Genetic Algorithms		", 50
exec AddWorkshop "2012-6-5", "8:38:00", "9:38:00", 58, 44, "Annual Convention of The Society", 53
exec AddWorkshop "2012-6-8", "12:43:00", "15:43:00", 80, 21, "�International� Corporate Citizenship Conference", 23
exec AddWorkshop "2013-1-21", "15:48:00", "17:48:00", 133, 40, "KU Leuven CSR Symposium", 11
exec AddWorkshop "2013-11-17", "16:13:00", "18:13:00", 142, 32, "International Conference on Signal and Imaging Systems Engineering", 56
exec AddWorkshop "2013-8-4", "18:43:00", "21:43:00", 123, 43, "European Space Technology Harmonisation Conference", 32
exec AddWorkshop "2011-2-19", "17:38:00", "20:38:00", 90, 49, "Client Summit", 35
exec AddWorkshop "2012-12-15", "18:40:00", "20:40:00", 123, 37, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 34
exec AddWorkshop "2012-1-11", "8:16:00", "10:16:00", 53, 41, "European Space Technology Harmonisation Conference", 28
exec AddWorkshop "2013-5-7", "13:12:00", "15:12:00", 98, 20, "Mechanisms Final Presentation Days", 10
exec AddWorkshop "2011-7-26", "10:39:00", "13:39:00", 105, 20, "LPVE Land product validation and evolution", 67
exec AddWorkshop "2012-11-2", "17:36:00", "18:36:00", 100, 40, "Client Summit", 27
exec AddWorkshop "2011-4-16", "15:29:00", "16:29:00", 95, 41, "Conference on Automated Deduction		", 66
exec AddWorkshop "2013-7-13", "10:19:00", "11:19:00", 132, 49, "KU Leuven CSR Symposium", 51
exec AddWorkshop "2011-6-3", "11:50:00", "14:50:00", 147, 36, "Conference on Learning Theory		", 64
exec AddWorkshop "2011-4-4", "11:6:00", "13:6:00", 55, 37, "International Conference on the Principles of", 2
exec AddWorkshop "2013-9-6", "16:20:00", "18:20:00", 114, 21, "Conference on Automated Deduction		", 45
exec AddWorkshop "2011-11-20", "18:56:00", "20:56:00", 63, 20, "ICATT", 78
exec AddWorkshop "2012-3-4", "8:59:00", "9:59:00", 128, 26, "Life in Space for Life on Earth Symposium", 77
exec AddWorkshop "2012-12-8", "18:54:00", "21:54:00", 148, 42, "Conference on Uncertainty in Artificial Intelligence", 61
exec AddWorkshop "2013-1-26", "11:26:00", "13:26:00", 119, 29, "Client Summit", 76
exec AddWorkshop "2013-10-23", "12:25:00", "14:25:00", 54, 28, "The Sixth International Conferences on Advances in Multimedia", 8
exec AddWorkshop "2011-5-3", "19:54:00", "21:54:00", 111, 44, "Corporate Community Involvement Conference", 14
exec AddWorkshop "2011-6-4", "13:13:00", "15:13:00", 103, 45, "International Conference on Automated Reasoning", 25
exec AddWorkshop "2012-3-21", "14:39:00", "16:39:00", 90, 24, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 76
exec AddWorkshop "2011-3-7", "15:4:00", "17:4:00", 71, 47, "48th ESLAB Symposium", 12
exec AddWorkshop "2012-10-12", "17:30:00", "20:30:00", 95, 38, "EuroCOW the Calibration and Orientation Workshop", 81
exec AddWorkshop "2013-9-7", "11:56:00", "14:56:00", 148, 21, "European Navigation Conference 2014 (ENC-GNSS 2014)", 68
exec AddWorkshop "2012-10-27", "8:27:00", "10:27:00", 86, 24, "Conference on Learning Theory		", 42
exec AddWorkshop "2013-3-3", "14:41:00", "17:41:00", 76, 40, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 46
exec AddWorkshop "2011-8-18", "13:27:00", "14:27:00", 115, 24, "X-Ray Universe", 56
exec AddWorkshop "2011-6-8", "14:3:00", "16:3:00", 84, 29, "Global Conference on Sustainability and Reporting", 78
exec AddWorkshop "2011-4-29", "12:23:00", "13:23:00", 143, 47, "The Sixth International Conferences on Advances in Multimedia", 32
exec AddWorkshop "2013-8-8", "15:35:00", "17:35:00", 63, 36, "International Conference on Autonomous Agents and", 57
exec AddWorkshop "2011-2-10", "19:2:00", "22:2:00", 146, 42, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 53
exec AddWorkshop "2012-12-30", "10:59:00", "12:59:00", 75, 42, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 29
exec AddWorkshop "2013-8-1", "15:53:00", "17:53:00", 74, 42, "Client Summit", 16
exec AddWorkshop "2012-3-15", "16:36:00", "19:36:00", 83, 31, "British	Machine	Vision	Conference		", 15
exec AddWorkshop "2012-12-26", "11:45:00", "12:45:00", 91, 36, "Science and Challenges of Lunar Sample Return Workshop", 48
exec AddWorkshop "2013-7-4", "11:19:00", "12:19:00", 104, 40, "Leadership Strategies for Information Technology in Health Care Boston", 8
exec AddWorkshop "2013-5-13", "15:42:00", "16:42:00", 133, 38, "Annual Convention of The Society", 50
exec AddWorkshop "2012-2-6", "17:38:00", "18:38:00", 96, 24, "LPVE Land product validation and evolution", 16
exec AddWorkshop "2013-2-3", "15:33:00", "17:33:00", 50, 23, "2013 Ethical Leadership Conference: Ethics in Action", 31
exec AddWorkshop "2012-5-3", "10:56:00", "11:56:00", 100, 31, "Conference on Uncertainty in Artificial Intelligence", 71
exec AddWorkshop "2011-9-27", "9:33:00", "12:33:00", 96, 49, "Sustainable Brands Conference", 26
exec AddWorkshop "2011-3-6", "12:50:00", "13:50:00", 58, 26, "European Space Power Conference", 15
exec AddWorkshop "2013-9-17", "9:50:00", "12:50:00", 94, 26, "�International� Corporate Citizenship Conference", 78
exec AddWorkshop "2012-4-19", "12:16:00", "14:16:00", 147, 37, "Cause Marketing Forum", 84
exec AddWorkshop "2013-4-30", "8:23:00", "9:23:00", 121, 20, "GNC 2014", 60
exec AddWorkshop "2011-6-22", "19:25:00", "21:25:00", 71, 35, "The 2nd International CSR Communication Conference", 1
exec AddWorkshop "2011-7-1", "11:15:00", "13:15:00", 65, 48, "Mayo Clinic Presents", 25
exec AddWorkshop "2013-2-10", "9:55:00", "12:55:00", 130, 42, "Ceres Conference", 80
exec AddWorkshop "2011-2-8", "11:37:00", "14:37:00", 92, 29, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 14
exec AddWorkshop "2013-3-29", "14:42:00", "16:42:00", 116, 47, "RuleML Symposium		", 54
exec AddWorkshop "2011-6-29", "8:11:00", "9:11:00", 67, 41, "Workshop on Logic ", 80
exec AddWorkshop "2011-11-28", "10:38:00", "13:38:00", 107, 42, "RuleML Symposium		", 10
exec AddWorkshop "2012-4-28", "12:48:00", "13:48:00", 64, 43, "Conference on Automated Deduction		", 81
exec AddWorkshop "2012-6-28", "10:10:00", "13:10:00", 68, 30, "European Conference on Computer Vision", 49
exec AddWorkshop "2013-12-28", "14:16:00", "16:16:00", 99, 49, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 19
exec AddWorkshop "2011-11-26", "12:42:00", "13:42:00", 120, 23, "European Conference on Computer Vision", 79
exec AddWorkshop "2011-11-27", "15:13:00", "16:13:00", 132, 32, "Sentinel-2 for Science WS", 8
exec AddWorkshop "2011-9-4", "19:48:00", "22:48:00", 57, 36, "Ceres Conference", 39
exec AddWorkshop "2011-6-8", "18:60:00", "21:60:00", 137, 35, "International Conference on Automated Planning", 41
exec AddWorkshop "2012-2-9", "12:55:00", "13:55:00", 85, 20, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 27
exec AddWorkshop "2012-11-11", "15:5:00", "18:5:00", 107, 44, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 3
exec AddWorkshop "2013-1-10", "19:25:00", "21:25:00", 69, 43, "International Conference on Computer Vision Theory and Applications", 76
exec AddWorkshop "2013-8-19", "10:59:00", "12:59:00", 114, 47, "X-Ray Universe", 84
exec AddWorkshop "2011-6-23", "17:50:00", "19:50:00", 121, 35, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 50
exec AddWorkshop "2011-10-10", "17:15:00", "19:15:00", 73, 24, "European Conference on Artificial Intelligence	", 33
exec AddWorkshop "2011-5-12", "16:1:00", "17:1:00", 102, 29, "2nd International Conference on Photonics, Optics and Laser Technology", 29
exec AddWorkshop "2013-11-18", "16:55:00", "19:55:00", 107, 27, "International Conference on Artificial Neural Networks", 76
exec AddWorkshop "2013-8-21", "8:14:00", "9:14:00", 81, 28, "European Navigation Conference 2014 (ENC-GNSS 2014)", 20
exec AddWorkshop "2011-11-26", "11:53:00", "14:53:00", 83, 21, "7 th International Conference on Bio-inspired Systems and Signal Processing", 46
exec AddWorkshop "2012-6-19", "18:59:00", "20:59:00", 91, 42, "European Space Technology Harmonisation Conference", 80
exec AddWorkshop "2012-8-24", "12:25:00", "13:25:00", 74, 46, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 84
exec AddWorkshop "2013-3-5", "11:24:00", "14:24:00", 129, 24, "Microwave Workshop", 17
exec AddWorkshop "2011-11-25", "10:15:00", "11:15:00", 87, 29, "2013 Ethical Leadership Conference: Ethics in Action", 59
exec AddWorkshop "2013-5-14", "15:21:00", "18:21:00", 89, 22, "The Corporate Philanthropy Forum", 16
exec AddWorkshop "2013-2-3", "10:33:00", "12:33:00", 91, 43, "Sustainable Brands Conference", 47
exec AddWorkshop "2013-7-13", "10:35:00", "13:35:00", 128, 39, "The BCLC CSR Conference", 12
exec AddWorkshop "2013-3-9", "8:60:00", "10:60:00", 141, 35, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 34
exec AddWorkshop "2013-9-5", "10:23:00", "12:23:00", 114, 29, "Workshop on Logic ", 56
exec AddWorkshop "2012-3-9", "13:25:00", "15:25:00", 88, 47, "Asian Conference on Computer Vision", 45
exec AddWorkshop "2013-4-5", "13:8:00", "14:8:00", 84, 31, "Annual Convention of The Society", 39
exec AddWorkshop "2012-4-24", "19:3:00", "20:3:00", 96, 34, "KU Leuven CSR Symposium", 40
exec AddWorkshop "2013-10-17", "12:7:00", "13:7:00", 129, 34, "4S Symposium 2014", 4
exec AddWorkshop "2011-7-11", "13:26:00", "14:26:00", 74, 29, "International Conference on Automated Reasoning", 62
exec AddWorkshop "2011-8-23", "17:3:00", "19:3:00", 84, 35, "European Navigation Conference 2014 (ENC-GNSS 2014)", 41
exec AddWorkshop "2011-2-5", "16:50:00", "19:50:00", 139, 44, "International Conference on Signal and Imaging Systems Engineering", 30
exec AddWorkshop "2012-3-26", "14:55:00", "17:55:00", 76, 22, "Cause Marketing Forum", 69
exec AddWorkshop "2012-4-15", "14:45:00", "15:45:00", 66, 25, "Conference on Learning Theory		", 72
exec AddWorkshop "2011-1-19", "15:1:00", "16:1:00", 69, 48, "Annual Convention of The Society", 34
exec AddWorkshop "2011-6-10", "10:14:00", "13:14:00", 64, 37, "Microwave Workshop", 37
exec AddWorkshop "2011-5-30", "13:31:00", "14:31:00", 145, 41, "Computer Analysis of Images and Patterns", 47
exec AddWorkshop "2013-9-18", "19:40:00", "22:40:00", 73, 24, "British	Machine	Vision	Conference		", 6
exec AddWorkshop "2013-12-29", "14:31:00", "16:31:00", 98, 32, "European Space Technology Harmonisation Conference", 64
exec AddWorkshop "2013-12-7", "16:50:00", "19:50:00", 145, 28, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 27
exec AddWorkshop "2012-10-28", "16:27:00", "19:27:00", 138, 28, "KU Leuven CSR Symposium", 84
exec AddWorkshop "2012-11-19", "12:39:00", "14:39:00", 148, 42, "ICATT", 41
exec AddWorkshop "2011-3-17", "9:42:00", "11:42:00", 139, 49, "Conference on Automated Deduction		", 71
exec AddWorkshop "2013-1-26", "18:40:00", "19:40:00", 61, 26, "KU Leuven CSR Symposium", 41
exec AddWorkshop "2012-2-17", "9:34:00", "12:34:00", 94, 24, "Conference on Automated Deduction		", 71
exec AddWorkshop "2012-7-23", "8:39:00", "9:39:00", 68, 39, "ICATT", 43
exec AddWorkshop "2012-4-18", "8:53:00", "9:53:00", 134, 49, "Microwave Workshop", 83
exec AddWorkshop "2011-2-14", "13:31:00", "15:31:00", 96, 31, "International Conference on Logic for Programming", 76
exec AddWorkshop "2012-9-29", "13:17:00", "14:17:00", 77, 33, "KU Leuven CSR Symposium", 3
exec AddWorkshop "2011-2-28", "8:50:00", "11:50:00", 72, 21, "Life in Space for Life on Earth Symposium", 62
exec AddWorkshop "2012-4-15", "14:12:00", "16:12:00", 145, 43, "The Corporate Philanthropy Forum", 71
exec AddWorkshop "2012-7-24", "14:11:00", "15:11:00", 88, 35, "Conference on Learning Theory		", 53
exec AddWorkshop "2011-3-25", "12:29:00", "13:29:00", 63, 46, "Sentinel-2 for Science WS", 46
exec AddWorkshop "2011-11-5", "10:53:00", "13:53:00", 98, 36, "Net Impact Conference", 59
exec AddWorkshop "2011-9-11", "13:37:00", "16:37:00", 105, 25, "International Conference on Automated Reasoning", 8
exec AddWorkshop "2011-12-9", "15:59:00", "18:59:00", 125, 37, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 23
exec AddWorkshop "2013-11-18", "9:41:00", "12:41:00", 82, 48, "International Conference on Artificial Neural Networks", 39
exec AddWorkshop "2013-9-11", "14:37:00", "16:37:00", 90, 24, "Net Impact Conference", 10
exec AddWorkshop "2011-12-29", "18:10:00", "21:10:00", 135, 48, "European Space Technology Harmonisation Conference", 9
exec AddWorkshop "2011-8-4", "18:26:00", "19:26:00", 114, 22, "Conference on Learning Theory		", 81
exec AddWorkshop "2013-5-11", "16:57:00", "17:57:00", 67, 29, "Corporate Community Involvement Conference", 15
exec AddWorkshop "2012-10-10", "17:51:00", "18:51:00", 86, 29, "The Corporate Philanthropy Forum", 42
exec AddWorkshop "2012-4-15", "9:5:00", "11:5:00", 137, 35, "Microwave Workshop", 39
exec AddWorkshop "2013-1-2", "16:43:00", "18:43:00", 65, 31, "ARTES 1 Final Presentation Days", 56
exec AddWorkshop "2013-8-17", "11:53:00", "12:53:00", 53, 38, "EuroCOW the Calibration and Orientation Workshop", 32
exec AddWorkshop "2011-5-27", "17:48:00", "18:48:00", 115, 23, "GNC 2014", 24
exec AddWorkshop "2012-1-10", "14:51:00", "16:51:00", 114, 34, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 83
exec AddWorkshop "2012-6-27", "11:46:00", "14:46:00", 63, 21, "Science and Challenges of Lunar Sample Return Workshop", 47
exec AddWorkshop "2012-1-22", "12:23:00", "13:23:00", 146, 27, "International Semantic Web Conference		", 48
exec AddWorkshop "2011-5-19", "11:30:00", "12:30:00", 79, 23, "Mechanisms Final Presentation Days", 29
exec AddWorkshop "2012-2-10", "15:60:00", "18:60:00", 123, 35, "International Semantic Web Conference		", 60
exec AddWorkshop "2013-1-23", "9:32:00", "11:32:00", 101, 42, "European Space Power Conference", 63
exec AddWorkshop "2012-9-16", "18:21:00", "21:21:00", 124, 24, "Annual Interdisciplinary Conference", 82
exec AddWorkshop "2012-8-25", "17:24:00", "18:24:00", 137, 28, "International Semantic Web Conference		", 82
exec AddWorkshop "2013-4-13", "19:49:00", "20:49:00", 74, 35, "Business4Better: The Community Partnership Movement", 67
exec AddWorkshop "2013-1-2", "19:22:00", "22:22:00", 148, 46, "International Conference on Artificial Neural Networks", 80
exec AddWorkshop "2012-3-13", "8:4:00", "9:4:00", 58, 45, "Foundations of Genetic Algorithms		", 73
exec AddWorkshop "2013-4-3", "8:10:00", "9:10:00", 92, 34, "British	Machine	Vision	Conference		", 21
exec AddWorkshop "2013-12-10", "8:33:00", "10:33:00", 59, 48, "LPVE Land product validation and evolution", 22
exec AddWorkshop "2012-6-24", "11:47:00", "13:47:00", 74, 39, "LPVE Land product validation and evolution", 52
exec AddWorkshop "2013-4-4", "12:27:00", "13:27:00", 68, 35, "International Conference on the Principles of", 34
exec AddWorkshop "2011-2-24", "19:23:00", "22:23:00", 124, 46, "ARTES 1 Final Presentation Days", 77
exec AddWorkshop "2012-4-24", "14:45:00", "17:45:00", 126, 38, "RuleML Symposium		", 32
exec AddWorkshop "2013-7-1", "10:2:00", "13:2:00", 72, 39, "Annual Interdisciplinary Conference", 1
exec AddWorkshop "2012-9-6", "17:32:00", "20:32:00", 110, 24, "48th ESLAB Symposium", 73
exec AddWorkshop "2012-2-18", "9:20:00", "12:20:00", 59, 24, "European Space Technology Harmonisation Conference", 13
exec AddWorkshop "2013-3-15", "14:48:00", "16:48:00", 107, 45, "X-Ray Universe", 62
exec AddWorkshop "2013-7-23", "18:57:00", "21:57:00", 142, 24, "International Conference on Signal and Imaging Systems Engineering", 59
exec AddWorkshop "2013-12-4", "18:39:00", "19:39:00", 118, 46, "GNC 2014", 23
exec AddWorkshop "2013-12-9", "15:16:00", "18:16:00", 81, 40, "The 2nd International CSR Communication Conference", 25
exec AddWorkshop "2013-8-30", "16:56:00", "17:56:00", 122, 38, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 83
exec AddWorkshop "2013-1-24", "9:52:00", "12:52:00", 78, 28, "GNC 2014", 39
exec AddWorkshop "2013-7-2", "9:38:00", "10:38:00", 85, 34, "Corporate Community Involvement Conference", 6
exec AddWorkshop "2013-6-20", "8:11:00", "10:11:00", 113, 42, "The BCLC CSR Conference", 83
exec AddWorkshop "2011-8-30", "14:38:00", "15:38:00", 55, 26, "International Joint Conference on Artificial Intelligence", 29
exec AddWorkshop "2012-10-13", "8:35:00", "10:35:00", 105, 24, "4th DUE Permafrost User Workshop", 17
exec AddWorkshop "2011-12-24", "17:32:00", "20:32:00", 66, 42, "International Conference on Artificial Neural Networks", 48
exec AddWorkshop "2011-3-29", "8:38:00", "9:38:00", 86, 27, "Leadership Strategies for Information Technology in Health Care Boston", 61
exec AddWorkshop "2013-1-30", "19:10:00", "20:10:00", 122, 29, "KU Leuven CSR Symposium", 16
exec AddWorkshop "2011-11-4", "13:43:00", "14:43:00", 88, 47, "International Conference on Artificial Neural Networks", 64
exec AddWorkshop "2012-8-18", "8:6:00", "10:6:00", 75, 22, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 45
exec AddWorkshop "2011-2-13", "10:38:00", "11:38:00", 87, 36, "International Conference on Automated Planning", 82
exec AddWorkshop "2011-8-22", "16:17:00", "18:17:00", 77, 28, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 36
exec AddWorkshop "2011-8-17", "17:29:00", "20:29:00", 68, 38, "ICATT", 6
exec AddWorkshop "2013-8-9", "18:59:00", "19:59:00", 58, 31, "European Conference on Computer Vision", 2
exec AddWorkshop "2013-5-19", "13:28:00", "15:28:00", 80, 48, "International Conference on Signal and Imaging Systems Engineering", 7
exec AddWorkshop "2011-3-19", "14:9:00", "15:9:00", 61, 21, "International Conference on Logic for Programming", 34
exec AddWorkshop "2011-11-24", "15:52:00", "16:52:00", 94, 44, "KU Leuven CSR Symposium", 44
exec AddWorkshop "2012-3-12", "8:21:00", "9:21:00", 74, 26, "48th ESLAB Symposium", 28
exec AddWorkshop "2011-3-10", "18:58:00", "19:58:00", 78, 24, "Workshop on Logic ", 59
exec AddWorkshop "2011-7-16", "18:33:00", "20:33:00", 69, 42, "International Conference on Artificial Neural Networks", 65
exec AddWorkshop "2011-7-5", "11:22:00", "12:22:00", 111, 20, "Photonics West", 75
exec AddWorkshop "2013-9-10", "17:6:00", "19:6:00", 59, 38, "Leadership Strategies for Information Technology in Health Care Boston", 15
exec AddWorkshop "2013-7-10", "13:29:00", "14:29:00", 94, 41, "�International� Corporate Citizenship Conference", 49
exec AddWorkshop "2012-12-30", "12:46:00", "15:46:00", 67, 20, "The 2nd International CSR Communication Conference", 18
exec AddWorkshop "2011-1-13", "8:14:00", "11:14:00", 58, 20, "Sentinel-2 for Science WS", 2
exec AddWorkshop "2011-5-18", "8:24:00", "9:24:00", 105, 40, "Sentinel-2 for Science WS", 44
exec AddWorkshop "2012-2-3", "8:34:00", "9:34:00", 139, 45, "International Conference on Logic for Programming", 78
exec AddWorkshop "2013-4-29", "16:51:00", "18:51:00", 140, 36, "RuleML Symposium		", 55
exec AddWorkshop "2011-10-26", "11:41:00", "13:41:00", 78, 21, "International Joint Conference on Automated Reasoning", 26
exec AddWorkshop "2012-5-25", "12:33:00", "13:33:00", 78, 48, "Cause Marketing Forum", 60
exec AddWorkshop "2012-8-3", "19:29:00", "22:29:00", 87, 27, "Annual Interdisciplinary Conference", 48
exec AddWorkshop "2011-8-1", "16:54:00", "19:54:00", 69, 32, "International Conference on Autonomous Agents and", 76
exec AddWorkshop "2013-3-14", "18:56:00", "20:56:00", 72, 29, "�International� Corporate Citizenship Conference", 51
exec AddWorkshop "2011-9-29", "19:12:00", "20:12:00", 123, 39, "International Semantic Web Conference		", 47
exec AddWorkshop "2012-8-27", "19:20:00", "21:20:00", 79, 28, "European Space Power Conference", 10
exec AddWorkshop "2012-11-20", "16:36:00", "18:36:00", 141, 35, "7 th International Conference on Bio-inspired Systems and Signal Processing", 17
exec AddWorkshop "2013-4-14", "14:20:00", "15:20:00", 139, 45, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 48
exec AddWorkshop "2013-7-1", "16:6:00", "17:6:00", 104, 42, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 78
exec AddWorkshop "2012-3-30", "18:5:00", "20:5:00", 68, 35, "The Corporate Philanthropy Forum", 6
exec AddWorkshop "2012-12-11", "17:57:00", "20:57:00", 116, 24, "Workshop on Image Processing", 27
exec AddWorkshop "2012-9-16", "9:23:00", "12:23:00", 119, 35, "The Sixth International Conferences on Advances in Multimedia", 47
exec AddWorkshop "2011-2-13", "18:58:00", "19:58:00", 69, 43, "The 12th annual Responsible Business Summit", 58
exec AddWorkshop "2011-5-18", "16:24:00", "18:24:00", 122, 45, "European Conference on Computer Vision", 71
exec AddWorkshop "2012-12-25", "19:38:00", "20:38:00", 94, 40, "Annual Convention of The Society", 75
exec AddWorkshop "2013-8-4", "16:60:00", "18:60:00", 56, 30, "International Conference on Automated Reasoning", 70
exec AddWorkshop "2012-10-11", "10:45:00", "12:45:00", 89, 23, "Cause Marketing Forum", 56
exec AddWorkshop "2013-4-17", "9:14:00", "12:14:00", 61, 26, "International Conference on Automated Reasoning", 17
exec AddWorkshop "2013-10-1", "14:57:00", "15:57:00", 113, 27, "Life in Space for Life on Earth Symposium", 75
exec AddWorkshop "2012-2-25", "10:23:00", "11:23:00", 137, 36, "European Conference on Computer Vision", 15
exec AddWorkshop "2011-10-28", "17:7:00", "20:7:00", 106, 27, "International Conference on MultiMedia Modeling", 82
exec AddWorkshop "2013-7-29", "9:33:00", "11:33:00", 107, 46, "Annual Convention of The Society", 17
exec AddWorkshop "2012-2-28", "13:9:00", "14:9:00", 95, 43, "4S Symposium 2014", 78
exec AddWorkshop "2012-5-11", "12:42:00", "13:42:00", 111, 33, "Foundations of Genetic Algorithms		", 56
exec AddWorkshop "2011-10-3", "15:24:00", "16:24:00", 127, 21, "International Conference on Computer Vision Theory and Applications", 67
exec AddWorkshop "2011-9-11", "18:42:00", "20:42:00", 63, 32, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 46
exec AddWorkshop "2011-6-17", "18:23:00", "20:23:00", 112, 41, "European Conference on Machine Learning", 0
exec AddWorkshop "2011-12-28", "14:37:00", "17:37:00", 50, 30, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 49
exec AddWorkshop "2013-9-19", "17:8:00", "19:8:00", 77, 21, "�International� Corporate Citizenship Conference", 14
exec AddWorkshop "2012-7-14", "10:17:00", "11:17:00", 145, 43, "International Semantic Web Conference		", 68
exec AddWorkshop "2012-12-26", "11:10:00", "12:10:00", 130, 33, "Conference on Computer Vision and Pattern", 61
exec AddWorkshop "2011-3-24", "16:3:00", "18:3:00", 68, 32, "Conference on Computer Vision and Pattern", 0
exec AddWorkshop "2013-8-25", "8:12:00", "9:12:00", 139, 28, "Conference on Automated Deduction		", 8
exec AddWorkshop "2012-10-26", "14:40:00", "15:40:00", 52, 49, "Mayo Clinic Presents", 14
exec AddWorkshop "2013-2-23", "17:60:00", "19:60:00", 148, 34, "European Conference on Computer Vision", 80
exec AddWorkshop "2012-2-7", "19:35:00", "21:35:00", 129, 37, "4S Symposium 2014", 61
exec AddWorkshop "2011-1-22", "11:7:00", "12:7:00", 102, 42, "RuleML Symposium		", 16
exec AddWorkshop "2011-10-26", "16:20:00", "18:20:00", 54, 49, "ARTES 1 Final Presentation Days", 12
exec AddWorkshop "2013-5-20", "12:47:00", "13:47:00", 83, 38, "Leadership Strategies for Information Technology in Health Care Boston", 70
exec AddWorkshop "2012-7-30", "16:52:00", "17:52:00", 67, 42, "International Conference on Automated Planning", 5
exec AddWorkshop "2013-6-18", "17:57:00", "19:57:00", 101, 33, "Workshop on Logic ", 18
exec AddWorkshop "2013-8-24", "11:31:00", "13:31:00", 132, 24, "Sentinel-2 for Science WS", 4
exec AddWorkshop "2013-11-29", "8:53:00", "10:53:00", 61, 28, "ICATT", 74
exec AddWorkshop "2011-5-4", "17:49:00", "19:49:00", 52, 27, "International Conference on Autonomous Agents and", 24
exec AddWorkshop "2012-3-19", "17:27:00", "20:27:00", 78, 26, "Workshop on Logic ", 45
exec AddWorkshop "2013-5-7", "9:48:00", "11:48:00", 83, 33, "Business4Better: The Community Partnership Movement", 52
exec AddWorkshop "2013-12-1", "18:60:00", "20:60:00", 92, 31, "The 2nd International CSR Communication Conference", 72
exec AddWorkshop "2012-6-14", "19:14:00", "21:14:00", 59, 40, "Conference on Uncertainty in Artificial Intelligence", 64
exec AddWorkshop "2011-11-20", "16:24:00", "18:24:00", 83, 30, "British	Machine	Vision	Conference		", 49
exec AddWorkshop "2011-12-10", "16:3:00", "17:3:00", 80, 44, "European Navigation Conference 2014 (ENC-GNSS 2014)", 36
exec AddWorkshop "2011-8-11", "16:42:00", "18:42:00", 60, 36, "X-Ray Universe", 60
exec AddWorkshop "2013-10-22", "9:8:00", "12:8:00", 103, 30, "Asian Conference on Computer Vision", 14
exec AddWorkshop "2012-4-15", "15:14:00", "17:14:00", 67, 49, "4th DUE Permafrost User Workshop", 17
exec AddWorkshop "2013-8-6", "18:10:00", "19:10:00", 145, 21, "KU Leuven CSR Symposium", 31
exec AddWorkshop "2011-11-28", "17:42:00", "18:42:00", 81, 31, "Net Impact Conference", 34
exec AddWorkshop "2011-9-28", "15:13:00", "17:13:00", 96, 33, "KU Leuven CSR Symposium", 55
exec AddWorkshop "2012-6-7", "11:59:00", "13:59:00", 102, 40, "European Conference on Machine Learning", 35
exec AddWorkshop "2013-8-2", "15:37:00", "16:37:00", 94, 31, "Annual Interdisciplinary Conference", 74
exec AddWorkshop "2011-4-22", "18:13:00", "20:13:00", 122, 23, "European Space Technology Harmonisation Conference", 11
exec AddWorkshop "2012-9-8", "12:30:00", "15:30:00", 136, 36, "RuleML Symposium				", 30
exec AddWorkshop "2012-12-19", "18:37:00", "21:37:00", 133, 36, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 57
exec AddWorkshop "2013-2-13", "19:3:00", "21:3:00", 109, 35, "Annual Convention of The Society", 69
exec AddWorkshop "2012-8-16", "8:30:00", "9:30:00", 52, 24, "Annual Convention of The Society", 47
exec AddWorkshop "2011-9-9", "17:28:00", "19:28:00", 96, 23, "The BCLC CSR Conference", 36
exec AddWorkshop "2012-3-15", "8:51:00", "9:51:00", 87, 20, "Business4Better: The Community Partnership Movement", 46
exec AddWorkshop "2013-4-16", "13:58:00", "16:58:00", 98, 45, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 53
exec AddWorkshop "2012-4-6", "11:43:00", "14:43:00", 52, 32, "International Conference on Logic for Programming", 39
exec AddWorkshop "2012-7-28", "19:48:00", "21:48:00", 132, 30, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 37
exec AddWorkshop "2013-1-21", "16:29:00", "17:29:00", 129, 27, "Conference on Learning Theory		", 27
exec AddWorkshop "2011-6-10", "13:7:00", "14:7:00", 100, 35, "International Conference on Autonomous Agents and", 2
exec AddWorkshop "2011-6-17", "12:45:00", "14:45:00", 121, 25, "International Conference on the Principles of", 65
exec AddWorkshop "2011-3-28", "13:6:00", "15:6:00", 70, 23, "3rd International Conference on Pattern Recognition Applications and Methods", 66
exec AddWorkshop "2011-12-14", "15:36:00", "18:36:00", 147, 30, "International Joint Conference on Automated Reasoning", 42
exec AddWorkshop "2013-10-21", "15:39:00", "17:39:00", 134, 20, "RuleML Symposium				", 6
exec AddWorkshop "2013-4-11", "12:12:00", "14:12:00", 147, 47, "The 2nd International CSR Communication Conference", 53
exec AddWorkshop "2011-2-26", "8:53:00", "10:53:00", 67, 26, "International Conference on Pattern Recognition", 82
exec AddWorkshop "2011-4-26", "17:11:00", "18:11:00", 104, 20, "The Corporate Philanthropy Forum", 72
exec AddWorkshop "2013-2-19", "14:55:00", "15:55:00", 58, 48, "Sentinel-2 for Science WS", 15
exec AddWorkshop "2012-11-1", "8:21:00", "11:21:00", 139, 39, "4S Symposium 2014", 67
exec AddWorkshop "2012-9-3", "13:21:00", "15:21:00", 89, 32, "ICATT", 75
exec AddWorkshop "2011-4-6", "16:27:00", "17:27:00", 90, 49, "International Conference on Autonomous Agents and", 23
exec AddWorkshop "2013-2-5", "14:1:00", "15:1:00", 90, 28, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 56
exec AddWorkshop "2012-6-8", "15:7:00", "18:7:00", 88, 37, "Business4Better: The Community Partnership Movement", 6
exec AddWorkshop "2012-3-21", "14:48:00", "17:48:00", 135, 27, "International Semantic Web Conference		", 81
exec AddWorkshop "2013-5-5", "10:12:00", "11:12:00", 56, 25, "Ceres Conference", 19
exec AddWorkshop "2011-7-10", "10:26:00", "12:26:00", 74, 31, "International Conference on MultiMedia Modeling", 2
exec AddWorkshop "2012-12-9", "10:34:00", "11:34:00", 59, 27, "Microwave Workshop", 12
exec AddWorkshop "2013-6-7", "14:22:00", "17:22:00", 140, 41, "EuroCOW the Calibration and Orientation Workshop", 26
exec AddWorkshop "2011-10-2", "19:54:00", "22:54:00", 62, 23, "Net Impact Conference", 36
exec AddWorkshop "2013-9-15", "15:46:00", "16:46:00", 67, 27, "International Conference on Signal and Imaging Systems Engineering", 32
exec AddWorkshop "2011-10-27", "19:47:00", "20:47:00", 108, 35, "European Conference on Computer Vision", 22
exec AddWorkshop "2013-6-4", "8:42:00", "11:42:00", 121, 37, "International Conference on Logic for Programming", 23
exec AddWorkshop "2011-11-7", "13:2:00", "14:2:00", 133, 42, "KU Leuven CSR Symposium", 56
exec AddWorkshop "2012-6-3", "19:43:00", "21:43:00", 71, 32, "Science and Challenges of Lunar Sample Return Workshop", 20
exec AddWorkshop "2012-2-10", "18:22:00", "21:22:00", 67, 37, "International Conference on Artificial Neural Networks", 46
exec AddWorkshop "2011-4-19", "12:54:00", "14:54:00", 98, 29, "International Conference on Autonomous Agents and", 39
exec AddWorkshop "2013-6-28", "10:42:00", "11:42:00", 68, 44, "International Conference on Automated Reasoning", 35
exec AddWorkshop "2013-12-8", "19:1:00", "21:1:00", 71, 39, "RuleML Symposium				", 14
exec AddWorkshop "2013-1-24", "19:49:00", "22:49:00", 52, 45, "Workshop on Image Processing", 3
exec AddWorkshop "2012-1-27", "10:15:00", "12:15:00", 149, 27, "2013 Ethical Leadership Conference: Ethics in Action", 52
exec AddWorkshop "2013-8-17", "10:32:00", "11:32:00", 92, 31, "Workshop on Logic ", 84
exec AddWorkshop "2013-11-3", "17:23:00", "18:23:00", 88, 41, "International Conference on Autonomous Agents and", 41
exec AddWorkshop "2012-10-23", "14:44:00", "15:44:00", 107, 33, "Mayo Clinic Presents", 72
exec AddWorkshop "2012-9-24", "13:18:00", "14:18:00", 111, 43, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 47
exec AddWorkshop "2012-9-20", "10:15:00", "12:15:00", 145, 41, "Photonics West", 4
exec AddWorkshop "2012-9-8", "15:41:00", "18:41:00", 110, 24, "International Conference on Automated Reasoning", 70
exec AddWorkshop "2012-5-16", "18:44:00", "19:44:00", 73, 31, "International Joint Conference on Artificial Intelligence", 8
exec AddWorkshop "2011-8-10", "14:56:00", "15:56:00", 125, 27, "Asian Conference on Computer Vision", 78
exec AddWorkshop "2013-4-25", "18:40:00", "19:40:00", 98, 28, "European Space Power Conference", 52
exec AddWorkshop "2011-9-5", "10:8:00", "11:8:00", 84, 29, "Computer Analysis of Images and Patterns", 61
exec AddWorkshop "2011-5-13", "10:55:00", "12:55:00", 81, 24, "GNC 2014", 81
exec AddWorkshop "2012-8-19", "18:47:00", "21:47:00", 136, 24, "Asian Conference on Computer Vision", 40
exec AddWorkshop "2012-4-7", "8:39:00", "10:39:00", 109, 35, "Asian Conference on Computer Vision", 39
exec AddWorkshop "2012-8-22", "15:30:00", "16:30:00", 57, 26, "Global Conference on Sustainability and Reporting", 43
exec AddWorkshop "2012-2-29", "15:47:00", "16:47:00", 92, 28, "European Navigation Conference 2014 (ENC-GNSS 2014)", 80
exec AddWorkshop "2012-5-4", "19:26:00", "20:26:00", 58, 41, "Workshop on Logic ", 25
exec AddWorkshop "2013-10-8", "11:55:00", "14:55:00", 53, 45, "Foundations of Genetic Algorithms		", 42
exec AddWorkshop "2011-7-15", "14:35:00", "15:35:00", 93, 34, "Conference on Uncertainty in Artificial Intelligence", 22
exec AddWorkshop "2013-12-3", "9:59:00", "10:59:00", 73, 23, "Net Impact Conference", 69
exec AddWorkshop "2012-8-23", "19:42:00", "20:42:00", 66, 41, "Business4Better: The Community Partnership Movement", 70
exec AddWorkshop "2012-12-21", "9:2:00", "12:2:00", 103, 34, "International Conference on Automated Reasoning", 2
exec AddWorkshop "2012-11-10", "10:37:00", "11:37:00", 78, 30, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 53
exec AddWorkshop "2013-10-11", "13:17:00", "14:17:00", 130, 42, "EuroCOW the Calibration and Orientation Workshop", 24
exec AddWorkshop "2011-5-21", "12:39:00", "15:39:00", 124, 35, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 0
exec AddWorkshop "2011-8-21", "9:6:00", "10:6:00", 121, 22, "European Navigation Conference 2014 (ENC-GNSS 2014)", 2
exec AddWorkshop "2013-11-10", "15:49:00", "16:49:00", 64, 42, "LPVE Land product validation and evolution", 9
exec AddWorkshop "2012-8-3", "13:6:00", "14:6:00", 81, 47, "International Conference on Logic for Programming", 23
exec AddWorkshop "2013-1-4", "15:3:00", "16:3:00", 95, 45, "ICATT", 63
exec AddWorkshop "2011-7-14", "10:53:00", "13:53:00", 106, 26, "ICATT", 25
exec AddWorkshop "2013-5-21", "12:35:00", "13:35:00", 130, 47, "International Conference on Artificial Neural Networks", 25
exec AddWorkshop "2012-9-18", "15:5:00", "16:5:00", 72, 29, "Foundations of Genetic Algorithms		", 43
exec AddWorkshop "2013-4-9", "17:48:00", "18:48:00", 129, 29, "Leadership Strategies for Information Technology in Health Care Boston", 30
exec AddWorkshop "2011-7-3", "9:39:00", "10:39:00", 63, 36, "Mechanisms Final Presentation Days", 2
exec AddWorkshop "2012-10-29", "17:14:00", "18:14:00", 142, 42, "Cause Marketing Forum", 82
exec AddWorkshop "2013-6-13", "18:11:00", "20:11:00", 82, 21, "European Conference on Computer Vision", 50
exec AddWorkshop "2013-12-3", "17:9:00", "20:9:00", 106, 26, "Workshop on Logic ", 79
exec AddWorkshop "2013-5-28", "19:26:00", "20:26:00", 147, 30, "Workshop on Logic ", 77
exec AddWorkshop "2011-10-24", "17:30:00", "19:30:00", 84, 47, "International Conference on Artificial Neural Networks", 64
exec AddWorkshop "2012-9-1", "13:20:00", "15:20:00", 59, 39, "International Joint Conference on Artificial Intelligence", 49
exec AddWorkshop "2012-4-7", "11:57:00", "13:57:00", 73, 26, "International Conference on MultiMedia Modeling", 13
exec AddWorkshop "2011-3-19", "9:25:00", "11:25:00", 129, 29, "Sentinel-2 for Science WS", 9
exec AddWorkshop "2013-8-2", "17:32:00", "18:32:00", 80, 22, "ARTES 1 Final Presentation Days", 53
exec AddWorkshop "2013-11-21", "18:41:00", "20:41:00", 124, 47, "GNC 2014", 29
exec AddWorkshop "2011-6-28", "10:27:00", "12:27:00", 114, 25, "International Semantic Web Conference		", 4
exec AddWorkshop "2013-3-17", "14:30:00", "15:30:00", 57, 28, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 53
exec AddWorkshop "2012-4-10", "16:45:00", "18:45:00", 67, 34, "European Space Technology Harmonisation Conference", 30
exec AddWorkshop "2011-10-24", "16:45:00", "19:45:00", 120, 29, "Annual Interdisciplinary Conference", 45
exec AddWorkshop "2012-1-30", "16:35:00", "19:35:00", 78, 36, "International Conference on Pattern Recognition", 13
exec AddWorkshop "2011-2-11", "17:10:00", "19:10:00", 54, 35, "International Conference on Automated Reasoning", 17
exec AddWorkshop "2012-12-17", "13:7:00", "16:7:00", 50, 30, "British	Machine	Vision	Conference		", 79
exec AddWorkshop "2011-11-20", "9:12:00", "11:12:00", 120, 28, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 29
exec AddWorkshop "2012-4-18", "17:39:00", "20:39:00", 147, 47, "International Conference on MultiMedia Modeling", 14
exec AddWorkshop "2012-6-12", "12:9:00", "15:9:00", 135, 35, "Annual Interdisciplinary Conference", 9
exec AddWorkshop "2013-2-28", "9:51:00", "12:51:00", 76, 46, "Sustainable Brands Conference", 35
exec AddWorkshop "2012-10-15", "13:15:00", "14:15:00", 132, 30, "�International� Corporate Citizenship Conference", 29
exec AddWorkshop "2012-1-6", "15:3:00", "17:3:00", 149, 46, "2nd International Conference on Photonics, Optics and Laser Technology", 39
exec AddWorkshop "2011-9-14", "10:12:00", "12:12:00", 57, 43, "European Navigation Conference 2014 (ENC-GNSS 2014)", 34
exec AddWorkshop "2012-5-3", "10:57:00", "11:57:00", 131, 47, "2nd International Conference on Photonics, Optics and Laser Technology", 60
exec AddWorkshop "2011-5-4", "9:38:00", "11:38:00", 88, 43, "International Conference on Computer Vision", 10
exec AddWorkshop "2012-3-4", "17:30:00", "19:30:00", 86, 38, "LPVE Land product validation and evolution", 15
exec AddWorkshop "2013-4-29", "10:29:00", "11:29:00", 139, 48, "The 2nd International CSR Communication Conference", 3
exec AddWorkshop "2012-12-6", "12:27:00", "15:27:00", 83, 35, "Global Conference on Sustainability and Reporting", 64
exec AddWorkshop "2011-1-17", "17:60:00", "20:60:00", 52, 35, "The 2nd International CSR Communication Conference", 38
exec AddWorkshop "2013-5-4", "11:15:00", "12:15:00", 138, 24, "Euclid Spacecraft Industry Day", 2
exec AddWorkshop "2012-12-15", "10:2:00", "13:2:00", 99, 43, "Client Summit", 42
exec AddWorkshop "2011-9-15", "19:31:00", "21:31:00", 102, 24, "GNC 2014", 18
exec AddWorkshop "2013-5-11", "10:3:00", "13:3:00", 65, 48, "4th DUE Permafrost User Workshop", 70
exec AddWorkshop "2012-8-2", "19:45:00", "20:45:00", 120, 43, "Corporate Community Involvement Conference", 23
exec AddWorkshop "2011-4-24", "9:30:00", "12:30:00", 58, 36, "Life in Space for Life on Earth Symposium", 32
exec AddWorkshop "2012-3-3", "19:24:00", "22:24:00", 129, 29, "Conference on Automated Deduction		", 47
exec AddWorkshop "2012-4-23", "13:13:00", "16:13:00", 109, 49, "7 th International Conference on Bio-inspired Systems and Signal Processing", 49
exec AddWorkshop "2012-5-25", "8:8:00", "10:8:00", 107, 49, "International Conference on Computer Vision", 0
exec AddWorkshop "2012-2-21", "17:46:00", "18:46:00", 115, 30, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 70
exec AddWorkshop "2011-1-9", "16:5:00", "19:5:00", 82, 35, "EuroCOW the Calibration and Orientation Workshop", 5
exec AddWorkshop "2013-7-6", "18:52:00", "20:52:00", 147, 37, "Conference on Uncertainty in Artificial Intelligence", 15
exec AddWorkshop "2011-8-6", "18:53:00", "19:53:00", 114, 44, "ICATT", 31
exec AddWorkshop "2011-5-1", "9:46:00", "11:46:00", 132, 23, "Photonics West", 82
exec AddWorkshop "2012-1-25", "16:14:00", "17:14:00", 120, 32, "International Joint Conference on Artificial Intelligence", 69
exec AddWorkshop "2013-9-2", "18:10:00", "20:10:00", 141, 23, "Computer Analysis of Images and Patterns", 66
exec AddWorkshop "2011-2-4", "11:33:00", "12:33:00", 89, 35, "International Conference on the Principles of", 23
exec AddWorkshop "2013-5-6", "8:42:00", "10:42:00", 76, 48, "LPVE Land product validation and evolution", 66
exec AddWorkshop "2013-2-19", "11:17:00", "12:17:00", 146, 46, "Conference on Uncertainty in Artificial Intelligence", 11
exec AddWorkshop "2011-6-26", "10:42:00", "13:42:00", 115, 33, "�International� Corporate Citizenship Conference", 78
exec AddWorkshop "2011-4-17", "17:39:00", "19:39:00", 137, 36, "Microwave Workshop", 44
exec AddWorkshop "2011-7-18", "13:9:00", "14:9:00", 88, 48, "RuleML Symposium				", 72
exec AddWorkshop "2012-6-5", "14:47:00", "16:47:00", 88, 40, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 28
exec AddWorkshop "2011-1-17", "18:17:00", "19:17:00", 111, 26, "Photonics West", 72
exec AddWorkshop "2012-2-14", "13:30:00", "16:30:00", 52, 42, "Leadership Strategies for Information Technology in Health Care Boston", 28
exec AddWorkshop "2011-5-1", "13:9:00", "14:9:00", 118, 34, "European Navigation Conference 2014 (ENC-GNSS 2014)", 14
exec AddWorkshop "2011-5-29", "12:48:00", "14:48:00", 132, 48, "Workshop on Logic ", 48
exec AddWorkshop "2013-10-4", "10:18:00", "12:18:00", 95, 29, "7 th International Conference on Bio-inspired Systems and Signal Processing", 18
exec AddWorkshop "2013-10-30", "18:4:00", "20:4:00", 123, 31, "Global Conference on Sustainability and Reporting", 32
exec AddWorkshop "2012-4-10", "8:37:00", "11:37:00", 61, 21, "Mayo Clinic Presents", 20
exec AddWorkshop "2013-8-16", "12:30:00", "15:30:00", 122, 45, "International Conference on Automated Planning", 5
exec AddWorkshop "2013-4-5", "16:27:00", "17:27:00", 149, 26, "International Conference on Autonomous Agents and", 59
exec AddWorkshop "2012-2-18", "8:37:00", "9:37:00", 94, 28, "European Conference on Artificial Intelligence	", 19
exec AddWorkshop "2011-9-17", "16:49:00", "19:49:00", 138, 32, "ARTES 1 Final Presentation Days", 62
exec AddWorkshop "2012-2-22", "17:2:00", "19:2:00", 129, 37, "Asian Conference on Computer Vision", 59
exec AddWorkshop "2012-11-23", "16:10:00", "19:10:00", 128, 49, "Corporate Community Involvement Conference", 8
exec AddWorkshop "2011-9-27", "17:56:00", "19:56:00", 131, 38, "Sustainable Brands Conference", 12
exec AddWorkshop "2012-3-1", "16:50:00", "18:50:00", 65, 22, "Cause Marketing Forum", 41
exec AddWorkshop "2011-2-15", "9:9:00", "11:9:00", 64, 34, "Ceres Conference", 55
exec AddWorkshop "2013-11-21", "11:57:00", "14:57:00", 76, 40, "British	Machine	Vision	Conference		", 31
exec AddWorkshop "2013-3-4", "16:29:00", "19:29:00", 72, 47, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 84
exec AddWorkshop "2012-11-11", "18:24:00", "19:24:00", 90, 24, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 3
exec AddWorkshop "2013-7-9", "9:17:00", "12:17:00", 110, 25, "Conference on Computer Vision and Pattern", 77
exec AddWorkshop "2012-11-5", "17:18:00", "20:18:00", 54, 33, "Microwave Workshop", 41
exec AddWorkshop "2011-4-2", "9:2:00", "11:2:00", 148, 45, "International Conference on Computer Vision Theory and Applications", 38
exec AddWorkshop "2011-3-3", "18:6:00", "21:6:00", 52, 28, "48th ESLAB Symposium", 84
exec AddWorkshop "2011-8-9", "19:58:00", "20:58:00", 66, 39, "Conference on Automated Deduction		", 65
exec AddWorkshop "2011-12-2", "10:20:00", "13:20:00", 65, 29, "European Navigation Conference 2014 (ENC-GNSS 2014)", 78
exec AddWorkshop "2012-1-8", "10:30:00", "11:30:00", 98, 31, "GNC 2014", 12
exec AddWorkshop "2012-8-14", "9:9:00", "11:9:00", 146, 25, "Conference on Uncertainty in Artificial Intelligence", 36
exec AddWorkshop "2013-2-2", "17:50:00", "19:50:00", 96, 43, "LPVE Land product validation and evolution", 16
exec AddWorkshop "2012-8-19", "8:44:00", "11:44:00", 119, 37, "International Joint Conference on Artificial Intelligence", 40
exec AddWorkshop "2011-2-12", "17:9:00", "18:9:00", 146, 29, "European Space Technology Harmonisation Conference", 30
exec AddWorkshop "2013-12-30", "14:57:00", "17:57:00", 141, 20, "48th ESLAB Symposium", 54
exec AddWorkshop "2013-12-5", "19:5:00", "21:5:00", 130, 25, "International Conference on the Principles of", 55
exec AddWorkshop "2013-7-29", "16:40:00", "19:40:00", 74, 37, "International Conference on Signal and Imaging Systems Engineering", 37
exec AddWorkshop "2012-1-4", "15:30:00", "17:30:00", 102, 46, "2nd International Conference on Photonics, Optics and Laser Technology", 44
exec AddWorkshop "2013-8-2", "17:55:00", "18:55:00", 95, 45, "Workshop on Logic ", 80
exec AddWorkshop "2013-9-25", "17:13:00", "19:13:00", 79, 42, "International Conference on Pattern Recognition", 39
exec AddWorkshop "2013-5-26", "9:47:00", "11:47:00", 131, 25, "International Conference on Logic for Programming", 33
exec AddWorkshop "2011-6-8", "14:28:00", "17:28:00", 110, 41, "4S Symposium 2014", 22
exec AddWorkshop "2013-11-26", "11:21:00", "13:21:00", 111, 34, "British	Machine	Vision	Conference		", 64
exec AddWorkshop "2011-5-30", "14:23:00", "17:23:00", 60, 37, "European Space Power Conference", 81
exec AddWorkshop "2013-6-20", "9:7:00", "11:7:00", 102, 22, "RuleML Symposium				", 71
exec AddWorkshop "2012-1-23", "16:41:00", "19:41:00", 128, 46, "Sentinel-2 for Science WS", 26
exec AddWorkshop "2013-1-16", "11:29:00", "14:29:00", 102, 21, "Foundations of Genetic Algorithms		", 67
exec AddWorkshop "2013-4-28", "15:56:00", "18:56:00", 126, 32, "European Conference on Machine Learning", 9
exec AddWorkshop "2013-8-22", "19:30:00", "22:30:00", 91, 35, "The Sixth International Conferences on Advances in Multimedia", 61
exec AddWorkshop "2013-4-12", "15:58:00", "18:58:00", 141, 39, "European Conference on Machine Learning", 30
exec AddWorkshop "2011-1-12", "8:38:00", "11:38:00", 104, 34, "International Conference on Autonomous Agents and", 2
exec AddWorkshop "2012-3-25", "17:57:00", "18:57:00", 129, 33, "Cause Marketing Forum", 61
exec AddWorkshop "2011-9-20", "17:46:00", "18:46:00", 127, 33, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 82
exec AddWorkshop "2012-2-14", "19:13:00", "20:13:00", 62, 23, "International Conference on Autonomous Agents and", 36
exec AddWorkshop "2011-4-19", "17:45:00", "18:45:00", 73, 32, "Conference on Computer Vision and Pattern", 20
exec AddWorkshop "2013-7-16", "10:59:00", "12:59:00", 100, 29, "LPVE Land product validation and evolution", 72
exec AddWorkshop "2012-7-12", "8:5:00", "11:5:00", 93, 23, "�International� Corporate Citizenship Conference", 82
exec AddWorkshop "2013-9-5", "11:25:00", "13:25:00", 77, 38, "European Conference on Machine Learning", 3
exec AddWorkshop "2012-8-20", "11:58:00", "12:58:00", 148, 31, "Corporate Community Involvement Conference", 77
exec AddWorkshop "2012-7-19", "17:24:00", "18:24:00", 75, 42, "International Semantic Web Conference		", 29
exec AddWorkshop "2012-10-2", "15:18:00", "16:18:00", 94, 46, "ARTES 1 Final Presentation Days", 34
exec AddWorkshop "2011-8-30", "12:1:00", "15:1:00", 129, 46, "The 12th annual Responsible Business Summit", 40
exec AddWorkshop "2012-2-10", "18:25:00", "19:25:00", 148, 48, "The 2nd International CSR Communication Conference", 50
exec AddWorkshop "2013-10-15", "18:44:00", "21:44:00", 64, 48, "Business4Better: The Community Partnership Movement", 60
exec AddWorkshop "2012-9-13", "16:38:00", "17:38:00", 110, 35, "48th ESLAB Symposium", 14
exec AddWorkshop "2012-5-15", "15:42:00", "16:42:00", 93, 40, "Ceres Conference", 73
exec AddWorkshop "2013-3-8", "9:34:00", "12:34:00", 54, 31, "Foundations of Genetic Algorithms		", 14
exec AddWorkshop "2013-11-18", "11:21:00", "12:21:00", 98, 25, "International Conference on Artificial Neural Networks", 39
exec AddWorkshop "2011-9-30", "11:51:00", "14:51:00", 117, 37, "Conference on Computer Vision and Pattern", 0
exec AddWorkshop "2012-5-7", "12:49:00", "15:49:00", 122, 39, "ICATT", 17
exec AddWorkshop "2011-3-17", "18:28:00", "20:28:00", 100, 45, "International Conference on MultiMedia Modeling", 10
exec AddWorkshop "2013-8-12", "10:44:00", "13:44:00", 101, 29, "Science and Challenges of Lunar Sample Return Workshop", 36
exec AddWorkshop "2011-12-23", "13:32:00", "16:32:00", 66, 36, "2013 Ethical Leadership Conference: Ethics in Action", 25
exec AddWorkshop "2013-11-2", "14:29:00", "16:29:00", 132, 29, "Cause Marketing Forum", 38
exec AddWorkshop "2012-5-13", "19:58:00", "21:58:00", 81, 35, "Business4Better: The Community Partnership Movement", 59
exec AddWorkshop "2012-3-11", "12:50:00", "13:50:00", 120, 32, "International Conference on Autonomous Agents and", 36
exec AddWorkshop "2011-3-17", "18:27:00", "19:27:00", 135, 39, "Leadership Strategies for Information Technology in Health Care Boston", 18
exec AddWorkshop "2011-5-29", "8:19:00", "11:19:00", 93, 25, "Cause Marketing Forum", 33
exec AddWorkshop "2011-5-27", "11:1:00", "14:1:00", 99, 44, "The 12th annual Responsible Business Summit", 84
exec AddWorkshop "2013-12-13", "11:59:00", "12:59:00", 85, 28, "Business4Better: The Community Partnership Movement", 80
exec AddWorkshop "2013-5-5", "11:30:00", "12:30:00", 95, 43, "International Conference on MultiMedia Modeling", 62
exec AddWorkshop "2012-3-23", "19:29:00", "21:29:00", 62, 30, "ICATT", 0
exec AddWorkshop "2011-11-11", "10:35:00", "11:35:00", 63, 20, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 1
exec AddWorkshop "2011-4-26", "9:46:00", "11:46:00", 145, 29, "International Conference on MultiMedia Modeling", 18
exec AddWorkshop "2013-6-10", "13:31:00", "16:31:00", 82, 44, "Foundations of Genetic Algorithms		", 56
exec AddWorkshop "2012-10-29", "17:10:00", "18:10:00", 127, 26, "3rd International Conference on Pattern Recognition Applications and Methods", 28
exec AddWorkshop "2012-4-9", "12:24:00", "13:24:00", 64, 38, "European Conference on Machine Learning", 57
exec AddWorkshop "2013-4-29", "19:39:00", "20:39:00", 73, 48, "The BCLC CSR Conference", 66
exec AddWorkshop "2011-5-28", "8:28:00", "9:28:00", 133, 47, "3rd International Conference on Pattern Recognition Applications and Methods", 53
exec AddWorkshop "2011-7-27", "8:58:00", "9:58:00", 75, 40, "Life in Space for Life on Earth Symposium", 57
exec AddWorkshop "2013-12-12", "10:3:00", "13:3:00", 137, 27, "Global Conference on Sustainability and Reporting", 60
exec AddWorkshop "2011-12-12", "11:48:00", "14:48:00", 130, 29, "Mayo Clinic Presents", 76
exec AddWorkshop "2011-6-12", "14:20:00", "15:20:00", 76, 31, "Corporate Community Involvement Conference", 38
exec AddWorkshop "2012-1-25", "17:23:00", "20:23:00", 127, 37, "International Conference on MultiMedia Modeling", 65
exec AddWorkshop "2013-11-6", "9:17:00", "10:17:00", 110, 35, "International Conference on Computer Vision Theory and Applications", 82
exec AddWorkshop "2013-12-14", "10:45:00", "11:45:00", 66, 37, "RuleML Symposium				", 44
exec AddWorkshop "2013-9-29", "17:40:00", "20:40:00", 122, 32, "Conference on Automated Deduction		", 37
exec AddWorkshop "2011-2-3", "18:52:00", "19:52:00", 87, 45, "International Conference on Computer Vision", 41
exec AddWorkshop "2011-6-12", "17:38:00", "18:38:00", 75, 46, "48th ESLAB Symposium", 71
exec AddWorkshop "2011-3-22", "19:60:00", "20:60:00", 73, 47, "RuleML Symposium		", 48
exec AddWorkshop "2013-9-22", "8:14:00", "11:14:00", 52, 34, "International Conference on Automated Planning", 59
exec AddWorkshop "2011-2-17", "16:50:00", "19:50:00", 80, 20, "48th ESLAB Symposium", 25
exec AddWorkshop "2011-5-15", "8:55:00", "10:55:00", 126, 30, "International Semantic Web Conference		", 29
exec AddWorkshop "2012-11-7", "17:49:00", "19:49:00", 109, 21, "4th DUE Permafrost User Workshop", 20
exec AddWorkshop "2011-6-17", "10:14:00", "13:14:00", 98, 29, "ARTES 1 Final Presentation Days", 24
exec AddWorkshop "2013-10-7", "13:38:00", "14:38:00", 124, 47, "Mechanisms Final Presentation Days", 29
exec AddWorkshop "2011-3-19", "8:30:00", "10:30:00", 80, 24, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 76
exec AddWorkshop "2011-2-27", "18:55:00", "21:55:00", 78, 40, "International Joint Conference on Artificial Intelligence", 19
exec AddWorkshop "2013-8-25", "13:32:00", "16:32:00", 56, 45, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 34
exec AddWorkshop "2013-11-24", "15:12:00", "18:12:00", 119, 29, "Global Conference on Sustainability and Reporting", 28
exec AddWorkshop "2012-7-4", "13:1:00", "15:1:00", 147, 47, "International Conference on the Principles of", 34
exec AddWorkshop "2013-4-24", "19:57:00", "21:57:00", 115, 49, "X-Ray Universe", 76
exec AddWorkshop "2011-1-12", "9:15:00", "11:15:00", 59, 23, "International Conference on Artificial Neural Networks", 80
exec AddWorkshop "2012-5-17", "19:39:00", "22:39:00", 92, 49, "Mayo Clinic Presents", 35
exec AddWorkshop "2012-11-30", "16:44:00", "19:44:00", 111, 35, "X-Ray Universe", 22
exec AddWorkshop "2013-7-28", "16:42:00", "19:42:00", 118, 30, "Microwave Workshop", 27
exec AddWorkshop "2012-10-18", "15:1:00", "18:1:00", 78, 43, "European Conference on Computer Vision", 39
exec AddWorkshop "2012-8-23", "19:42:00", "21:42:00", 127, 33, "RuleML Symposium		", 45
exec AddWorkshop "2013-12-4", "9:38:00", "12:38:00", 52, 20, "LPVE Land product validation and evolution", 75
exec AddWorkshop "2012-9-22", "18:10:00", "19:10:00", 64, 44, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 72
exec AddWorkshop "2012-3-9", "10:16:00", "11:16:00", 57, 48, "Sustainable Brands Conference", 18
exec AddWorkshop "2011-10-28", "17:5:00", "20:5:00", 73, 29, "7 th International Conference on Bio-inspired Systems and Signal Processing", 51
exec AddWorkshop "2013-3-29", "14:4:00", "17:4:00", 117, 47, "Microwave Workshop", 50
exec AddWorkshop "2012-6-21", "12:22:00", "13:22:00", 131, 27, "7 th International Conference on Bio-inspired Systems and Signal Processing", 6
exec AddWorkshop "2012-3-18", "9:38:00", "12:38:00", 141, 23, "International Semantic Web Conference		", 43
exec AddWorkshop "2011-7-18", "9:54:00", "12:54:00", 147, 25, "International Conference on Computer Vision Theory and Applications", 64
exec AddWorkshop "2013-11-16", "14:21:00", "15:21:00", 81, 33, "Net Impact Conference", 0
exec AddWorkshop "2012-12-27", "9:8:00", "10:8:00", 103, 22, "Workshop on Logic ", 20
exec AddWorkshop "2011-5-19", "19:10:00", "21:10:00", 98, 45, "International Conference on Artificial Neural Networks", 40
exec AddWorkshop "2013-12-17", "19:49:00", "22:49:00", 82, 45, "International Conference on Automated Reasoning", 80
exec AddWorkshop "2011-9-8", "19:3:00", "20:3:00", 97, 30, "Conference on Computer Vision and Pattern", 25
exec AddWorkshop "2012-4-25", "14:37:00", "16:37:00", 50, 40, "GNC 2014", 44
exec AddWorkshop "2013-5-2", "10:26:00", "11:26:00", 104, 32, "RuleML Symposium		", 35
exec AddWorkshop "2012-12-15", "11:25:00", "13:25:00", 64, 21, "Cause Marketing Forum", 41
exec AddWorkshop "2013-6-25", "16:13:00", "17:13:00", 115, 44, "European Space Power Conference", 39
exec AddWorkshop "2011-11-17", "17:25:00", "18:25:00", 131, 35, "The Sixth International Conferences on Advances in Multimedia", 35
exec AddWorkshop "2011-12-14", "9:19:00", "12:19:00", 98, 30, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 83
exec AddWorkshop "2011-6-18", "19:56:00", "21:56:00", 70, 44, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 48
exec AddWorkshop "2011-12-11", "11:7:00", "14:7:00", 130, 32, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 82
exec AddWorkshop "2012-7-4", "10:6:00", "13:6:00", 126, 24, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 74
exec AddWorkshop "2013-7-6", "9:40:00", "11:40:00", 86, 49, "International Semantic Web Conference		", 21
exec AddWorkshop "2012-1-5", "9:26:00", "10:26:00", 60, 44, "LPVE Land product validation and evolution", 62
exec AddWorkshop "2011-9-15", "12:22:00", "14:22:00", 124, 44, "�International� Corporate Citizenship Conference", 30
exec AddWorkshop "2011-10-24", "10:2:00", "12:2:00", 70, 26, "Foundations of Genetic Algorithms		", 38
exec AddWorkshop "2012-8-11", "8:35:00", "11:35:00", 87, 30, "International Joint Conference on Automated Reasoning", 76
exec AddWorkshop "2013-11-13", "10:31:00", "12:31:00", 113, 30, "RuleML Symposium				", 56
exec AddWorkshop "2011-11-24", "16:45:00", "19:45:00", 102, 34, "3rd International Conference on Pattern Recognition Applications and Methods", 68
exec AddWorkshop "2011-11-25", "11:36:00", "13:36:00", 102, 32, "International Conference on Automated Reasoning", 82
exec AddWorkshop "2012-10-2", "17:22:00", "18:22:00", 65, 38, "International Conference on Logic for Programming", 12
exec AddWorkshop "2013-7-1", "12:57:00", "15:57:00", 72, 31, "International Conference on Autonomous Agents and", 47
exec AddWorkshop "2011-6-21", "11:16:00", "14:16:00", 100, 31, "European Space Power Conference", 50
exec AddWorkshop "2012-1-19", "9:43:00", "11:43:00", 61, 27, "Cause Marketing Forum", 40
exec AddWorkshop "2012-4-23", "17:51:00", "18:51:00", 65, 28, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 61
exec AddWorkshop "2012-3-5", "8:51:00", "9:51:00", 112, 24, "Cause Marketing Forum", 72
exec AddWorkshop "2013-10-3", "19:33:00", "22:33:00", 106, 34, "European Space Technology Harmonisation Conference", 47
exec AddWorkshop "2011-1-10", "16:31:00", "17:31:00", 131, 44, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 21
exec AddWorkshop "2011-6-23", "18:21:00", "20:21:00", 139, 31, "Conference on Learning Theory		", 24
exec AddWorkshop "2011-6-23", "9:60:00", "11:60:00", 65, 20, "2013 Ethical Leadership Conference: Ethics in Action", 36
exec AddWorkshop "2012-2-30", "15:26:00", "17:26:00", 92, 27, "European Conference on Machine Learning", 75
exec AddWorkshop "2011-4-18", "8:22:00", "11:22:00", 122, 32, "Net Impact Conference", 58
exec AddWorkshop "2012-12-6", "15:25:00", "18:25:00", 134, 40, "Client Summit", 16
exec AddWorkshop "2013-7-19", "8:24:00", "10:24:00", 120, 37, "European Space Technology Harmonisation Conference", 58
exec AddWorkshop "2013-10-21", "13:52:00", "16:52:00", 67, 23, "Science and Challenges of Lunar Sample Return Workshop", 41
exec AddWorkshop "2012-7-12", "11:44:00", "12:44:00", 77, 24, "Workshop on Image Processing", 13
exec AddWorkshop "2011-9-23", "9:50:00", "11:50:00", 119, 32, "Workshop on Logic ", 69
exec AddWorkshop "2013-12-1", "17:58:00", "20:58:00", 147, 36, "Workshop on Logic ", 14
exec AddWorkshop "2012-10-7", "12:28:00", "14:28:00", 61, 32, "International Conference on Automated Reasoning", 12
exec AddWorkshop "2013-5-12", "19:7:00", "21:7:00", 111, 44, "Mechanisms Final Presentation Days", 65
exec AddWorkshop "2012-4-29", "12:29:00", "15:29:00", 127, 29, "3rd International Conference on Pattern Recognition Applications and Methods", 19
exec AddWorkshop "2012-11-29", "16:53:00", "18:53:00", 74, 48, "Workshop on Logic ", 45
exec AddWorkshop "2012-4-5", "11:52:00", "12:52:00", 73, 27, "Mayo Clinic Presents", 64
exec AddWorkshop "2011-1-19", "18:28:00", "19:28:00", 77, 31, "British	Machine	Vision	Conference		", 50
exec AddWorkshop "2012-10-12", "15:49:00", "18:49:00", 100, 48, "RuleML Symposium				", 27
exec AddWorkshop "2011-5-18", "10:35:00", "12:35:00", 112, 37, "Annual Convention of The Society", 82
exec AddWorkshop "2013-2-23", "12:30:00", "14:30:00", 81, 34, "Conference on Automated Deduction		", 77
exec AddWorkshop "2012-5-21", "16:4:00", "17:4:00", 60, 27, "Annual Convention of The Society", 27
exec AddWorkshop "2013-11-5", "12:12:00", "13:12:00", 135, 34, "Net Impact Conference", 77
exec AddWorkshop "2013-10-29", "8:49:00", "10:49:00", 56, 34, "Workshop on Image Processing", 48
exec AddWorkshop "2012-10-16", "14:50:00", "15:50:00", 107, 34, "International Conference on Automated Reasoning", 35
exec AddWorkshop "2013-12-28", "12:40:00", "15:40:00", 105, 44, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 71
exec AddWorkshop "2012-1-28", "9:32:00", "12:32:00", 81, 31, "British	Machine	Vision	Conference		", 19
exec AddWorkshop "2013-3-17", "16:41:00", "18:41:00", 96, 47, "European Navigation Conference 2014 (ENC-GNSS 2014)", 18
exec AddWorkshop "2011-4-6", "15:31:00", "18:31:00", 96, 23, "International Semantic Web Conference		", 28
exec AddWorkshop "2013-1-17", "11:34:00", "13:34:00", 133, 33, "The 2nd International CSR Communication Conference", 68
exec AddWorkshop "2013-12-14", "14:3:00", "16:3:00", 142, 44, "Corporate Community Involvement Conference", 74
exec AddWorkshop "2012-9-28", "19:38:00", "21:38:00", 85, 40, "2nd International Conference on Photonics, Optics and Laser Technology", 16
exec AddWorkshop "2012-12-10", "10:6:00", "13:6:00", 135, 48, "Mechanisms Final Presentation Days", 69
exec AddWorkshop "2011-6-21", "19:26:00", "22:26:00", 97, 40, "Computer Analysis of Images and Patterns", 44
exec AddWorkshop "2011-9-12", "12:48:00", "13:48:00", 133, 25, "Sustainable Brands Conference", 20
exec AddWorkshop "2013-11-28", "8:52:00", "11:52:00", 105, 42, "European Conference on Computer Vision", 42
exec AddWorkshop "2013-9-3", "18:15:00", "19:15:00", 55, 39, "RuleML Symposium				", 48
exec AddWorkshop "2011-10-8", "17:49:00", "20:49:00", 55, 33, "Conference on Automated Deduction		", 39
exec AddWorkshop "2011-6-1", "11:44:00", "14:44:00", 79, 29, "International Conference on Pattern Recognition", 65
exec AddWorkshop "2013-3-27", "19:46:00", "22:46:00", 88, 21, "X-Ray Universe", 76
exec AddWorkshop "2013-11-16", "9:58:00", "11:58:00", 145, 21, "Euclid Spacecraft Industry Day", 4
exec AddWorkshop "2012-7-19", "17:49:00", "19:49:00", 50, 47, "Computer Analysis of Images and Patterns", 1
exec AddWorkshop "2011-3-4", "17:46:00", "18:46:00", 92, 45, "International Conference on Autonomous Agents and", 24
exec AddWorkshop "2011-9-17", "15:6:00", "17:6:00", 80, 30, "ARTES 1 Final Presentation Days", 3
exec AddWorkshop "2011-6-7", "14:31:00", "15:31:00", 108, 37, "Client Summit", 63
exec AddWorkshop "2012-4-8", "9:35:00", "11:35:00", 78, 34, "2013 Ethical Leadership Conference: Ethics in Action", 25
exec AddWorkshop "2013-4-9", "13:11:00", "16:11:00", 59, 31, "The Corporate Philanthropy Forum", 56
exec AddWorkshop "2012-2-15", "13:16:00", "16:16:00", 121, 23, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 31
exec AddWorkshop "2013-8-30", "12:2:00", "14:2:00", 69, 42, "�International� Corporate Citizenship Conference", 30
exec AddWorkshop "2011-12-4", "17:36:00", "18:36:00", 86, 45, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 76
exec AddWorkshop "2012-9-14", "13:26:00", "16:26:00", 122, 29, "4S Symposium 2014", 82
exec AddWorkshop "2011-7-23", "11:47:00", "14:47:00", 93, 45, "Workshop on Logic ", 53
exec AddWorkshop "2013-3-5", "17:43:00", "18:43:00", 58, 29, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 75
exec AddWorkshop "2012-2-3", "17:45:00", "20:45:00", 140, 36, "5th International Workshop on Analog and Mixed-Signal Integrated Circuits for Space Applications ", 39
exec AddWorkshop "2012-4-29", "13:57:00", "16:57:00", 108, 23, "Annual Interdisciplinary Conference", 3
exec AddWorkshop "2012-11-10", "18:46:00", "21:46:00", 101, 23, "Life in Space for Life on Earth Symposium", 19
exec AddWorkshop "2012-8-12", "8:31:00", "11:31:00", 79, 48, "International Conference on Computer Vision", 13
exec AddWorkshop "2013-11-22", "9:3:00", "12:3:00", 145, 39, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 33
exec AddWorkshop "2011-11-17", "18:22:00", "21:22:00", 146, 34, "Microwave Workshop", 42
exec AddWorkshop "2013-5-26", "19:20:00", "21:20:00", 142, 29, "International Conference on Autonomous Agents and", 2
exec AddWorkshop "2013-2-7", "11:10:00", "13:10:00", 140, 29, "International Conference on Computer Vision Theory and Applications", 71
exec AddWorkshop "2013-3-23", "16:55:00", "18:55:00", 131, 21, "Corporate Community Involvement Conference", 62
exec AddWorkshop "2011-12-8", "15:60:00", "16:60:00", 87, 20, "International Conference on Autonomous Agents and", 58
exec AddWorkshop "2013-8-8", "10:33:00", "12:33:00", 105, 35, "British	Machine	Vision	Conference		", 52
exec AddWorkshop "2012-1-14", "10:32:00", "13:32:00", 89, 25, "Conference on Learning Theory		", 30
exec AddWorkshop "2011-12-10", "10:31:00", "11:31:00", 145, 32, "Conference on Automated Deduction		", 24
exec AddWorkshop "2011-3-17", "15:59:00", "17:59:00", 107, 46, "International Semantic Web Conference		", 7
exec AddWorkshop "2013-1-4", "14:3:00", "17:3:00", 144, 49, "International Conference on Autonomous Agents and", 38
exec AddWorkshop "2013-11-20", "18:36:00", "20:36:00", 103, 25, "Euclid Spacecraft Industry Day", 62
exec AddWorkshop "2013-5-20", "11:51:00", "14:51:00", 118, 26, "KU Leuven CSR Symposium", 77
exec AddWorkshop "2011-11-26", "13:39:00", "15:39:00", 60, 31, "Business4Better: The Community Partnership Movement", 54
exec AddWorkshop "2012-4-20", "19:37:00", "22:37:00", 94, 26, "Conference on Computer Vision and Pattern", 48
exec AddWorkshop "2012-2-2", "18:59:00", "21:59:00", 112, 26, "The Corporate Philanthropy Forum", 76
exec AddWorkshop "2012-12-19", "17:33:00", "18:33:00", 50, 39, "International Conference on the Principles of", 35
exec AddWorkshop "2013-5-26", "15:38:00", "16:38:00", 132, 41, "Business4Better: The Community Partnership Movement", 82
exec AddWorkshop "2012-3-26", "12:28:00", "15:28:00", 103, 39, "Cause Marketing Forum", 1
exec AddWorkshop "2011-4-30", "14:26:00", "17:26:00", 146, 33, "International Conference on Automated Reasoning", 34
exec AddWorkshop "2012-1-14", "9:18:00", "12:18:00", 143, 31, "Mechanisms Final Presentation Days", 68
exec AddWorkshop "2012-7-12", "16:5:00", "18:5:00", 88, 33, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 12
exec AddWorkshop "2011-2-20", "17:49:00", "19:49:00", 146, 36, "Photonics West", 81
exec AddWorkshop "2011-2-3", "19:31:00", "22:31:00", 88, 22, "KU Leuven CSR Symposium", 53
exec AddWorkshop "2012-5-14", "15:18:00", "16:18:00", 65, 32, "Sustainable Brands Conference", 44
exec AddWorkshop "2012-10-3", "17:27:00", "20:27:00", 56, 40, "International Conference on Computer Vision", 65
exec AddWorkshop "2011-9-7", "11:40:00", "13:40:00", 137, 20, "European Conference on Artificial Intelligence	", 6
exec AddWorkshop "2013-2-27", "8:21:00", "9:21:00", 112, 46, "RuleML Symposium				", 29
exec AddWorkshop "2013-11-9", "18:12:00", "21:12:00", 119, 24, "LPVE Land product validation and evolution", 9
exec AddWorkshop "2011-9-17", "16:38:00", "18:38:00", 93, 36, "�International� Corporate Citizenship Conference", 49
exec AddWorkshop "2011-9-25", "18:4:00", "21:4:00", 148, 38, "International Conference on Signal and Imaging Systems Engineering", 80
exec AddWorkshop "2013-2-1", "8:1:00", "11:1:00", 87, 29, "Annual Convention of The Society", 67
exec AddWorkshop "2013-7-17", "9:32:00", "11:32:00", 96, 39, "ARTES 1 Final Presentation Days", 66
exec AddWorkshop "2012-12-12", "8:49:00", "9:49:00", 126, 43, "European Space Power Conference", 39
exec AddWorkshop "2012-8-29", "18:49:00", "19:49:00", 138, 30, "48th ESLAB Symposium", 26
exec AddWorkshop "2012-10-1", "17:25:00", "19:25:00", 85, 38, "European Conference on Computer Vision", 53
exec AddWorkshop "2012-6-19", "13:55:00", "16:55:00", 95, 48, "The 2nd International CSR Communication Conference", 79
exec AddWorkshop "2011-3-11", "10:44:00", "13:44:00", 52, 38, "EuroCOW the Calibration and Orientation Workshop", 32
exec AddWorkshop "2013-4-28", "11:57:00", "13:57:00", 131, 39, "International Conference on Automated Reasoning", 34
exec AddWorkshop "2011-1-10", "11:17:00", "12:17:00", 98, 33, "International Conference on Signal and Imaging Systems Engineering", 75
exec AddWorkshop "2013-3-20", "8:50:00", "9:50:00", 111, 39, "Corporate Community Involvement Conference", 56
exec AddWorkshop "2012-12-25", "12:15:00", "14:15:00", 126, 32, "Net Impact Conference", 25
exec AddWorkshop "2011-8-12", "16:43:00", "19:43:00", 104, 26, "European Space Power Conference", 4
exec AddWorkshop "2012-8-29", "15:21:00", "18:21:00", 82, 24, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 28
exec AddWorkshop "2011-9-14", "19:51:00", "21:51:00", 143, 36, "International Conference on the Principles of", 53
exec AddWorkshop "2011-4-3", "8:7:00", "11:7:00", 125, 40, "Sustainable Brands Conference", 84
exec AddWorkshop "2012-11-23", "17:15:00", "20:15:00", 127, 42, "International Conference on MultiMedia Modeling", 73
exec AddWorkshop "2013-2-11", "9:5:00", "10:5:00", 126, 43, "Global Conference on Sustainability and Reporting", 17
exec AddWorkshop "2013-7-8", "13:23:00", "14:23:00", 107, 41, "Cause Marketing Forum", 35
exec AddWorkshop "2013-6-6", "9:2:00", "10:2:00", 100, 33, "International Conference on Signal and Imaging Systems Engineering", 24
exec AddWorkshop "2012-10-9", "9:32:00", "10:32:00", 136, 38, "2013 Ethical Leadership Conference: Ethics in Action", 63
exec AddWorkshop "2011-10-8", "18:13:00", "20:13:00", 121, 34, "Computer Analysis of Images and Patterns", 43
exec AddWorkshop "2012-10-28", "14:10:00", "15:10:00", 134, 38, "International Conference on Automated Planning", 68
exec AddWorkshop "2013-10-8", "11:48:00", "13:48:00", 89, 31, "GNC 2014", 25
exec AddWorkshop "2011-3-28", "16:20:00", "19:20:00", 133, 20, "European Conference on Machine Learning", 80
exec AddWorkshop "2013-8-12", "10:37:00", "11:37:00", 92, 21, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 15
exec AddWorkshop "2013-10-26", "9:27:00", "12:27:00", 106, 36, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 41
exec AddWorkshop "2012-11-1", "10:29:00", "12:29:00", 95, 25, "Sentinel-2 for Science WS", 47
exec AddWorkshop "2012-9-11", "14:16:00", "16:16:00", 127, 22, "Microwave Workshop", 78
exec AddWorkshop "2012-5-1", "8:22:00", "11:22:00", 82, 27, "International Conference on MultiMedia Modeling", 53
exec AddWorkshop "2011-10-2", "10:21:00", "11:21:00", 88, 42, "Client Summit", 62
exec AddWorkshop "2013-10-5", "8:8:00", "9:8:00", 62, 38, "European Conference on Computer Vision", 7
exec AddWorkshop "2012-1-4", "18:57:00", "19:57:00", 110, 31, "International Conference on Pattern Recognition", 26
exec AddWorkshop "2011-2-1", "9:51:00", "12:51:00", 88, 26, "International Conference on the Principles of", 74
exec AddWorkshop "2011-12-7", "12:15:00", "14:15:00", 145, 34, "Conference on Automated Deduction		", 39
exec AddWorkshop "2013-9-23", "11:51:00", "14:51:00", 111, 41, "European Space Power Conference", 76
exec AddWorkshop "2013-4-24", "16:16:00", "17:16:00", 133, 32, "4th DUE Permafrost User Workshop", 51
exec AddWorkshop "2012-10-5", "12:5:00", "14:5:00", 130, 44, "International Joint Conference on Artificial Intelligence", 38
exec AddWorkshop "2012-2-13", "12:36:00", "15:36:00", 141, 42, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 26
exec AddWorkshop "2013-5-28", "14:25:00", "16:25:00", 69, 38, "Computer Analysis of Images and Patterns", 38
exec AddWorkshop "2011-6-20", "12:55:00", "13:55:00", 139, 49, "Global Conference on Sustainability and Reporting", 5
exec AddWorkshop "2012-5-12", "17:3:00", "20:3:00", 80, 47, "International Conference on Automated Planning", 34
exec AddWorkshop "2011-3-6", "13:56:00", "16:56:00", 111, 25, "RuleML Symposium				", 44
exec AddWorkshop "2011-12-15", "9:42:00", "11:42:00", 69, 47, "LPVE Land product validation and evolution", 48
exec AddWorkshop "2011-4-30", "8:18:00", "10:18:00", 63, 48, "Workshop on Logic ", 63
exec AddWorkshop "2011-1-19", "18:3:00", "19:3:00", 122, 43, "European Navigation Conference 2014 (ENC-GNSS 2014)", 47
exec AddWorkshop "2013-7-15", "14:26:00", "15:26:00", 147, 47, "Ceres Conference", 38
exec AddWorkshop "2011-7-14", "9:46:00", "12:46:00", 102, 25, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 19
exec AddWorkshop "2011-3-20", "15:15:00", "18:15:00", 126, 33, "The Corporate Philanthropy Forum", 21
exec AddWorkshop "2013-12-23", "12:55:00", "13:55:00", 137, 35, "International Semantic Web Conference		", 32
exec AddWorkshop "2012-10-20", "16:51:00", "19:51:00", 68, 20, "Euclid Spacecraft Industry Day", 28
exec AddWorkshop "2012-6-9", "10:23:00", "11:23:00", 141, 41, "The BCLC CSR Conference", 30
exec AddWorkshop "2011-3-4", "8:6:00", "10:6:00", 116, 48, "�International� Corporate Citizenship Conference", 15
exec AddWorkshop "2013-8-15", "16:57:00", "18:57:00", 60, 45, "2nd International Conference on Photonics, Optics and Laser Technology", 58
exec AddWorkshop "2011-1-9", "13:41:00", "16:41:00", 95, 20, "RuleML Symposium				", 49
exec AddWorkshop "2013-7-4", "8:43:00", "11:43:00", 119, 23, "Conference on Computer Vision and Pattern", 40
exec AddWorkshop "2013-1-16", "11:26:00", "14:26:00", 86, 33, "KU Leuven CSR Symposium", 61
exec AddWorkshop "2012-2-21", "19:31:00", "22:31:00", 120, 37, "European Conference on Artificial Intelligence	", 71
exec AddWorkshop "2011-1-12", "13:41:00", "16:41:00", 118, 24, "The 12th annual Responsible Business Summit", 2
exec AddWorkshop "2012-7-8", "17:11:00", "20:11:00", 110, 39, "Leadership Strategies for Information Technology in Health Care Boston", 36
exec AddWorkshop "2011-10-15", "16:14:00", "19:14:00", 95, 34, "12th Annual Best Practices Summit on Employee Engagement in Corporate Citizenship", 2
exec AddWorkshop "2011-3-16", "13:8:00", "15:8:00", 88, 42, "4th DUE Permafrost User Workshop", 24
exec AddWorkshop "2012-2-28", "9:57:00", "12:57:00", 51, 31, "48th ESLAB Symposium", 2
exec AddWorkshop "2013-5-26", "9:14:00", "10:14:00", 136, 21, "Foundations of Genetic Algorithms		", 31
exec AddWorkshop "2012-5-17", "18:17:00", "20:17:00", 118, 30, "Conference on Learning Theory		", 54
exec AddWorkshop "2012-2-10", "19:13:00", "21:13:00", 149, 29, "Russia-ESA Lunar Exploration Corporation: Lunar Mission Speed Dating", 15
exec AddWorkshop "2011-10-4", "11:16:00", "14:16:00", 100, 39, "Life in Space for Life on Earth Symposium", 10
exec AddWorkshop "2012-9-27", "8:18:00", "11:18:00", 100, 23, "10th International Workshop on Greenhouse Gas Measurements from Space (IWGGMS 10)", 27
exec AddWorkshop "2011-9-29", "9:46:00", "12:46:00", 53, 29, "GNC 2014", 18
exec AddWorkshop "2013-1-11", "9:21:00", "12:21:00", 63, 45, "Mechanisms Final Presentation Days", 70
exec AddWorkshop "2012-7-2", "15:9:00", "16:9:00", 142, 40, "Mechanisms Final Presentation Days", 0
exec AddWorkshop "2012-2-25", "16:58:00", "18:58:00", 90, 38, "2013 Ethical Leadership Conference: Ethics in Action", 54
exec AddWorkshop "2013-1-24", "8:44:00", "10:44:00", 58, 20, "International Conference on Artificial Neural Networks", 30
exec AddWorkshop "2011-7-16", "13:26:00", "16:26:00", 134, 28, "Sustainable Brands Conference", 52
exec AddWorkshop "2012-5-18", "13:52:00", "16:52:00", 82, 32, "The 2nd International CSR Communication Conference", 84
exec AddWorkshop "2012-8-16", "16:34:00", "17:34:00", 104, 46, "European Navigation Conference 2014 (ENC-GNSS 2014)", 73
exec AddWorkshop "2013-10-30", "8:7:00", "10:7:00", 113, 25, "Corporate Community Involvement Conference", 67
exec AddWorkshop "2012-1-27", "17:33:00", "18:33:00", 138, 38, "European Space Technology Harmonisation Conference", 29
exec AddWorkshop "2012-5-20", "13:50:00", "16:50:00", 93, 33, "European Space Power Conference", 40
exec AddWorkshop "2012-11-12", "11:42:00", "13:42:00", 139, 47, "Client Summit", 47
exec AddWorkshop "2013-8-23", "19:10:00", "22:10:00", 147, 26, "European Navigation Conference 2014 (ENC-GNSS 2014)", 35
exec AddWorkshop "2011-2-11", "17:30:00", "18:30:00", 104, 44, "48th ESLAB Symposium", 7
exec AddWorkshop "2013-7-23", "18:25:00", "19:25:00", 130, 46, "European Space Power Conference", 18
exec AddWorkshop "2012-9-11", "19:8:00", "21:8:00", 82, 27, "The BCLC CSR Conference", 50
exec AddWorkshop "2013-1-7", "11:39:00", "14:39:00", 130, 27, "International Conference on Automated Planning", 82
exec AddWorkshop "2012-4-21", "15:37:00", "16:37:00", 103, 49, "Cause Marketing Forum", 28
exec AddWorkshop "2012-5-22", "15:38:00", "18:38:00", 123, 23, "�International� Corporate Citizenship Conference", 56
exec AddWorkshop "2013-4-20", "10:26:00", "12:26:00", 88, 44, "KU Leuven CSR Symposium", 25
exec AddWorkshop "2011-5-27", "8:46:00", "9:46:00", 90, 26, "KU Leuven CSR Symposium", 36
exec AddWorkshop "2011-10-13", "17:6:00", "18:6:00", 96, 29, "ACCSR�s 6th Annual Conference: The State of CSR in Australia", 61
exec AddWorkshop "2013-10-24", "17:2:00", "20:2:00", 60, 23, "International Conference on Autonomous Agents and", 54
exec AddWorkshop "2011-2-17", "12:17:00", "13:17:00", 93, 32, "Ceres Conference", 17
exec AddWorkshop "2011-8-16", "14:34:00", "15:34:00", 125, 33, "X-Ray Universe", 43
exec AddWorkshop "2011-1-5", "11:2:00", "13:2:00", 138, 34, "9th ESA Round Table on Micro and Nano Technologies for Space Applications", 12
exec AddWorkshop "2012-12-7", "15:54:00", "16:54:00", 68, 48, "The Sixth International Conferences on Advances in Multimedia", 41
exec AddWorkshop "2013-12-25", "15:40:00", "17:40:00", 85, 42, "7 th International Conference on Bio-inspired Systems and Signal Processing", 39
exec AddWorkshop "2012-7-8", "12:58:00", "13:58:00", 120, 33, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 1
exec AddWorkshop "2013-2-9", "15:40:00", "18:40:00", 91, 20, "3rd International Conference on Pattern Recognition Applications and Methods", 64
exec AddWorkshop "2013-2-28", "9:35:00", "11:35:00", 113, 42, "International Conference on the Principles of", 17
exec AddWorkshop "2011-4-24", "14:17:00", "15:17:00", 113, 31, "Client Summit", 44
exec AddWorkshop "2012-3-5", "9:34:00", "10:34:00", 112, 42, "Conference on Automated Deduction		", 56
exec AddWorkshop "2013-3-14", "18:1:00", "19:1:00", 116, 35, "5th Electronic Materials, Processes and Packaging for Space (EMPPS) Workshop", 43
exec AddWorkshop "2013-2-9", "14:51:00", "17:51:00", 116, 37, "Conference on Computer Vision and Pattern", 79
exec AddWorkshop "2013-6-8", "18:31:00", "21:31:00", 148, 26, "Leadership Strategies for Information Technology in Health Care Boston", 38
exec AddWorkshop "2012-5-9", "15:43:00", "16:43:00", 131, 22, "5th Int. Workshop on Remote Sensing of Vegetation Fluorescence", 69
exec AddWorkshop "2013-6-7", "13:4:00", "14:4:00", 77, 40, "Sentinel-2 for Science WS", 42
