#include <iostream>
#include <vector>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <sstream>


using namespace std;

int main()
{
    vector<string> names;
    vector<string> lastnames;
    vector<string> conferences;
    vector<string> companies;
    string line;

    ostringstream ss;
    srand(time(NULL));

    ifstream fileNames ("imiona.txt");
    if (fileNames.is_open()){
        while (!fileNames.eof()){
            getline(fileNames, line);
            names.push_back(line);
        }
        fileNames.close();
    }else{
        cout << "Nie mozna otworzyc pliku";
    }

    ifstream fileLastnames ("nazwiska.txt");
    if(fileLastnames.is_open()){
        while (!fileLastnames.eof()){
            getline(fileLastnames, line);
            lastnames.push_back(line);
        }
        fileLastnames.close();
    }else{
        cout << "Nie mozna otworzyc pliku";
    }

    ifstream fileConferences ("konferencje.txt");
    if(fileConferences.is_open()){
        while (!fileConferences.eof()){
            getline(fileConferences, line);
            conferences.push_back(line);
        }
        fileConferences.close();
    }else{
        cout << "Nie mozna otworzyc pliku";
    }

    ifstream fileCompanies ("firmy.txt");
    if(fileCompanies.is_open()){
        while (!fileCompanies.eof()){
            getline(fileCompanies, line);
            companies.push_back(line);
        }
        fileCompanies.close();
    }else{
        cout << "Nie mozna otworzyc pliku";
    }


    //////////////////////////////////////////////////////////
    /////////////////GENEROWANE KLIENTOW//////////////////////
    //////////////////////////////////////////////////////////

    ofstream clientsOut ("Clients.sql");
    int clientsCount = 0;
    if(clientsOut.is_open()){
        clientsOut << "use ConferenceCompany" << endl;
        for(clientsCount; clientsCount < 50; ++clientsCount){

            long int phone = rand() % 1000000000 + 100000000;
            ss << phone;
            string temp = ss.str();

            clientsOut << "exec AddClient " << "\"" << names[rand() % names.size()] << " " << names[rand() % names.size()]
                        << "\", " << "\"" << companies[rand() % companies.size()] << "\", " << "\"" <<  temp << "\"" << endl;
            ss.str("");
            ss.clear();
            phone = 0;
        }
        clientsOut.close();
    }else{
        cout << "Nie mozna otworzyc pliku\n";
    }

    /////////////////////////////////////////////
    ////////GENEROWANIE KONFERENCJI//////////////
    /////////////////////////////////////////////

    ofstream conferencesOut ("Conferences.sql");
    int conferenceCount = 0;
    if (conferencesOut.is_open()){
        conferencesOut << "use ConferenceCompany" << endl;
        for(conferenceCount; conferenceCount < 85; ++conferenceCount){
            int year = rand() % 3 + 2011;
            int month = rand() % 12 + 1;
            int day = rand() % 27 + 1;

            conferencesOut << "exec AddConference " << "\"" << conferences[rand() % conferences.size()] << "\", "
                        << (rand() % clientsCount + 1) << ", \"" << year << "-" << month << "-" << day << "\", "
                        << "\"" << year << "-" << month << "-" << (day + rand() % 3 + 1) << "\"" << endl;
        }
        conferencesOut.close();
    }else{
        cout << "Nie mozna otwirzyc pliku";
    }

    //////////////////////////////////////////////////////////
    ///////////////////GENEROWANIE REZERWACJI/////////////////
    //////////////////////////////////////////////////////////

    ofstream reservationsOut ("Reservations.sql");
    int reservationsCount = 0;
    if(reservationsOut.is_open()){
        reservationsOut << "use ConferenceCompany" << endl;
        for(reservationsCount; reservationsCount < 500; ++reservationsCount){
            int year = rand() % 3 + 2011;
            int month = rand() % 12 + 1;
            int day = rand() % 29 + 1;

            reservationsOut << "exec AddReservation " << "\"" << year << "-" << month << "-" << day << "\", " << rand() % 150 + 100 << ", "
                    << rand() % clientsCount + 1 << endl;
        }
        reservationsOut.close();
    }else{
        cout << "Nie mozna otworzyc pliku";
    }



    /////////////////////////////////////////////////////
    ////////////GENEROWANIE WARSZTATOW///////////////////
    /////////////////////////////////////////////////////

    ofstream workshopsOut ("Workshops.sql");
    int workshopsCount = 0;
    if(workshopsOut.is_open()){
        workshopsOut << "use ConferenceCompany" << endl;
        for(workshopsCount; workshopsCount < 5000; ++workshopsCount){
            int year = rand() % 3 + 2011;
            int month = rand() % 12 + 1;
            int day = rand() % 30 + 1;
            int hour = rand() % 12 + 8;
            int minutes = rand() % 60 + 1;

            workshopsOut << "exec AddWorkshop " << "\"" << year << "-" << month << "-" << day << "\", \"" << hour << ":"
                    << minutes << ":" << "00" << "\", " << "\"" << hour + rand() % 3 + 1 << ":" << minutes << ":" << "00"
                    << "\", " << rand() % 100 + 50 << ", " << rand() % 30 + 20 << ", " << "\""
                    << conferences[rand() % conferences.size()] << "\""
                    << ", " << rand() % conferenceCount << endl;
        }
        workshopsOut.close();
    }else{
        cout << "Nie mozna otworzyc pliku\n";
    }


    ////////////////////////////////////////////////////////
    /////////////////GENEROWANIE UCZESTNIKOW////////////////
    ////////////////////////////////////////////////////////

    ofstream participantsOut ("Participants.sql");
    int participantsCount = 0;
    if(participantsOut.is_open()){
        participantsOut << "use ConferenceCompany" << endl;
        for(participantsCount; participantsCount < 16000; ++participantsCount){
            int temp = rand() % 1000000 + 100000;
            string studentID;
            if(rand() % 4){
                studentID = "NULL";
            }else{
                ss << temp;
                studentID = ss.str();
            }

            participantsOut << "exec AddParticicpant " << rand() % reservationsCount << ", " << "\""
                    << names[rand() % names.size()] << "\", \"" << lastnames[rand() % lastnames.size()]
                    << "\", " << studentID << ", " << "\"" << companies[rand() % companies.size()] << "\"" << endl;

            ss.str("");
            ss.clear();
        }
    }else{
        cout << "Nie mozna otworzyc pliku";
    }



return 0;
}



