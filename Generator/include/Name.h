#ifndef NAME_H
#define NAME_H


class Name
{
    public:
        Name();
        virtual ~Name();
    protected:
    private:
        vector<string> names;
        vector<string> lastnames;
};

#endif // NAME_H
